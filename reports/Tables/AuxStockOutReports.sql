﻿CREATE TABLE [reports].[AuxStockOutReports] (
    [IdComprador]      NVARCHAR (50)  NULL,
    [Nombre]           NVARCHAR (200) NULL,
    [GroupId]          INT            NOT NULL,
    [FantasyName]      NVARCHAR (255) NULL,
    [SKU]              INT            NOT NULL,
    [Name]             NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Id]               INT            NULL,
    [Fabricante]       NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Cobertura]        INT            NULL,
    [LocalesQuebrados] INT            NULL,
    [Combinaciones]    INT            NULL,
    [PorcQuiebre]      FLOAT (53)     NULL,
    [suggest5]         FLOAT (53)     NULL,
    [LTProv]           INT            NULL,
    [UnidadesCompra]   FLOAT (53)     NULL,
    [StockTotal]       FLOAT (53)     NULL,
    [StockCD]          INT            NULL,
    [DíasCD]           INT            NULL,
    [DíasCoberturaOC]  INT            NULL,
    CONSTRAINT [PK_AuxStockOutReports] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

