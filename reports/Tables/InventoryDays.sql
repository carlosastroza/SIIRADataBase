﻿CREATE TABLE [reports].[InventoryDays] (
    [sku]                    INT            NOT NULL,
    [DivisionUnificadaDesc]  NVARCHAR (100) NULL,
    [CategoriaUnificadaDesc] NVARCHAR (100) NULL,
    [ManufacturerDesc]       VARCHAR (50)   NULL,
    [StockTotal]             INT            NULL,
    [Dias_Alcance]           FLOAT (53)     NULL,
    [Venta_Promedio]         FLOAT (53)     NULL,
    [Casuistica]             NVARCHAR (50)  NULL,
    CONSTRAINT [PK_InventoryDays] PRIMARY KEY CLUSTERED ([sku] ASC)
);

