﻿CREATE TABLE [reports].[ForecastedPurchase_OLD] (
    [SKU]             INT            NOT NULL,
    [División]        NVARCHAR (100) NULL,
    [Nombre]          NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Categoría]       NVARCHAR (100) NULL,
    [Subcategoría]    NVARCHAR (100) NULL,
    [Posicionamiento] VARCHAR (50)   NULL,
    [Manufacturer]    NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Ranking]         VARCHAR (1)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MesActual]       INT            NULL,
    [MesProximo]      INT            NULL,
    [MesSubsiguiente] INT            NULL,
    CONSTRAINT [PK_ForecastedPurchase] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

