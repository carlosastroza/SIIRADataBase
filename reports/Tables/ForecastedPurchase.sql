﻿CREATE TABLE [reports].[ForecastedPurchase] (
    [SKU]             INT            NOT NULL,
    [IsGeneric]       BIT            NOT NULL,
    [División]        NVARCHAR (100) NULL,
    [Nombre]          NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Categoría]       NVARCHAR (100) NULL,
    [Subcategoría]    NVARCHAR (100) NULL,
    [Posicionamiento] VARCHAR (50)   NULL,
    [Manufacturer]    NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Ranking]         VARCHAR (1)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MES ACTUAL]      INT            NULL,
    [MES 2]           INT            NULL,
    [MES 3]           INT            NULL,
    CONSTRAINT [PK_ForecastedPurchase_1] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

