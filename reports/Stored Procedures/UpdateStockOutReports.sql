﻿-- =============================================
-- Author:		dani Albornoz
-- Create date: 2018-08-10
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [reports].[UpdateStockOutReports] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @date DATE = (SELECT TOP 1 [date] FROM Salcobrand.reports.LastReportStores)--(SELECT CAST(MAX(date) AS DATE) FROM [Salcobrand].reports.ReportStores) 
	DECLARE @LastSale DATE =(SELECT TOP 1 [date] FROM Salcobrand.series.LastSale)--(SELECT CAST(MAX(date) AS DATE) FROM [Salcobrand].[series].[Sales]) 
	DECLARE @date2 DATE =  dateadd(day,-1, @date)
	DECLARE @hoy date = GETDATE()


	truncate table SalcobrandAbastecimiento.reports.AuxStockOutReports

INSERT INTO [reports].[AuxStockOutReports]
           ([IdComprador]
           ,[Nombre]
           ,[GroupId]
           ,[FantasyName]
           ,[SKU]
           ,[Name]
           ,[Id]
           ,[Fabricante]
		   ,[Cobertura]
           ,[LocalesQuebrados]
           ,[Combinaciones]
           ,[PorcQuiebre]
           ,[suggest5]
           ,[LTProv]
           ,[UnidadesCompra]
           ,[StockTotal]
           ,[StockCD]
           ,[DíasCD]
           ,[DíasCoberturaOC])
		   SELECT u.id IdComprador,
					u.Nombre,
					bi.GroupId,
					bi.FantasyName,
					bi.SKU,
					bi.[Name],
					mi.Id,
					mi.Fabricante,
					mi.Cobertura,
					rs.LocalesQuebrados,
					rs.Combinaciones,
					rs.PorcQuiebre,
					f.suggest5,
					lp.LeadTime LTProv,
					lp.UnidadesCompra,
					lp.UnidadesCompra+StockCD StockTotal ,
					CASE WHEN ls.StockCD>0 THEN ls.StockCD ELSE 0 END StockCD,
					CAST (ISNULL(5*ls.StockCD/NULLIF(f.suggest5,0),0) AS INT) [DíasCD],
					CAST (ISNULL(5*lp.UnidadesCompra/NULLIF(f.suggest5,0),0) AS int) [DíasCoberturaOC]
			FROM [salcobrandabastecimiento].[aux].[BaseIndicadores] bi
			LEFT JOIN (select Nombre, Id from [StdUsuarios].[dbo].[usuario]) u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS = bi.comprador
			LEFT JOIN  (select sku, stockcd from  SalcobrandAbastecimiento.setup.laststock) ls ON ls.SKU=bi.SKU
			LEFT JOIN ( -- DECLARE @hoy date = GETDATE() DECLARE @LastSale DATE =(SELECT TOP 1 [date] FROM Salcobrand.series.LastSale)
				SELECT isnull(a.SKU, b.SKU)SKU,isnull(nullif(a.Fit,0),b.VentaPromedio) Suggest5
				FROM (
					SELECT	ds.SKU,SUM(Fit) fit
					FROM [Salcobrand].[forecast].[DailyForecast] ds
					WHERE	ds.[Fecha] BETWEEN @hoy AND DATEADD(DAY,5,@hoy)
					group by ds.SKU
				) a
				full join (
					SELECT	SKU,SUM(Quantity)*1.0 VentaPromedio
					FROM [Salcobrand].[series].[DailySales]
					WHERE [date]=DATEADD(DAY,-5,@LastSale)
					GROUP BY Sku
				) b ON b.sku=a.sku
			) f ON f.SKU=bi.SKU
			LEFT JOIN (
				SELECT	SKU, 
						SUM(CASE WHEN Stock=0 THEN 1 ELSE 0 END) LocalesQuebrados,
						COUNT(Store) Combinaciones ,
						SUM(CASE WHEN Stock=0 THEN 1 ELSE 0 END)/(CAST (COUNT(Store) AS float)) PorcQuiebre
				FROM [Salcobrand].[reports].[LastReportStores]
				WHERE [Date]=@date
				GROUP BY sku
			) rs ON rs.sku=bi.SKU
			LEFT JOIN ( -- DECLARE @hoy date = GETDATE()
				select *
				FROM (
					SELECT	SKU, FechaExpiracionActual, @hoy FechaHoy,
							DATEDIFF(DAY,@hoy,FechaExpiracionActual) LeadTime,
							sum(UnidadesCompra) UnidadesCompra,
							RANK() OVER (PARTITION BY SKU ORDER BY FechaExpiracionActual, COUNT(*) ASC) AS Ranking  
					FROM series.PurchaseOrders
					WHERE DATEDIFF(DAY,CAST(@hoy AS DATE),FechaExpiracionActual)>0
					GROUP BY SKU,FechaExpiracionActual
				) a where Ranking=1
			) lp ON lp.sku=bi.sku 
			LEFT JOIN (
				SELECT	p.SKU,
								m.Id, 
								m.[Name] Fabricante,
								ps.coverage Cobertura
				FROM [products].[products] p
				LEFT JOIN  [products].[Manufacturers] m ON m.Id=p.ManufacturerId
				LEFT JOIN  [aux].[PurchaseSummary] ps ON ps.SKU=p.SKU
			) mi on mi.SKU=bi.SKU
			WHERE bi.SKU not in (select SKU from salcobrand.products.GenericFamilyDetail)

			union

			--DECLARE @date DATE = (SELECT TOP 1 [date] FROM Salcobrand.reports.LastReportStores)--(SELECT CAST(MAX(date) AS DATE) FROM [Salcobrand].reports.ReportStores) 
			--DECLARE @LastSale DATE =(SELECT TOP 1 [date] FROM Salcobrand.series.LastSale)--(SELECT CAST(MAX(date) AS DATE) FROM [Salcobrand].[series].[Sales]) 
			--DECLARE @date2 DATE =  dateadd(day,-1, @date)
			--DECLARE @hoy date = GETDATE()
			SELECT	u.id,
					u.Nombre,
					bi.GroupId,
					bi.FantasyName,
					bi.SKU,
					bi.[Name],
					mi.Id,
					mi.Fabricante,
					mi.Cobertura,
					rs.LocalesQuebrados,
					rs.Combinaciones,
					rs.PorcQuiebre,
					f.suggest5,
					lp.LeadTime LTProv,
					lp.UnidadesCompra,
					lp.UnidadesCompra+StockCD StockTotal,
					CASE WHEN ls.StockCD>0 THEN ls.StockCD ELSE 0 END StockCD,
					CAST (ISNULL(5*ls.StockCD/NULLIF(f.suggest5,0),0) AS INT) [DíasCD],
					CAST (ISNULL(5*lp.UnidadesCompra/NULLIF(f.suggest5,0),0) AS int) [DíasCoberturaOC]
			FROM [SalcobrandAbastecimiento].[aux].[BaseIndicadores] bi
			INNER JOIN (
				SELECT	grs.GenericFamilyId,
						t.sku,
						SUM(CASE WHEN Stock=0 THEN 1 ELSE 0 END) LocalesQuebrados,
						COUNT(Store) Combinaciones,
						SUM(CASE WHEN Stock=0 THEN 1 ELSE 0 END)/(CAST (COUNT(Store) AS float)) PorcQuiebre 			
				FROM [Salcobrand].[reports].[GenericReportStores] grs
				INNER JOIN (select sku,GenericFamilyId from SalcobrandAbastecimiento.products.GenericTenderedSKU) t on t.GenericFamilyId=grs.GenericFamilyId
				WHERE [Date]=@date2
				GROUP BY grs.GenericFamilyId,t.SKU
			) rs ON rs.sku=bi.SKU
			LEFT JOIN (select Nombre, Id from [StdUsuarios].[dbo].[usuario]) u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS = bi.comprador
			LEFT JOIN  (select sku, stockcd from  SalcobrandAbastecimiento.setup.laststock) ls ON ls.SKU=bi.SKU
			LEFT JOIN (
				SELECT isnull(a.SKU, b.SKU)SKU,isnull(nullif(a.Fit,0),b.VentaPromedio) Suggest5
				FROM (
					SELECT	ds.SKU,SUM(Fit) fit
					FROM [Salcobrand].[forecast].[DailyForecast] ds
					WHERE	ds.[Fecha] BETWEEN @hoy AND DATEADD(DAY,5,@hoy)
					group by ds.SKU
				) a
				full join (
					SELECT	SKU,SUM(Quantity)*1.0 VentaPromedio
					FROM [Salcobrand].[series].[DailySales]
					WHERE [date]=DATEADD(DAY,-5,@LastSale)
					GROUP BY Sku
				) b ON b.sku=a.sku
			) f ON f.SKU=bi.SKU
			LEFT JOIN (
				SELECT	SKU,
						FechaExpiracionActual,
						@hoy FechaHoy,
						DATEDIFF(DAY,@hoy,FechaExpiracionActual) LeadTime,
						sum(UnidadesCompra) UnidadesCompra,
						RANK() OVER (PARTITION BY SKU ORDER BY FechaExpiracionActual, COUNT(*) ASC) AS Ranking  
				FROM SalcobrandAbastecimiento.series.PurchaseOrders
				WHERE DATEDIFF(DAY,@hoy,FechaExpiracionActual)>0
				GROUP BY SKU,FechaExpiracionActual
			) lp ON lp.sku=bi.sku AND lp.Ranking=1
			LEFT JOIN (
				SELECT	p.SKU,
						m.Id, 
						m.[Name] Fabricante,
						Coverage Cobertura
				FROM [salcobrandabastecimiento].[products].[products] p
				LEFT JOIN  SalcobrandAbastecimiento.[products].[Manufacturers] m ON m.Id=p.ManufacturerId
				LEFT JOIN  [aux].[PurchaseSummary] ps ON ps.SKU=p.SKU
			) mi on mi.SKU=bi.SKU



END
