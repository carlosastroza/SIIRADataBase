﻿-- =============================================
-- Author:		Gabriel Espinoza + Carlos Astroza
-- Create date: 2018-05-10
-- Description:	Obtiene la cantidad de días de inventario que cubre un cierto stock. Se basa en DistributionOrdersForecastCurrent
--
-- Modified:	Gabriel Espinoza
-- Modified on:	2018-07-31
-- Description:	Se modifica función para que calcule los días en función de los días de inventario promedio (creada por Esteban Cea)
-- =============================================
CREATE FUNCTION [suggest].[GetInventoryDays]
(
	@sku INT,
	@quantity INT,
	@fromWhen DATE
)
RETURNS 
@result TABLE 
(
	[Days] INT NULL
)
AS
BEGIN
        
	insert into @result ([Days])
	select CASE WHEN @quantity/Venta_Promedio >= 0 THEN @quantity/Venta_Promedio ELSE 0 END [Days]
	from reports.InventoryDays
	where sku = @sku 

	RETURN 
END
