﻿CREATE TABLE [suggest].[PurchaseSuggest] (
    [SKU]              INT        NOT NULL,
    [ServiceLevel]     FLOAT (53) NULL,
    [DaysToCover]      INT        NULL,
    [PurchaseDate]     DATE       NULL,
    [NextPurchaseDate] DATE       NULL,
    [LeadTime]         INT        NULL,
    [SBDemand]         FLOAT (53) NULL,
    [PUDemand]         FLOAT (53) NULL,
    [Suggest]          INT        NULL,
    [Shortage]         INT        NULL,
    [Fillrate]         FLOAT (53) NULL,
    [StockCD]          INT        NULL,
    [ActiveOrders]     INT        NULL,
    [BuyingMultiple]   INT        NULL,
    [ConveniencePack]  INT        NULL,
    [ConveniencePack%] FLOAT (53) NULL,
    [Purchase]         INT        NULL,
    CONSTRAINT [PK_PurchaseSuggest] PRIMARY KEY CLUSTERED ([SKU] ASC),
    CONSTRAINT [FK_PurchaseSuggest_Products] FOREIGN KEY ([SKU]) REFERENCES [products].[Products] ([SKU]) ON DELETE CASCADE ON UPDATE CASCADE
);

