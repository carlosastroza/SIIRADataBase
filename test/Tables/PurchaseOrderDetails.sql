﻿CREATE TABLE [test].[PurchaseOrderDetails] (
    [Id]              INT IDENTITY (1, 1) NOT NULL,
    [PurchaseOrderId] INT NULL,
    [SKU]             INT NULL,
    [Quantity]        INT NULL
);

