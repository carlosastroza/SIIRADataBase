﻿CREATE TABLE [test].[VademecumCarlos] (
    [LAB]                NUMERIC (38)   NULL,
    [COD]                NUMERIC (38)   NULL,
    [PRODUCTO]           NVARCHAR (255) NULL,
    [ISAPRE]             NVARCHAR (255) NULL,
    [ASIGNACION COLMENA] NVARCHAR (255) NULL,
    [CONSALUD]           BIT            NULL,
    [COLMENA]            BIT            NULL
);

