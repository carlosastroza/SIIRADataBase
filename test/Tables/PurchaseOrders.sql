﻿CREATE TABLE [test].[PurchaseOrders] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [ParentId]       INT            NULL,
    [RUT]            INT            NOT NULL,
    [LogisticAreaId] NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [OrderComment]   NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DeliveryTypeId] INT            NOT NULL,
    [Store]          INT            NULL,
    [CreatedBy]      NVARCHAR (255) NULL,
    [CreatedDate]    DATE           NOT NULL,
    [LeadTime]       INT            NULL,
    [ReceptionDate]  DATE           NULL,
    [IsFree]         BIT            NOT NULL,
    [PurchaseType]   NVARCHAR (3)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [IsRetail]       BIT            NULL
);

