﻿CREATE PROCEDURE [test].[PurchaseForAdditionalDay]
@SKU int, 
@ADiasAdicionales int,
@grupo int
AS
BEGIN
SET NOCOUNT ON;

DECLARE @date date = ( SELECT max (date) FROM [SalcobrAND].series.StockCD )

-----------------------     Obtengo el stock actual del CD y las unidades de las órdenes activas -----------------------------------------
		--SELECT scd.SKU, StockCD, ISNULL(po.activeOrders, 0) ActiveOrders
		--INTO #stock
		--FROM (
		--  SELECT * FROM [SalcobrAND].series.StockCD 
		--  WHERE [Date] = DATEADD(DAY, -1, CAST(@date AS DATE))
		--)scd
		--LEFT JOIN (
		-- SELECT a.SKU, SUM(UnidadesCompra) as activeOrders
		--  FROM [SalcobrAND].[aux].[TempPurchaseOrders] a
		--  INNER JOIN [setup].[SuggestParameters] b on b.SKU=a.SKU
		--  WHERE FechaEntrega between cast(@date as date) AND dateadd(day,DaysToCover,@date)
		--	AND EstadoOOCC=2
		--	AND UnidadesRecibidas=0 AND UnidadesCompra>0
		--	AND Store=996
		--  GROUP BY a.SKU
		--) po ON scd.SKU = po.SKU
		--WHERE scd.SKU = @sku
		
		--select SKU, StockCD,  ISNULL(activeOrders, 0) ActiveOrders, Fillrate
		--INTO #stock
		--from suggest.PurchaseSuggest
		--WHERE SKU = @sku 

		SELECT p.SKU, ISNULL(StockCD,0)StockCD, ISNULL(Fillrate,0) FillRate	, 0 activeOrders
		into #stock
		FROM products.Products p
		LEFT JOIN suggest.PurchaseSuggest ps ON p.SKU = ps.SKU
		WHERE (@SKU is null or p.SKU = @SKU)
		AND   (@grupo IS NULL OR p.GroupId = @grupo)

		update p set activeOrders=j.activeOrders
		from #stock p
		inner JOIN suggest.PurchaseSuggest ps ON p.SKU = ps.SKU
		cross apply (
			SELECT a.SKU, SUM(UnidadesCompra) as activeOrders
			FROM [Salcobrand].[aux].[TempPurchaseOrders] a
			where a.SKU = ps.SKU
				and FechaEntrega between ps.PurchaseDate  and dateadd(day, @ADiasAdicionales, NextPurchaseDate)
				and EstadoOOCC=2
				and UnidadesRecibidas=0 and UnidadesCompra>0
				and Store=996
			group by a.SKU
		) j
		--select * from #stock

-----------------------  CALCULO LA COMPRA SEGUN LOS DÍAS ADICIONALES ------------------------------
		DECLARE @Today Date = CONVERT(date, getdate())

		SELECT SP.SKU, 
		ceiling([SalcobrAND].[function].NormalProbability(
		Sum(Case when Pedidos.Date>=SP.PurchaseDate AND Pedidos.Date <= Dateadd(day,SP.DaysToCover + @ADiasAdicionales,SP.PurchaseDate) then Pedidos.PedidoEfectivo else 0 end),
		std.dvStd * Sqrt(SP.DaysToCover + @ADiasAdicionales),
		Max(isnull(SP.ServiceLevel,0))
		)) + isnull(Shortage.Shortage,0) Suggest
 		INTO #compra
		FROM [setup].[SuggestParameters] SP
		INNER JOIN products.Products pp on pp.SKU=sp.SKU
		INNER JOIN   [SalcobrAND].[minmax].[DistributionOrdersForecastCurrent] Pedidos  ON SP.sku=Pedidos.sku
		INNER JOIN (-- Subquery con el promedio de las desviaciones estANDar por dia de la semana, cuANDo no son 0.
		SELECT a.Sku,avg(a.StdPedidos) dvStd
			FROM (
				SELECT SKU,DATEPART(dw,Date) dow,STDEV(PedidoEfectivo) StdPedidos,count(*) Total 
				FROM [SalcobrAND].[minmax].[DistributionOrdersForecastCurrent] 
				GROUP BY SKU,DATEPART(dw,Date) 
				Having sum(PedidoEfectivo)>0
			) a
		GROUP BY a.Sku
		) std on std.SKU=Pedidos.sku 

		LEFT JOIN (
			SELECT ls.SKU,sum(ls.suggest)-sum(case when ls.stock>=0 then ls.stock else 0 end) -sum(ls.Transit)-sum(isnull(d.Picking,0))  Shortage
			FROM SalcobrAND.series.LastStock ls 
			INNER JOIN SalcobrAND.stores.Stores S on S.Store=ls.Store
			LEFT JOIN (SELECT sku,store,Picking FROM SalcobrAND.series.DespachosCD WHERE date=DATEADD(day,-1,@Today)) d on d.SKU=ls.SKU AND d.Store=ls.Store 
			WHERE ls.suggest>(case when ls.stock>=0 then ls.stock else 0 end) +ls.Transit+isnull(d.Picking,0)
			GROUP BY ls.SKU
		) Shortage ON Shortage.SKU= SP.sku 

		--WHERE sp.SKU=@sku
		WHERE (@sku IS NULL OR pp.SKU = @sku)
		AND (@grupo IS NULL OR pp.GroupId = @grupo)

		GROUP BY SP.SKU,isnull(SP.ServiceLevel,0.95),SP.DaysToCover,SP.PurchaseDate,SP.NextPurchaseDate,SP.LeadTime,std.dvStd,Shortage.Shortage

		--select * from #compra
	
  --------------------------  Sugerido con Fillrate  ------------------------------------
		
		SELECT a.SKU, a.StockCD, a.ActiveOrders, a.Fillrate, isnull(b.Suggest,0)Suggest
		INTO #result
		FROM #stock a 
		LEFT JOIN #compra b on a.SKU = b.SKU
		
		SELECT * FROM #result
		
		declare @minFillrate float = 0.75

		--declare @comprasugerida int = 
		--(select  
		--	case when (isnull(Suggest,0)-isnull(ActiveOrders,0)-isnull([StockCD],0))/(case when isnull(Fillrate,1)<@minFillrate
		--																			  then @minFillrate 
		--																			  else isnull(Fillrate,1)
		--																			  end)<0 
		--	then 0 
		--	else (isnull(Suggest,0)-isnull(ActiveOrders,0)-isnull([StockCD],0))/(case when isnull(Fillrate,1)<@minFillrate 
		--																		 then @minFillrate 
		--																		 else isnull(Fillrate,1) end) 
		--	end 
		--FROM #result s)


  --------------------------  RESULTADO  ------------------------------------------------
      	  
	   SELECT 
	   ps.SKU,case when  r.UnidadesTotales >=0 then r.UnidadesTotales else 0 end UnidadesTotales -- los numeros negativos representan mas stock y unidades en transito

	   FROM suggest.PurchaseSuggest ps
	   INNER JOIN products.Products p ON ps.SKU = p.SKU
	   INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	   INNER JOIN #result rr on ps.SKU = rr.SKU
	   CROSS APPLY [function].[RoundLogisticUnits] (
			case when (isnull(rr.Suggest,0)-isnull(rr.ActiveOrders,0)-isnull(rr.[StockCD],0))/(case when isnull(rr.Fillrate,1)<@minFillrate then @minFillrate else isnull(rr.Fillrate,1) end)<0 then 0 
			else (isnull(rr.Suggest,0)-isnull(rr.ActiveOrders,0)-isnull(rr.[StockCD],0))/(case when isnull(rr.Fillrate,1)<@minFillrate then @minFillrate else isnull(rr.Fillrate,1) end) end ,
	
	  null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
	     
	  WHERE (@sku IS NULL OR ps.SKU = @sku)
	  AND (@grupo IS NULL OR p.GroupId = @grupo)

	  drop  table #stock
	  drop  table #compra
	  drop table  #result
	  
END
