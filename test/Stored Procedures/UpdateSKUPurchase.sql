﻿CREATE PROCEDURE [test].[UpdateSKUPurchase]
 @SKU_C INT,
 @DIAS_ADICIONALES INT,
 @COMPRA_CON_DIAS INT, --Compra calculada incluyendo días adicionales, en caso que no ingrese días, toma el sugerido de compra
 @COMPRA INT --Compra ingresa
AS
BEGIN
SET NOCOUNT ON;

DECLARE @COMPRA_SUMMARY INT = (SELECT Purchase FROM aux.PurchaseSummary WHERE SKU = @SKU_C);

UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = @DIAS_ADICIONALES, Purchase = isnull(@COMPRA_CON_DIAS,NULL) WHERE SKU = @SKU_C;

IF @COMPRA_SUMMARY <> @COMPRA BEGIN
UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = NULL, Purchase = @COMPRA WHERE SKU = @SKU_C;
END

END
