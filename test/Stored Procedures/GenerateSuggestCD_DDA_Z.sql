﻿-- =============================================
-- Author:		Gonzalo Cornejo
-- Create date: 2018-03-19
-- Description:	Calcula el sugerido de compra--		
-- =============================================
create PROCEDURE [test].[GenerateSuggestCD_DDA_Z]

AS
BEGIN
	SET NOCOUNT ON;

declare @fechaCalc date = (select FechaCalculo from Salcobrand.aux.UltimoPronostico)
  
  truncate table [suggest].[PurchaseSuggest]

  insert into [suggest].[PurchaseSuggest]
  
  select SP.SKU SKU, 
  Max(SP.ServiceLevel) ServiceLevel,
  Max(SP.DaysToCover) DaysToCover,
  Max(SP.PurchaseDate) PurchaseDate,
  Max(SP.NextPurchaseDate) NextPurchaseDate,
  Max(SP.LeadTime) LeadTime,
  Sum(Case when Pedidos.Date>=SP.PurchaseDate and Pedidos.Date <= Dateadd(day,SP.DaysToCover,SP.PurchaseDate) then Pedidos.PedidoEfectivo else 0 end) SBDemand,
  Stdev(PedidoEfectivo) * Sqrt(count(*)) std,
  
  ceiling([Salcobrand].[function].NormalProbability(
  Sum(Case when Pedidos.Date>=SP.PurchaseDate and Pedidos.Date <= Dateadd(day,SP.DaysToCover,SP.PurchaseDate) then Pedidos.PedidoEfectivo else 0 end),
  Stdev(PedidoEfectivo) * Sqrt(count(*)),
  Max(SP.ServiceLevel)
  )) Suggest,
  Null Shortage,
	Null [Fillrate],
      Null [StockCD],
      Null [ActiveOrders],
     Null [BuyingMultiple],
      Null [ConveniencePack],
      Null [ConveniencePackPorc],
      Null [Purchase]

  from [setup].[SuggestParameters] SP

  INNER JOIN 
  --[Salcobrand].[minmax].[DistributionOrdersForecastCurrent] Pedidos 
    (select ProductId SKU,Date, isnull(CorrectedFit,Fit) PedidoEfectivo 
from [Salcobrand].[forecast].[PredictedDailySales]
where DateKey = @fechaCalc) Pedidos ---Reemplazo pedidos por Venta, Mientras no este PEdidos
  
  ON SP.sku=Pedidos.sku
  --where Pedidos.Date<='2018-05-28'

  group by SP.SKU

	
END
