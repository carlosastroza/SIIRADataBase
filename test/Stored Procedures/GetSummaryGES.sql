﻿-- =============================================
-- Author:		Juan M Hernandez
-- Create date: 27/02/2018
-- Description:	Obtener los GES con sugerido de compra
-- =============================================

CREATE PROCEDURE [test].[GetSummaryGES]
@USERNAME NVARCHAR(MAX),
@SKU INT,
@PROVEEDORES INT,
@COMPRAMAYORCERO INT,
@ESTADOCOMPRA INT,
@OC INT
AS
BEGIN

SET NOCOUNT ON;

DECLARE @USUARIO_LOCAL NVARCHAR(MAX) = @USERNAME;

IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USERNAME = null;
END

IF @OC = 0 BEGIN
SET @OC = null
END


----------- compras mayor que 0 ----------------------

declare @auxcompramayor int  ;
if @COMPRAMAYORCERO = 2 begin
	 set @auxcompramayor  = 0;
end 

------------- estado para filtro ------------------------

declare @auxestado bit  ;
declare @purcahseestado bit;

if @ESTADOCOMPRA = 1 begin
	 set @auxestado  = 0;
end else begin
		if @ESTADOCOMPRA = 2 begin 
		 set @auxestado = 1;
		end else begin
			if @ESTADOCOMPRA = 3 begin 
				set @purcahseestado = 1;
			end
		end
end

------------- sku para filtro de proveedores ------------
SELECT p.SKU 
INTO #SkuManufacturer 
FROM products.Products p
INNER JOIN products.Manufacturers m on m.Id = p.ManufacturerId
INNER JOIN products.Vademecum v on v.SKU = p.SKU
WHERE ManufacturerId = @PROVEEDORES 
-------------  detalle de OC proveedores ----------------
select po.Id, po.RUT, pod.sku, pod.Quantity
INTO #Proveedores
from aux.PurchaseOrders po
INNER JOIN aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
where po.RUT not in (select DistributorRUT from products.Vademecum) 
group by po.Id, po.RUT, pod.sku, pod.Quantity
	

SELECT *
,cast (DENSE_RANK() over (order by sku desc) as int ) [Rank]
 FROM (
	             /*Dsitribuidores*/
				SELECT
									   
					   distinct
					   d.[Name] Laboratorio,
					   p.Category Categoria,
					   d.RUT,
					   i.Isapre,
					   i.IdZona,
					   p.[Name] Nombre,
					   p.LogisticAreaId Rubro,
					   v.SKU,
					   (ISNULL(ps.LastMonthSalesGES,0) ) FillRate, 
					   NULL DiasInventario,
					   NULL DiasAdicionales,
					   NULL RemainingStockDays,
					   NULL FaltanteDistribuidor,
					   ps.LastMonthSalesGES RotacionDistribuidor,
					   pc.Price Precio ,
					   pc.Cost Costo ,
					   (pc.Cost * ps.Purchase) CostoTotal, 					   
					   NULL StockLocales,
					   NULL StockCD,
					   NULL Quiebre,
					   ISNULL(ps.LastMonthSalesGES,0)  VentaUltimoMes,
					   NULL DemandaSB,
					   NULL PUDemand,
					   NULL Sugerencia,
					   CASE WHEN ps.LastMonthSalesGES is not null 
					   THEN ISNULL(ps.LastMonthSalesGES, 0) 
					   else NULL end SugerenciaFinal,
					   ps.Purchase Compra,
					   cast (0 as bit) IsPromotion,
					   a.[Days] DiasCompra,
					   NULL Cobertura,
					   NULL TransitoCD,
					   NULL TransitoLocal,
					   NULL Faltante,
					   NULL TotalRanking,
					   CAST(CASE WHEN(CASE WHEN pod.SKU IS NOT NULL THEN 1 ELSE 0 END) >0
												THEN 'comprado.png'
												ELSE 
												  CASE WHEN ps.Status = 1 
												  THEN  'proceso.png'
												  ELSE 'no_comprado.png' END
												END
					   as nvarchar) [Status],
					   1 IsDistributor,
					   null LostSaleLastMonth,
					   null FitNextMonth,
					   p.Motive,
					   p.motiveId,
					   --CASE WHEN lp.BuyingMultiple IS NULL THEN LP.RoundToBoxes ELSE LP.BuyingMultiple END BuyingMultiple,
					   lp.BuyingMultiple,
					   lp.UnitsInBox UnidadesCaja,
					   b.Id PurchaseOrderId,
					   --.det.PurchaseOrderId,
					   CAST(CASE WHEN b.SKU IS NOT NULL THEN 0 else 1 END AS BIT) PermitirEdicion
				FROM Products.Vademecum v				
				INNER JOIN  Products.Distributors d on d.RUT = v.DistributorRUT
				INNER JOIN Products.Products p on p.SKU = v.SKU
				INNER JOIN users.SkuUsers su on p.SKU = su.SKU
				LEFT JOIN aux.PurchaseSummaryDistributor ps on ps.SKU = v.SKU AND ps.RUT  COLLATE DATABASE_DEFAULT = v.DistributorRUT AND v.IdZona = ps.IdZona
				INNER JOIN setup.DistributorIsapre i on i.IdZona = ps.IdZona
				LEFT JOIN setup.PriceAndCostGES pc on pc.SKU = v.SKU and pc.IdZona = ps.IdZona 
				LEFT JOIN ( select * from [function].SKUPurchased ('GES'))pod on pod.SKU = v.SKU and pod.RUT COLLATE DATABASE_DEFAULT = d.RUT and pod.IdZona = ps.IdZona
				LEFT JOIN [setup].[LogisticParameters] lp on lp.SKU = ps.SKU
				
				--LEFT JOIN aux.PurchaseOrderDetails det ON v.SKU = det.SKU
				--LEFT JOIN aux.PurchaseOrders oc ON det.PurchaseOrderId = oc.Id
				LEFT JOIN(
					SELECT
					SKU, oc.Id
					FROM aux.PurchaseOrders oc
					INNER JOIN aux.PurchaseOrderDetails det ON oc.Id = det.PurchaseOrderId
					WHERE OC.CreatedBy = @USUARIO_LOCAL
					AND IdZona <> 37
				)b ON p.SKU = b.SKU

				OUTER APPLY [suggest].[GetInventoryDays](p.SKU,ps.Purchase,GETDATE()) a
				WHERE    (@USERNAME IS NULL OR su.Username = @USERNAME) 
						 AND (v.SKU = @SKU or @SKU = 0) AND (v.SKU in (select sku from #SkuManufacturer ) or @PROVEEDORES = 0)
						 AND ((PS.Status = @auxestado and pod.sku is null) or @auxestado is null ) AND ((pod.sku is not null and @purcahseestado =1 ) or (pod.sku is null and @purcahseestado is null) or @purcahseestado is null )
						 AND (ps.Purchase> @auxcompramayor or @auxcompramayor IS NULL )
						 AND (@OC IS NULL OR b.Id = @OC)
				
				UNION  
				/* Proveedores */
				SELECT	
					   distinct
				       m.[Name] Laboratorio,
					   p.Category Categoria,
					   m.RUT,
					   null Isapre,
					   37 IdZona,
					   p.[Name] Nombre,
					   p.LogisticAreaId Rubro,
					   v.SKU,
					   ps.FillRate ,
					   ps.DaysToCover DiasInventario,
					   CASE WHEN ps.AdditionalDaysToCover = 0 THEN NULL ELSE ps.AdditionalDaysToCover END DiasAdicionales,
					   ps.RemainingStockDays,
					   null FaltanteDistribuidor,
					   null RotacionDistribuidor,
					   pc.Price  Precio,
					   pc.Cost Costo,
					   (pc.Cost * ps.Purchase) CostoTotal, 
					   ps.StoresStock StockLocales,
					   ps.DCStock StockCD,
					   ps.StockOut Quiebre ,
					   ps.LastMonthSale - (ISNULL(psd.LastMonthSalesGES,0) ) VentaUltimoMes,
					   ps.SBDemand DemandaSB,
					   ps.PUDemand,
					   ps.suggest Sugerencia ,
					   ps.PurchaseSuggest SugerenciaFinal,
					   
					   isnull (ps.Purchase, 0) Compra,
					  
					   case when sp.SKU is null then cast (0 as bit)  else cast (1 as bit) end IsPromotion, 
					   a.[Days] DiasCompra,
					   ps.Coverage Cobertura,
					   ps.DCTransit TransitoCD,
					   ps.StoresTransit TransitoLocal,
					   ps.StoresShortage Faltante,
					   ps.TotalRanking,
					   CAST(CASE WHEN (CASE WHEN pod.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0
												THEN 'comprado.png'
												ELSE 
												  CASE WHEN ps.Status =1  
												  THEN  'proceso.png'
												  ELSE 'no_comprado.png' END
												END
									   as nvarchar) [Status],
						0 IsDistributor

						, ps.LostSaleLastMonth
						,ps.FitNextMonth
						,p.Motive 
						,p.MotiveId
						--,CASE WHEN lp.BuyingMultiple IS NULL THEN LP.RoundToBoxes ELSE LP.BuyingMultiple END BuyingMultiple
						,lp.BuyingMultiple
					    ,lp.UnitsInBox UnidadesCaja
						,b.Id PurchaseOrderId
						,CAST(CASE WHEN b.SKU IS NOT NULL THEN 0 else 1 END AS BIT) PermitirEdicion
				FROM Products.Vademecum v
				INNER JOIN aux.PurchaseSummary ps on ps.SKU = v.SKU
				INNER JOIN Products.Products p on p.SKU = ps.SKU
				INNER JOIN products.Manufacturers m on m.Id = p.ManufacturerId
				INNER JOIN setup.PriceAndCost pc on pc.SKU = v.SKU
				INNER JOIN users.SkuUsers su on p.SKU = su.SKU
				LEFT JOIN aux.PurchaseSummaryDistributor psd on psd.SKU = v.SKU AND psd.RUT  COLLATE DATABASE_DEFAULT = v.DistributorRUT and v.IdZona = psd.IdZona
				LEFT JOIN #Proveedores prov on prov.SKU = v.SKU AND prov.RUT = m.RUT
				LEFT JOIN products.AdditionalStockDays asd on ps.SKU = asd.SKU
				LEFT JOIN (select * from [function].SKUPurchased ('GES')) pod on pod.SKU = v.SKU and pod.RUT  = m.RUT COLLATE DATABASE_DEFAULT
				LEFT JOIN suggest.PromotionProducts sp on sp.SKU = ps.SKU
				LEFT JOIN [setup].[LogisticParameters] lp on lp.SKU = ps.SKU

				LEFT JOIN(
					SELECT
					SKU, oc.Id
					FROM aux.PurchaseOrders oc
					INNER JOIN aux.PurchaseOrderDetails det ON oc.Id = det.PurchaseOrderId
					WHERE OC.CreatedBy = @USUARIO_LOCAL
					AND IdZona = 37
				)b ON p.SKU = b.SKU
				--LEFT JOIN aux.PurchaseOrderDetails det ON v.SKU = det.SKU
				--LEFT JOIN aux.PurchaseOrders oc ON det.PurchaseOrderId = oc.Id

				OUTER APPLY [suggest].[GetInventoryDays](p.SKU,ps.Purchase,GETDATE()) a
				WHERE (@USERNAME IS NULL OR su.Username = @USERNAME) 
					  AND (PS.SKU = @SKU or @SKU = 0) AND (ps.SKU in (select sku from #SkuManufacturer ) or @PROVEEDORES = 0)
				      AND ((PS.Status = @auxestado and pod.sku is null) or @auxestado is null ) AND ((pod.sku is not null and @purcahseestado =1 ) or (pod.sku is null and @purcahseestado is null) or @purcahseestado is null )
					  AND (ps.Purchase> @auxcompramayor or @auxcompramayor IS NULL )
					  AND (@OC IS NULL OR b.Id = @OC)
					   
				
				) a 
				order by a.SKU
				

DROP TABLE #Proveedores
DROP TABLE #SkuManufacturer


end




