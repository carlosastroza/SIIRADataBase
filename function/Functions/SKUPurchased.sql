﻿-- =============================================
-- Author:		<Juan M >
-- Create date: <10-04-2018>
-- Description:	<Función que permite saber cúales son los SKU que no se pueden aun compra de acuedo a su última venta>
-- =============================================
CREATE FUNCTION [function].[SKUPurchased] 
(	
	@type varchar(3)
)
RETURNS @Result TABLE ( 

sku int,
Quantity int,
RUT varchar(15),
IdZona int,
Cost int
)
AS

BEGIN
	-- Fecha actual
	DECLARE @date date = (SELECT convert(date, getdate()))
	-- con la fecha actual obtengo el día en que estoy parado para realizar la compra lunes= 2; martes = 3 miércoles = 4...
	DECLARE @dato int = (SELECT DATEPART(DW, @date))

	 
	INSERT INTO @Result
	SELECT a.SKU, Quantity, RUT, IdZona, Cost
	--into #tabla1
	FROM (
		
			SELECT 
			CASE WHEN PurchaseOnSunday = 1 and @dato = 1 then 1 else
				CASE WHEN PurchaseOnMonday = 1 and @dato = 2 then 2 else 
					CASE WHEN PurchaseOnTuesday = 1  and @dato = 3 then 3 else	
						CASE WHEN PurchaseOnWednesday = 1 and @dato = 4 then 4 else 
							CASE WHEN PurchaseOnTuesday = 1 and @dato = 5 then 5 else 
								CASE WHEN PurchaseOnFriday = 1 and @dato = 6 then 6 else 
									CASE WHEN PurchaseOnSaturday = 1 and @dato = 7 then 7 
									END
								END
							END 
						END 
					END 
				END 
			END ciclo,
			p.SKU,
			g.OrderCycle
			
			from products.Groups g
			INNER JOIN  products.Products p on p.GroupId = g.Id 
	)a 
	INNER JOIN  (
		SELECT SKU ,CreatedDate,Quantity,RUT, IdZona, Cost from purchase.PurchaseOrders po
		INNER JOIN  purchase.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
		where po.PurchaseType = @type
	
	) po on po.SKU = a.SKU
	WHERE po.CreatedDate >= DATEADD(day, -OrderCycle, @date) 
	and (SELECT [dbo].[getPurchaseCycle] (po.CreatedDate,a.ciclo)) between DATEADD(day, -OrderCycle, @date) and @date
	
	Return
END