﻿-- =============================================
-- Author:		<Plan Z>
-- Create date: <04-04-2018>
-- Description:	<Función que permite redondear la compra sugerida a una unidad logística o convinience pack según corresponda>
-- =============================================
CREATE FUNCTION [function].[RoundLogisticUnits] 
(	
	-- Add the parameters for the function here

	--copmpra sugerida 
	@purchase float,

	-- cantidad de unidades de cada sku por unidad logística 
	@unitsInInner int = null,
	@unitsInBox int,
	@unitsInLayer int,
	@unitsInPallet int,
	@buyMult int,

	--variables que determinan a qué unidad logísitca se debe redondear (solo una variable debe ser 1, y el resto 0)
	@roundToInner int = null,
	@roundToBoxes int,
	@roundToLayers int,
	@roundToPallets int,
	
	--porcentaje que determina cuál es la cantidad mínima del sku que debe comprar para adquirir una unidad logística adicional 
	@umbral float = null,

	--cantidad de sku asociada al convinience pack (puede ser null)
	@conveniencePack int,

	-- porcentaje que define la cantidad mínima de sku que se debe copmprar para poder adquirir un convinience pack adicional (si la variable anterior es null, esta también debe ser null)
	@conveniencePackPorc float

	
)
RETURNS @Result TABLE ( 

UnidadesLogisticas int,
MultiploUnidadLogistica int,
UnidadesTotales int
)
AS

BEGIN
	
	--set @unitsInInner = isnull(@buyMult,@unitsInInner)
	--set @unitsInBox = isnull(@buyMult,@unitsInBox)
	--set @unitsInLayer = isnull(@buyMult,@unitsInLayer)
	--set @unitsInPallet = isnull(@buyMult,@unitsInPallet)

	-- Add the SELECT statement with parameter references here

	-- Cantidad de unidades de sku para la unidad logítica a utilizar
	Declare @cantidadUnidades int =  isnull(@buyMult,@unitsInBox)--isnull(@roundToInner*@unitsInInner,0) + @roundToBoxes*@unitsInBox + @roundToLayers*@unitsInLayer + @roundToPallets*@unitsInPallet 
	
	-- Numero decimal con la cantidad de unidades logísticas necesarias
	Declare @cantidadUnidadesLogisticas float = case when @cantidadUnidades>0 then cast(@purchase as float)/@cantidadUnidades else 0 end
	
	--Número entero con la cantidad definitiva de unidades logísticas a adquirir en función de la cantidad entera (número decimal de la variable anterior redondeado hacia abajo), sumado a la evaluación de si es necesario adquirir una unidad logística adicional, a partir del umbral definido para la unidad logística.
	Declare @unidadesLogisticasFinal int = floor(@cantidadUnidadesLogisticas) + (select case when  (@cantidadUnidadesLogisticas - floor(@cantidadUnidadesLogisticas)) > isnull(@umbral,0.5) then 1 else 0 end)
	
	-- Si existe convinience pack, define la cantidad de convinience pack que se debe adquirir en función del numero entero que resulta de la división entre la cantidad de unidades de sku a adquirir (ya redondeadas según unidad logística) y la cantidad de sku por convinience pack , a lo anterior se suma la evaluación de si es necesario adquirir una unidad adicional de convinience pack en función del porcentaje de convinience pack
	Declare @cantidadConviniencePack int = isnull(floor(@purchase/@conveniencePack) + (case when (cast(@purchase as float)/@conveniencePack - floor(@purchase/@conveniencePack)) > @conveniencePackPorc then 1 else 0 end),0)
	
	 
	INSERT INTO @Result
	SELECT

	-- Cantidad que se debe adquirir de la unidad logística empleada o convinience pack, según corresponda
	case when @cantidadConviniencePack = 0  then @unidadesLogisticasFinal  else @cantidadConviniencePack end UnidadesLogisticas,
	
	--Cantidad de unidades de sku defnida para la unidad logística empleada o convinience pack, según corresponda
	case when @cantidadConviniencePack = 0  then @cantidadUnidades else @conveniencePack end MultiploUnidadLogistica,
	
	--Cantidad de sku total a adquirir en función de la unidad logística empleada o convinience pack, según corresponda
	case when @cantidadConviniencePack = 0  then (case when @cantidadUnidades=0 then Ceiling(@purchase) else @unidadesLogisticasFinal* @cantidadUnidades end) else @cantidadConviniencePack*@conveniencePack end UnidadesTotales


	Return
END