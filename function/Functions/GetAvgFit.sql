﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [function].[GetAvgFit]
(
	-- Add the parameters for the function here
	@SKU int
)
RETURNS 
@result TABLE 
(
	AvgFit int Null
)
AS
BEGIN
        
    declare @average FLOAT;
    declare @data_used_for_average INT;
    declare @result_aux FLOAT;

    declare @dates_to_use_past TABLE (Fecha Date);


	--lo primero es intentar calcular el promedio utilizando los pronósticos
	select @data_used_for_average = COUNT(*), @average = AVG(FitCorregido)
    from (
        select Fecha
        from Salcobrand.forecast.GenericVariables gv
        where   Fecha BETWEEN getdate() AND DATEADD(wk, 4, getdate()) 
            AND Feriado <> 1 
            AND FeriadoIrrenunciable <> 1
    ) gv
    inner join (select Fecha, FitCorregido from Salcobrand.forecast.DailyForecast df where df.SKU = @sku) df ON gv.Fecha = df.Fecha

	--si tenemos menos de 3 semanas, trataremos de usar venta + venta perdida
    IF(@data_used_for_average < 21 or isnull(@average,0)=0) 
    BEGIN 
        INSERT INTO @dates_to_use_past
        select Fecha
        from Salcobrand.forecast.GenericVariables gv
        where   Fecha BETWEEN  DATEADD(WEEK, -4, getdate()) and GETDATE()
            AND Feriado <> 1 
            AND FeriadoIrrenunciable <> 1

        select @data_used_for_average = COUNT(*), @average = avg(Venta)
        from @dates_to_use_past gv
        inner join (
            select [Date] Fecha, (VtaReal + VtaPerdida) Venta 
            from Salcobrand.reports.LostSaleBySKUCambio ls 
            where ls.SKU = @sku
        ) ls ON gv.Fecha = ls.Fecha

    END

	--si tenemos menos de 3 semanas con venta y venta perdida, trataremos de usar venta pura (a veces no existe data en LostSaleBySKUCambio si no nos llega el stock del sku)
    IF(ISNULL(@data_used_for_average, 0) < 21 or isnull(@average,0)=0)
    BEGIN
        
        select @data_used_for_average = COUNT(*), @average = avg(Venta)
        from @dates_to_use_past gv
        inner join (
            select [Date] Fecha, (Quantity) Venta 
            from Salcobrand.series.DailySales ls
            where ls.SKU = @sku
        ) ls ON gv.Fecha = ls.Fecha
    END

    SET @result_aux = isnull(@average,0)

	insert into @result VALUES (@result_aux)

	RETURN 
END
