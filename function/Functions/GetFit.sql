﻿-- =============================================
-- Author:		Plan Z
-- Create date: 23/05/2018
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [function].[GetFit]
(
	-- Add the parameters for the function here
	@sku int,
	@days int,
	@date date
)
RETURNS 
@result TABLE 
(
	Fit float NULL
)
AS
BEGIN
        
    declare @suma FLOAT;
    declare @data_used_for_average INT;
    declare @result_aux FLOAT;

    declare @dates_to_use_past TABLE (Fecha Date);

	if(@days>0)
	begin
			--lo primero es intentar calcular el promedio utilizando los pronósticos
			select @data_used_for_average = COUNT(*), @suma = sum(FitCorregido)
			from (
			    select Fecha
			    from Salcobrand.forecast.GenericVariables gv
			    where   Fecha BETWEEN @date AND DATEADD(dy, @days, @date) 
			        AND Feriado <> 1 
			        AND FeriadoIrrenunciable <> 1
			) gv
			inner join (select Fecha, FitCorregido from Salcobrand.forecast.DailyForecast df where df.SKU = @sku) df ON gv.Fecha = df.Fecha

			--si tenemos menos de 3 semanas, trataremos de usar venta + venta perdida
			IF(@data_used_for_average < @days) 
			BEGIN 
			    INSERT INTO @dates_to_use_past
			    select Fecha
			    from Salcobrand.forecast.GenericVariables gv
			    where   Fecha BETWEEN DATEADD(dy, (-1)*@days, @date) AND @date
			        AND Feriado <> 1 
			        AND FeriadoIrrenunciable <> 1

			    select @data_used_for_average = COUNT(*), @suma = SUM(Venta)
			    from @dates_to_use_past gv
			    inner join (
			        select [Date] Fecha, (VtaReal + VtaPerdida) Venta 
			        from Salcobrand.reports.LostSaleBySKUCambio ls 
			        where ls.SKU = @sku
			    ) ls ON gv.Fecha = ls.Fecha

			END

			--si tenemos menos de 3 semanas con venta y venta perdida, trataremos de usar venta pura (a veces no existe data en LostSaleBySKUCambio si no nos llega el stock del sku)
			IF(ISNULL(@data_used_for_average, 0) < @days or isnull(@suma,0)=0)
			BEGIN
			    
			    select @data_used_for_average = COUNT(*), @suma = Sum(Venta)
			    from @dates_to_use_past gv
			    inner join (
			        select [Date] Fecha, (Quantity) Venta 
			        from Salcobrand.series.DailySales ls
			        where ls.SKU = @sku
			    ) ls ON gv.Fecha = ls.Fecha
			END
	end
	else
	begin
		select @suma=0
	end

			SET @result_aux = isnull(@suma,0)

			insert into @result VALUES (@result_aux)

			RETURN 
END
