﻿CREATE FUNCTION [dbo].[ConcatenatePendingGenericSKU]
(
  @Familia SYSNAME,
  @USERNAME NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
  
AS 
BEGIN
  DECLARE @s NVARCHAR(MAX);
 
	SELECT @s = 
	
	COALESCE(@s + N', ', N'') + CONCAT(CAST(p.SKU as nvarchar),' (',SUM(d.Quantity),')')
	--FROM Salcobrand.products.GenericFamilyDetail g
	FROM products.Products p
	INNER JOIN aux.PurchaseOrderDetails d on p.SKU = d.SKU
	--INNER JOIN aux.PurchaseOrders oc ON d.PurchaseOrderId = oc.Id
	WHERE p.GenericFamilyId = @Familia
	--AND oc.CreatedBy = @USERNAME
	GROUP BY p.SKU


  RETURN (@s);
END