﻿CREATE FUNCTION [dbo].[SumarDetalleOC]
(
  @OC SYSNAME
)
RETURNS INT
  
AS 
BEGIN
  DECLARE @s FLOAT;
  SELECT @s = (  select 
				sum(round(sp.Costo,0)*ocd.Cantidad) Total
				from aux.PurchaseOrdersDetails ocd
				INNER JOIn dbo.SuggestProducts sp on ocd.SKU = sp.SKU
				where ocd.OC = @OC
			);
  RETURN (@s);
END

