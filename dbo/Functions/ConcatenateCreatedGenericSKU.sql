﻿CREATE FUNCTION [dbo].[ConcatenateCreatedGenericSKU]
(
  @Familia SYSNAME,
  @USERNAME NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
  
AS 
BEGIN
  DECLARE @s NVARCHAR(MAX);
 
	SELECT @s = COALESCE(@s + N', ', N'') + CONCAT(CAST(p.SKU as nvarchar),' (',SUM(ad.Quantity),')')

	FROM products.Products p
	inner JOIN purchase.PurchaseOrderDetails ad on p.SKU = ad.Sku
	--INNER JOIN purchase.PurchaseOrders oc ON ad.PurchaseOrderId = oc.Id
	WHERE p.GenericFamilyId = @Familia
	--AND oc.CreatedBy = @USERNAME
	GROUP BY p.SKU

  RETURN (@s);
END