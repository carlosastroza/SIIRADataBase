﻿CREATE FUNCTION [dbo].[GetUsersDivision]
(
  @USERNAME SYSNAME
)
RETURNS NVARCHAR(MAX)
  
AS 
BEGIN
  DECLARE @s NVARCHAR(MAX);
 
	SELECT @s = COALESCE(@s + N',', N'') + CAST(Division as nvarchar)
	FROM setup.DivisionUsers
	WHERE Username = @USERNAME
	ORDER BY Division ASC

  RETURN (@s);
END

