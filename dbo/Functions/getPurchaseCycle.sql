﻿-- =============================================
-- Author:		Juan M Henández
-- Create date: 2018-04-09
-- Description:	devuelve sugún la fecha y el ciclo de compra si el grupo debe ser comprado o no
-- =============================================
CREATE FUNCTION [dbo].[getPurchaseCycle]
(
	@date date,
	@cycle int 
)
RETURNS date
AS
BEGIN
	
	DECLARE @dato date;
	DECLARE @index INT = @cycle
	SELECT @dato = (
					SELECT  
						
						case when @cycle <> null then  
						
							CASE WHEN  @cycle = DATEPART(DW, @date)
							THEN @date
							ELSE
								CASE WHEN @index > ( DATEPART(DW, @date)) 
									THEN DATEADD(DAY, @index - (7+( DATEPART(DW, @date))) ,@date)
								ELSE
									 DATEADD(DAY, @index - (( DATEPART(DW, @date))) ,@date)
								  END
							END
						else
							@date
						end 

	)
	RETURN (@dato)
END

