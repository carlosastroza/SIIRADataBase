﻿Create FUNCTION [dbo].[SumarDetalleOCGes]
(
  @OC SYSNAME
)
RETURNS INT
  
AS 
BEGIN
  DECLARE @s FLOAT;
 
  SELECT @s = (  
					SELECT 
						SUM(sp.Costo*ocd.Cantidad) Total
					FROM test.Ges_OCDetalle ocd
					INNER JOIN test.SuggestProducts  sp on ocd.sku = sp.SKU
					WHERE ocd.OC =@OC

			);


  RETURN (@s);
END

