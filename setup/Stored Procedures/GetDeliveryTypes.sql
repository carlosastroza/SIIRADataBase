﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-15
-- Description:	Obtiene los tipos de despacho de las compras
-- =============================================
CREATE PROCEDURE setup.[GetDeliveryTypes]
	@tipoCompra NVARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM setup.DeliveryType
	--El parámetro TipoCompra existe para que en un futuro filtremos esta data. Por ahora no pasa nah
END
