﻿-- =============================================
-- Author:		Gonzalo Cornejo
-- Create date: 2018-03-19
-- Description:	Proceso para correr individualmete el calendario de compras para todos los SKU, debiera demorar menos de 1 segundo. 
-- Esta pensado para correrlo todos los días dentro del proceso diario de sugerido.
-- =============================================
CREATE PROCEDURE [setup].[FillPurchaseCalendar]
	@today DATE,
	@group int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--drop table #NextPurchase
--drop table #NextPurchaseTemp

---
---
-- Query para determinar cuándo es la próxima compra después de comprar hoy, suponiendo que estoy adelantando la fecha de compra de lo que se está comprando hoy a menos que no este más cerca la fecha de la compra pasada, caso para el cual estaría retrasando la compra anterior y manteniendo la compra futura.
---
---
--DECLARE @today date = getdate()
--declare @group int = null


SELECT 
--RUT,
--LogisticAreaId,
--Username,
Id,
--LeadTime,
CASE datepart("dw",@today)
when 1 then case when LTSunday>0 then LTSunday else LeadTime end
when 2 then case when LTMonday>0 then LTMonday else LeadTime end
when 3 then case when LTTuesday>0 then LTTuesday else LeadTime end
when 4 then case when LTWednesday>0 then LTWednesday else LeadTime end
when 5 then case when LTThursday>0 then LTThursday else LeadTime end
when 6 then case when LTFriday>0 then LTFriday else LeadTime end
when 7 then case when LTSaturday>0 then LTSaturday else LeadTime end
end LeadTime,
1 ReceptionLT,
	@today PurchaseDate,
	CASE 
	WHEN datepart("dw",dateadd(day,OrderCycle,getdate())) <= 2*[PurchaseOnMonday] then dateadd(day,OrderCycle+2-datepart("dw",dateadd(day,OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,OrderCycle,getdate())) <= 3*[PurchaseOnTuesday] then dateadd(day,OrderCycle+3-datepart("dw",dateadd(day,OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,OrderCycle,getdate())) <= 4*[PurchaseOnWednesday] then dateadd(day,OrderCycle+4-datepart("dw",dateadd(day,OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,OrderCycle,getdate())) <= 5*[PurchaseOnThursday] then dateadd(day,OrderCycle+5-datepart("dw",dateadd(day,OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,OrderCycle,getdate())) <= 6*[PurchaseOnFriday] then dateadd(day,OrderCycle+6-datepart("dw",dateadd(day,OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,OrderCycle,getdate())) <= 7*[PurchaseOnSaturday] then dateadd(day,OrderCycle+7-datepart("dw",dateadd(day,OrderCycle,getdate())) ,@Today)
	WHEN [PurchaseOnMonday] =1 then dateadd(day,OrderCycle+9-datepart("dw",dateadd(day,OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnTuesday] =1 then dateadd(day,OrderCycle+10-datepart("dw",dateadd(day,OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnWednesday] =1 then dateadd(day,OrderCycle+11-datepart("dw",dateadd(day,OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnThursday] =1 then dateadd(day,OrderCycle+12-datepart("dw",dateadd(day,OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnFriday] =1 then dateadd(day,OrderCycle+13-datepart("dw",dateadd(day,OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnSaturday] =1 then dateadd(day,OrderCycle+14-datepart("dw",dateadd(day,OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnSunday] =1 then dateadd(day,OrderCycle+15-datepart("dw",dateadd(day,OrderCycle,getdate())),@Today)
	End NextPurchase2,
	CASE 
	WHEN datepart("dw",getdate()) <= 2*[PurchaseOnMonday] then dateadd(day,2-datepart("dw",getdate()) ,@Today)
	WHEN datepart("dw",getdate()) <= 3*[PurchaseOnTuesday] then dateadd(day,3-datepart("dw",getdate()) ,@Today)
	WHEN datepart("dw",getdate()) <= 4*[PurchaseOnWednesday] then dateadd(day,4-datepart("dw",getdate()) ,@Today)
	WHEN datepart("dw",getdate()) <= 5*[PurchaseOnThursday] then dateadd(day,5-datepart("dw",getdate()) ,@Today)
	WHEN datepart("dw",getdate()) <= 6*[PurchaseOnFriday] then dateadd(day,6-datepart("dw",getdate()) ,@Today)
	WHEN datepart("dw",getdate()) <= 7*[PurchaseOnSaturday] then dateadd(day,7-datepart("dw",getdate()) ,@Today)
	WHEN [PurchaseOnMonday] =1 then dateadd(day,9-datepart("dw",getdate()) ,@Today)
	WHEN [PurchaseOnTuesday] = 1 then dateadd(day,10-datepart("dw",getdate()) ,@Today)
	WHEN [PurchaseOnWednesday] =1 then dateadd(day,11-datepart("dw",getdate()) ,@Today)
	WHEN [PurchaseOnThursday] =1 then dateadd(day,12-datepart("dw",getdate()) ,@Today)
	WHEN [PurchaseOnFriday] =1 then dateadd(day,13-datepart("dw",getdate()) ,@Today)
	WHEN [PurchaseOnSaturday] =1 then dateadd(day,14-datepart("dw",getdate()) ,@Today)
	WHEN [PurchaseOnSunday] =1 then dateadd(day,15-datepart("dw",getdate()) ,@Today)
	end NextPurchase1,
	CASE 
	WHEN datepart("dw",dateadd(day,-OrderCycle,getdate())) <= 2*[PurchaseOnMonday] then dateadd(day,-OrderCycle+2-datepart("dw",dateadd(day,-OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,-OrderCycle,getdate())) <= 3*[PurchaseOnTuesday] then dateadd(day,-OrderCycle+3-datepart("dw",dateadd(day,-OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,-OrderCycle,getdate())) <= 4*[PurchaseOnWednesday] then dateadd(day,-OrderCycle+4-datepart("dw",dateadd(day,-OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,-OrderCycle,getdate())) <= 5*[PurchaseOnThursday] then dateadd(day,-OrderCycle+5-datepart("dw",dateadd(day,-OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,-OrderCycle,getdate())) <= 6*[PurchaseOnFriday] then dateadd(day,-OrderCycle+6-datepart("dw",dateadd(day,-OrderCycle,getdate())) ,@Today)
	WHEN datepart("dw",dateadd(day,-OrderCycle,getdate())) <= 7*[PurchaseOnSaturday] then dateadd(day,-OrderCycle+7-datepart("dw",dateadd(day,-OrderCycle,getdate())) ,@Today)
	WHEN [PurchaseOnMonday] =1 then dateadd(day,-OrderCycle+9-datepart("dw",dateadd(day,-OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnTuesday] =1 then dateadd(day,-OrderCycle+10-datepart("dw",dateadd(day,-OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnWednesday] =1 then dateadd(day,-OrderCycle+11-datepart("dw",dateadd(day,-OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnThursday] =1 then dateadd(day,-OrderCycle+12-datepart("dw",dateadd(day,-OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnFriday] =1 then dateadd(day,-OrderCycle+13-datepart("dw",dateadd(day,-OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnSaturday] =1 then dateadd(day,-OrderCycle+14-datepart("dw",dateadd(day,-OrderCycle,getdate())),@Today)
	WHEN [PurchaseOnSunday] =1 then dateadd(day,-OrderCycle+15-datepart("dw",dateadd(day,-OrderCycle,getdate())),@Today)
	End LastPurchase
	INTO #NextPurchaseTemp
  FROM [products].[Groups] G
  where (id=@group or @group is null)


  Select Id,LeadTime,ReceptionLT,PurchaseDate,Case when DATEDIFF(day,LastPurchase,PurchaseDate) < DATEDIFF(day,PurchaseDate,NextPurchase1) THEN NextPurchase1 ELSE NextPurchase2 END NextPurchase
  INTO #NextPurchase
  from #NextPurchaseTemp
  


if(@group is null)
	begin
  TRUNCATE TABLE [setup].[SuggestParameters]
    end
  
else
	begin 
	 
	 delete s from [setup].[SuggestParameters] s
	  inner join (
		  SELECT p.SKU--,p.ServiceLevel,DATEDIFF(day,G.PurchaseDate,G.NextPurchase)+LeadTime + ReceptionLT DaysToCover,g.PurchaseDate,g.NextPurchase,g.LeadTime +ReceptionLT LeadTime
		  FROM products.products p
		  INNER JOIN #NextPurchase G on G.Id=p.GroupId
	  ) b on b.SKU=s.SKU

	end 

  INSERT INTO [setup].[SuggestParameters]
  SELECT p.SKU,p.ServiceLevel,DATEDIFF(day,G.PurchaseDate,G.NextPurchase)+LeadTime + ReceptionLT DaysToCover,g.PurchaseDate,g.NextPurchase,g.LeadTime +ReceptionLT LeadTime,null
  		  FROM products.products p
		  INNER JOIN #NextPurchase G on G.Id=p.GroupId
  where DATEDIFF(day,G.PurchaseDate,G.NextPurchase)+LeadTime is not null

  -- compras por solicitud
	update a set DaysToCover=60+LeadTime,NextPurchaseDate=dateadd(day,60,PurchaseDate)--select *
	from [setup].[SuggestParameters] a
	inner join products.Products pp on pp.SKU=a.SKU
	where pp.MotiveId in (10,6)

END
