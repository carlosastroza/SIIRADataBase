﻿-- =============================================
-- Author:		Gabriel Espinoza
-- Create date: 2018-05-31
-- Description:	Obtiene el diccionario de configuraciones de SalcobrandAbastecimiento
-- =============================================
CREATE PROCEDURE setup.GetConfigurationDictionary
	@company CHAR(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Key],[Value] 
	FROM [setup].[ConfigurationDictionary]
	Where Company = @company
END
