﻿-- =============================================
-- Author:		Carlos Astroza San Martín
-- Create date: 2018-06-21
-- Description:	Obtiene los compradores
-- =============================================
CREATE PROCEDURE [setup].[GetBuyers]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 9999 Id, 'Desasignar' Description
	UNION ALL
	SELECT BuyerId Id, Username Description FROM users.UserRules 

END
