﻿CREATE TABLE [setup].[DistributorIsapre] (
    [IdZona]          INT            NOT NULL,
    [Isapre]          NVARCHAR (50)  NULL,
    [DistributorRUT]  NVARCHAR (15)  NULL,
    [DistributorName] NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    CONSTRAINT [PK_Isapre] PRIMARY KEY CLUSTERED ([IdZona] ASC)
);

