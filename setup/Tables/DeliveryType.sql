﻿CREATE TABLE [setup].[DeliveryType] (
    [Id]          INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Description] VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CONSTRAINT [PK_TipoDespacho] PRIMARY KEY CLUSTERED ([Id] ASC)
);

