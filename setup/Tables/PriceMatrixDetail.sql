﻿CREATE TABLE [setup].[PriceMatrixDetail] (
    [Organization]           INT           NULL,
    [ManufacturerId]         INT           NOT NULL,
    [MatrixId]               INT           NULL,
    [SKU]                    INT           NOT NULL,
    [IsConsigned]            NVARCHAR (50) NULL,
    [ListPrice]              FLOAT (53)    NULL,
    [DiscountFact1]          FLOAT (53)    NULL,
    [DiscountFact2]          FLOAT (53)    NULL,
    [DiscountFact3]          FLOAT (53)    NULL,
    [DiscountFact4]          NVARCHAR (50) NULL,
    [DiscountFact5]          FLOAT (53)    NULL,
    [DiscountFact6]          FLOAT (53)    NULL,
    [DiscountFact7]          FLOAT (53)    NULL,
    [DiscountFact8]          NVARCHAR (50) NULL,
    [DiscountNC1]            FLOAT (53)    NULL,
    [DiscountNC2]            FLOAT (53)    NULL,
    [DiscountNC3]            FLOAT (53)    NULL,
    [DiscountNC4]            NVARCHAR (50) NULL,
    [DiscountFactMarketing1] FLOAT (53)    NULL,
    [DiscountFactMarketing2] FLOAT (53)    NULL,
    [DiscountFactMarketing3] FLOAT (53)    NULL,
    [DiscountFactMarketing4] FLOAT (53)    NULL,
    [DiscountProntoPago]     FLOAT (53)    NULL,
    [IdZona]                 INT           NOT NULL,
    [FinalPrice]             AS            ((((((((((((((((([ListPrice]*((1)-[DiscountFact1]/(100.0)))*((1)-[DiscountFact2]/(100.0)))*((1)-[DiscountFact3]/(100.0)))*((1)-[DiscountFact4]/(100.0)))*((1)-[DiscountFact5]/(100.0)))*((1)-[DiscountFact6]/(100.0)))*((1)-[DiscountFact7]/(100.0)))*((1)-[DiscountFact8]/(100.0)))*((1)-[DiscountNC1]/(100.0)))*((1)-[DiscountNC2]/(100.0)))*((1)-[DiscountNC3]/(100.0)))*((1)-[DiscountNC4]/(100.0)))*((1)-[DiscountFactMarketing1]/(100.0)))*((1)-[DiscountFactMarketing2]/(100.0)))*((1)-[DiscountFactMarketing3]/(100.0)))*((1)-[DiscountFactMarketing4]/(100.0)))*((1)-[DiscountProntoPago]/(100.0))),
    CONSTRAINT [PK_PriceMatrixDetail] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [SKU] ASC, [IdZona] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PriceMatrixDetail_IdZona]
    ON [setup].[PriceMatrixDetail]([IdZona] ASC)
    INCLUDE([ManufacturerId], [SKU], [FinalPrice]);

