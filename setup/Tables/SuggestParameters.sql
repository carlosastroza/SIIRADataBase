﻿CREATE TABLE [setup].[SuggestParameters] (
    [SKU]              INT        NOT NULL,
    [ServiceLevel]     FLOAT (53) NOT NULL,
    [DaysToCover]      INT        NOT NULL,
    [PurchaseDate]     DATE       NOT NULL,
    [NextPurchaseDate] DATE       NOT NULL,
    [LeadTime]         INT        NOT NULL,
    [FillRate]         FLOAT (53) NULL,
    CONSTRAINT [PK_SuggestParameters] PRIMARY KEY CLUSTERED ([SKU] ASC),
    CONSTRAINT [FK_SuggestParameters_Products] FOREIGN KEY ([SKU]) REFERENCES [products].[Products] ([SKU]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nivel de servicio objetivo para este SKU', @level0type = N'SCHEMA', @level0name = N'setup', @level1type = N'TABLE', @level1name = N'SuggestParameters', @level2type = N'COLUMN', @level2name = N'ServiceLevel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Días que debe cubrir el sugerido. Incluye frecuencia de compra y leadtimes', @level0type = N'SCHEMA', @level0name = N'setup', @level1type = N'TABLE', @level1name = N'SuggestParameters', @level2type = N'COLUMN', @level2name = N'DaysToCover';

