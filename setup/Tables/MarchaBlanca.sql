﻿CREATE TABLE [setup].[MarchaBlanca] (
    [SKU]       INT  NOT NULL,
    [EntryDate] DATE NOT NULL,
    CONSTRAINT [PK_MarchaBlanca_1] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

