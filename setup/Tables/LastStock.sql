﻿CREATE TABLE [setup].[LastStock] (
    [SKU]          INT        NOT NULL,
    [StockCD]      INT        NULL,
    [ActiveOrders] FLOAT (53) NOT NULL,
    CONSTRAINT [PK_LastStock] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

