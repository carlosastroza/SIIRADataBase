﻿CREATE TABLE [setup].[LogisticParameters] (
    [SKU]              INT        NOT NULL,
    [UnitsInBox]       INT        NOT NULL,
    [UnitsInLayer]     INT        NOT NULL,
    [UnitsInPallet]    INT        NOT NULL,
    [RoundToBoxes]     BIT        NOT NULL,
    [RoundToLayers]    BIT        NOT NULL,
    [RoundToPallets]   BIT        NOT NULL,
    [ConveniencePack]  INT        NULL,
    [ConveniencePack%] FLOAT (53) NULL,
    [RoundThreshold]   FLOAT (53) NULL,
    [BuyingMultiple]   INT        NULL,
    CONSTRAINT [PK_LogisticParameters] PRIMARY KEY CLUSTERED ([SKU] ASC),
    CONSTRAINT [FK_LogisticParameters_Products] FOREIGN KEY ([SKU]) REFERENCES [products].[Products] ([SKU]) ON DELETE CASCADE ON UPDATE CASCADE
);

