﻿CREATE TABLE [setup].[MarchaBlancaNO] (
    [SKU]       INT  NOT NULL,
    [EntryDate] DATE NOT NULL,
    CONSTRAINT [PK_MarchaBlanca] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

