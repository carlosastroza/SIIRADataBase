﻿CREATE TABLE [setup].[PriceAndCostGES] (
    [SKU]    INT        NOT NULL,
    [Price]  FLOAT (53) NOT NULL,
    [Cost]   FLOAT (53) NOT NULL,
    [IdZona] INT        NOT NULL,
    CONSTRAINT [PK_PriceAndCostGES] PRIMARY KEY CLUSTERED ([SKU] ASC, [IdZona] ASC)
);

