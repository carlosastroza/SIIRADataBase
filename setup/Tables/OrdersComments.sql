﻿CREATE TABLE [setup].[OrdersComments] (
    [Id]         INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Comment]    NVARCHAR (40)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [ModifiedBy] NVARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [ModifiedOn] DATE           NOT NULL,
    CONSTRAINT [PK_OrdersComments] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Glosas estándar de las OOCC', @level0type = N'SCHEMA', @level0name = N'setup', @level1type = N'TABLE', @level1name = N'OrdersComments';

