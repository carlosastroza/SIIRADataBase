﻿CREATE TABLE [setup].[PriceAndCost] (
    [SKU]   INT        NOT NULL,
    [Price] FLOAT (53) NOT NULL,
    [Cost]  FLOAT (53) NOT NULL,
    CONSTRAINT [PK_PriceAndCost] PRIMARY KEY CLUSTERED ([SKU] ASC),
    CONSTRAINT [FK_PriceAndCost_Products] FOREIGN KEY ([SKU]) REFERENCES [products].[Products] ([SKU]) ON DELETE CASCADE ON UPDATE CASCADE
);

