﻿CREATE TABLE [setup].[ConfigurationDictionary] (
    [Company] CHAR (2)       NOT NULL,
    [Key]     NVARCHAR (50)  NOT NULL,
    [Value]   NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConfigurationDictionary_1] PRIMARY KEY CLUSTERED ([Company] ASC, [Key] ASC)
);

