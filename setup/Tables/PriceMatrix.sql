﻿CREATE TABLE [setup].[PriceMatrix] (
    [Organization]             INT           NOT NULL,
    [ManufacturerId]           INT           NOT NULL,
    [MatrixId]                 INT           NULL,
    [MatrixName]               NVARCHAR (50) NULL,
    [CreatedDate]              DATE          NULL,
    [CreatedBy]                NVARCHAR (50) NULL,
    [UpdatedDate]              DATE          NULL,
    [UpdatedBy]                NVARCHAR (50) NULL,
    [Currency]                 NVARCHAR (50) NULL,
    [ExchangeType]             NVARCHAR (50) NULL,
    [ComprasAMostrarExcepcion] NVARCHAR (50) NULL,
    [IdZona]                   NVARCHAR (50) NOT NULL,
    [StartingDate]             DATE          NULL,
    [PosIndex]                 INT           NOT NULL,
    CONSTRAINT [PK_PriceMatrix] PRIMARY KEY CLUSTERED ([Organization] ASC, [ManufacturerId] ASC, [IdZona] ASC)
);

