﻿CREATE TABLE [products].[DefaultManufacturerBuyer] (
    [ManufacturerId] INT             NOT NULL,
    [Username]       NVARCHAR (4000) NULL,
    CONSTRAINT [PK_DefaultManufacturerBuyer] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC)
);

