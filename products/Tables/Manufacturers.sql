﻿CREATE TABLE [products].[Manufacturers] (
    [Id]                 INT            NOT NULL,
    [Name]               NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [RUT]                NVARCHAR (15)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [FillRateMonths]     INT            NOT NULL,
    [OpenByManufacturer] BIT            CONSTRAINT [DF_Manufacturers_OpenByManufacturer] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Manufacturers] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Meses que se deben usar para el cálculo de fillrate de este proveedor
', @level0type = N'SCHEMA', @level0name = N'products', @level1type = N'TABLE', @level1name = N'Manufacturers', @level2type = N'COLUMN', @level2name = N'FillRateMonths';

