﻿CREATE TABLE [products].[PromotionInfo] (
    [SKU]               INT NOT NULL,
    [IsInPromotion]     BIT NOT NULL,
    [WillBeInPromotion] BIT NOT NULL,
    CONSTRAINT [PK_PromotionInfo] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

