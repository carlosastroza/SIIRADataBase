﻿CREATE TABLE [products].[DistributorRotation] (
    [SKU]               INT            NOT NULL,
    [DistributorRUT]    NVARCHAR (15)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [IdZona]            INT            NOT NULL,
    [LastMonthSalesGES] INT            NOT NULL,
    [Month]             DATE           NOT NULL,
    [ModifiedBy]        NVARCHAR (255) NOT NULL,
    [ModifiedOn]        DATE           NOT NULL,
    CONSTRAINT [PK_DistributorRotation_1] PRIMARY KEY CLUSTERED ([SKU] ASC, [DistributorRUT] ASC, [IdZona] ASC, [Month] ASC),
    CONSTRAINT [FK_DistributorRotation_Distributors] FOREIGN KEY ([DistributorRUT]) REFERENCES [products].[Distributors] ([RUT]),
    CONSTRAINT [FK_DistributorRotation_Products] FOREIGN KEY ([SKU]) REFERENCES [products].[Products] ([SKU])
);

