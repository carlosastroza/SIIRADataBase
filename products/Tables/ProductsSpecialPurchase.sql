﻿CREATE TABLE [products].[ProductsSpecialPurchase] (
    [SKU]             INT            NOT NULL,
    [Name]            NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Category]        NVARCHAR (100) NULL,
    [IsGes]           BIT            NOT NULL,
    [IsGeneric]       BIT            NOT NULL,
    [GenericFamilyId] INT            NULL,
    [ManufacturerId]  INT            NULL,
    [LogisticAreaId]  NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [TotalRanking]    VARCHAR (1)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ServiceLevel]    FLOAT (53)     NULL,
    [MotiveId]        INT            NULL,
    [Motive]          VARCHAR (50)   NULL,
    [Price]           FLOAT (53)     NULL,
    [Cost]            FLOAT (53)     NULL,
    [LastMonthSale]   INT            NULL,
    [StockCD]         INT            NULL,
    [ActiveOrders]    INT            NULL,
    [StoresShortage]  INT            NULL,
    CONSTRAINT [PK_ProductsSpecialPurchase] PRIMARY KEY CLUSTERED ([SKU] ASC)
);

