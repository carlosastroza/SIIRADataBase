﻿CREATE TABLE [products].[Vademecum] (
    [SKU]            INT            NOT NULL,
    [DistributorRUT] NVARCHAR (15)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [IdZona]         NCHAR (10)     NOT NULL,
    [ModifiedBy]     NVARCHAR (255) NOT NULL,
    [ModifiedOn]     DATE           NOT NULL,
    CONSTRAINT [PK_Vademecum_1] PRIMARY KEY CLUSTERED ([SKU] ASC, [DistributorRUT] ASC, [IdZona] ASC),
    CONSTRAINT [FK_Vademecum_Distributors] FOREIGN KEY ([DistributorRUT]) REFERENCES [products].[Distributors] ([RUT]),
    CONSTRAINT [FK_Vademecum_Products] FOREIGN KEY ([SKU]) REFERENCES [products].[Products] ([SKU]) ON DELETE CASCADE ON UPDATE CASCADE
);

