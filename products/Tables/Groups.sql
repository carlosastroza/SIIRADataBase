﻿CREATE TABLE [products].[Groups] (
    [Id]                  INT            IDENTITY (4, 1) NOT FOR REPLICATION NOT NULL,
    [SpecialGroup]        NVARCHAR (5)   NULL,
    [RUT]                 NVARCHAR (15)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ManufacturerId]      INT            NULL,
    [LogisticAreaId]      NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Username]            NVARCHAR (50)  CONSTRAINT [DF_Groups_OpenByUser] DEFAULT ((0)) NULL,
    [Name]                VARCHAR (255)  COLLATE Latin1_General_CI_AS NULL,
    [FantasyName]         NVARCHAR (255) NULL,
    [OrderCycle]          INT            NOT NULL,
    [LeadTime]            TINYINT        NOT NULL,
    [LTMonday]            TINYINT        CONSTRAINT [DF__Groups__LTMonday__546180BB] DEFAULT ((0)) NULL,
    [LTTuesday]           TINYINT        CONSTRAINT [DF__Groups__LTTuesda__5555A4F4] DEFAULT ((0)) NULL,
    [LTWednesday]         TINYINT        CONSTRAINT [DF__Groups__LTWednes__5649C92D] DEFAULT ((0)) NULL,
    [LTThursday]          TINYINT        CONSTRAINT [DF__Groups__LTThursd__573DED66] DEFAULT ((0)) NULL,
    [LTFriday]            TINYINT        CONSTRAINT [DF__Groups__LTFriday__5832119F] DEFAULT ((0)) NULL,
    [LTSaturday]          TINYINT        CONSTRAINT [DF__Groups__LTSaturd__592635D8] DEFAULT ((0)) NULL,
    [LTSunday]            TINYINT        CONSTRAINT [DF__Groups__LTSunday__5A1A5A11] DEFAULT ((0)) NULL,
    [PurchaseOnMonday]    BIT            CONSTRAINT [DF_Groups_PurchaseOnMonday] DEFAULT ((0)) NOT NULL,
    [PurchaseOnTuesday]   BIT            CONSTRAINT [DF_Groups_PurchaseOnTuesday] DEFAULT ((0)) NOT NULL,
    [PurchaseOnWednesday] BIT            CONSTRAINT [DF_Groups_PurchaseOnWednesday] DEFAULT ((0)) NOT NULL,
    [PurchaseOnThursday]  BIT            CONSTRAINT [DF_Groups_PurchaseOnThursday] DEFAULT ((0)) NOT NULL,
    [PurchaseOnFriday]    BIT            CONSTRAINT [DF_Groups_PurchaseOnFriday] DEFAULT ((0)) NOT NULL,
    [PurchaseOnSaturday]  BIT            CONSTRAINT [DF_Groups_PurchaseOnSaturday] DEFAULT ((0)) NOT NULL,
    [PurchaseOnSunday]    BIT            CONSTRAINT [DF_Groups_PurchaseOnSunday] DEFAULT ((0)) NOT NULL,
    [ModifiedBy]          NVARCHAR (50)  NULL,
    [ModifiedOn]          DATETIME       NULL,
    [CustomGroup]         BIT            CONSTRAINT [DF_Groups_CustomGroup] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Establece la frecuencia de compra de este grupo de productos', @level0type = N'SCHEMA', @level0name = N'products', @level1type = N'TABLE', @level1name = N'Groups', @level2type = N'COLUMN', @level2name = N'OrderCycle';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LeadTime con el que se recepciona la compra', @level0type = N'SCHEMA', @level0name = N'products', @level1type = N'TABLE', @level1name = N'Groups', @level2type = N'COLUMN', @level2name = N'LeadTime';

