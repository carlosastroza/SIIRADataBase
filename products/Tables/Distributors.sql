﻿CREATE TABLE [products].[Distributors] (
    [RUT]         NVARCHAR (15)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Name]        NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Description] NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    CONSTRAINT [PK_Distributors] PRIMARY KEY CLUSTERED ([RUT] ASC)
);

