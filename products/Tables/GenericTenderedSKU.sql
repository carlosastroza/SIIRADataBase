﻿CREATE TABLE [products].[GenericTenderedSKU] (
    [GenericFamilyId] INT            NULL,
    [SKU]             INT            NULL,
    [ModifiedBy]      NVARCHAR (MAX) NULL,
    [ModifiedOn]      DATETIME       NULL
);

