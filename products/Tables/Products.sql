﻿CREATE TABLE [products].[Products] (
    [SKU]             INT            NOT NULL,
    [Name]            NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Category]        NVARCHAR (100) NULL,
    [IsGes]           BIT            NOT NULL,
    [IsGeneric]       BIT            NOT NULL,
    [GenericFamilyId] INT            NULL,
    [ManufacturerId]  INT            NULL,
    [LogisticAreaId]  NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [TotalRanking]    VARCHAR (1)    COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [GroupId]         INT            NULL,
    [IsGroupLocked]   BIT            CONSTRAINT [DF_Products_IsGroupLocked] DEFAULT ((0)) NOT NULL,
    [ServiceLevel]    FLOAT (53)     NULL,
    [MotiveId]        INT            NULL,
    [Motive]          VARCHAR (50)   NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([SKU] ASC),
    CONSTRAINT [FK_Products_Groups] FOREIGN KEY ([GroupId]) REFERENCES [products].[Groups] ([Id]),
    CONSTRAINT [FK_Products_Manufacturers] FOREIGN KEY ([ManufacturerId]) REFERENCES [products].[Manufacturers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_products_GenericFamilyId]
    ON [products].[Products]([GenericFamilyId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contiene información de productos necesaria
', @level0type = N'SCHEMA', @level0name = N'products', @level1type = N'TABLE', @level1name = N'Products';

