﻿CREATE TABLE [products].[ItemConfiguration] (
    [BODEGA]        VARCHAR (50) NOT NULL,
    [UnitsInLayer]  INT          NOT NULL,
    [UnitsInPallet] INT          NOT NULL,
    CONSTRAINT [PK_ItemConfiguration] PRIMARY KEY CLUSTERED ([BODEGA] ASC)
);

