﻿CREATE TABLE [products].[VendorConfiguration] (
    [RUT]                     INT NOT NULL,
    [PalletsByTruck]          INT NULL,
    [DayForEstimatedPurchase] INT NULL,
    CONSTRAINT [PK_VendorConfiguration] PRIMARY KEY CLUSTERED ([RUT] ASC)
);

