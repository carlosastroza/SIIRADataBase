﻿-- =============================================
-- Author:		Plan Z + Dani 
-- Create date: 2018-04-04
-- Description:	proceso diario completo para generar compras

-- Update:	Gabriel Espinoza
-- Date:	2018-07-30
-- Desc:	Se agrega paso 10 para calcular los días de inventario (a pedido de Esteban Cea)
--			Se modifica nivel de log de 'Tracer' a 'Error'
-- =============================================
CREATE PROCEDURE [operation].[GeneratePurchaseFull] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @msg VARCHAR(100) 


	declare @date DATE = GETDATE();

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de proceso diario completo para generar órdenes de compra, SP [operation].[GeneratePurchaseFull]', host_name(), 'System', 'POST')

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 1 LoadLastStock' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec operation.LoadLastStock @date;-- 0
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 1 (LoadLastStock) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH


	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 2 LoadLogisticParameters' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec operation.LoadLogisticParameters
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 2 (LoadLogisticParameters) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH


	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 3 LoadPriceAndCost' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[LoadPriceAndCost]
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 3 (LoadPriceAndCost) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH


	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 4 FillPurchaseCalendar' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [setup].[FillPurchaseCalendar] @date,null--0

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 4 (FillPurchaseCalendar) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH


	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 5 CalculateFillRate' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[CalculateFillRate] null, null
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 5 (CalculateFillRate) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		---- para pedidos
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 6 GenerateSuggestCD' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[GenerateSuggestCD] null,null,null -- 10 seg

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 6 (GenerateSuggestCD) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 7 Calcular reporte InventoryDays' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		EXEC operation.CalculateInventoryDays 
	END TRY

	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 7 (Calcular reporte InventoryDays) , SP [operation].[CalculateInventoryDays], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH


	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 8 GeneratePurchaseSuggested' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[GeneratePurchaseSuggested] null,null,null,null,null -- 0

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 8 (GeneratePurchaseSuggested) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH


	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 9 LoadPurchaseSummary' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		EXEC [operation].[LoadPurchaseSummary] -- 1 Min
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 9 (LoadPurchaseSummary) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 10 SkuUserPermission' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		EXEC [operation].SkuUserPermission -- 1 Min
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 10 (SkuUserPermission) , SP [operation].[SkuUserPermission], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
		
	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 11 LoadPriceAndCostGES' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[LoadPriceAndCostGes]
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 11 (LoadPriceAndCostGES) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 12 CreateAutomaticGroups' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

		EXEC [operation].[CreateAutomaticGroups]
		EXEC [operation].[UpdateProductGroups]


	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 12 (CreateAutomaticGroups) , SP [operation].[CreateAutomaticGroups], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY

		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 13 [AuxStockOutReports]' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 

		exec reports.UpdateStockOutReports

		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de paso 13 AuxStockOutReports', host_name(), 'System', 'POST')

	

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 12 (CreateAutomaticGroups) , SP [operation].[CreateAutomaticGroups], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH




	EXEC [operation].[GenerateReportsPurchaseFull]  
	
END
