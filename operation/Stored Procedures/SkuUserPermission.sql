﻿-- =============================================
-- Author:	    Carlos Astroza
-- Create date: 2018-04-23
-- Description:	Genera permisos de sku-usuario
-- =============================================
CREATE PROCEDURE [operation].[SkuUserPermission]
	
AS
BEGIN

truncate table users.skuusers

/*1. Llenar Tabla */
SELECT p.SKU, p.IsGeneric, IsGes, p.ManufacturerId, fp.CategoriaUnificadaId, fp.SubcategoriaUnificadaId , fp.divisionUnificadaId
into #data
FROM products.Products p
INNER JOIN Salcobrand.products.FullProducts fp on p.sku = fp.sku
/* 1. End */


/* --- FARMA ---- */

/*Genéricos*/
INSERT INTO users.SkuUsers
select temp.sku, ur.username 
FROM #data temp
LEFT JOIN products.Products p on temp.sku = p.SKU 
LEFT JOIN users.UserRules ur on p.IsGeneric = ur.generic
WHERE p.IsGeneric = 1
AND p.SKU NOT IN (SELECT SKU FROM products.Vademecum);
DELETE  FROM #data where IsGeneric = 1 
AND SKU NOT IN (SELECT SKU FROM products.Vademecum);
/* --- End --- */

/*Ges*/
INSERT INTO users.SkuUsers
select temp.sku, ur.username 
FROM #data temp
INNER JOIN products.Products p on temp.sku = p.SKU 
INNER JOIN users.UserRules ur on p.IsGes = ur.Ges
WHERE p.isges = 1
AND temp.SKU IN (SELECT SKU FROM products.Vademecum);
DELETE  FROM #data where IsGes = 1
AND SKU IN (SELECT SKU FROM products.Vademecum);
/* --- End --- */

/* VMI */
INSERT INTO users.SkuUsers
SELECT ts.SKU, ur.Username
FROM #data ts
INNER JOIN users.UserRules ur on ts.ManufacturerId IN  (SELECT * FROM Salcobrand.dbo.SplitString(ur.VMI))
AND ts.DivisionUnificadaId = 1
DELETE d FROM #data d
INNER JOIN users.UserRules ur on d.ManufacturerId IN (SELECT * FROM Salcobrand.dbo.SplitString(ur.VMI))
WHERE d.DivisionUnificadaId = 1
/* --- END VMI --- */

/* SUPPLY CHAIN */
INSERT INTO users.SkuUsers
SELECT ts.SKU, ur.Username
FROM #data ts
INNER JOIN users.UserRules ur on ts.ManufacturerId IN  (SELECT * FROM Salcobrand.dbo.SplitString(ur.SupplyChain))
AND ts.DivisionUnificadaId = 1
DELETE d FROM #data d
INNER JOIN users.UserRules ur on d.ManufacturerId IN (SELECT * FROM Salcobrand.dbo.SplitString(ur.SupplyChain))
WHERE d.DivisionUnificadaId = 1
/* END SUPPLY CHAIN */

/* Guardo en la tabla Auxiliar los SKU que no fueron asignados y luego los elimino.*/
SELECT *
INTO #tablaAuxiliar
FROM #data
WHERE DivisionUnificadaId = 1
DELETE FROM #data WHERE DivisionUnificadaId = 1
/* --- END --- */

/* --- END FARMA ---- */

/*--------------------------------------------------*/

/* --- CONSUMO MASIVO ---- */

/* VMI */
INSERT INTO users.SkuUsers
SELECT ts.SKU, ur.Username
FROM #data ts
INNER JOIN users.UserRules ur on ts.ManufacturerId IN  (SELECT * FROM Salcobrand.dbo.SplitString(ur.VMI))
DELETE d FROM #data d
INNER JOIN users.UserRules ur on d.ManufacturerId IN (SELECT * FROM Salcobrand.dbo.SplitString(ur.VMI))
/* --- END VMI --- */

/* DERMOCOSMÉTICA */
INSERT INTO users.SkuUsers
SELECT ts.SKU, ur.Username
FROM #data ts
INNER JOIN users.UserRules ur on ts.SubcategoriaUnificadaId IN  (SELECT * FROM Salcobrand.dbo.SplitString(ur.SubCategory))
DELETE d FROM #data d
INNER JOIN users.UserRules ur on d.SubcategoriaUnificadaId IN (SELECT * FROM Salcobrand.dbo.SplitString(ur.SubCategory))
/* --- END DERMOCOSMÉTICA --- */

/* CATEGORY */
INSERT INTO users.SkuUsers
SELECT ts.SKU, ur.Username
FROM #data ts
INNER JOIN users.UserRules ur on ts.CategoriaUnificadaId IN  (SELECT * FROM Salcobrand.dbo.SplitString(ur.Category))
DELETE d FROM #data d
INNER JOIN users.UserRules ur on d.CategoriaUnificadaId IN (SELECT * FROM Salcobrand.dbo.SplitString(ur.Category))
/* --- END DERMOCOSMÉTICA --- */

/* En caso que quedara algo de Consumo Masivo se respalda en la tabla Auxiliar */
INSERT INTO #tablaAuxiliar
SELECT * FROM #data
/* --- END CONSUMO MASIVO ---- */

/* Asignar Comprador de acuerdo a quién compra más el laboratorio */
INSERT INTO users.SkuUsers
SELECT t.SKU,A.Username
FROM #tablaAuxiliar t 
INNER JOIN (
SELECT p.ManufacturerId, a.Username 
FROM products.Products p
	INNER JOIN(
		SELECT p.ManufacturerId, su.Username, COUNT(*) CantidadSKU, RANK() OVER (PARTITION BY p.ManufacturerId ORDER BY p.ManufacturerId, COUNT(*) DESC) AS Ranking  
		FROM products.Products p
		INNER JOIN users.SkuUsers su ON p.SKU = su.SKU
		Group By p.ManufacturerId, su.Username) a ON p.ManufacturerId = a.ManufacturerId
WHERE a.Ranking = 1
GROUP BY p.ManufacturerId, a.Username ) A on t.ManufacturerId = A.ManufacturerId
INNER JOIN products.Products p ON t.SKU = p.SKU
WHERE t.ManufacturerId NOT IN (SELECT ManufacturerId FROM [products].[DefaultManufacturerBuyer])

DELETE t FROM #tablaAuxiliar t 
INNER JOIN products.Products p ON t.ManufacturerId = p.ManufacturerId
INNER JOIN users.SkuUsers su   ON p.SKU = su.SKU
WHERE t.ManufacturerId NOT IN (SELECT ManufacturerId FROM [products].[DefaultManufacturerBuyer])
/* End */

/* Distribuir por comprador por defecto del laboratorio */
INSERT INTO users.SkuUsers
SELECT t.SKU, d.Username FROM #tablaAuxiliar t
INNER JOIN products.DefaultManufacturerBuyer d ON t.ManufacturerId = d.ManufacturerId
DELETE t FROM #tablaAuxiliar t
INNER JOIN products.DefaultManufacturerBuyer d ON t.ManufacturerId = d.ManufacturerId


/* Distribuye finalmente por los usuarios Comodines */
DECLARE @ComodinFarma   NVARCHAR(50) = (SELECT Username FROM users.UserRules WHERE WildcardFarma   = 1);
DECLARE @ComodinConsumo NVARCHAR(50) = (SELECT Username FROM users.UserRules WHERE WildcardConsumo = 1);

INSERT INTO users.SkuUsers
SELECT SKU, @ComodinFarma FROM #tablaAuxiliar WHERE DivisionUnificadaId   = 1
DELETE FROM #tablaAuxiliar WHERE DivisionUnificadaId = 1

INSERT INTO users.SkuUsers
SELECT SKU, @ComodinConsumo FROM #tablaAuxiliar WHERE DivisionUnificadaId = 2
DELETE FROM #tablaAuxiliar WHERE DivisionUnificadaId = 2


/* --- END --- */

DROP TABLE #data
DROP TABLE #tablaAuxiliar
	
END



