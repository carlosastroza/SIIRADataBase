﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 2018-08-10
-- Description:	Carga los SKU para compras especiales
-- =============================================
CREATE PROCEDURE [operation].[LoadProductsSpecialPurchase]
AS
BEGIN
	declare @date        date = getdate()
	declare @date_monday date = Salcobrand.dbo.GetMonday(@date);
	
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga SKU, SP [operation].[LoadProductsSpecialPurchase]', host_name(), 'System', 'POST')

	BEGIN TRY
			MERGE INTO products.ProductsSpecialPurchase d
			USING (
				SELECT
					p.SKU, p.[Name], p.IsGes, CASE WHEN fg.SKU IS NOT NULL THEN 1 ELSE 0 END IsGeneric, fg.GenericFamilyId
					, ManufacturerId, WMSZone,TotalRanking, MotiveId,Motive,CategoriaUnificadaDesc, Price, Cost, s.Quantity
				FROM (
						SELECT 
						fp.SKU, Description Name, CategoriaUnificadaDesc, Ges isGes, IsGeneric, ManufacturerId, ISNULL(pa.WMSZone,'NA')WMSZone,TotalRanking, MotiveId, Motive, Price, Cost
						FROM Salcobrand.products.FullProducts fp
						LEFT JOIN (
						select SKU, Price, Cost, ROW_NUMBER() OVER(partition by sku order by [Date] desc) R
						from Salcobrand.series.PriceAndCost 
						) a ON fp.SKU = A.SKU AND R = 1
						LEFT JOIN Salcobrand.products.PurchaseAreas pa ON fp.sku = pa.sku
						WHERE ISNULL(TotalRanking,'null') <> 'Z'
						AND MotiveId <> 2
						AND VigenteId = 999
						AND IsActive = 1
						AND isnull(fp.DivisionUnificadaId, fp.division) IN (1,2)
						UNION
						SELECT v.SKU, fp.Description Name, fp.CategoriaUnificadaDesc,fp.Ges isGes,IsGeneric, ManufacturerId, ISNULL(pa.WMSZone,'NA')WMSZone,TotalRanking, MotiveId, Motive, Price, Cost
						FROM products.Vademecum v
						INNER JOIN Salcobrand.Products.FullProducts fp ON v.SKU = fp.SKU
						LEFT JOIN Salcobrand.products.PurchaseAreas pa ON fp.sku = pa.sku
						LEFT JOIN (
						select SKU, Price, Cost, ROW_NUMBER() OVER(partition by sku order by [Date] desc) R
						from Salcobrand.series.PriceAndCost 
						) a ON fp.SKU = A.SKU AND R = 1
				) p
				LEFT join Salcobrand.products.GenericFamilyDetail fg ON p.SKU = fg.SKU
				LEFT JOIN (
					select SKU, SUM(Quantity) Quantity 
					from Salcobrand.series.DailySales
					where [Date] BETWEEN DATEADD(WEEK, -4, @date_monday) AND @date
					GROUP BY SKU
				) s ON p.SKU = s.SKU

			) o ON d.SKU = o.SKU
			WHEN MATCHED THEN 
			UPDATE SET 
				d.[Name] = o.[Name], d.IsGes = o.IsGes, d.IsGeneric = o.IsGeneric
				, d.ManufacturerId = o.ManufacturerId, d.LogisticAreaId = o.WMSZone
				, d.TotalRanking = o.TotalRanking, d.GenericFamilyId = o.GenericFamilyId, 
				d.MotiveId = o.MotiveId ,d.Motive = o.Motive, Category = CategoriaUnificadaDesc, 
				Price = o.Price, Cost = o.Cost, d.LastMonthSale = Quantity
			WHEN NOT MATCHED BY TARGET THEN
			INSERT ([SKU], [Name], [IsGes], [IsGeneric], [ManufacturerId], [LogisticAreaId], [TotalRanking], [GenericFamilyId], ServiceLevel,Category,MotiveId, Motive, Price, Cost, LastMonthSale)
			VALUES (o.SKU, o.[Name], o.IsGes, o.IsGeneric, o.ManufacturerId, o.WMSZone, o.TotalRanking, o.GenericFamilyId, cast(0.95 as float),CategoriaUnificadaDesc,MotiveId, Motive, Price, Cost,Quantity)
			WHEN NOT MATCHED BY SOURCE THEN
			DELETE;

	/* --- STOCK CD, Active Orders --- */
		
		/* Para genéricos se agrupa */
	    select b.SKU,case when isnull(x.stockcd,0)<0 then 0 else isnull(x.stockcd,0) end stockcd
		,case when isnull(x.activeorders,0)<0 then 0 else isnull(x.activeorders,0) end activeorders,g.GenericFamilyId
		into #laststockCD
		from setup.LastStock b
		inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=b.SKU
		left join (
			select g.GenericFamilyId,sum(b.StockCD) stockcd,sum(b.ActiveOrders) activeorders
			from setup.LastStock b 
			inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=b.SKU
			group by g.GenericFamilyId
		) x on x.GenericFamilyId=g.GenericFamilyId
		inner join products.Productsspecialpurchase pp on pp.SKU=b.SKU
		order by g.GenericFamilyId

		select case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end SKU
		,sum(case when ct.SKUTira is null then ls.stockcd else ls.stockcd/conversion end) stockcd
		,sum(case when ct.SKUTira is null then ls.activeorders else ls.activeorders/conversion end) activeorders,GenericFamilyId
		into #laststockCD2
		from #laststockCD ls
		left join salcobrand.products.[CajaTira] ct on ct.SKUTira=ls.SKU
		group by case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end,GenericFamilyId

		update a set a.StockCD=b.StockCD,a.ActiveOrders=b.ActiveOrders
		from [products].[ProductsSpecialPurchase] a
		inner join #laststockCD2 b on b.sku=a.sku

		/*Para no genéricos*/
		select case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end SKU
		,sum(case when isnull(ls.stockcd,0)<0 then 0 else (case when ct.SKUTira is null then isnull(ls.stockcd,0) else isnull(ls.stockcd,0)/conversion end) end) stockcd
		,sum(case when isnull(ls.activeorders,0)<0 then 0 else (case when ct.SKUTira is null then isnull(ls.activeorders,0) else isnull(ls.activeorders,0)/conversion end) end) activeorders
		into #laststockCD3
		from setup.LastStock ls
		left join salcobrand.products.[CajaTira] ct on ct.SKUTira=ls.SKU
		group by case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end
		order by case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end

		update a set a.StockCD=b.StockCD,a.ActiveOrders=b.ActiveOrders
		from [products].[ProductsSpecialPurchase] a
		inner join #laststockCD3 b on b.sku=a.sku
		left join Salcobrand.products.GenericFamilyDetail g on g.SKU=a.SKU
		inner join [products].[ProductsSpecialPurchase] pp on pp.SKU=a.SKU

	/* --- END STOCK CD, Active Orders --- */
		
	/* --- SHORTAGE --- */

		SELECT ls.SKU,SUM(ls.suggest)-SUM(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) -SUM(ls.Transit)-SUM(ISNULL(d.Picking,0))  Shortage
		into #shortage
		FROM Salcobrand.series.LastStock ls 
		INNER JOIN Salcobrand.stores.Stores S on S.Store=ls.Store
		LEFT JOIN (SELECT sku,store,Picking FROM Salcobrand.series.DespachosCD WHERE date=DATEADD(day,-1,@date)) d on d.SKU=ls.SKU and d.Store=ls.Store 
		WHERE ls.suggest>(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) +ls.Transit+ISNULL(d.Picking,0)
		GROUP BY ls.SKU

		-- agrupar para genericos
	update p set p.Shortage=b.Shortage
	from #shortage p
	inner join(
		select g.sku,x.Shortage
		from #shortage b
		inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=b.SKU
		inner join (
			SELECT g.GenericFamilyId,min(Shortage) Shortage
			FROM #shortage o
			inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=o.SKU
			group by g.GenericFamilyId
		) x on x.GenericFamilyId=g.GenericFamilyId
		) b on b.sku=p.sku

		UPDATE a SET a.StoresShortage = b.Shortage
		FROM [products].[ProductsSpecialPurchase] a
		INNER JOIN(
		SELECT CASE WHEN ct.SKUTira is null THEN s.SKU ELSE ct.SKUCaja END SKU,
		SUM(CASE WHEN ct.SKUTira is null THEN Shortage ELSE Shortage/conversion END) Shortage
		FROM #shortage s
		LEFT JOIN salcobrand.products.[CajaTira] ct on ct.SKUTira=s.SKU
		GROUP BY CASE WHEN ct.SKUTira is null THEN s.SKU ELSE ct.SKUCaja END) b ON a.SKU = b.sku

	DROP TABLE #laststockCD2
	DROP TABLE #laststockCD
	DROP TABLE #laststockCD3
	DROP TABLE #shortage

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló carga de SKU, SP [operation].[LoadProductsSpecialPurchase], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
	

END

