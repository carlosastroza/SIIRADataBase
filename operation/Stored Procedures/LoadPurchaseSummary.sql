﻿-- =============================================
-- Author:		Fedex
-- Create date: 2018-04-04
-- Description:	Carga tabla aux.PurchaseSummary
-- =============================================
CREATE PROCEDURE [operation].[LoadPurchaseSummary]
	
AS
BEGIN
	
----------------------  Limpiar en primera instacias las tablas AUX -----------

DELETE  aux.PurchaseOrderDetails
DELETE  aux.PurchaseOrders

--------------------  CARGAR TABLA aux.purchasesummary ---------------

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga de la tabla PurchaseSummary., SP [operation].[LoadPurchaseSummary]', host_name(), 'System', 'POST')


	declare @date        date = getdate()
	declare @date_monday date = Salcobrand.dbo.GetMonday(@date);

	DECLARE @MaxDateSugeridoHoy DATE = (SELECT MAX(Date) FROM Salcobrand.reports.MetricasStocks)

select
		p.SKU
	  , ps.Fillrate FillRate
	  , ps.DaysToCover DaysToCover	
	  , NULL AdditionalDaysToCover
	  , NULL RemainingStockDays 
	  , ISNULL(rs.Stock, 0) StoresStock -- Ojo, despu�s debemos sumar lo de PU
	 -- , CASE WHEN ps.StockCD is not null THEN ps.StockCD ELSE  scd.Stock END DCStock
	  ,ps.StockCD
	  , rs.Quiebre StockOut 
	  , s.Quantity LastMonthSale
	  , ps.SBDemand
	  , ps.PUDemand
	 -- , CASE WHEN ps.Suggest IS NOT NULL THEN ps.Suggest ELSE 1000 END Suggest  ---   pendiente con los datos de prueba
	 -- , CASE WHEN ps.Purchase IS NOT NULL THEN ps.Purchase ELSE 1000 END  PurchaseSuggest ---   pendiente con los datos de prueba
	 -- , CASE WHEN ps.Purchase IS NOT NULL THEN ps.Purchase ELSE 1000 END Purchase -- igual a compra sugerida.
	  , ps.Suggest
	  --, ps.Purchase SugeridoCompra
	  , ps.Purchase Compra
	  , c.Cobertura Coverage
	  , ps.ActiveOrders DCTransit
	  , rs.TransitoLocales StoresTransit
	  , ps.Shortage StoresShortage--, rs.Sugerido - rs.StockAjustado StoresShortage
	  , p.TotalRanking TotalRanking
	  , 0 Estado
	  , p.IsGes isGes
	  , p.IsGeneric
	  , sug.Sugerido
INTO #data
from products.Products p 
LEFT JOIN (
	select sku
		, sum(stock) Stock
		, SUM(CASE WHEN Stock <= 0 THEN 1 ELSE 0 END) / CAST(COUNT(Store) AS FLOAT) Quiebre
		, SUM(rs.Transit) TransitoLocales
		, SUM(Suggest) Sugerido
		, SUM(Salcobrand.[function].InlineMin(suggest, stock)) StockAjustado
	from Salcobrand.reports.LastReportStores rs
	group by sku
)rs ON p.SKU = rs.SKU
/*LEFT join (
	select * from (
		select *, ROW_NUMBER() OVER(partition by sku order by [Date] desc) R
		from Salcobrand.series.[StockCDDia] 
	) a WHERE R = 1
) scd ON p.SKU = scd.SKU */
LEFT JOIN (
	select SKU, SUM(Quantity) Quantity 
	from Salcobrand.series.DailySales
	where [Date] BETWEEN DATEADD(WEEK, -4, @date_monday) AND @date
	GROUP BY SKU
) s ON p.SKU = s.SKU
LEFT JOIN (
	select sku, count(*) Cobertura 
	from Salcobrand.series.LastStock
	group by sku
) c on p.sku = c.SKU
LEFT JOIN suggest.PurchaseSuggest ps on ps.SKU = p.SKU
LEFT JOIN (
SELECT SKU, Sugerido
FROM Salcobrand.reports.MetricasStocks
WHERE DATE = @MaxDateSugeridoHoy) sug ON p.SKU = sug.sku

MERGE INTO aux.PurchaseSummary as ps
USING #data d ON ps.SKU = d.SKU
WHEN MATCHED THEN
	UPDATE SET 
		ps.FillRate = d.FillRate, ps.DaysToCover = d.DaysToCover, ps.RemainingStockDays = d.RemainingStockDays, ps.StoresStock = d.StoresStock,
		ps.DCStock = d.StockCD, ps.StockOut = d.StockOut, ps.LastMonthSale = d.LastMonthSale, ps.SBDemand = d.SBDemand, ps.PUDemand = d.PUDemand, 
		ps.Suggest = d.Suggest, ps.PurchaseSuggest = d.Compra,ps.Coverage = d.Coverage, ps.DCTransit = d.DCTransit, ps.StoresTransit = d.StoresTransit, 
		ps.StoresShortage = d.StoresShortage, ps.TotalRanking = d.TotalRanking, ps.isGES = d.isGes, ps.isGeneric = d.isGeneric, ps.DistributionSuggest = d.Sugerido
WHEN NOT MATCHED BY TARGET THEN
	INSERT(SKU,FillRate,DaysToCover,AdditionalDaysToCover,RemainingStockDays,StoresStock,DCStock,StockOut,LastMonthSale,SBDemand,
		   PUDemand,Suggest,PurchaseSuggest,Purchase,Coverage,DCTransit,StoresTransit,StoresShortage,TotalRanking,isGES,isGeneric,Status,ModifyBy,ModifyOn,Purchased,DistributionSuggest)
	VALUES(d.SKU,d.FillRate,d.DaysToCover,0,d.RemainingStockDays,d.StoresStock,d.StockCD,d.StockOut,d.LastMonthSale,d.SBDemand,d.PUDemand,d.Suggest,d.Compra,d.Compra,d.Coverage,d.DCTransit,
           d.StoresTransit,d.StoresShortage,d.TotalRanking,d.isGes,d.isGeneric,0,'SYSTEM',GETDATE(),0,Sugerido)
WHEN NOT MATCHED BY SOURCE 
THEN DELETE;


MERGE INTO aux.PurchaseSummary as ps
USING #data d ON ps.SKU = d.SKU
WHEN MATCHED AND ( 

	--si compra no es de hoy, se pisa siempre
	( CAST(ps.ModifyOn AS DATE) < CAST(GETDATE() AS DATE) )
	OR
	--si compra es de hoy, se chequea que no haya sido modificada o no haya sido comprada
	( CAST(ps.ModifyOn AS DATE) = CAST(GETDATE() AS DATE) AND ps.ModifyBy = 'SYSTEM' AND ps.Purchased = 0 )

) THEN
UPDATE SET ps.Purchase = d.Compra, ps.AdditionalDaysToCover = 0,ps.ModifyBy = 'SYSTEM', ps.ModifyOn = GETDATE(), Purchased = 0;

--truncate table aux.purchasesummary

----- PASO 1---
--BEGIN TRY
--	INSERT INTO aux.purchasesummary ([SKU], [FillRate], [DaysToCover], [AdditionalDaysToCover], [RemainingStockDays], [StoresStock], [DCStock], [StockOut], [LastMonthSale], [SBDemand], [PUDemand], [Suggest], [PurchaseSuggest], [Purchase], [Coverage], [DCTransit], [StoresTransit], [StoresShortage], [TotalRanking], [Status], [IsGES], [IsGeneric])
--	SELECT * FROM #data
--END TRY
--BEGIN CATCH
--	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
--	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 1, Falló inserción de datos en la tabla aux.purchasesummary , SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
--	THROW;
--END CATCH

---  PASO 2 ----
/* Actualiza Quiebres de locales por Familia */
BEGIN TRY

	DECLARE @tablaGenericos as table (
	SKU INT,
	Quiebre FLOAT)
	INSERT INTO @tablaGenericos
	SELECT 
	p.SKU,
	Quiebre
	FROM (
	SELECT 
	r.GenericFamilyId,
	SUM(CASE WHEN Stock <= 0 THEN 1 ELSE 0 END) / CAST(COUNT(Store) AS FLOAT) Quiebre
	FROM salcobrand.[reports].[GenericReportStores] r
	WHERE 
	Date =	DATEADD(DAY, -2, CONVERT(date, getdate()))
	GROUP BY r.GenericFamilyId) AS A
	LEFT JOIN products.Products p ON A.GenericFamilyId = p.GenericFamilyId

	MERGE INTO aux.purchasesummary as s
	USING @tablaGenericos AS  t
	on s.sku = t.sku
	WHEN MATCHED THEN
	UPDATE SET s.stockout = t.quiebre;

END TRY
BEGIN CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 2, Falló actualización de quiebres de locales por Familia , SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;
END CATCH


/* End */

--MERGE INTO SalcobrandAbastecimiento.aux.purchasesummary as ps
--USING products.DistributorRotation AS  dr
--on ps.sku = dr.sku
--WHEN MATCHED THEN
--UPDATE SET ps.LastMonthSalesGES = dr.LastMonthSalesGES, ps.LastMonthShortageGES = dr.LastMonthShortage;


--- PASO 3 ---
/* Script para solamente dejar sugerido de compra a Genéricos Licitados */

	DECLARE @updatePurchaseSuggest as table (
	SKU INT,PurchaseSuggest INT)

	insert into @updatePurchaseSuggest
	SELECT 
	p.[SKU] SKU,
	CASE WHEN t.SKU IS NOT NULL THEN PurchaseSuggest else NULL end PurchaseSuggest
	FROM aux.purchasesummary p
	LEFT JOIN products.GenericTenderedSKU t on p.SKU = t.SKU
	where IsGeneric = 1

BEGIN TRY
	MERGE INTO aux.purchasesummary as s
	USING @updatePurchaseSuggest AS  t
	on s.sku = t.sku
	WHEN MATCHED THEN
	UPDATE SET s.PurchaseSuggest = t.purchasesuggest, s.purchase = t.PurchaseSuggest;
END TRY
BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 3, Falló la actualización de compra y compra sugerida, SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
END CATCH
/* END */

------------------- llenado de tabla  aux.PurchaseSummaryDistributor  para GES ----------------------------------
---- PASO 4 ----
BEGIN TRY
	TRUNCATE TABLE aux.PurchaseSummaryDistributor
	INSERT INTO aux.PurchaseSummaryDistributor
	SELECT
	   v.SKU,
	   v.DistributorRUT,
	   v.IdZona,
	   dr.LastMonthSalesGES, 
	   case when dr.LastMonthSalesGES is not null then ISNULL(dr.LastMonthSalesGES, 0) else null end PurchaseSuggest,
	   ISNULL(dr.LastMonthSalesGES, 0) Purchase,
	   0 [Status] 
	FROM Products.Vademecum v
	LEFT JOIN products.DistributorRotation dr on dr.SKU = v.SKU AND dr.DistributorRUT = v.DistributorRUT AND dr.IdZona = v.IdZona
END TRY
BEGIN CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 4, Falló la inserción de datos en la tabla aux.PurchaseSummaryDistributor, SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;
END CATCH

--------- Update a aux.PurchaseSummary  solo en compra sugerida donde se le resta la rotacion y faltante----
---- PASO 5 ----
BEGIN TRY

	SELECT sku,SUM(PurchaseSuggest)PurchaseSuggest
	INTO #Dist
	FROM aux.PurchaseSummaryDistributor 
	GROUP BY sku

	UPDATE ps
	SET  Purchase = case when ps.Purchase - isnull (psd.PurchaseSuggest, 0) > 0 then ps.PurchaseSuggest - isnull (psd.PurchaseSuggest, 0) else 0 end
	--select *
	from aux.PurchaseSummary ps
	INNER JOIN #Dist psd on psd.sku = ps.sku

	--redondeo la compra sugerida de todos los SKU GES ya que no vienen redondeados

	UPDATE ps
	SET  PurchaseSuggest = r.UnidadesTotales
	--select *
	from aux.PurchaseSummary ps
	--INNER JOIN aux.PurchaseSummaryDistributor psd on psd.sku = ps.sku
	INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	CROSS APPLY [function].[RoundLogisticUnits](ps.PurchaseSuggest,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
	where IsGES = 1 and IsGeneric = 0

	--Teniendo la resta de las compas proveedor - distribuidor redondeo la compra de todos los SKU GES 

	UPDATE ps
	SET  Purchase = r.UnidadesTotales
	--select *
	from aux.PurchaseSummary ps
	--INNER JOIN aux.PurchaseSummaryDistributor psd on psd.sku = ps.sku
	INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	CROSS APPLY [function].[RoundLogisticUnits](ps.Purchase,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
	where IsGES = 1 and IsGeneric = 0

	/* Redondeo unidades distribuidor*/
	UPDATE ps
	SET  
	Purchase = case when r.UnidadesTotales>0 then r.UnidadesTotales else 0 end,
	--CASE WHEN ps.Purchase > 0 THEN r.UnidadesTotales ELSE ps.Purchase END, 
	PurchaseSuggest = case when r.UnidadesTotales>0 then r.UnidadesTotales else 0 end
	--CASE WHEN ps.Purchase > 0 THEN r.UnidadesTotales ELSE ps.Purchase END
	--select *
	from aux.PurchaseSummaryDistributor ps
	INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	CROSS APPLY [function].[RoundLogisticUnits](ps.Purchase,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r


END TRY
BEGIN CATCH

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 5, Falló la actualización de compra en la tabla aux.PurchaseSummary, SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;
END CATCH

DROP TABLE #data




--Actualizamos la información de OPAC de cada producto----
---- PASO 6 ----
BEGIN TRY
	
	PRINT N'Actualizando información de OPAC de cada producto'
	UPDATE p SET 
		OPACCurrent = CASE WHEN EXISTS(
				select ProductId 
				from Salcobrand.promotion.PromotionStock ps 
				where p.SKU = ps.ProductId AND ps.BeginDate <= GETDATE() AND ps.EndDate >= GETDATE()
			) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END
		,  OPACUpcoming = CASE WHEN EXISTS(
				select ProductId 
				from Salcobrand.promotion.PromotionStock ps 
				where p.SKU = ps.ProductId AND ps.BeginDate > GETDATE() AND ps.EndDate > GETDATE()
			) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END
	FROM aux.PurchaseSummary p

END TRY
BEGIN CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 6, Falló la actualización de OPAC en la tabla aux.PurchaseSummary, SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;
END CATCH


--Actualizamos la información de venta perdida de cada producto ----
---- PASO 7 ----
BEGIN TRY
	
	PRINT N'Actualizando información de venta perdida'
	UPDATE ps SET 
		ps.LostSaleLastMonth = ls.LostSale
	from aux.PurchaseSummary ps
	INNER JOIN (
		SELECT SKU, SUM(ls.VtaPerdida) LostSale
		FROM Salcobrand.reports.LostSaleBySKUCambio ls 
		WHERE ls.[Date] BETWEEN DATEADD(WEEK, -4, GETDATE()) AND GETDATE()
		GROUP BY SKU
	) ls ON ps.SKU = ls.SKU

END TRY
BEGIN CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 7, Falló la actualización de VentaPerdida en la tabla aux.PurchaseSummary, SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;
END CATCH

		

--Actualizamos la información de fit next month----
---- PASO 8 ----
BEGIN TRY
	
	PRINT N'Actualizando información de Fit del próximo mes'
	UPDATE ps
		SET FitNextMonth = f.FitNextMonth
	from aux.PurchaseSummary ps
	inner join (
		select SKU, SUM(ISNULL(FitCorregido, 0)) FitNextMonth
		from Salcobrand.forecast.DailyForecast
		WHERE Fecha BETWEEN GETDATE() AND DATEADD(WEEK, +4, GETDATE()) 
		GROUP BY SKU
	) f ON ps.SKU = f.SKU

END TRY
BEGIN CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA','PASO 8, Falló la actualización de Fit del próximo mes en la tabla aux.PurchaseSummary, SP [operation].[LoadPurchaseSummary], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;
END CATCH


END
