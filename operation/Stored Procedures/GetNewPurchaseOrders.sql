﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2018-06-11
-- Description:	Archivo que se carga de OC cada media hora.
-- =============================================
CREATE PROCEDURE [operation].[GetNewPurchaseOrders]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @date DATE = GETDATE()

    -- Insert statements for procedure here
	SELECT distinct RUT, 2 as Emp, c.Id as Nro_oc_pricing
		, case isfree when 1 then 'Y' else 'N' end as Gratis_oc
		, convert(nvarchar(20),CreatedDate,112) as Fecha_emi, convert(nvarchar(20),ReceptionDate,112) as Fecha_recep
		, right(concat('00', convert(nvarchar(3), isnull(BuyerId,0) ) ), 3) as Comprador, isnull(OrderComment,'') as Glosa
		, c.IdZona
	FROM [purchase].[PurchaseOrderDetailsStaging] d 
	inner join [purchase].[PurchaseOrdersStaging] c on c.id=d.PurchaseOrderId
	left join (select Username, BuyerId from [users].[UserRules] Where BuyerId>0) bi on bi.Username=c.CreatedBy	WHERE CreatedDate=@date
		and Quantity>0


END
