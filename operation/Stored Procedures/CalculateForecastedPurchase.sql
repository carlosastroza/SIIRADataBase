﻿-- =============================================
-- Author:		Esteban Cea
-- Create date: 2018-08-15
-- Description:	Calcula el reporte de venta proyectada
--
-- Updated:		Daniela Albornoz
--				2018-08-15
--				Considera varios casos de borde adicionales
-- =============================================
CREATE PROCEDURE [operation].[CalculateForecastedPurchase]
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ADiasAdicionales int = DATEDIFF(DAY,GETDATE(),DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)))
	DECLARE @ADiasAdicionales2 int = DATEDIFF(DAY,GETDATE(),DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+2,0)))
	DECLARE @ADiasAdicionales3 int = DATEDIFF(DAY,GETDATE(),DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+3,0)))
	DECLARE @date date = ( SELECT max (date) FROM [Salcobrand].series.StockCD )
	DECLARE @hoy date = ( getdate() )
	--select 

		/**ORDENES ACTIVAS**/

		select * 
		into #stock
		from (
			SELECT p.SKU, ISNULL(StockCD,0)StockCD, ISNULL(fr.Fillrate,1) FillRate	, 0 activeOrders, 0 activeOrders2, 0 activeOrders3
			FROM products.Products p
			LEFT JOIN aux.FillRate fr on fr.sku=p.SKU
			LEFT JOIN (SELECT * FROM Salcobrand.series.StockCD where [Date]=DATEADD(DAY,-1,@hoy)) ps ON p.SKU = ps.SKU
			WHERE p.SKU NOT IN (SELECT SKU FROM Salcobrand.products.GenericFamilyDetail)
			union
			SELECT distinct g.TenderedSKU, ISNULL(StockCD,0)StockCD, ISNULL(fr.Fillrate,1) FillRate	, 0 activeOrders, 0 activeOrders2, 0 activeOrders3 --select *
			FROM products.Products p
			INNER JOIN (
				SELECT d.GenericFamilyId, d.SKU, isnull(t.SKU, f.AuxSKU) TenderedSKU --si familia no tiene sku licitado, usa el que esté vendiendo
				FROM Salcobrand.products.GenericFamilyDetail d
				inner join Salcobrand.products.GenericFamily f on f.Id=d.GenericFamilyId
				left join [products].[GenericTenderedSKU] t on t.GenericFamilyId=f.Id
			) g on g.SKU=p.SKU
			LEFT JOIN aux.FillRate fr on fr.sku=g.TenderedSKU
			LEFT JOIN (SELECT * FROM Salcobrand.series.StockCD where [Date]=DATEADD(DAY,-1,@hoy)) ps ON ps.SKU = g.TenderedSKU
		) a

		create index #idx3  on #stock (SKU)
		 
		update p set activeOrders=(j.activeOrders), activeOrders2=(j.activeOrders2), activeOrders3=(j.activeOrders3)
		from #stock p
		INNER JOIN (
			SELECT g.TenderedSKU SKU, sum(activeOrders)activeOrders, sum(activeOrders2)activeOrders2, sum(activeOrders3)activeOrders3
			FROM (
				SELECT d.GenericFamilyId, d.SKU, isnull(t.SKU, f.AuxSKU) TenderedSKU --si familia no tiene sku licitado, usa el que esté vendiendo
				FROM Salcobrand.products.GenericFamilyDetail d
				inner join Salcobrand.products.GenericFamily f on f.Id=d.GenericFamilyId
				left join [products].[GenericTenderedSKU] t on t.GenericFamilyId=f.Id
				union all
				SELECT NULL, SKU, SKU
				from Salcobrand.products.Products fp
				where SKU not in (select sku from Salcobrand.products.GenericFamilyDetail)
			) g 
			inner join (
				SELECT a.SKU
					, SUM(case when FechaExpiracionActual between @hoy and dateadd(day,@ADiasAdicionales,@hoy ) then UnidadesCompra else 0 end) as activeOrders
					, SUM(case when FechaExpiracionActual between @hoy and dateadd(day,@ADiasAdicionales2,@hoy ) then UnidadesCompra else 0 end) as activeOrders2
					, SUM(case when FechaExpiracionActual between @hoy and dateadd(day,@ADiasAdicionales3,@hoy ) then UnidadesCompra else 0 end) as activeOrders3
				FROM series.PurchaseOrders a
				where EstadoOOCC=2
					and UnidadesRecibidas=0 and UnidadesCompra>0
					and Store=996
				group by a.SKU
			) j on j.SKU=g.SKU
			group by g.TenderedSKU
		) j on j.SKU=p.SKU
		

		/**PEDIDO DE LOCALES. Requiere convertir Tira a Caja**/

		SELECT TenderedSKU as SKU, [Date], SUM(isnull(pc.PedidoEfectivo,0)) PedidoEfectivo
		into #pedidoOrg
		FROM [SalcobrAND].[minmax].[DistributionOrdersForecastCurrent] pc
		INNER JOIN (
			SELECT d.GenericFamilyId, d.SKU, isnull(t.SKU, f.AuxSKU) TenderedSKU --si familia no tiene sku licitado, usa el que esté vendiendo
			FROM Salcobrand.products.GenericFamilyDetail d
			inner join Salcobrand.products.GenericFamily f on f.Id=d.GenericFamilyId
			left join [products].[GenericTenderedSKU] t on t.GenericFamilyId=f.Id
			union all
			SELECT NULL, SKU, SKU
			from Salcobrand.products.Products fp
			where SKU not in (select sku from Salcobrand.products.GenericFamilyDetail)
		) g on g.SKU=pc.SKU
		group by TenderedSKU, [Date]
		
		create index #idx5  on #pedidoOrg (SKU)

		insert into #pedidoOrg
		select SKUCaja, [date],0
		from Salcobrand.products.CajaTira ct
		cross join (select distinct [date] from #pedidoOrg) p
		where not exists (
			select *
			from #pedidoOrg o
			where o.SKU=ct.SKUCaja and o.[Date]=p.[Date])
	
		
		update j set PedidoEfectivo=j.PedidoEfectivo+(p.PedidoEfectivo*1.0/ct.Conversion)
		from #pedidoOrg j
		INNER JOIN Salcobrand.products.CajaTira ct on ct.SKUCaja=j.SKU
		INNER JOIN #pedidoOrg p on p.SKU=ct.SKUTira and p.[Date]=j.[Date]

		DELETE FROM #pedidoOrg WHERE SKU IN (SELECT SKUTira FROM salcobrand.products.[CajaTira])

		SELECT SKU
			, SUM(case when [date] between @hoy and dateadd(day,@ADiasAdicionales,@hoy ) then PedidoEfectivo else 0 end) as PedidoEfectivo
			, SUM(case when [date] between @hoy and dateadd(day,@ADiasAdicionales2,@hoy ) then PedidoEfectivo else 0 end) as PedidoEfectivo2
			, SUM(case when [date] between @hoy and dateadd(day,@ADiasAdicionales3,@hoy ) then PedidoEfectivo else 0 end) as PedidoEfectivo3
		into #pedido
		FROM #pedidoOrg
		group by SKU

		create index #idx1  on #pedido (SKU)

		/**FALTANTE EN LOCALES. Requiere convertir Tira a Caja**/
		--DECLARE @hoy date = ( getdate() ) 
		SELECT ls.SKU
			, SUM(case when isnull(ls.Suggest,0)-isnull(ls.Stock,0)-isnull(ls.Transit,0)-isnull(d.Picking,0) > 0 then isnull(ls.Suggest,0)-isnull(ls.Stock,0)-isnull(ls.Transit,0)-isnull(d.Picking,0) else 0 end) Shortage
		into #faltante
		FROM  (
			SELECT g.TenderedSKU as SKU, Store, Max(Suggest)Suggest, SUM(Stock)Stock, SUM(Transit)Transit
			FROM Salcobrand.series.LastStock ls
			INNER JOIN (
				SELECT d.GenericFamilyId, d.SKU, isnull(t.SKU, f.AuxSKU) TenderedSKU --si familia no tiene sku licitado, usa el que esté vendiendo
				FROM Salcobrand.products.GenericFamilyDetail d
				inner join Salcobrand.products.GenericFamily f on f.Id=d.GenericFamilyId
				left join [products].[GenericTenderedSKU] t on t.GenericFamilyId=f.Id
				union all
				SELECT NULL, SKU, SKU
				from Salcobrand.products.Products fp
				where SKU not in (select sku from Salcobrand.products.GenericFamilyDetail)
			) g on g.SKU=ls.SKU
			GROUP BY g.TenderedSKU, Store
		) ls 
		LEFT JOIN ( --DECLARE @hoy date = ( getdate() ) 
			SELECT g.TenderedSKU as SKU,store,SUM(Picking)Picking
			FROM SalcobrAND.series.DespachosCD  ls
			INNER JOIN (
				SELECT d.GenericFamilyId, d.SKU, isnull(t.SKU, f.AuxSKU) TenderedSKU --si familia no tiene sku licitado, usa el que esté vendiendo
				FROM Salcobrand.products.GenericFamilyDetail d
				inner join Salcobrand.products.GenericFamily f on f.Id=d.GenericFamilyId
				left join [products].[GenericTenderedSKU] t on t.GenericFamilyId=f.Id
				union all
				SELECT NULL, SKU, SKU
				from Salcobrand.products.Products fp
				where SKU not in (select sku from Salcobrand.products.GenericFamilyDetail)
			) g on g.SKU=ls.SKU
			WHERE date=DATEADD(day,-1,@hoy)
			GROUP BY g.TenderedSKU, Store
		) d on d.SKU=ls.SKU AND d.Store=ls.Store 
		--WHERE ls.suggest>(case when ls.stock>=0 then ls.stock else 0 end) +ls.Transit+isnull(d.Picking,0)
		GROUP BY ls.SKU

		create index #idx2  on #faltante (SKU)

		insert into #faltante
		select SKUCaja,0
		from Salcobrand.products.CajaTira ct
		where not exists (
			select *
			from #faltante o
			where o.SKU=ct.SKUCaja)

		update j 
			set Shortage=j.Shortage+CEILING(p.Shortage*1.0/ct.Conversion)
		from #faltante j
		INNER JOIN Salcobrand.products.CajaTira ct on ct.SKUCaja=j.SKU
		INNER JOIN #faltante p on p.SKU=ct.SKUTira

		DELETE FROM #faltante WHERE SKU IN (SELECT SKUTira FROM salcobrand.products.[CajaTira])

		/*CÁLCULO DE COMPRA*/
		
		SELECT pedidos.SKU, 
				ceiling([SalcobrAND].[function].NormalProbability (pedidos.PedidoEfectivo,isnull(std.dvStd,0) * Sqrt(@ADiasAdicionales),isnull(SP.ServiceLevel,0.95)
			)) + isnull(Shortage.Shortage,0) Suggest,

					ceiling([SalcobrAND].[function].NormalProbability(pedidos.PedidoEfectivo2,isnull(std.dvStd,0) * Sqrt( @ADiasAdicionales2),isnull(SP.ServiceLevel,0.95)
			)) + isnull(Shortage.Shortage,0) Suggest2,

					ceiling([SalcobrAND].[function].NormalProbability(pedidos.PedidoEfectivo3,isnull(std.dvStd,0) * Sqrt(@ADiasAdicionales3),isnull(SP.ServiceLevel,0.95)
			)) + isnull(Shortage.Shortage,0) Suggest3
 		INTO #compra --DECLARE @hoy date = ( getdate() )  select count(*)
		FROM  #pedido pedidos
		LEFT JOIN (-- Subquery con el promedio de las desviaciones estANDar por dia de la semana, cuANDo no son 0.
			SELECT a.Sku,avg(a.StdPedidos) dvStd
				FROM (
					SELECT SKU,DATEPART(dw,[Date]) dow,STDEV(PedidoEfectivo) StdPedidos,count(*) Total 
					FROM #pedidoOrg 
					GROUP BY SKU,DATEPART(dw,[Date]) 
					Having sum(PedidoEfectivo)>0
				) a
			GROUP BY a.Sku
		) std on std.SKU=pedidos.sku 
		LEFT JOIN #faltante Shortage ON Shortage.SKU= pedidos.sku 
		LEFT JOIN [setup].[SuggestParameters] sp ON SP.sku=Pedidos.sku

		create index #idx4  on #compra (SKU)

				
		SELECT isnull(a.SKU,b.SKU)SKU, isnull(a.StockCD,0)StockCD
			, ISNULL(a.ActiveOrders,0)ActiveOrders, isnull(a.activeOrders2,0)activeOrders2, isnull(a.activeOrders3 ,0)activeOrders3
			, isnull(a.Fillrate,1)Fillrate
			, isnull(b.Suggest,0)Suggest,isnull(b.Suggest2,0)Suggest2, isnull(b.Suggest3,0)Suggest3
			, case when b.sku is null then 1 else 0 end flag
		--SELECT *
		INTO #result
		FROM #stock a 
		FULL JOIN #compra b on a.SKU = b.SKU
		-- select*from #result where sku=430246
		
  --------------------------  RESULTADO  ------------------------------------------------
      	INSERT INTO reports.ForecastedPurchase
	   SELECT 
		   rr.SKU
		   ,po.IsGeneric
		   ,po.DivisionUnificadaDesc División
		   ,po.Name Nombre
		   ,po.Category Categoría
		   ,po.SubcategoriaUnificadaDesc Subcategoría
		   ,po.Positioning_Desc Posicionamiento
		   ,m.Name Manufacturer
		   ,po.TotalRanking Ranking
		   ,case when  r1.UnidadesTotales >=0 then r1.UnidadesTotales else 0 end [MES ACTUAL] -- los numeros negativos representan mas stock y unidades en transito
		   ,case when  r2.UnidadesTotales >=0 then r2.UnidadesTotales else 0 end [MES 2]
		   ,case when  r3.UnidadesTotales >=0 then r3.UnidadesTotales else 0 end [MES 3]
		  -- , flag --select *
	   FROM suggest.PurchaseSuggest ps
	   INNER JOIN (
			select p.*, f.DivisionUnificadaDesc, f.SubcategoriaUnificadaDesc,f.Positioning_Desc 
			from salcobrandabastecimiento.products.Products p
			inner join salcobrand.products.FullProducts f ON f.SKU=p.sku
		) po ON ps.SKU = po.SKU
	   INNER JOIN products.Manufacturers m ON m.Id=po.ManufacturerId
	   left JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	   INNER JOIN #result rr on ps.SKU = rr.SKU
	   outer APPLY [function].[RoundLogisticUnits] (
			case when (isnull(rr.Suggest,0)-isnull(rr.ActiveOrders,0)-isnull(rr.[StockCD],0))<0 then 0 
			else (isnull(rr.Suggest,0)-isnull(rr.ActiveOrders,0)-isnull(rr.[StockCD],0)) end ,
	   null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r1
	   
	   outer APPLY [function].[RoundLogisticUnits] (
			case when (isnull(rr.Suggest2,0)-isnull(rr.ActiveOrders2-rr.ActiveOrders,0)-isnull(rr.[StockCD],0)-isnull(r1.UnidadesTotales,0))<0 then 0 
			else (isnull(rr.Suggest2,0)-isnull(rr.ActiveOrders2-rr.ActiveOrders,0)-isnull(rr.[StockCD],0)-isnull(r1.UnidadesTotales,0)) end ,
	   null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r2
	   
	   outer APPLY [function].[RoundLogisticUnits] (
			case when (isnull(rr.Suggest3,0)-isnull(rr.ActiveOrders3-rr.ActiveOrders2,0)-isnull(rr.[StockCD],0)-isnull(r1.UnidadesTotales,0)-isnull(r2.UnidadesTotales,0) )<0 then 0 
			else (isnull(rr.Suggest3,0)-isnull(rr.ActiveOrders3-rr.ActiveOrders2,0)-isnull(rr.[StockCD],0)-isnull(r1.UnidadesTotales,0)-isnull(r2.UnidadesTotales,0)) end ,
	  null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r3  
	   
	  WHERE (ISNULL(TotalRanking,'') <> 'E') --and Flag=1 

	  ORDER BY SKU desc


	  /*REVISION TABLAS INTERMEDIAS*/
		--select * from #stock order by sku  -- where sku in (SELECT SKUCaja from Salcobrand.products.CajaTira) order by SKU
		--select * from #pedido order by sku -- where sku in (SELECT SKUCaja from Salcobrand.products.CajaTira) and PedidoEfectivo3>0
		--select * from #faltante order by sku --  where sku in (SELECT SKUCaja from Salcobrand.products.CajaTira) and Shortage>0
		--select * from #compra order by sku -- where sku in (SELECT SKUCaja from Salcobrand.products.CajaTira) order by SKU
		--select * from #result order by sku -- where sku in (SELECT SKUCaja from Salcobrand.products.CajaTira) order by SKU
		--select * from #result  where flag=1

	 drop  table #stock
	 drop  table #pedidoOrg
	 drop  table #pedido
	 drop  table #faltante
	 drop  table #compra
	 drop  table  #result
	  
END
