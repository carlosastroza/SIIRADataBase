﻿

-- =============================================
-- Author:		Juan M Hernández
-- Create date: 2018-05-16
-- Description:	reportes proceso diario completo para generar compras
-- =============================================
CREATE PROCEDURE [operation].[GenerateReportsPurchaseFull] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @msg VARCHAR(100) 


	declare @date DATE = GETDATE();

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de reportes del proceso diario completo para generar órdenes de compra, SP [operation].[GeneratePurchaseFull]', host_name(), 'System', 'POST')

	--PASO 1

	  -- CHEQUEAMOS LA CANTIDAD DE SKU CON STOCKCD > 0, TAMBIEN PODEMOS DETERMINAR SI series.StockCD TIENE DATOS 
		  DECLARE @STOCKCD_LOCAL INT 
		  SET @STOCKCD_LOCAL = (
		  --declare @date DATE = GETDATE()
		  SELECT COUNT(*) FROM  [Salcobrand].series.StockCD 
		  WHERE [Date] = DATEADD(DAY, -1, CAST(@date AS DATE))
		  AND StockCD > 0
		  )
		  IF @STOCKCD_LOCAL > 0	BEGIN 
				INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
				VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga de información de tránsito y stockCD en LoadLastStock, se cargaron ' + cast (@STOCKCD_LOCAL as nvarchar) + ' con stockCD > 0 , SP operation.LoadLastStock, Paso 1', host_name(), 'System', 'POST')
		  END
		  ELSE BEGIN
		 
		     DECLARE @DATE_CALCULO NVARCHAR(50)  = (SELECT DATEADD(DAY, -1, CAST(@date AS DATE)))
        	 INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			 VALUES (getdate(), 'error', 'Salcobrand.SIIRA',' No se posee información de STOCKCD  para el día ' + cast (@DATE_CALCULO as nvarchar)  + ' o no se tiene información de stockCD >0, SP operation.LoadLastStock, Paso 1', host_name(), 'System', 'POST');
			 
			 set @msg = ' No se posee información de STOCKCD  para el día ' + cast (@DATE_CALCULO as nvarchar)  + ' o no se tiene información de stockCD >0, SP operation.LoadLastStock, Paso 1';
			 THROW 50001, @msg ,1
		  END
   
   --PASO 2
		
		
		SELECT COUNT (*) Total_SKU,
			SUM( CASE WHEN ISNULL(cd.BuyingMultiple, 0) = 0 THEN 1 ELSE 0 END ) +
			SUM(  CASE WHEN ISNULL(cd.BuyingMultiple, 0) > 0 AND cd.BuyingMultiple = cd.UnidadesxCamada THEN 1 ELSE 0 END) +
		    SUM(  CASE WHEN ISNULL(cd.BuyingMultiple, 0) > 0 AND cd.BuyingMultiple = cd.UnidadesxPallet THEN 1 ELSE 0 END) SKU_Redondeados
		INTO #DATOS	 
		FROM Salcobrand.products.DUNStore s
		FULL join Salcobrand.products.DUNCD cd ON s.SKU = cd.SKU 
		WHERE COALESCE(s.SKU, cd.SKU) IN (select SKU from products.products)

		DECLARE @Total_SKU_LOCAL INT  = (SELECT Total_SKU FROM #DATOS)
		DECLARE @SKU_Redandeados_LOCAL INT = (SELECT SKU_Redondeados FROM #DATOS)

		IF @SKU_Redandeados_LOCAL > 0	BEGIN 
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga en setup.LogisticParameter, se cargó la información de ' +cast (@Total_SKU_LOCAL as nvarchar) + ' sku, donde '+ cast (@SKU_Redandeados_LOCAL as nvarchar) +' tienen redondeo a caja, camada y pallet. SP [operation].[LoadLogisticParameters], Paso 2', host_name(), 'System', 'POST')
		END
		ELSE BEGIN
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Alerta', 'Salcobrand.SIIRA',' No se posee información de unidades logisticas con redondeo a caja, camada y pallet, SP [operation].[LoadLogisticParameters], Paso 2', host_name(), 'System', 'POST')

			 set @msg = 'No se posee información de unidades logisticas con redondeo a caja, camada y pallet, SP [operation].[LoadLogisticParameters], Paso 2';
			 THROW 50001, @msg ,1

		END
		DROP TABLE #DATOS

	--PASO 3

		DECLARE @TOTAL_PRICEANDCOST_LOCAL INT  = (SELECT COUNT (*) FROM setup.PriceAndCost where Cost>0)
		IF @TOTAL_PRICEANDCOST_LOCAL > 0	BEGIN 
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga en setup.PriceAndCost, donde se cargó la información de ' +cast (@TOTAL_PRICEANDCOST_LOCAL as nvarchar) + ' sku con Cost > 0. [operation].[LoadPriceAndCost], Paso 3', host_name(), 'System', 'POST')
		END
		ELSE BEGIN
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' No se posee información de Precio y Costo para nigún SKU, SP [operation].[LoadPriceAndCost], Paso 3', host_name(), 'System', 'POST')

			 set @msg = 'No se posee información de Precio y Costo para nigún SKU, SP [operation].[LoadPriceAndCost], Paso 3';
			 THROW 50001, @msg ,1

		END

	--PASO 4

		DECLARE @TOTAL_SuggestParameters_LOCAL INT  = (SELECT COUNT(*) FROM [setup].[SuggestParameters] )
		IF @TOTAL_SuggestParameters_LOCAL > 0	BEGIN 
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga en [setup].[SuggestParameters], donde se cargó la información de parámetros de ' +cast (@TOTAL_SuggestParameters_LOCAL as nvarchar) + ' sku. SP[setup].[FillPurchaseCalendar], Paso 4', host_name(), 'System', 'POST')
		END
		ELSE BEGIN
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' No se posee información de parámetros de sugerido para nigún SKU, SP [setup].[FillPurchaseCalendar], Paso 4', host_name(), 'System', 'POST');

			 set @msg = 'No se posee información de parámetros de sugerido para nigún SKU, SP [setup].[FillPurchaseCalendar], Paso 4';
			 THROW 50001, @msg ,1
		END
		
	--PASO 5
		
		SELECT sum (case when FillRate is null then 1 else 0 end) FRNull
			 , sum (case when FillRate < 0.2 and FillRate is not null  then 1 else 0 end) FRMenoraveinte
			 ,sum (case when FillRate >= 0.2 and FillRate <=0.5  then 1 else 0 end) FRMayoraVeinteyMenorcincuenta
			 ,sum (case when FillRate >0.5  then 1 else 0 end) FRMayoracincuenta
		into #datos2
		FROM [setup].[SuggestParameters] 

	

		declare @FRNull int = (select FRNull from #datos2)
		declare @FRMenoraveinte int = (select FRMenoraveinte from #datos2)
		declare @FRMayoraVeinteyMenorcincuenta int = (select FRMayoraVeinteyMenorcincuenta from #datos2)
		declare @FRMayoracincuenta int = (select FRMayoracincuenta from #datos2)

		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Resumen de actualización de fillRate. FR = null:'+ cast ( @FRNull as nvarchar) +
						    ', FillRate < 20% :'+cast (@FRMenoraveinte as nvarchar)  +
							', FillRate >= 20% y FillRate < 50%: '+cast (@FRMayoraVeinteyMenorcincuenta as nvarchar)  +
							', FillRate >= 50% '+ cast (@FRMayoracincuenta as nvarchar)  +
							', SP [operation].[CalculateFillRate], Paso 5 ' , host_name(), 'System', 'POST');

		DROP TABLE #datos2

	--PASO 6

		DECLARE @TOTAL_Suggest_LOCAL INT  = ( SELECT count(*) FROM [suggest].[PurchaseSuggest]	WHERE Suggest> 0)
		IF @TOTAL_Suggest_LOCAL > 0	BEGIN 
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','se genera sugerido de compra para '+ cast (@TOTAL_Suggest_LOCAL as nvarchar) +' SKU. [operation].[GenerateSuggestCD], Paso 6', host_name(), 'System', 'POST')
		END
		ELSE BEGIN
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' No se se generó sugerido de compra, SP [operation].[GenerateSuggestCD], Paso 6', host_name(), 'System', 'POST');

			 set @msg = 'No se se generó sugerido de compra, SP [operation].[GenerateSuggestCD], Paso 6';
			 THROW 50001, @msg ,1
		END

	--PASO 7
	
		DECLARE @TOTAL_PurchaseSuggest_LOCAL INT  = ( SELECT count(*) FROM [suggest].[PurchaseSuggest]	WHERE Purchase> 0)
		IF @TOTAL_PurchaseSuggest_LOCAL > 0	BEGIN 
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','se genera compra sugerida para '+ cast (@TOTAL_PurchaseSuggest_LOCAL as nvarchar) +' SKU. [operation].[GenerateSuggestCD], Paso 7', host_name(), 'System', 'POST')
		END
		ELSE BEGIN
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' No se se generó compra sugerida, SP [operation].[GenerateSuggestCD], Paso 7', host_name(), 'System', 'POST');
			
			set @msg = 'No se se generó compra sugerida, SP [operation].[GenerateSuggestCD], Paso 7';
			THROW 50001, @msg ,1
		END

	--PASO 8
	
		declare @compraSugeridamayorcero int = (select count (*) from aux.purchasesummary where PurchaseSuggest > 0)
		declare @sugerido int = (select count (*) from aux.purchasesummary where Suggest > 0)
		declare @fillrate int = (select count (*) from aux.purchasesummary where FillRate is null)

		DECLARE @TOTAL_purchasesummary_LOCAL INT  = ( SELECT count(*) FROM aux.purchasesummary)
		IF @TOTAL_purchasesummary_LOCAL > 0	BEGIN 
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','se genera purchasesummary con '+ cast (@TOTAL_purchasesummary_LOCAL as nvarchar)  +' SKU.
			          SKU com compra sugerida > 0'+ cast (isnull(@compraSugeridamayorcero, 0) as nvarchar)  +' 
					  SKU con sugerido > 0 '+ cast (@sugerido as nvarchar)  +' 
					  SKU con FillRate = null : '+ cast (@fillrate as nvarchar)  +'. [operation].[LoadPurchaseSummary], Paso 8', host_name(), 'System', 'POST')
		END
		ELSE BEGIN
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' No se generó datos en aux.purchasesummary, SP [operation].[LoadPurchaseSummary], Paso 8', host_name(), 'System', 'POST');


			 set @msg = 'No se generó datos en aux.purchasesummary, SP [operation].[LoadPurchaseSummary], Paso 8';
			 THROW 50001, @msg ,1
		END

		--PASO 9

				DECLARE @TOTAL_PRICEANDCOSTGES_LOCAL INT  = (SELECT COUNT (*) FROM setup.PriceAndCostGES where  Cost>0)
		IF @TOTAL_PRICEANDCOSTGES_LOCAL > 0	BEGIN 
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga en setup.PriceAndCostGes, donde se cargó la información de ' +cast (@TOTAL_PRICEANDCOSTGES_LOCAL as nvarchar) + ' sku con Cost > 0. [operation].[LoadPriceAndCostGes], Paso 9', host_name(), 'System', 'POST')
		END
		ELSE BEGIN
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' No se posee información de Precio y Costo para ningún SKU, SP [operation].[LoadPriceAndCostGes], Paso 9', host_name(), 'System', 'POST')

			 set @msg = 'No se posee información de Precio y Costo para nigún SKU, SP [operation].[LoadPriceAndCostGest], Paso 9';
			 THROW 50001, @msg ,1

		END
END
