﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 2018-08-07
-- Description:	Crear Grupos
-- =============================================
CREATE PROCEDURE [operation].[CreateAutomaticGroups]
	
AS
BEGIN
	
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de creación de grupos, SP [operation].[CreateGroups]', host_name(), 'System', 'POST')

	BEGIN TRY

	/*RUT - LogisticAreaId - Username*/
	SELECT DISTINCT m.RUT, LogisticAreaId, Username, CAST(RUT AS nvarchar) + ' - ' + LogisticAreaId + ' - ' + Username collate Modern_Spanish_CI_AS [Name]
	INTO #GruposAperturaNormal
	FROM products.Products p
	INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
	INNER JOIN users.SkuUsers su ON p.SKU = su.SKU
	WHERE (GroupId IS NULL)
	AND (RUT > 0)
	AND (OpenByManufacturer = 0)

	MERGE INTO products.groups as g
	USING #GruposAperturaNormal a on g.RUT = a.RUT AND g.LogisticAreaId = a.LogisticAreaId AND g.Username = a.Username
	WHEN NOT MATCHED THEN 
	INSERT (RUT, LogisticAreaId,Username, Name, OrderCycle, LeadTime) VALUES (a.RUT, a.LogisticAreaId,a.Username, a.Name, 0,0);

	/* END Apertura RUT - LogisticAreaId - Username*/


	/*RUT - ManufacturerId -LogisticAreaId - Username*/
	SELECT DISTINCT m.RUT, m.Id Laboratorio,LogisticAreaId, Username, 
	CAST(RUT AS nvarchar) + ' - ' + CAST(m.Id AS nvarchar) + ' - '+ LogisticAreaId + ' - ' + Username collate Modern_Spanish_CI_AS [Name]
	INTO #GruposAperturaLaboratorio
	FROM products.Products p
	INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
	INNER JOIN users.SkuUsers su ON p.SKU = su.SKU
	WHERE (GroupId IS NULL)
	AND (RUT > 0)
	AND (OpenByManufacturer = 1)

	MERGE INTO products.groups as g
	USING #GruposAperturaLaboratorio a on g.RUT = a.RUT AND g.ManufacturerId = a.Laboratorio AND g.LogisticAreaId = a.LogisticAreaId AND g.Username = a.Username
	WHEN NOT MATCHED THEN 
	INSERT (RUT, ManufacturerId,LogisticAreaId,Username, Name, OrderCycle, LeadTime) VALUES (a.RUT, a.Laboratorio,a.LogisticAreaId,a.Username, a.Name, 0,0);

	/* END Apertura RUT - ManufacturerId - LogisticAreaId - Username*/

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Proceso de creación de grupos culminado con éxio, SP [operation].[CreateGroups]', host_name(), 'System', 'POST')


	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló creación de grupos, SP [operation].[CreateGroups], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
	
	DROP TABLE #GruposAperturaNormal
	DROP TABLE #GruposAperturaLaboratorio

END
