﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-09
-- Description:	Mueve los productos de grupo según el LogisticAreaId y Proveedor al que pertenecen
-- Update:    : Castroza, Asignar SKU de Ges, Genéricos,Genéricos Lab Chile y Andrómaco. 07-05-2018 
-- =============================================
CREATE PROCEDURE [operation].[UpdateProductGroups]
	
AS
BEGIN
SET NOCOUNT ON;
		UPDATE products.Products set GroupId = null WHERE IsGroupLocked = 0

		-- Actualizamos todos los productos que no Z, ni genéricos
		update p set GroupId = g.Id
		from (
			select * from products.Products 
			where (IsGroupLocked = 0)
				AND (IsGeneric = 0)
				--and (isnull(TotalRanking,'null') not in ('Z'))
				and (sku not in (select sku from products.vademecum))

		)p
		inner join products.Manufacturers m on p.ManufacturerId = m.Id
	    inner join users.SkuUsers su on p.SKU = su.SKU
		inner join (select * from products.Groups where LogisticAreaId IS NOT NULL AND RUT IS NOT NULL) g on p.LogisticAreaId = g.LogisticAreaId AND m.RUT = g.RUT AND g.Username = su.Username
		where m.OpenByManufacturer <> 1 /* Distribuir todo menos los SKU asignados a operadores logísticos. */

		/***********************************************************/

		update p set GroupId = g.Id
		--select *
		from (
			select * from products.Products 
			where (IsGroupLocked = 0)
			AND (IsGeneric = 0)
			--and (isnull(TotalRanking,'null') not in ('Z'))
			and (sku not in (select sku from products.vademecum))
		)p
		INNER JOIN users.SkuUsers su on p.SKU = su.SKU
		INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id 
		INNER JOIN products.Groups g ON  m.RUT = g.RUT AND p.ManufacturerId = g.ManufacturerId AND p.LogisticAreaId = g.LogisticAreaId AND su.Username = g.Username
		WHERE m.OpenByManufacturer = 1 /* Distribuir los operadores Lógísticos*/


	DECLARE @ID_GES INT, @ID_GEN INT, @ID_GENCH INT, @ID_GENAN INT;

	SET @ID_GES   = (SELECT Id FROM products.Groups WHERE SpecialGroup = 'GES');
	SET @ID_GEN   = (SELECT Id FROM products.Groups WHERE SpecialGroup = 'GEN');
	SET @ID_GENCH = (SELECT Id FROM products.Groups WHERE SpecialGroup = 'GENCH');
	SET @ID_GENAN = (SELECT Id FROM products.Groups WHERE SpecialGroup = 'GENAN');
	

	/* Actualizar Grupo Genéricos */
	UPDATE a SET GroupId = @ID_GEN
		from (
			select * from products.Products 
			where IsGroupLocked = 0
				AND IsGeneric = 1
				AND ManufacturerId NOT IN (3,18,43)
		)a

		/* Actualizar Grupo Genéricos Chile*/
	UPDATE a SET GroupId = @ID_GENCH
		from (
			select * from products.Products 
			where IsGroupLocked = 0
				AND IsGeneric = 1
				AND ManufacturerId = 43
		)a

	/* Actualizar Grupo Genéricos Andrómaco*/
	UPDATE a SET GroupId = @ID_GENAN
		from (
			select * from products.Products 
			where IsGroupLocked = 0
				AND IsGeneric = 1
				AND ManufacturerId IN (3,18)
		)a

		/* Actualizar Grupo Ges*/
	UPDATE a SET GroupId = @ID_GES
		from (
			select p.* from products.Products p
			INNER JOIN products.Vademecum v on p.SKU = v.SKU
			WHERE IsGes = 1
		)a




END



