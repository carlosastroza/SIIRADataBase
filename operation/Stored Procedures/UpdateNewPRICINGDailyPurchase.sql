﻿-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2018-06-19
-- Description:	elige oocc a enviar
-- =============================================
CREATE PROCEDURE [operation].[UpdateNewPRICINGDailyPurchase]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--vacia tablas staging
	TRUNCATE TABLE [purchase].[PurchaseOrdersStaging]
	TRUNCATE TABLE [purchase].[PurchaseOrderDetailsStaging]

	DECLARE @date DATE = GETDATE()
	DECLARE @now DATETIME = GETDATE()

	--actualiza procesadas por el ERP
	UPDATE po SET WasProcessed=1 --select *
	FROM [purchase].[PurchaseOrders] po
	INNER JOIN [purchase].[PurchaseOrderDetails] pod ON pod.PurchaseOrderId=po.Id 
	INNER JOIN [purchase].[SentPurchaseOrders] spo ON spo.Nro_oc_pricing=po.Id AND spo.SKU=pod.SKU and po.Store is NULL 
	WHERE  cast(CreatedDate as date) =@date --creadas hoy
		and EXISTS (
			--compras ya hechas
			SELECT * 
			FROM series.DailyPurchaseOrders
			WHERE IdOrigen=po.Id and SKU=pod.SKU and isnull(Store,0)=isnull(po.Store,0) 
		)

	--llena las tablas staging
	insert into [purchase].[PurchaseOrdersStaging]  --	DECLARE @date DATE = GETDATE()
	SELECT distinct po.*
	FROM [purchase].[PurchaseOrders] po
	INNER JOIN [purchase].[PurchaseOrderDetails] pod ON pod.PurchaseOrderId=po.Id 
	LEFT JOIN (
		select Nro_oc_pricing, SKU, NULL as Store, Max(SentDate)SentDate
		from [purchase].[SentPurchaseOrders]
		group by Nro_oc_pricing, SKU
	) spo ON spo.Nro_oc_pricing=po.Id AND spo.SKU=pod.SKU and po.Store is NULL 
	WHERE  cast(CreatedDate as date) =@date --creadas hoy
		and NOT EXISTS (
			--compras nuevas
			SELECT * 
			FROM series.DailyPurchaseOrders
			WHERE IdOrigen=po.Id and SKU=pod.SKU and isnull(Store,0)=isnull(po.Store,0) 
		)
		AND (spo.SentDate IS NULL 
			/*OR DATEDIFF(hour, spo.SentDate, @now) >= 2 */  --compras enviadas e ignoradas en más de dos horas
			) 
		--AND pod.SKU IN (select SKU from setup.MarchaBlanca)
	
	insert into [purchase].[PurchaseOrderDetailsStaging]  --	DECLARE @date DATE = GETDATE()
	SELECT distinct pod.*
	FROM [purchase].[PurchaseOrders] po
	INNER JOIN [purchase].[PurchaseOrderDetails] pod ON pod.PurchaseOrderId=po.Id 
	LEFT JOIN (
		select Nro_oc_pricing, SKU, NULL as Store, Max(SentDate)SentDate
		from [purchase].[SentPurchaseOrders]
		group by Nro_oc_pricing, SKU
	) spo ON spo.Nro_oc_pricing=po.Id AND spo.SKU=pod.SKU and po.Store is NULL 
	WHERE  cast(CreatedDate as date) =@date --creadas hoy
		and NOT EXISTS (
			--compras nuevas
			SELECT * 
			FROM series.DailyPurchaseOrders
			WHERE IdOrigen=po.Id and SKU=pod.SKU and isnull(Store,0)=isnull(po.Store,0) 
		) 
		AND (spo.SentDate IS NULL 
			/*OR DATEDIFF(hour, spo.SentDate, @now) >= 2 */  --compras enviadas e ignoradas en más de dos horas
			) 
		--AND pod.SKU IN (select SKU from setup.MarchaBlanca)
		

END
