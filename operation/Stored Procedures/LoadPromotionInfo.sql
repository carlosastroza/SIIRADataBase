﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-05-28
-- Description:	Carga la información si un producto está en promoción o no o si estará en promoción dado lo informado
-- =============================================
CREATE PROCEDURE operation.LoadPromotionInfo
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
   TRUNCATE TABLE products.PromotionInfo

   INSERT INTO products.PromotionInfo (SKU, IsInPromotion, WillBeInPromotion)
   SELECT p.SKU, 
		CAST(CASE WHEN EXISTS(
			select ProductId 
			from Salcobrand.promotion.PromotionStock ps 
			where ProductId = p.SKU AND GETDATE() BETWEEN BeginDate AND EndDate
		) THEN 1 ELSE 0 END AS BIT) IsInPromotion
		, CAST(CASE WHEN EXISTS(
			select ProductId
			from Salcobrand.promotion.PromotionStock 
			where ProductId = p.SKU AND BeginDate > GETDATE()
		) THEN 1 ELSE 0 END AS BIT) WillBeInPromotion
	FROM products.Products p


END
