﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-08
-- Description:	Carga los manufacturers
-- =============================================
CREATE PROCEDURE [operation].[LoadManufacturers]
	
AS
BEGIN
	
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga los manufacturers, SP [operation].[LoadManufacturers]', host_name(), 'System', 'POST')

	 BEGIN TRY
		MERGE INTO products.Manufacturers as d
		USING ( 
			SELECT *, 3 MonthsForFillRate
			FROM Salcobrand.products.Manufacturers
		) AS o
		on o.Id = d.Id
		WHEN MATCHED THEN
		UPDATE SET d.[Name] = o.[Name], d.RUT = o.RUT
		WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Id], [Name], [RUT], [FillRateMonths])
		VALUES (o.Id, o.[Name], o.RUT, o.MonthsForFillRate)
		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
		
	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló carga de los manufacturers, SP [operation].[LoadManufacturers], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
	

END

