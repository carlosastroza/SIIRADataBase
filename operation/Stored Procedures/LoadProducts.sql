﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-09
-- Description:	Carga los SKU
-- =============================================
CREATE PROCEDURE [operation].[LoadProducts]
AS
BEGIN
	
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga SKU, SP [operation].[LoadProducts]', host_name(), 'System', 'POST')

	BEGIN TRY
			MERGE INTO products.Products d
			USING (
				SELECT
					p.SKU, p.[Name], p.IsGes, CASE WHEN fg.SKU IS NOT NULL THEN 1 ELSE 0 END IsGeneric, fg.GenericFamilyId
					, ManufacturerId, WMSZone,TotalRanking, MotiveId,Motive,CategoriaUnificadaDesc
				FROM (
					select p.SKU, [Name],GES IsGes, TotalRanking, ManufacturerId, pa.WMSZone,MotiveId,Motive, CategoriaUnificadaDesc 
					from Salcobrand.products.Products p
					LEFT JOIN Salcobrand.products.PurchaseAreas pa ON p.sku = pa.sku
					WHERE (MotiveId NOT IN (1,2,3,91))
					AND (p.SKU NOT IN (select SKUTira from Salcobrand.products.CajaTira))				
					AND (ISNULL(TotalRanking,'null') <> 'Z')
					AND (ManufacturerId <> 330)				
					UNION 
					select fp.SKU, fp.[Description], fp.Ges, TotalRanking, ManufacturerId, pa.WMSZone, MotiveId,Motive,CategoriaUnificadaDesc
					from Salcobrand.products.GenericFamilyDetail fd
					inner join Salcobrand.products.FullProducts fp on fp.SKU = fd.SKU 
					LEFT JOIN Salcobrand.products.PurchaseAreas pa ON fp.sku = pa.sku
					WHERE (MotiveId NOT IN (1,2,3,91)) 
					AND (fp.SKU NOT IN (select SKUTira from Salcobrand.products.CajaTira))
					AND (ISNULL(TotalRanking,'null') <> 'Z')
					AND (ManufacturerId <> 330)
					/* Incluir TODOS los productos del Vademecúm*/
					UNION
					SELECT v.SKU, fp.Description, fp.Ges, TotalRanking, ManufacturerId,pa.WMSZone, MotiveId,Motive,CategoriaUnificadaDesc
					FROM products.Vademecum v
					INNER JOIN Salcobrand.Products.FullProducts fp ON v.SKU = fp.SKU
					LEFT JOIN Salcobrand.products.PurchaseAreas pa ON fp.sku = pa.sku
					/* END */
                    
                    UNION
                    SELECT fp.SKU, fp.[Description], fp.Ges, TotalRanking, ManufacturerId, ISNULL(pa.WMSZone, 'NA'), MotiveId,Motive,CategoriaUnificadaDesc 
                    FROM Salcobrand.products.FullProducts fp
					LEFT JOIN Salcobrand.products.PurchaseAreas pa ON fp.sku = pa.sku
                    WHERE fp.SKU IN (select SKUCaja from Salcobrand.products.CajaTira)
                        AND (MotiveId NOT IN (1,2,3,91))
					    AND (ISNULL(TotalRanking,'null') <> 'Z')
					    AND (ManufacturerId <> 330)
                        AND VigenteId = 999
                        -- AND GenericCategory = 9999


				) p
				LEFT join Salcobrand.products.GenericFamilyDetail fg ON p.SKU = fg.SKU
			) o ON d.SKU = o.SKU
			WHEN MATCHED THEN 
			UPDATE SET 
				d.[Name] = o.[Name], d.IsGes = o.IsGes, d.IsGeneric = o.IsGeneric
				, d.ManufacturerId = o.ManufacturerId, d.LogisticAreaId = o.WMSZone
				, d.TotalRanking = o.TotalRanking, d.GenericFamilyId = o.GenericFamilyId, d.MotiveId = o.MotiveId ,d.Motive = o.Motive, Category = CategoriaUnificadaDesc
			WHEN NOT MATCHED BY TARGET THEN
				INSERT ([SKU], [Name], [IsGes], [IsGeneric], [ManufacturerId], [LogisticAreaId], [TotalRanking], [GenericFamilyId], ServiceLevel,Category,MotiveId, Motive)
				VALUES (o.SKU, o.[Name], o.IsGes, o.IsGeneric, o.ManufacturerId, o.WMSZone, o.TotalRanking, o.GenericFamilyId, cast(0.95 as float),CategoriaUnificadaDesc,MotiveId, Motive)
			WHEN NOT MATCHED BY SOURCE THEN
				DELETE;

		
	



	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló carga de SKU, SP [operation].[LoadProducts], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
	
	BEGIN TRY
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Actualizando productos en grupos, SP [operation].[UpdateProductGroups]', host_name(), 'System', 'POST')
		EXEC [operation].[UpdateProductGroups]
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló actualizacion de productos en grupos, SP [operation].[UpdateProductGroups], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;	
	END CATCH
	

END

