﻿-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-03-14
-- Description:	Calcula la compra sugerida		--		
-- =============================================
CREATE PROCEDURE [operation].[GeneratePurchaseSuggested]
   @minFillrateParametro float = null,
   @FillrateParametro float = null,
	@grupo int=null,
	@Mid int=null,
	@sku int=null
AS
BEGIN
	SET NOCOUNT ON;

declare @minFillrate float = isnull(@minFillrateParametro,0.75)

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio cálculo de compra sugerida, SP [operation].[GeneratePurchaseSuggested]', host_name(), 'System', 'POST')

	-- update stock, transito y fill rate

	BEGIN TRY
		update a set a.Fillrate=b.FillRate
		from suggest.PurchaseSuggest a
		inner join setup.SuggestParameters b on b.SKU=a.SKU
		inner join products.Products pp on pp.SKU=a.SKU
		where (a.SKU=@sku or @sku is null)
		and (pp.ManufacturerId=@Mid or @Mid is null)
		and (pp.GroupId=@grupo or @grupo is null)

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló UPDATE FillRate en PurchaseSuggest  , SP [operation].[GeneratePurchaseSuggested], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH


	---- para genéricos se agrupa stock y tránsito del grupo. Se repite para cada sku del grupo para que sean cálculos independientes

		select b.SKU,case when isnull(x.stockcd,0)<0 then 0 else isnull(x.stockcd,0) end stockcd
		,case when isnull(x.activeorders,0)<0 then 0 else isnull(x.activeorders,0) end activeorders,g.GenericFamilyId
		into #laststockCD
		from setup.LastStock b
		inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=b.SKU
		left join (
			select g.GenericFamilyId,sum(b.StockCD) stockcd,sum(b.ActiveOrders) activeorders
			from setup.LastStock b 
			inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=b.SKU
			group by g.GenericFamilyId
		) x on x.GenericFamilyId=g.GenericFamilyId
		inner join products.Products pp on pp.SKU=b.SKU
		where (b.SKU=@sku or @sku is null)
		and (pp.ManufacturerId=@Mid or @Mid is null)
		and (pp.GroupId=@grupo or @grupo is null)
		order by g.GenericFamilyId

		-- transformación Caja-Tira
		select case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end SKU
		,sum(case when ct.SKUTira is null then ls.stockcd else ls.stockcd/conversion end) stockcd
		,sum(case when ct.SKUTira is null then ls.activeorders else ls.activeorders/conversion end) activeorders,GenericFamilyId
		into #laststockCD2
		from #laststockCD ls
		left join salcobrand.products.[CajaTira] ct on ct.SKUTira=ls.SKU
		group by case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end,GenericFamilyId


	BEGIN TRY
		update a set a.StockCD=b.StockCD,a.ActiveOrders=b.ActiveOrders
		from suggest.PurchaseSuggest a
		inner join #laststockCD2 b on b.sku=a.sku
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló UPDATE StockCD,ActiveOrders  en PurchaseSuggest  , SP [operation].[GeneratePurchaseSuggested], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	--- para no genéricos

	BEGIN TRY

		-- transformación Caja-Tira
		select case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end SKU
		,sum(case when isnull(ls.stockcd,0)<0 then 0 else (case when ct.SKUTira is null then isnull(ls.stockcd,0) else isnull(ls.stockcd,0)/conversion end) end) stockcd
		,sum(case when isnull(ls.activeorders,0)<0 then 0 else (case when ct.SKUTira is null then isnull(ls.activeorders,0) else isnull(ls.activeorders,0)/conversion end) end) activeorders
		into #laststockCD3
		from setup.LastStock ls
		left join salcobrand.products.[CajaTira] ct on ct.SKUTira=ls.SKU
		group by case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end
		order by case when ct.SKUTira is null then ls.SKU else ct.SKUCaja end

		update a set a.StockCD=b.StockCD,a.ActiveOrders=b.ActiveOrders
		from suggest.PurchaseSuggest a
		inner join #laststockCD3 b on b.sku=a.sku
		left join Salcobrand.products.GenericFamilyDetail g on g.SKU=a.SKU
		inner join products.Products pp on pp.SKU=a.SKU
		where g.sku is null and (a.SKU=@sku or @sku is null)
		and (pp.ManufacturerId=@Mid or @Mid is null)
		and (pp.GroupId=@grupo or @grupo is null)
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló UPDATE StockCD,ActiveOrders  en PurchaseSuggest  para no genéricos , SP [operation].[GeneratePurchaseSuggested], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
	    update s set Purchase = case when (isnull(Suggest,0)-isnull(ActiveOrders,0)-isnull([StockCD],0))/(case when isnull(@FillrateParametro,isnull([FillRate],1))<@minFillrate 
		then @minFillrate else isnull(@FillrateParametro,isnull([FillRate],1)) end)<0 then 0 
		else (isnull(Suggest,0)-isnull(ActiveOrders,0)-isnull([StockCD],0))/(case when isnull(@FillrateParametro,isnull([FillRate],1))<@minFillrate then @minFillrate else isnull(@FillrateParametro,isnull([FillRate],1)) end) 
	    end 
		FROM suggest.PurchaseSuggest s
	  	
		-----------------------------------------------------
		-------------------Filtro promociones----------------
		-----------------------------------------------------
 
		-----Identificación de los sku´s en promoción dentro de la sugerencia de compra
 
		  select distinct  ps.SKU
			,ps.PurchaseDate
		  into #skusPromocion
		  from [suggest].[PurchaseSuggest] ps
		  inner join [Salcobrand].[promotion].[PromotionStock] p on ps.SKU=p.ProductId
		  where PurchaseDate between dateadd(dy,-30, BeginDate) and dateadd(dy,-10,EndDate)
		  order by ps.SKU

		-----Incorporación de los sku´s caja para cada sku tira en promoción

		  insert into #skusPromocion
		  select ct.SKUCaja, p.PurchaseDate
		  from #skusPromocion p
		  inner join salcobrand.products.[CajaTira] ct on ct.SKUTira=p.SKU
		  where ct.SKUCaja not in (select distinct sku from #skusPromocion)

		-----Corrección del sugerido de compra para los sku´s en promoción 

		  update ps set ps.purchase=0
		  from [suggest].[PurchaseSuggest] ps
		  where ps.SKU in (select sku from #skusPromocion)

		-----Respaldo de los productos en promoción

			truncate table [suggest].[PromotionProducts]
			insert into [suggest].[PromotionProducts]
			select PurchaseDate
			,SKU 
			from #skusPromocion		
			
		  -- Compra sugerida cero para Ranking E
		  update s set s.purchase=0
		  from suggest.PurchaseSuggest s
		  inner join products.Products pp on pp.SKU=s.SKU
		  where pp.TotalRanking in ('E')

		-- Por lo menos comprar faltante + venta promedio*days to cover {restando stock CD y Transito a CD (falta hacerlo)} para los SKU sin modelo
		update s set Purchase= CASE WHEN (s.Shortage IS NULL and id.venta_promedio IS NULL) THEN NULL ELSE 
		(CASE 
		WHEN (isnull(s.Shortage,0)+isnull(id.venta_promedio,0)*s.daystocover-s.StockCD-s.ActiveOrders)>0 
		THEN (isnull(s.Shortage,0)+isnull(id.venta_promedio,0)*s.daystocover-s.StockCD-s.ActiveOrders) ELSE 0 END) END/(case when isnull(@FillrateParametro,isnull([FillRate],1))<@minFillrate 
		then @minFillrate else isnull(@FillrateParametro,isnull([FillRate],1)) end)

		from suggest.PurchaseSuggest s
		inner join products.Products pr on pr.sku=s.sku
		left join reports.InventoryDays id on id.sku=s.sku
		left join [suggest].[PromotionProducts] pp on pp.sku=s.sku
		where suggest is null and pp.sku is null and pr.TotalRanking not like 'E'
		and s.SKU not in (Select SKU from salcobrand.products.GenericFamilyDetail where sku not in (select sku from  [products].[GenericTenderedSKU]))

		
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló UPDATE Purchase en PurchaseSuggest  , SP [operation].[GeneratePurchaseSuggested], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		  update ps set ps.Purchase=r.UnidadesTotales
		  ,ps.BuyingMultiple=r.MultiploUnidadLogistica
		  ,ps.ConveniencePack=lp.ConveniencePack
		  ,ps.[ConveniencePack%]=lp.[ConveniencePack%]
		  from suggest.PurchaseSuggest ps
		  inner join [setup].[LogisticParameters] lp on lp.sku=ps.sku
		  cross apply [function].[RoundLogisticUnits] (ps.Purchase, null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet,lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
		  inner join products.Products pp on pp.SKU=ps.SKU
		  where (ps.SKU not in (SELECT distinct [SKU] FROM [products].[Vademecum])) 
		  and (ps.SKU=@sku or @sku is null)
		  and (pp.ManufacturerId=@Mid or @Mid is null)
		  and (pp.GroupId=@grupo or @grupo is null)
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló UPDATE Purchase,BuyingMultiple,ConveniencePack,ConveniencePack% en PurchaseSuggest  , SP [operation].[GeneratePurchaseSuggested], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
	
END
