﻿
-- =============================================
-- Author:		Daniela Albornoz
-- Create date: 2018-06-11
-- Description:	Archivo que se carga de OC cada media hora.
-- =============================================
CREATE PROCEDURE [operation].[GetNewPurchaseOrderDetails]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @date DATE = GETDATE()
	DECLARE @now datetime = GETDATE()

    -- Insert statements for procedure here
	SELECT distinct RUT, c.Id as Nro_oc_pricing, right(concat('000000',convert(nvarchar(10),SKU)),7)SKU
		, Quantity as Cantidad
		, convert(nvarchar(20),CreatedDate,112) as Fecha_emi, convert(nvarchar(20),ReceptionDate,112) as Fecha_recep
		, 'UN' as Unidad
		, @now SentDate
	INTO #aux
	FROM [purchase].[PurchaseOrderDetailsStaging] d 
	inner join [purchase].[PurchaseOrdersStaging] c on c.id=d.PurchaseOrderId
	WHERE CreatedDate=@date
		and Quantity>0

	-- guarda registro de la información enviada
	INSERT INTO purchase.SentPurchaseOrders
	SELECT *
	FROM #aux
	
	--info a enviar
	SELECT RUT, Nro_oc_pricing, SKU, Cantidad, Fecha_emi, Fecha_recep, Unidad
	FROM #aux

	drop table #aux
END
