﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-08-15
-- Description:	Versión liviana del GeneratePurchaseFull para ser ejecutado durante el día
-- =============================================
CREATE PROCEDURE operation.GeneratePurchaseLite
AS
BEGIN
	SET NOCOUNT ON;
    
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Trace', 'Salcobrand.SIIRA','Inicio de proceso LITE de Actualización de Compras, SP [operation].[GeneratePurchaseLite]', host_name(), 'System', 'POST')

	DECLARE @msg VARCHAR(100) 
	DECLARE @date DATE = GETDATE();

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 1 LoadLastStock' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec operation.LoadLastStock @date;-- 0
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 1 (LoadLastStock) , SP [operation].[GeneratePurchaseFull], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 2 LoadPriceAndCost' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[LoadPriceAndCost]
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 2 (LoadPriceAndCost) , SP [operation].[GeneratePurchaseLite], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 3 LoadPriceAndCostGES' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[LoadPriceAndCost]
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 3 (LoadPriceAndCostGES) , SP [operation].[GeneratePurchaseLite], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

	BEGIN TRY
		SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Paso 4 GeneratePurchaseSuggested' 
		RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
		exec [operation].[GeneratePurchaseSuggested] null,null,null,null,null -- 0

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló Paso 4 (GeneratePurchaseSuggested) , SP [operation].[GeneratePurchaseLite], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
END
