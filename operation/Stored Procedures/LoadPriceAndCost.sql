﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-19
-- Description:	Carga el precio y costo vigente de un producto.
-- =============================================
CREATE PROCEDURE [operation].[LoadPriceAndCost]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga el precio y costo vigente de un producto., SP [operation].[LoadPriceAndCost]', host_name(), 'System', 'POST')

    BEGIN TRY 

		MERGE INTO setup.PriceAndCost as pc
		USING 
		(
			SELECT p.SKU, ISNULL(pr.Price, 0) Price, pc.FinalPrice Cost
			FROM products.Products p
			INNER JOIN setup.PriceMatrixDetail pc ON  p.ManufacturerId = pc.ManufacturerId AND p.SKU = pc.SKU  AND pc.IdZona = 37
			LEFT JOIN (
				SELECT * FROM (
					SELECT SKU, Price, DAte, ROW_NUMBER() OVER (partition by SKU order by Date DESC ) R  
					FROM Salcobrand.series.PriceAndCost
				) w WHERE w.R = 1
			) pr ON p.SKU = pr.SKU
		) AS m
		ON pc.SKU = m.SKU
		WHEN MATCHED THEN
		UPDATE SET pc.Cost = m.Cost
		WHEN NOT MATCHED BY TARGET THEN
		INSERT ([SKU], [Price], [Cost]) 
		VALUES (m.SKU, m.Price, m.Cost)
		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
		
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló carga el precio y costo vigente de un producto, SP [operation].[LoadPriceAndCost], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
	

END
