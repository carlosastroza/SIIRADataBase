﻿-- =============================================
-- Author:		Esteban Cea
-- Create date: 2018-07-30
-- Description:	Calcula los días de alcance de cada producto y los guarda en una tabla para su posterior acceso
-- Modificación Fairenes Zerpa 2018-08-09 (FIT)
-- =============================================
CREATE PROCEDURE [operation].[CalculateInventoryDays]
AS
BEGIN
	SET NOCOUNT ON;

		declare @date date = getdate()

		declare @semanas int = datediff(week, dateadd(month,-15,@date), @date)

		--análisis de efecto promocional. Primero se identifican los casos con mucha promocion para ser ignorados. 
		--Posteriormente, se identifican los SKU-semana a ignorar en los promedios
		select SKU, count(*) semanasProm
		into #skus
		from ( 
			select  Salcobrand.dbo.GetMonday(Fecha)Wk, SKU, count(*) H
			from Salcobrand.[view].PromotionView
			where Fecha >=  dateadd(month,-15,@date)
				and SKU not in (select sku from Salcobrand.products.GenericFamilyDetail) --- se define que genérico no tiene promociones
			group by Salcobrand.dbo.GetMonday(Fecha), SKU
			having count(*)>=4
		) p
		group by SKU
		having count(*) >= 0.60*@semanas

		select Salcobrand.dbo.GetMonday([date])Wk, SKU--, count(*) H
		into #ignorar
		from Salcobrand.forecast.DetectedPromotions
		where [Date] >=  dateadd(month,-15,@date)
			and SKU not in (select sku from Salcobrand.products.GenericFamilyDetail) --- se define que genérico no tiene promociones
		group by Salcobrand.dbo.GetMonday([date]), SKU
		having count(*)>=4

		CREATE INDEX #ix1 ON #ignorar (SKU, wk)


PRINT N'Iniciando carga de #Venta1: Venta de los últimos 3 meses del año actual'
select  s.SKU
		,sum(s.Quantity*c.Cost) VentaCost1 	-- Venta los 3 últimos meses año actual
		,c.Cost
	into #Venta1
	from [Salcobrand].[forecast].[WeeklySales] s
	inner join [Salcobrand].[pricing].[LastPriceAndCost] c on c.SKU=s.SKU
	where s.date>=dateadd(month,-3,@date)
		and not exists (
			SELECT * 
			FROM #ignorar
			WHERE SKU=s.SKU and Wk=s.[Date]
		)
	group by  s.sku, c.Cost
	--select * from #Venta1

PRINT N'Iniciando carga de #Venta2: Venta de los últimos 3 meses del año anterior'
select  s.SKU
		,sum(s.Quantity*c.Cost) VentaCost2 -- Venta los 3 últimos meses año anterior
	into #Venta2
	from [Salcobrand].[forecast].[WeeklySales] s
	inner join [Salcobrand].[pricing].[LastPriceAndCost] c on c.SKU=s.SKU
	where s.date between dateadd(month,-15,@date) and dateadd(month,-12,@date) --and s.sku=30230
		and not exists (
			SELECT * 
			FROM #ignorar
			WHERE SKU=s.SKU and Wk=s.[Date]
		)
	group by s.sku
	--select * from #Venta2


PRINT N'Iniciando carga de #Venta3: Venta los 3 meses futuro del año anterior'
select  s.SKU
		,sum(s.Quantity*c.Cost) VentaCost3 -- Venta los 3 meses futuro año anterior
		into #Venta3
	from [Salcobrand].[forecast].[WeeklySales] s
	inner join [Salcobrand].[pricing].[LastPriceAndCost] c on c.SKU=s.SKU
	where s.date between dateadd(month,-12,@date) and dateadd(month,-9,@date) --and s.sku=30230
		and not exists (
			SELECT * 
			FROM #ignorar
			WHERE SKU=s.SKU and Wk=s.[Date]
		)
	group by s.sku
	--select * from #Venta3
	
--VENTA GENERICOS--

PRINT N'Iniciando carga de #VentaG1: Venta de los últimos 3 meses del año actual para GENÉRICOS'		 
select  t.SKU
		,g.GenericFamilyId
		,case when count (distinct s.[date])>=70 then sum(s.Quantity*c.Cost) else NULL end VentaCost1
		,count (distinct s.[date]) Days -- Venta los 3 últimos meses año actual
		into #VentaG1
	from [Salcobrand].[series].[dailySales] s
	inner join [Salcobrand].[pricing].[LastPriceAndCost] c on c.SKU=s.SKU
	inner join (select SKU, GenericFamilyId from Salcobrand.products.GenericFamilyDetail) g on g.SKU=s.SKU
	inner join (select * from products.GenericTenderedSKU) t on t.GenericFamilyId=g.GenericFamilyId 
	where s.date>=dateadd(month,-3,@date) 
	group by t.sku,g.GenericFamilyId
	--select * from #VentaG1


PRINT N'Iniciando carga de #VentaG2: Venta de los últimos 3 meses del año anterior para GENÉRICOS'
select  t.SKU
		,g.GenericFamilyId
		,case when count (distinct s.[date])>=70 then sum(s.Quantity*c.Cost) else NULL end VentaCost2
		,count (distinct s.[date]) Days -- Venta los 3 ultimos meses año anterior
		into #VentaG2
	from [Salcobrand].[series].[dailySales] s
	inner join [Salcobrand].[pricing].[LastPriceAndCost] c on c.SKU=s.SKU
	inner join (select SKU, GenericFamilyId from Salcobrand.products.GenericFamilyDetail) g on g.SKU=s.SKU
	inner join (select*from products.GenericTenderedSKU) t on t.GenericFamilyId=g.GenericFamilyId 
	where s.date between dateadd(month,-15,@date) and dateadd(month,-12,@date)
	group by t.sku,g.GenericFamilyId
	--select * from #VentaG2


PRINT N'Iniciando carga de #VentaG3: Venta los 3 próximos meses año anterior para GENÉRICOS'
select  t.SKU
		,g.GenericFamilyId
		,case when count (distinct s.[date])>=70 then sum(s.Quantity*c.Cost) else NULL end VentaCost3
		,count (distinct s.[date]) Days -- Venta los 3 próximos meses año anterior
		into #VentaG3
	from [Salcobrand].[series].[dailySales] s
	inner join [Salcobrand].[pricing].[LastPriceAndCost] c on c.SKU=s.SKU
	inner join (select SKU, GenericFamilyId from Salcobrand.products.GenericFamilyDetail) g on g.SKU=s.SKU
	inner join (select*from products.GenericTenderedSKU) t on t.GenericFamilyId=g.GenericFamilyId 
	where s.date between dateadd(month,-12,@date) and dateadd(month,-9,@date)
	group by t.sku,g.GenericFamilyId
	--select * from #VentaG3

---------------------------------------------------------------------------------------------------------------------------------------------------


TRUNCATE TABLE reports.InventoryDays

INSERT INTO reports.InventoryDays
select da.sku
	, da.DivisionUnificadaDesc
	, da.CategoriaUnificadaDesc
	, da.ManufacturerDesc
	, da.StockTotal
	, da.Dias_Alcance
	, case 
		when da.PromPronóstico is not null and da.PromPronóstico <> 0 then da.PromPronóstico  
		when da.PromPronóstico = 0 and VtaProm1 is not null then cast(VtaProm1 as float)  
		else VtaProm2 
	  end Venta_Promedio
	  	, case 
		when da.PromPronóstico is not null and da.PromPronóstico <> 0 then 'PromPronóstico' 
		when da.PromPronóstico = 0 and VtaProm1 is not null then 'VtaProm1'  
		else 'VtaProm2' 
	  end Verificador
from (
	Select f.SKU
		,f.DivisionUnificadaDesc
		,f.CategoriaUnificadaDesc
		,f.ManufacturerDesc
		,f.StockTotal
		,case when f.DíasAlcance is not null then f.DíasAlcance else (f.StockTotal)/nullif((s.VtaProm),0) end Dias_Alcance
		,case when  f.costo <> 0 then cast(f.VentaCost/nullif((f.Costo*84),0) as float) else NULL end VtaProm1
		,s.VtaProm VtaProm2
		,f.PrónosticoUn/nullif(f.[Días c/FIT],0) PromPronóstico 
	from (
		Select a.SKU
			, a.DivisionUnificadaDesc
			,a.CategoriaUnificadaDesc
			,a.ManufacturerDesc
			,a.StockTotal
			,a.FIT PrónosticoUn
			,a.[Días c/FIT]
			,b.Venta VentaCost
			,a.cost Costo
			,case when Días_Inventario is not null then Días_Inventario else 84*a.StockTotal*a.Cost/NULLIF(b.Venta,0) end DíasAlcance
		from (
			Select t1.sku
				,t1.DivisionUnificadaDesc
				,t1.CategoriaUnificadaDesc
				,t1.ManufacturerDesc
				,t1.[Días c/FIT]
				,t1.Días
				,case when t1.[Días c/FIT] <= t1.Días*0.75 Then 0 else t1.FIT end FIT
				,t1.StockTotal
				,t1.Cost
				,t1.Días*(t1.cost*t1.StockTotal)/NULLIF((case when t1.[Días c/FIT] <= t1.Días*0.75 Then 0 else t1.FIT end)*t1.Cost,0) Días_Inventario
			from (
				select p.sku
					,p.DivisionUnificadaDesc
					,p.CategoriaUnificadaDesc
					,p.ManufacturerDesc
					,sum(case when fitcorregido is not null and fitcorregido>=0 then fitcorregido else 0 end) FIT --sum(isnull(fitcorregido,0)) FIT 
					,sum(case when fit is not null and fit>=0 then 1 else 0 end) [Días c/FIT] -- Que no sean Fit Negativos
					,(case when ms.Stock>=0 then ms.Stock else 0 end)+(case when ms.StockTransito>=0 then ms.StockTransito else 0 end)+(case when sc.StockCD>=0 then sc.StockCD else 0 end) StockTotal
					,pc.Cost
					,count(df.Fecha) [Días]
				from Salcobrand.products.Products p
				left join  salcobrand.forecast.DailyForecast df on p.SKU = df.sku
				left join (
					select * 
					from salcobrand.reports.MetricasStocks
					where date = dateadd(day,-1,@date)
				) ms on ms.sku=df.SKU
				left join (
					select * from salcobrand.pricing.LastPriceAndCost
				) pc on pc.sku=df.SKU
				left join (
					select * 
					from salcobrand.series.StockCD 
					where date = dateadd(day,-1,@date)
				)sc on sc.sku=df.SKU
				where fecha between @date and dateadd(month,3,@date)
					and p.sku not in (select sku from salcobrand.products.GenericFamilyDetail)
					--and p.TotalRanking not in ('E','Z')
				group by p.sku,p.CategoriaUnificadaDesc,p.DivisionUnificadaDesc,p.manufacturerdesc,pc.Cost,ms.Stock,ms.StockTransito,sc.StockCD
			) t1
		) a
		left join (
			SELECT p.sku
				, p.DivisionUnificadaDesc
				,p.CategoriaUnificadaDesc
				,p.ManufacturerDesc
				,(v1.ventacost1*v3.ventacost3)/nullif(v2.ventacost2,0) Venta
			FROM [Salcobrand].products.Products p
			left join #Venta1 v1 on v1.SKU=p.SKU
			left join #Venta2 v2 on v2.SKU=p.SKU and v2.SKU=p.SKU
			left join #Venta3 v3 on v3.SKU=p.SKU and v3.sku=v2.sku and v3.sku=v1.sku
			Where (v1.ventacost1*v3.ventacost3)/nullif(v2.ventacost2,0) is not null
			) b on b.SKU=a.sku
		where a.Cost is not null
        
        UNION

        Select a.SKU
            , a.DivisionUnificadaDesc
            ,a.CategoriaUnificadaDesc
            ,a.ManufacturerDesc
            ,a.StockTotal
            ,a.FIT PrónosticoUn
            ,a.[Días c/FIT]
            ,b.Venta VentaCost
            ,a.cost Costo
            ,case when Días_Inventario is not null then Días_Inventario else 84*a.StockTotal*a.Cost/NULLIF(b.Venta,0) end DíasAlcance
        from (
            Select t1.sku
                ,t1.DivisionUnificadaDesc
                ,t1.CategoriaUnificadaDesc
                ,t1.ManufacturerDesc
                ,t1.[Días c/FIT]
                ,t1.Días
                ,case when t1.[Días c/FIT] <= t1.Días*0.75 Then 0 else t1.FIT end FIT
                ,t1.StockTotal
                ,t1.Cost
                ,t1.Días*(t1.cost*t1.StockTotal)/NULLIF((case when t1.[Días c/FIT] <= t1.Días*0.75 Then 0 else t1.FIT end)*t1.Cost,0) Días_Inventario
            from (
				select p.sku
					,p.DivisionUnificadaDesc
					,p.CategoriaUnificadaDesc
					,p.ManufacturerDesc
					,sum(case when fitcorregido is not null and fitcorregido>=0 then fitcorregido else 0 end) FIT --sum(isnull(fitcorregido,0)) FIT 
					,sum(case when fit is not null and fit>=0 then 1 else 0 end) [Días c/FIT] -- Que no sean Fit Negativos
					,(case when ms.Stock>=0 then ms.Stock else 0 end)+(case when ms.StockTransito>=0 then ms.StockTransito else 0 end)+(case when sc.StockCD>=0 then sc.StockCD else 0 end) StockTotal
					,pc.Cost
					,count(df.Fecha) [Días]
				from Salcobrand.products.Products p
				left join  salcobrand.forecast.DailyForecast df on p.SKU = df.sku
				left join (
					select t.sku,g.GenericFamilyId, sum(m.Stock) Stock,sum(m.StockTransito) StockTransito  
					from salcobrand.reports.MetricasStocks m
					inner join (
						select SKU, GenericFamilyId from Salcobrand.products.GenericFamilyDetail
					) g on g.SKU=m.SKU
					inner join (
						select*from products.GenericTenderedSKU
					) t on t.GenericFamilyId=g.GenericFamilyId 
					where date = dateadd(day,-1,@date) 
					group by t.sku,g.GenericFamilyId
				) ms on ms.sku=df.SKU
				left join salcobrand.pricing.LastPriceAndCost pc on pc.sku=df.SKU
				left join (
					select t.sku,g.GenericFamilyId, sum(m.StockCD) StockCD  
					from salcobrand.series.stockcd m
					inner join (
						select SKU, GenericFamilyId from Salcobrand.products.GenericFamilyDetail
					) g on g.SKU=m.SKU
					inner join products.GenericTenderedSKU t on t.GenericFamilyId=g.GenericFamilyId 
					where date = dateadd(day,-1,@date) 
					group by t.sku,g.GenericFamilyId
				)sc on sc.sku=df.SKU
				where fecha between @date and dateadd(month,3,@date)
					and p.sku in (select sku from products.GenericTenderedSKU)
					--and p.TotalRanking not in ('E','Z')
				group by p.sku,p.CategoriaUnificadaDesc,p.DivisionUnificadaDesc,p.manufacturerdesc,pc.Cost,ms.Stock,ms.StockTransito,sc.StockCD
			) t1
		) a
		left join (
			SELECT p.sku
				, p.DivisionUnificadaDesc
				,p.CategoriaUnificadaDesc
				,p.ManufacturerDesc
			    ,case when vg2.ventacost2>=0 then (vg1.ventacost1*vg3.ventacost3)/nullif(vg2.ventacost2,0) else vg1.VentaCost1 end Venta
		 	FROM [Salcobrand].products.Products p
			left join #VentaG1 vg1 on vg1.SKU=p.SKU
			left join #VentaG2 vg2 on vg2.SKU=p.SKU and vg2.SKU=p.SKU
			left join #VentaG3 vg3 on vg3.SKU=p.SKU and vg3.sku=vg2.sku and vg3.sku=vg1.sku
			Where (vg1.ventacost1*vg3.ventacost3)/nullif(vg2.ventacost2,0) is not null
		) b on b.SKU=a.sku
		where a.Cost is not null
	) f
	left join (
		select sku, sum(cast(Quantity as float))/nullif(365,0) VtaProm 
		from salcobrand.series.dailySales 
		where date >= dateadd(year,-1,@date)						
			and sku in (select sku from salcobrand.products.Products)
		group by sku
	) s on s.SKU=f.SKU  
) da
ORDER BY Dias_Alcance

drop table #skus
drop table #ignorar
drop table #Venta1
drop table #Venta2
drop table #Venta3
drop table #VentaG1
drop table #VentaG2
drop table #VentaG3

END
