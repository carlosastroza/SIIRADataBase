﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-14
-- Description:	Carga las unidades logisticas para la compra
-- =============================================
CREATE PROCEDURE [operation].[LoadLogisticParameters]
	
AS
BEGIN
	
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga las unidades logisticas para la compra, SP [operation].[LoadLogisticParameters]', host_name(), 'System', 'POST')

	TRUNCATE TABLE setup.LogisticParameters
	
	BEGIN TRY
		INSERT INTO setup.LogisticParameters ([SKU], [UnitsInBox], [UnitsInLayer], [UnitsInPallet], [RoundToBoxes], [RoundToLayers], [RoundToPallets], [ConveniencePack], [ConveniencePack%], [RoundThreshold], [BuyingMultiple])
		select
			COALESCE(s.SKU, cd.SKU) SKU
			, ISNULL(s.Units, 0) UnitsInBox
			, CASE WHEN ISNULL(s.Units, 0) = 0 THEN 0 ELSE ISNULL(cd.UnidadesxCamada, 0)  END UnitsInLayer
			, CASE WHEN ISNULL(s.Units, 0) = 0 THEN 0 ELSE ISNULL(cd.UnidadesxPallet, 0)  END UnitsInPallet
			, CASE WHEN ISNULL(cd.BuyingMultiple, 0) = 0 THEN 1 ELSE 0 END RoundToBoxes
			, CASE WHEN ISNULL(cd.BuyingMultiple, 0) > 0 AND cd.BuyingMultiple = cd.UnidadesxCamada THEN 1 ELSE 0 END RoundToLayer
			, CASE WHEN ISNULL(cd.BuyingMultiple, 0) > 0 AND cd.BuyingMultiple = cd.UnidadesxPallet THEN 1 ELSE 0 END RoundToPallet
			, CASE WHEN ISNULL(cd.ConveniencePack, 0) > 0 AND ISNULL(cd.[ConveniencePack%], 0) > 0 THEN cd.ConveniencePack ELSE NULL END ConveniencePack
			, CASE WHEN ISNULL(cd.ConveniencePack, 0) > 0 AND ISNULL(cd.[ConveniencePack%], 0) > 0 THEN cd.[ConveniencePack%] ELSE NULL END [ConveniencePack%]
			, 0 RoundThreshold
			, CASE WHEN cd.BuyingMultiple = 0 THEN NULL ELSE cd.BuyingMultiple END BuyingMultiple
		from Salcobrand.products.DUNStore s
		full join Salcobrand.products.DUNCD cd ON s.SKU = cd.SKU 
		where COALESCE(s.SKU, cd.SKU) IN (select SKU from products.products)
		
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Proceso de carga las unidades logisticas para la compra culminado con éxio, SP [operation].[LoadLogisticParameters]', host_name(), 'System', 'POST')


	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló carga de las unidades logisticas para la compra, SP [operation].[LoadLogisticParameters], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH
	

END
