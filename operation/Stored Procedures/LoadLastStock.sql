﻿-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-03-14
-- Description:	Llena los datos de stock y tránsito		--		
-- =============================================
CREATE PROCEDURE [operation].[LoadLastStock]
	@date DATE
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Trace', 'Salcobrand.SIIRA','Inicio de carga de stock y tránsito, SP [operation].[LoadLastStock]', host_name(), 'System', 'POST')

	BEGIN TRY

		MERGE INTO setup.LastStock AS ls
		USING (
			select scd.SKU, StockCD, ISNULL(po.activeOrders, 0) ActiveOrders
			from (
				select * from [Salcobrand].series.StockCD 
				WHERE [Date] = DATEADD(DAY, -1, CAST(@date AS DATE))
			) scd
			left join (
				SELECT a.SKU, SUM(UnidadesCompra) as activeOrders
				FROM (
					select Id, FechaExpiracionActual, SKU, UnidadesCompra
					FROM series.PurchaseOrders
					where	EstadoOOCC = 2 
						AND UnidadesRecibidas = 0 
						AND UnidadesCompra > 0
						AND Store = 996
					UNION
					SELECT Id, FechaExpiracionActual, SKU, UnidadesCompra
					FROM series.DailyPurchaseOrders
					WHERE	EstadoOOCC = 2
						AND UnidadesCompra > 0
						AND ISNULL(Store, 996) = 996
				) a
				inner join [setup].[SuggestParameters] b on b.SKU=a.SKU
				where a.FechaExpiracionActual between cast(@date as date) and dateadd(day,DaysToCover,@date)
				group by a.SKU
			) po ON scd.SKU = po.SKU
		) AS s ON ls.SKU = s.SKU
		WHEN MATCHED THEN
			UPDATE 
			SET ls.StockCD = s.StockCD, ls.ActiveOrders = s.ActiveOrders
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (SKU, StockCD, ActiveOrders)
			VALUES (s.SKU, s.StockCD, s.ActiveOrders)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló carga de LastStock, SP [operation].[LoadLastStock], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

END
