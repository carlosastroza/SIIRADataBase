﻿-- =============================================
-- Author:		Family Guy
-- Create date: 2018-07-31
-- Description:	Carga el precio y costo vigente de productos GES.
-- =============================================
CREATE PROCEDURE [operation].[LoadPriceAndCostGes]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Trace', 'Salcobrand.SIIRA','Inicio de carga el precio y costo vigente de un producto GES, SP [operation].[LoadPriceAndCostGES]', host_name(), 'System', 'POST')

    BEGIN TRY 

		MERGE INTO setup.PriceAndCostGES AS pc
		USING (
			SELECT v.SKU, ISNULL(pr.Price, 0) Price, pc.FinalPrice Cost, V.IdZona
			FROM products.vademecum V
			inner join products.products p on v.sku = p.sku
			INNER JOIN setup.PriceMatrixDetail pc on V.SKU = PC.SKU AND P.MANUFACTURERID = PC.MANUFACTURERID AND v.idzona = pc.idzona
			LEFT JOIN (
				SELECT * FROM (
					SELECT SKU, Price, DAte, ROW_NUMBER() OVER (partition by SKU order by Date DESC ) R  
					FROM Salcobrand.series.PriceAndCost
				) w WHERE w.R = 1
			) pr ON p.SKU = pr.SKU
		) AS m ON pc.SKU = m.SKU AND pc.IdZona = m.IdZona
		WHEN MATCHED THEN 
			UPDATE SET pc.Price = m.Price,
					   pc.Cost = m.Cost,
					   pc.IdZona = m.IdZona
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (SKU, Price, Cost, IdZona)
			VALUES (m.SKU, m.Price, m.Cost, m.IdZona)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló carga el precio y costo vigente de un producto GES, SP [operation].[LoadPriceAndCostGES], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
		THROW;
	END CATCH

END
