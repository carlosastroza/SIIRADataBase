﻿-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-03-14
-- Description:	Calcula el fill rate de compras		--		
-- =============================================
CREATE PROCEDURE [operation].[CalculateFillRate]
	@group int = null,
	@ManuId int = null
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Calculando el fill rate de compras , SP [operation].[CalculateFillRate]', host_name(), 'System', 'POST')

	-- por SKU aplicando caja-tira
select case when ct.SKUTira is null then a.SKU else ct.SKUCaja end SKU
	,sum(case when ct.SKUTira is null then a.UnidadesCompra else a.UnidadesCompra/conversion end) UnidadesCompra
	,sum(case when ct.SKUTira is null then a.UnidadesRecibidas else a.UnidadesRecibidas/conversion end) UnidadesRecibidas
	, sum(case when ct.SKUTira is null then a.UnidadesRecibidas else a.UnidadesRecibidas/conversion end)
	/sum(case when ct.SKUTira is null then a.UnidadesCompra else a.UnidadesCompra/conversion end) FillRate
into #OC1
from series.PurchaseOrders a
inner join products.Manufacturers m on m.id=a.ManufacturerId
left join salcobrand.products.[CajaTira] ct on ct.SKUTira=a.sku
where FechaExpiracionActual <= dateadd(day,-1,getdate()) and a.Date between dateadd(month,-m.FillRateMonths,getdate()) and dateadd(day,-1,getdate())
	and EstadoOOCC=2 and Store=996 and a.UnidadesCompra>0
	and (m.Id=@ManuId or @ManuId is null)
	and (m.RUT in (select distinct RUT from [products].[Groups] where id = @group) or @group is null)
group by case when ct.SKUTira is null then a.SKU else ct.SKUCaja end

-- por Rut
select m.rut,sum(a.UnidadesCompra) UnidadesCompra,sum(a.UnidadesRecibidas) UnidadesRecibidas, sum(a.UnidadesRecibidas)/sum(a.UnidadesCompra) FillRate
into #OC2
from series.PurchaseOrders a
inner join products.Manufacturers m on m.rut=a.rut
where FechaExpiracionActual <= dateadd(day,-1,getdate()) and a.Date between dateadd(month,-m.FillRateMonths,getdate()) and dateadd(day,-1,getdate())
	and EstadoOOCC=2 and Store=996 and a.UnidadesCompra>0
	and (m.Id=@ManuId or @ManuId is null)
	and (m.RUT in (select distinct RUT from [products].[Groups] where id = @group) or @group is null)
group by m.rut

-- Si no hay por SKU, nos quedamos con la agrupación de Rut
truncate table aux.FillRate

insert into aux.FillRate
select p.sku,isnull(oc1.UnidadesCompra,oc2.UnidadesCompra) UnidadesCompra,isnull(oc1.UnidadesRecibidas,oc2.UnidadesRecibidas) UnidadesRecibidas,isnull(oc1.FillRate,oc2.FillRate) FillRate
from products.Products p
inner join products.Manufacturers m on m.id=p.manufacturerid
left join #OC1 oc1 on oc1.SKU=p.SKU
left join #OC2 oc2 on oc2.rut=m.rut
where (m.Id=@ManuId or @ManuId is null)
and (m.rut in (select distinct RUT from [products].[Groups] where id = @group) or @group is null)

---- para genéricos
--BEGIN TRY 

--	update p set p.FillRate=b.FillRate
--	from [setup].[SuggestParameters] p
--	inner join(
--		select b.SKU,x.UnidadesRecibidas/x.UnidadesCompra FillRate
--		from #OC b
--		inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=b.SKU
--		inner join (
--			SELECT g.GenericFamilyId,sum(UnidadesCompra) UnidadesCompra,sum(UnidadesRecibidas) UnidadesRecibidas
--			FROM #OC o
--			inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=o.SKU
--			group by g.GenericFamilyId
--		) x on x.GenericFamilyId=g.GenericFamilyId
--	) b on b.sku=p.sku
--END TRY 
--BEGIN CATCH
--	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
--	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló cálculo de fill rate compras para genéricos , SP [operation].[CalculateFillRate], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
--	THROW;

--END CATCH

-- para no genéricos
BEGIN TRY 
	update p set p.FillRate=b.FillRate
	from [setup].[SuggestParameters] p
	inner join aux.FillRate b on b.sku=p.sku
	--left join Salcobrand.products.GenericFamilyDetail g on g.SKU=p.SKU
	--where g.sku is null

END TRY 
BEGIN CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló cálculo de fill rate compras para sku no genéricos , SP [operation].[CalculateFillRate], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;
END CATCH
	
END
