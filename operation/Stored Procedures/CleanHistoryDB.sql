﻿-- =============================================
-- Author:		Dani Albornoz
-- Description: 2018-06-25
-- =============================================
CREATE PROCEDURE operation.CleanHistoryDB 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	DECLARE @today date = GETDATE()
	DECLARE @minDate date, @maxDate date
	DECLARE @msg NVARCHAR(100) 

	SET NOCOUNT ON;

	--tabla sent guarda un mes
    SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tabla [purchase].[SentPurchaseOrders] guarda un mes de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxdate = (SELECT MAX(SentDate) FROM [purchase].[SentPurchaseOrders])
    SET @minDate = DATEADD(MONTH, -1, @maxdate)

	DELETE FROM [purchase].[SentPurchaseOrders] WHERE cast(SentDate as date) < @minDate

	--tablas purchase orders y orders detail guardan 92 días
    SET @msg = ''+DATENAME(HH, getdate())+':'+  DATENAME(MINUTE, getdate())+' '+ 'Tablas [PurchaseOrders] y [PurchaseOrderDetails] guardan tres meses de historia' RAISERROR (@msg, 10, 0 ) WITH NOWAIT 
	SET @maxdate = (SELECT MAX(CreatedDate) FROM [purchase].[PurchaseOrders])
    SET @minDate = DATEADD(DAY, -92, @maxdate)

	DELETE d FROM [purchase].[PurchaseOrders] o
	INNER JOIN [purchase].[PurchaseOrderDetails] d ON d.PurchaseOrderId=o.ID
	WHERE o.CreatedDate < @minDate
	DELETE FROM [purchase].[PurchaseOrders] WHERE CreatedDate < @minDate
END
