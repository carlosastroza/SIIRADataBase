﻿-- =============================================
-- Author:		Gonzalo Cornejo
-- Create date: 2018-03-19
-- Description:	Calcula el sugerido de compra--		
-- =============================================
CREATE PROCEDURE [operation].[GenerateSuggestCD]
	@grupo int=null,
	@Mid int=null,
	@sku int=null
AS
BEGIN
	SET NOCOUNT ON;

declare @fechaCalc date = (select FechaCalculo from Salcobrand.aux.UltimoPronostico)
declare @Today Date = CONVERT(date, getdate())

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio cálculo Sugerido de Compra, SP [operation].[GenerateSuggestCD]', host_name(), 'System', 'POST')

	

	if(@sku is null and @Mid is null and @grupo is null)
	  begin
		truncate table [suggest].[PurchaseSuggest]
	  end
  
	else
		begin 
	
		  delete sp
		  from [setup].[SuggestParameters] SP
		  inner join products.Products pp on pp.SKU=sp.SKU
		  where (sp.SKU=@sku or @sku is null)
		  and (pp.ManufacturerId=@Mid or @Mid is null)
		  and (pp.GroupId=@grupo or @grupo is null)

	end

BEGIN TRY 
-- shortage 
		  SELECT ls.SKU,SUM(ls.suggest)-SUM(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) -SUM(ls.Transit)-SUM(ISNULL(d.Picking,0))  Shortage
		  into #shortage
		  FROM Salcobrand.series.LastStock ls 
			INNER JOIN Salcobrand.stores.Stores S on S.Store=ls.Store
			LEFT JOIN (SELECT sku,store,Picking FROM Salcobrand.series.DespachosCD WHERE date=DATEADD(day,-1,@Today)) d on d.SKU=ls.SKU and d.Store=ls.Store 
		  WHERE ls.suggest>(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) +ls.Transit+ISNULL(d.Picking,0)
		  GROUP BY ls.SKU

-- agrupar para genericos
	update p set p.Shortage=b.Shortage
	from #shortage p
	inner join(
		select g.sku,x.Shortage
		from #shortage b
		inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=b.SKU
		inner join (
			SELECT g.GenericFamilyId,min(Shortage) Shortage
			FROM #shortage o
			inner join Salcobrand.products.GenericFamilyDetail g on g.SKU=o.SKU
			group by g.GenericFamilyId
		) x on x.GenericFamilyId=g.GenericFamilyId
	) b on b.sku=p.sku
  
  SELECT SP.SKU, 
		ISNULL(SP.ServiceLevel,0.95) ServiceLevel,
		SP.DaysToCover DaysToCover,
		SP.PurchaseDate PurchaseDate,
		SP.NextPurchaseDate NextPurchaseDate,
		SP.LeadTime LeadTime,
		SUM(Case when Pedidos.Date>=SP.PurchaseDate and Pedidos.Date <= Dateadd(day,SP.DaysToCover,SP.PurchaseDate) then 
			(case when pedidoefectivo+isnull(Delta_Restricciones,0)<0 then 0 else pedidoefectivo+isnull(Delta_Restricciones,0) end) else 0 end) SBDemand,
		std.dvStd * Sqrt(SP.DaysToCover) std,
		CEILING([Salcobrand].[function].NormalProbability (
					SUM(CASE WHEN Pedidos.Date>=SP.PurchaseDate AND Pedidos.Date <= Dateadd(day,SP.DaysToCover,SP.PurchaseDate) THEN 
						(case when pedidoefectivo+isnull(Delta_Restricciones,0)<0 then 0 else pedidoefectivo+isnull(Delta_Restricciones,0) end) else 0 end),
					std.dvStd * SQRT(SP.DaysToCover),
					MAX(ISNULL(SP.ServiceLevel,0))
				))
			+ ISNULL(Shortage.Shortage,0) Suggest,
		ISNULL(Shortage.Shortage,0) Shortage,
		Null [Fillrate],
		Null [StockCD],
		Null [ActiveOrders],
		Null [BuyingMultiple],
		Null [ConveniencePack],
		Null [ConveniencePackPorc],
		Null [Purchase]
  INTO #auxSuggest
  FROM [setup].[SuggestParameters] SP
    INNER JOIN	products.Products pp ON pp.SKU=sp.SKU
    LEFT JOIN   [Salcobrand].[minmax].[DistributionOrdersForecastCurrent] Pedidos  ON SP.sku=Pedidos.sku
    LEFT JOIN (-- Subquery con el promedio de las desviaciones estandar por dia de la semana, cuando no son 0.
		SELECT a.Sku,avg(a.StdPedidos) dvStd
		FROM (
			SELECT SKU,DATEPART(dw,Date) dow,STDEV(case when pedidoefectivo+isnull(Delta_Restricciones,0)<0 then 0 else pedidoefectivo+isnull(Delta_Restricciones,0) end) StdPedidos,COUNT(*) Total 
			FROM [Salcobrand].[minmax].[DistributionOrdersForecastCurrent] 
			GROUP BY SKU,DATEPART(dw,Date) 
			HAVING SUM(case when pedidoefectivo+isnull(Delta_Restricciones,0)<0 then 0 else pedidoefectivo+isnull(Delta_Restricciones,0) end)>0
				) a
		GROUP BY a.Sku
			) std ON std.SKU = Pedidos.sku 
    LEFT JOIN #shortage Shortage ON Shortage.SKU= SP.sku 
  WHERE (sp.SKU=@sku or @sku is null) AND (pp.ManufacturerId=@Mid or @Mid is null)  AND (pp.GroupId=@grupo or @grupo is null)
   AND isnull(pp.TotalRanking,'') not in ('Z') --no comprar Z
  GROUP BY SP.SKU,isnull(SP.ServiceLevel,0.95),SP.DaysToCover,SP.PurchaseDate,SP.NextPurchaseDate,SP.LeadTime,std.dvStd,Shortage.Shortage


  --SE SACA PORQUE DEBE SER CONSIDERADO EN LA COMPRA, NO EN EL INVENTARIO OBJETIVO
  ---- Compra sugerida cero para Ranking E
  --update s set Suggest=0
  --from #auxSuggest s
  --inner join products.Products pp on pp.SKU=s.SKU
  --where pp.TotalRanking in ('E')
  
  

  -- Por lo menos comprar faltante para los SKU sin modelo
  --update s set Suggest=Shortage
  --from #auxSuggest s
  --where suggest is null
  

  update s set std=0
  from #auxSuggest s
  where std is null

  ---- Transformación caja tira
  INSERT INTO [suggest].[PurchaseSuggest]
  SELECT CASE WHEN ct.SKUTira is null THEN aux.SKU ELSE ct.SKUCaja END SKU
			,MAX(aux.ServiceLevel) ServiceLevel,MAX(aux.DaysToCover) DaysToCover,MAX(PurchaseDate) PurchaseDate,MAX(NextPurchaseDate) NextPurchaseDate,MAX(LeadTime) LeadTime
			,SUM(CASE WHEN ct.SKUTira is null THEN SBDemand ELSE SBDemand/conversion END) SBDemand
			,SUM(CASE WHEN ct.SKUTira is null THEN std ELSE std/conversion END) std
			,SUM(CASE WHEN ct.SKUTira is null THEN Suggest ELSE Suggest/conversion END) Suggest 
			,SUM(CASE WHEN ct.SKUTira is null THEN Shortage ELSE Shortage/conversion END) Shortage
			,[Fillrate],[StockCD],[ActiveOrders],[BuyingMultiple],[ConveniencePack], [ConveniencePackPorc],[Purchase]
  FROM #auxSuggest aux
   LEFT JOIN salcobrand.products.[CajaTira] ct on ct.SKUTira=aux.SKU
  GROUP BY CASE WHEN ct.SKUTira is null THEN aux.SKU ELSE ct.SKUCaja END
  ,[Fillrate],[StockCD],[ActiveOrders],[BuyingMultiple],[ConveniencePack], [ConveniencePackPorc],[Purchase]

-------------------------------------------------------
---------------------Filtro promociones----------------
-------------------------------------------------------
 
-------Identificación de los sku´s en promoción dentro de la sugerencia de compra
 
--  select distinct  ps.SKU
--	,ps.PurchaseDate
--  into #skusPromocion
--  from [suggest].[PurchaseSuggest] ps
--  inner join [Salcobrand].[promotion].[PromotionStock] p on ps.SKU=p.ProductId
--  where PurchaseDate between dateadd(dy,-30, BeginDate) and dateadd(dy,-10,EndDate)
--  order by ps.SKU

-------Incorporación de los sku´s caja para cada sku tira en promoción

--  insert into #skusPromocion
--  select ct.SKUCaja, p.PurchaseDate
--  from #skusPromocion p
--  inner join salcobrand.products.[CajaTira] ct on ct.SKUTira=p.SKU
--  where ct.SKUCaja not in (select distinct sku from #skusPromocion)

-------Corrección del sugerido de compra para los sku´s en promoción 

--  update ps set ps.Suggest=0
--  from [suggest].[PurchaseSuggest] ps
--  where ps.SKU in (select sku from #skusPromocion)

-------Respaldo de los productos en promoción

--	truncate table [suggest].[PromotionProducts]
--	insert into [suggest].[PromotionProducts]
--	select PurchaseDate
--	,SKU 
--	from #skusPromocion

------------------------------------------
------------------------------------------

END TRY
BEGIN CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Tracer', 'Salcobrand.SIIRA',' Falló en cálculo Sugerido de Compra  , SP [operation].[GenerateSuggestCD], ' + ERROR_MESSAGE() , host_name(), 'System', 'POST');
	THROW;

END CATCH


	
END

