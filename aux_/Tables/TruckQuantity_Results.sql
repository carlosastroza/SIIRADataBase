﻿CREATE TABLE [aux].[TruckQuantity_Results] (
    [RUT]           INT          NULL,
    [ReceptionDate] DATE         NULL,
    [Unidades]      INT          NULL,
    [Cost]          FLOAT (53)   NULL,
    [Pallets]       INT          NULL,
    [Camiones]      INT          NULL,
    [scheme]        NVARCHAR (1) NULL
);

