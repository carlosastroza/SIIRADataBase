﻿CREATE TABLE [aux].[LoadPurchaseOrdersDivision] (
    [SKU]           INT              NULL,
    [CorrelativoOC] INT              NULL,
    [Unidades]      INT              NULL,
    [ProcessId]     UNIQUEIDENTIFIER NULL
);

