﻿CREATE TABLE [aux].[PurchaseOrderDetails] (
    [Id]              INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PurchaseOrderId] INT NULL,
    [SKU]             INT NULL,
    [Quantity]        INT NULL,
    [PurchaseSuggest] INT NULL,
    CONSTRAINT [PK_PurchaseOrderDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PurchaseOrderDetails_PurchaseOrders] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [aux].[PurchaseOrders] ([Id])
);

