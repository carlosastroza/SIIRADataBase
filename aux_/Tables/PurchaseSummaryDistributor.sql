﻿CREATE TABLE [aux].[PurchaseSummaryDistributor] (
    [SKU]               INT           NOT NULL,
    [RUT]               NVARCHAR (15) NOT NULL,
    [IdZona]            INT           NOT NULL,
    [LastMonthSalesGES] INT           NULL,
    [PurchaseSuggest]   INT           NULL,
    [Purchase]          INT           NULL,
    [Status]            BIT           NULL,
    CONSTRAINT [PK_PurchaseSummaryDistributor_1] PRIMARY KEY CLUSTERED ([SKU] ASC, [RUT] ASC, [IdZona] ASC)
);

