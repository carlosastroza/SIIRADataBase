﻿CREATE TABLE [aux].[PurchaseOrders] (
    [Id]             INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ParentId]       INT            NULL,
    [RUT]            INT            NOT NULL,
    [LogisticAreaId] NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [OrderComment]   NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DeliveryTypeId] INT            NOT NULL,
    [Store]          INT            NULL,
    [CreatedBy]      NVARCHAR (255) NULL,
    [CreatedDate]    DATETIME       NOT NULL,
    [LeadTime]       INT            NULL,
    [ReceptionDate]  DATE           NULL,
    [IsFree]         BIT            NOT NULL,
    [PurchaseType]   NVARCHAR (3)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [IsRetail]       BIT            NULL,
    [ManufacturerId] INT            NULL,
    [AvailableToBuy] NVARCHAR (3)   CONSTRAINT [DF_PurchaseOrders_AvailableToBuy] DEFAULT ((1)) NULL,
    [IdZona]         INT            CONSTRAINT [DF_PurchaseOrders_IdZona] DEFAULT ((37)) NULL,
    [CustomGroup]    INT            CONSTRAINT [DF_PurchaseOrders_CustomGroup] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_PurchaseOrders] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PurchaseOrders_DeliveryType] FOREIGN KEY ([DeliveryTypeId]) REFERENCES [setup].[DeliveryType] ([Id])
);

