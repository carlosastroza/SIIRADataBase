﻿CREATE TABLE [aux].[OptimizationSkus] (
    [SKU]             INT           NULL,
    [Username]        NVARCHAR (50) NULL,
    [SimulatedAmount] BIGINT        NULL
);

