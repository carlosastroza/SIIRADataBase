﻿CREATE TABLE [aux].[LoadSkuSpecialPurchases] (
    [SKU]           INT              NULL,
    [id_proceso]    UNIQUEIDENTIFIER NULL,
    [InsertionDate] DATETIME         NULL,
    [Purchase]      INT              NULL
);

