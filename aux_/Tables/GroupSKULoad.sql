﻿CREATE TABLE [aux].[GroupSKULoad] (
    [SKU]           INT              NULL,
    [Locked]        INT              NULL,
    [ProcessId]     UNIQUEIDENTIFIER NULL,
    [InsertionDate] DATE             NULL
);

