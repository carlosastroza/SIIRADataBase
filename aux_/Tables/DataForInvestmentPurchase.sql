﻿CREATE TABLE [aux].[DataForInvestmentPurchase] (
    [SKU]                 INT           NULL,
    [Cost]                FLOAT (53)    NULL,
    [UnitProfit]          FLOAT (53)    NULL,
    [StockAndActiveOrder] INT           NULL,
    [Fit]                 FLOAT (53)    NULL,
    [MaxPurchase]         INT           NULL,
    [UnLog_Quantity]      INT           NULL,
    [UnLog_Desc]          VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [IndicadorMagno]      FLOAT (53)    NULL,
    [Group]               INT           NULL,
    [AvgFit]              INT           NULL,
    [Username]            NVARCHAR (50) NULL
);

