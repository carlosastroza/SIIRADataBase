﻿CREATE TABLE [aux].[MonthlyEstimatedPurchaseView] (
    [RUT]               NVARCHAR (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Division]          INT           NOT NULL,
    [CompraMensualUn]   FLOAT (53)    NULL,
    [CompraMensualNeto] FLOAT (53)    NULL,
    [CompraActualUn]    FLOAT (53)    NULL,
    [CompraActualNeto]  FLOAT (53)    NULL,
    CONSTRAINT [PK_MonthlyEstimatedPurchaseView_1] PRIMARY KEY CLUSTERED ([RUT] ASC, [Division] ASC)
);

