﻿CREATE TABLE [aux].[InvestmentPurchase] (
    [Sku]                 INT            NULL,
    [Cantidad]            INT            NULL,
    [UnidadLogisticaDesc] NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Username]            NVARCHAR (255) NULL
);

