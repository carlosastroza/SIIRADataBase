﻿CREATE TABLE [aux].[FillRate] (
    [sku]               INT        NOT NULL,
    [UnidadesCompra]    FLOAT (53) NULL,
    [UnidadesRecibidas] FLOAT (53) NULL,
    [FillRate]          FLOAT (53) NULL,
    CONSTRAINT [PK_FillRate] PRIMARY KEY CLUSTERED ([sku] ASC)
);

