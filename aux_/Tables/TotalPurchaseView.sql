﻿CREATE TABLE [aux].[TotalPurchaseView] (
    [SKU]           INT        NULL,
    [RUT]           INT        NULL,
    [ReceptionDate] DATE       NULL,
    [Unidades]      FLOAT (53) NULL,
    [Cost]          FLOAT (53) NULL
);

