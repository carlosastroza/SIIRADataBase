﻿CREATE TABLE [aux].[PurchaseOrderDivision] (
    [Guid]       UNIQUEIDENTIFIER NULL,
    [Fecha]      DATE             NULL,
    [SKU]        INT              NULL,
    [IndexOC]    INT              NULL,
    [Porcentaje] FLOAT (53)       NULL,
    [OCid]       INT              NULL,
    [Unidades]   INT              NULL
);

