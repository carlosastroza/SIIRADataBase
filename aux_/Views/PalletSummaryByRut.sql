﻿






CREATE VIEW [aux].[PalletSummaryByRut] AS

  select xx.RUT,xx.ReceptionDate,sum(xx.Unidades) Unidades,sum(xx.Cost) Cost
  ,case when ceiling(sum(Pallets))>ceiling(cast(count(distinct SKU)*(1*1.0/6*1.0) as float)) 
	then ceiling(sum(Pallets)) else ceiling(cast(count(distinct SKU)*(1*1.0/6*1.0) as float)) end Pallets
--,ceiling(sum(Pallets))/isnull(rr.PalletsByTruck,30) Camiones
,ceiling((case when ceiling(sum(Pallets))>ceiling(cast(count(distinct SKU)*(1*1.0/6*1.0) as float)) 
	then ceiling(sum(Pallets)) else ceiling(cast(count(distinct SKU)*(1*1.0/6*1.0) as float)) end)/isnull(rr.PalletsByTruck,30)) Camiones
from (
SELECT a.[Id]
      ,[PurchaseOrderId]
      ,a.[SKU],a.Quantity Unidades,a.Quantity*isnull(pc.Cost,1) Cost
      ,[PurchaseSuggest]
	  ,rut.RUT,rut.ReceptionDate
	  --,cast((case when isnull(p.UnitsInPallet,0)=0 then z.UnitsInPallet else p.UnitsInPallet end)
	  --/(case when isnull(p.UnitsInLayer,0)=0 then z.UnitsInLayer else p.UnitsInLayer end) as float) LayerxPallet
	  --,ceiling([Quantity]/(case when isnull(p.UnitsInLayer,0)=0 then z.UnitsInLayer else p.UnitsInLayer end)) layers
	  ,ceiling([Quantity]/(case when isnull(p.UnitsInPallet,0)=0 or isnull(p.UnitsInLayer,0)=0 then z.UnitsInLayer else p.UnitsInLayer end))
	  /cast((case when isnull(p.UnitsInPallet,0)=0 or isnull(p.UnitsInLayer,0)=0 then z.UnitsInPallet else p.UnitsInPallet end)*1.0
		/(case when isnull(p.UnitsInPallet,0)=0 or isnull(p.UnitsInLayer,0)=0 then z.UnitsInLayer else p.UnitsInLayer end)*1.0 as float) Pallets
  FROM [aux].[PurchaseOrderDetails] a
  left join setup.LogisticParameters p on p.SKU=a.SKU
  inner join [aux].[PurchaseOrders] rut on rut.Id=a.PurchaseOrderId
  left join [setup].[PriceAndCost] pc on pc.SKU=a.SKU
  inner join (
  	  SELECT a.[SKU],CASE when a.DistributionArea like 'ALI' THEN 'AliVi' ELSE a.DistributionArea END [BODEGA]
	  ,case when pp.Division=1 then isnull(f.UnitsInLayer,b.UnitsInLayer) else isnull(cm.UnitsInLayer,b.UnitsInLayer) end UnitsInLayer
	  ,case when pp.Division=1 then isnull(f.UnitsInPallet,b.UnitsInPallet) else isnull(cm.UnitsInPallet,b.UnitsInPallet) end UnitsInPallet
	  FROM [Salcobrand].[products].[DistributionAreas] a
	  inner join [Salcobrand].[products].[FullProducts] pp on pp.SKU=a.SKU
	  inner join products.ItemConfiguration b on b.bodega=a.DistributionArea
	  left join [products].[ItemConfigurationFarma] f on f.BODEGA=pp.LogisticAreaId
	  left join [products].[ItemConfigurationCM] cm on cm.[CLASE_UNIFICADA_ID]=pp.[ClaseUnificadaId]
  ) z on z.SKU=a.SKU
  where isnull(p.UnitsInPallet,0)>=isnull(p.UnitsInLayer,0) and rut.ReceptionDate is not null
) xx
left join [products].[VendorConfiguration] rr on rr.RUT=xx.RUT
group by xx.RUT,xx.ReceptionDate,rr.PalletsByTruck
