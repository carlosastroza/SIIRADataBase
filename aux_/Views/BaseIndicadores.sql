﻿


CREATE VIEW [aux].[BaseIndicadores] AS

select 
	u.Username Comprador
	, g.Id GroupId, g.FantasyName
	, p.SKU, p.[Name], p.IsGeneric, p.TotalRanking
	, CASE 
		WHEN DATEPART(weekday, GETDATE()) = 1 AND g.PurchaseOnSunday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 2 AND g.PurchaseOnMonday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 3 AND g.PurchaseOnTuesday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 4 AND g.PurchaseOnWednesday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 5 AND g.PurchaseOnThursday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 6 AND g.PurchaseOnFriday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 7 AND g.PurchaseOnSaturday = 1 THEN CAST(GETDATE() AS DATE)
		ELSE ps.NextPurchaseDate
	END FechaCompra
	, s.[Status], o.Quantity, o.Cost
from products.products p
inner join products.Groups g ON p.GroupId = g.Id
left join users.SkuUsers u ON u.SKU = p.SKU
left join aux.PurchaseSummary s ON p.SKU = s.SKU
left join suggest.PurchaseSuggest ps ON p.SKU = ps.SKU
left join (
	select pd.SKU, cast(po.CreatedDate as date) Fecha, SUM(pd.Quantity) Quantity, SUM(pd.Quantity * pd.Cost) Cost
	FROM purchase.PurchaseOrders po
	INNER JOIN purchase.PurchaseOrderDetails pd ON po.Id = pd.PurchaseOrderId
	GROUP BY pd.SKU, po.CreatedDate
) o ON p.SKU = o.SKU AND CASE 
		WHEN DATEPART(weekday, GETDATE()) = 1 AND g.PurchaseOnSunday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 2 AND g.PurchaseOnMonday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 3 AND g.PurchaseOnTuesday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 4 AND g.PurchaseOnWednesday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 5 AND g.PurchaseOnThursday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 6 AND g.PurchaseOnFriday = 1 THEN CAST(GETDATE() AS DATE)
		WHEN DATEPART(weekday, GETDATE()) = 7 AND g.PurchaseOnSaturday = 1 THEN CAST(GETDATE() AS DATE)
		ELSE ps.NextPurchaseDate
	END = o.Fecha
--WHERE NOT EXISTS (select SKU FROM products.Vademecum v WHERE v.SKU = p.SKU) 


