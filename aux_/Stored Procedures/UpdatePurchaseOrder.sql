﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-19
-- Description:	Actualiza la orden de compra
-- =============================================
CREATE PROCEDURE [aux].[UpdatePurchaseOrder]
	
	@purchaseOrderId INT,
	@receptionDate DATE,
	@deliveryTypeId INT,
	@store INT,
	@orderComment NVARCHAR(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Actualizando parámetros de órdenes de Compra , SP[aux].[UpdatePurchaseOrder]', host_name(),  'POST')

	BEGIN TRY
		UPDATE aux.PurchaseOrders SET
		ReceptionDate = @receptionDate,
		DeliveryTypeId = @deliveryTypeId,
		Store = @store,
		OrderComment = @orderComment
		WHERE Id = @purchaseOrderId
	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló al actualizar parámetros órdenes de compra, SP [aux].[UpdatePurchaseOrder], ' + ERROR_MESSAGE() , host_name(), 'POST');
		THROW;
	END CATCH
	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Actualización de parámetros realizada con éxito, SP [aux].[UpdatePurchaseOrder], ' , host_name())

END
