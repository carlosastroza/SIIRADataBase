﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 19-04-2018
-- Description:	Autorizar Compra de OC

--UPDATE: 24-07-2018 se agrega la columna idzona en el insert a [purchase].[PurchaseOrders]
-- =============================================

CREATE PROCEDURE [aux].[AuthorizePurchaseOrders]
@ID     INT
AS
BEGIN

SET NOCOUNT ON;

	IF('PEN' <> (SELECT AvailableToBuy FROM aux.PurchaseOrders WHERE Id = @ID))
	BEGIN
	PRINT ('raised error')
		RAISERROR('La OC especificada no se encuentra en estado pendiente. Imposible autorizar', 16, 1)
	END
	ELSE
	BEGIN
		PRINT ('started transaction')
	
		BEGIN TRAN;
		BEGIN TRY

			UPDATE aux.PurchaseOrders SET AvailableToBuy = 'RDY' WHERE Id = @id

			INSERT INTO [purchase].[PurchaseOrders]
			SELECT	oc.Id,
					oc.ParentId,
					oc.RUT,
					oc.LogisticAreaId,
					oc.OrderComment,
					oc.DeliveryTypeId,
					oc.Store,
					oc.CreatedBy,
					oc.CreatedDate,
					oc.LeadTime,
					oc.ReceptionDate,
					oc.IsFree,
					oc.PurchaseType,
					0,
					oc.IsRetail,
					oc.ManufacturerId,
					oc.IdZona
			FROM aux.[PurchaseOrders] oc
			WHERE oc.Id = @ID

			INSERT INTO [purchase].[PurchaseOrderDetails]
			SELECT	de.Id,
					de.PurchaseOrderId, 
					de.SKU, 
					de.Quantity, 
					p.Cost, 
					de.PurchaseSuggest 
			FROM aux.PurchaseOrderDetails de
			inner join aux.PurchaseOrders oc on de.PurchaseOrderId = oc.Id
			inner join setup.PriceAndCost p  on de.SKU = p.SKU
			WHERE oc.Id = @ID

			DELETE d
			FROM aux.PurchaseOrderDetails d
			inner join aux.PurchaseOrders po ON d.PurchaseOrderId = po.Id
			where po.Id = @ID

			DELETE FROM aux.PurchaseOrders WHERE Id = @ID

			COMMIT TRAN;
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN;
			THROW;
		END CATCH
	END
END
