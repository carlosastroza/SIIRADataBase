﻿
-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-06-19
-- Description:	Muestra el resumen de compras por rut para el mes, comparando con el estimado de la compra total del mes
-- =============================================

CREATE PROCEDURE [aux].[LoadMonthlyEstimatedPurchase]

AS
BEGIN

SET NOCOUNT ON;

  declare @hoy date = cast(getdate() as date)

  declare @days int = datediff(day, dateadd(day,1,@hoy), dateadd([month], datediff([month], '18991231', @hoy), '18991231'))
  set @days = (select case when @days<0 then 0 else @days end)

  -- Fechas variables por RUT
    select pp.sku
  ,cast(dateadd(day,r.[DayForEstimatedPurchase]-1,dateadd(month,-(case when r.[DayForEstimatedPurchase]>day(@hoy) then 1 else 0 end),dateadd([month], datediff([month], '19000101', @hoy), '19000101'))) as date) inicio
  ,cast(dateadd(day,r.[DayForEstimatedPurchase]-2,dateadd(month,1-(case when r.[DayForEstimatedPurchase]>day(@hoy) then 1 else 0 end),dateadd([month], datediff([month], '19000101', @hoy), '19000101'))) as date) fin
  ,case when datediff(day, dateadd(day,1,@hoy), cast(dateadd(day,r.[DayForEstimatedPurchase]-2,dateadd(month,1-(case when r.[DayForEstimatedPurchase]>day(@hoy) then 1 else 0 end),dateadd([month], datediff([month], '19000101', @hoy), '19000101'))) as date))<0
	then 0 else datediff(day, dateadd(day,1,@hoy), cast(dateadd(day,r.[DayForEstimatedPurchase]-2,dateadd(month,1-(case when r.[DayForEstimatedPurchase]>day(@hoy) then 1 else 0 end),dateadd([month], datediff([month], '19000101', @hoy), '19000101'))) as date))
	end days
	into #base
  from products.Products pp
  inner join products.manufacturers m on pp.manufacturerid = m.id
  inner join [products].[VendorConfiguration] r on r.rut=m.rut


  -- Se calcula el faltante de ayer, para el cálculo de la compra estimada
		 SELECT ls.SKU,SUM(ls.suggest)-SUM(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) -SUM(ls.Transit)-SUM(ISNULL(d.Picking,0))  Shortage
		 into #short
		  FROM Salcobrand.series.LastStock ls 
			INNER JOIN Salcobrand.stores.Stores S on S.Store=ls.Store
			LEFT JOIN (SELECT sku,store,Picking FROM Salcobrand.series.DespachosCD WHERE date=DATEADD(day,-1,@hoy)) d on d.SKU=ls.SKU and d.Store=ls.Store 
		  WHERE ls.suggest>(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) +ls.Transit+ISNULL(d.Picking,0)
		  GROUP BY ls.SKU

	-- Promedio de pedidos, para el cálculo de la compra estimada
  		SELECT a.Sku,avg(a.StdPedidos) dvStd
		into #std
		FROM (
			SELECT SKU,DATEPART(dw,Date) dow,STDEV(case when pedidoefectivo+isnull(Delta_Restricciones,0)<0 then 0 else pedidoefectivo+isnull(Delta_Restricciones,0) end) StdPedidos,COUNT(*) Total 
			FROM [Salcobrand].[minmax].[DistributionOrdersForecastCurrent] 
			GROUP BY SKU,DATEPART(dw,Date) 
			HAVING SUM(case when pedidoefectivo+isnull(Delta_Restricciones,0)<0 then 0 else pedidoefectivo+isnull(Delta_Restricciones,0) end)>0
				) a
		GROUP BY a.Sku

	-- Cálculo del inventario objetivo para cubrir pedidos desde mañana hasta fin de mes
	  select a.sku,CEILING([Salcobrand].[function].NormalProbability (
					SUM(case when pedidoefectivo+isnull(Delta_Restricciones,0)<0 then 0 else pedidoefectivo+isnull(Delta_Restricciones,0) end),
					std.dvStd * SQRT(isnull(b.days,@days)),0.95
				))
			+ ISNULL(Shortage.Shortage,0) ped
	  into #compraRest
	  from Salcobrand.minmax.DistributionOrdersForecastCurrent a
	  inner JOIN #std std ON std.SKU = a.sku
	  left join #short  Shortage on Shortage.sku=a.sku
	  left join #base b on b.sku=a.sku
	  where date between dateadd(day,1,@hoy) and isnull(b.fin,dateadd([month], datediff([month], '18991231', @hoy), '18991231'))
	  group by a.sku ,std.dvStd ,Shortage.Shortage,b.fin,b.days

	  -- Compras que llegan desde inicio de mes hasta hoy y emitidas en el mismo mes
	  select sku,sum(activeOrders) activeOrders,sum([ValoresCompra]) [ValoresCompra]
	  into #compraAct
	  from (
		  SELECT a.SKU, SUM(UnidadesCompra) as activeOrders,sum([ValoresCompra]) [ValoresCompra]
		  --into #compraAct
		  FROM series.PurchaseOrders a
		  left join #base b on b.sku=a.sku
		  where FechaExpiracionActual between isnull(b.inicio,dateadd([month], datediff([month], '19000101', @hoy), '19000101')) and @hoy
		    and Date between isnull(b.inicio,dateadd([month], datediff([month], '19000101', @hoy), '19000101')) and @hoy
			and EstadoOOCC=2
			and UnidadesCompra>0
			--and Store=996
		  group by a.SKU ,b.inicio 

		  union all

		  SELECT a.SKU, SUM(UnidadesCompra) as activeOrders,sum([ValoresCompra]) [ValoresCompra]
		  FROM series.DailyPurchaseOrders a
		  left join #base b on b.sku=a.sku
		  where FechaExpiracionActual between isnull(b.inicio,dateadd([month], datediff([month], '19000101', @hoy), '19000101')) and @hoy
		    and Date between isnull(b.inicio,dateadd([month], datediff([month], '19000101', @hoy), '19000101')) and @hoy
			and EstadoOOCC=2
			and UnidadesRecibidas=0 and UnidadesCompra>0
			and Store=996
		  group by a.SKU,b.inicio  
	  ) x
	  group by sku

	  -- Compras que van a llegar desde mañana hasta fin de mes
	  select sku,sum(activeOrders) activeOrders
	  into #transit
	  from (
		  SELECT a.SKU, SUM(UnidadesCompra) as activeOrders
		  FROM series.PurchaseOrders a
		  left join #base b on b.sku=a.sku
		  where FechaExpiracionActual between dateadd(day,1,@hoy) and isnull(b.fin,dateadd([month], datediff([month], '18991231', @hoy), '18991231'))
		    and Date between dateadd(day,1,@hoy) and isnull(b.fin,dateadd([month], datediff([month], '18991231', @hoy), '18991231'))
			and EstadoOOCC=2
			and UnidadesCompra>0
		  group by a.SKU ,b.fin

		  union all

		  SELECT a.SKU, SUM(UnidadesCompra) as activeOrders
		  FROM series.DailyPurchaseOrders a
		  left join #base b on b.sku=a.sku
		  where FechaExpiracionActual between dateadd(day,1,@hoy) and isnull(b.fin,dateadd([month], datediff([month], '18991231', @hoy), '18991231'))
			and Date between dateadd(day,1,@hoy) and isnull(b.fin,dateadd([month], datediff([month], '18991231', @hoy), '18991231'))
			and EstadoOOCC=2
			and UnidadesRecibidas=0 and UnidadesCompra>0
			and Store=996
		  group by a.SKU ,b.fin
	  ) x
	  group by sku

-- la compra estimada considera Stock, tránsito, fill rate y redondeo
-- CompraMensualUn es el total del mes (unidades proyectadas + unidades ya compradas)
-- CompraMensualNeto es el total neto del mes (neto proyectado + neto ya compradas)
-- UnidadesTotales es la proyección de compra en unidades
-- CompraFutura es la proyección de compra neta

truncate table [aux].[MonthlyEstimatedPurchaseView]

insert into [aux].[MonthlyEstimatedPurchaseView]
select RUT,Division,sum(UnidadesTotales+activeOrders+Transito) CompraMensualUn,sum(CompraFutura+ValoresCompra+ValorTransito) CompraMensualNeto,sum(activeOrders) CompraActualUn,sum(ValoresCompra) CompraActualNeto
from (

	select z.sku,sp.Division,m.RUT,z.Price,z.ProyeccionDeComprasUn,r.UnidadesTotales,r.UnidadesTotales*z.Price CompraFutura,z.activeOrders,z.ValoresCompra,z.Transito,z.Transito*z.Price ValorTransito
	from(
		SELECT pc.sku,pc.price
		,(case when (isnull(ped,0)-(case when isnull(ls.StockCD,0)<0 then 0 else isnull(ls.StockCD,0) end)-(case when isnull(t.ActiveOrders,0)<0 then 0 else isnull(t.ActiveOrders,0) end))<0 then 0 
		else (isnull(ped,0)-(case when isnull(ls.StockCD,0)<0 then 0 else isnull(ls.StockCD,0) end)-(case when isnull(t.ActiveOrders,0)<0 then 0 else isnull(t.ActiveOrders,0) end)) end)
			/(case when isnull(sp.FillRate,0)<0.75 then 0.75 else sp.FillRate end) ProyeccionDeComprasUn	  
		,isnull(x.activeOrders,0) activeOrders,isnull(x.[ValoresCompra],0) [ValoresCompra]
		,(case when isnull(t.ActiveOrders,0)<0 then 0 else isnull(t.ActiveOrders,0) end) Transito
		FROM [setup].[PriceAndCost] pc
		left join setup.LastStock ls on ls.SKU=pc.SKU
		left join #transit t on t.SKU=pc.SKU
		left join #compraRest d on d.sku=pc.sku 
		left join #compraAct x on x.sku=pc.sku
		left join .[setup].[SuggestParameters] sp on sp.SKU=pc.SKU
	) z
	inner join [setup].[LogisticParameters] lp on lp.sku=z.sku
	inner join products.Products pp on pp.SKU=z.SKU
	inner join [Salcobrand].[products].[FullProducts] sp on sp.SKU=pp.SKU
	inner join products.manufacturers m on pp.manufacturerid = m.id
	cross apply [function].[RoundLogisticUnits] (z.ProyeccionDeComprasUn, null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet,lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
) b
group by RUT,Division

END