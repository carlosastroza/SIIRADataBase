﻿-- =============================================
-- Author:		Carlos A
-- Create date: 26-04-2018
-- Description:	Obtiene SKU de un Grupo Compra Normal

-- UPDATE: 2018-05-30
-- Author: Gabriel Espinoza
-- Desc:   Se agregan 4 nuevas columnas: OPAC Actual, OPAC Siguiente, VP último mes y Fit próximo mes

-- UPDATE: 20178-05-31
-- Author: Gabriel Espinoza
-- Desc:   Se crea método genérico para permitir la obtención de esta data para GENERICO y NORMAL
-- =============================================

CREATE PROCEDURE [aux].[GetPurchaseSuggest]
	@groupId INT,
	@oc INT,
	@genericFamilyId INT,
	@specialPurchase BIT,
	@investmentPurchase BIT,
	@username NVARCHAR (50)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @groupId_Local INT = @groupId
DECLARE @genericFamilyId_Local INT = @genericFamilyId
DECLARE @specialPurchase_Local BIT = @specialPurchase
DECLARE @username_Local NVARCHAR (50) = @username
DECLARE @oc_local INT = @OC

IF @username_Local IN (SELECT Username FROM setup.AdminUsers)
BEGIN
SET @username_Local = null;
END -- Acceso total a todos los SKUs

IF(@oc_local IS NOT NULL) 
BEGIN
SELECT 
det.SKU,
psp.[Name] Nombre,
p.Category Categoria,
m.[Name] Laboratorio,
ISNULL(p.logisticareaid,psp.LogisticAreaId) Rubro ,
CASE WHEN fp.PetitorioMinimo > 0 THEN 'SI' ELSE 'NO' END Petitorio,
FillRate ,
DaysToCover DiasInventario,
ISNULL(ps.AdditionalDaysToCover,0) DiasAdicionales,
ISNULL(pc.Price,psp.Price) Precio,
ISNULL(pc.Cost,psp.Cost) Costo,
(ISNULL(pc.Cost,psp.Cost) * det.Quantity) CostoTotal, 
StoresStock StockLocales,
ISNULL(DCStock,psp.StockCD) StockCD,
StockOut Quiebre,
ISNULL(ps.LastMonthSale,psp.LastMonthSale) VentaUltimoMes,
ps.Suggest Sugerencia,
ps.SBDemand,
ps.PurchaseSuggest SugerenciaFinal,
det.Quantity Compra,
case when sp.SKU is null then cast (0 as bit)  else cast (1 as bit) end IsPromotion,
a.[Days] DiasCompra,
Coverage Cobertura,
ISNULL(DCTransit,psp.ActiveOrders) TransitoCD,
StoresTransit TransitoLocal,
ISNULL(ps.StoresShortage, psp.StoresShortage) Faltante,
ISNULL(p.TotalRanking, psp.TotalRanking) TotalRanking,
ISNULL(ps.OPACCurrent,0) OPACCurrent, 
ISNULL(ps.OPACUpcoming,0) OPACUpcoming,
ps.LostSaleLastMonth, 
ps.FitNextMonth, 
NULL LeadTime,
NULL ServiceLevel,
p.Motive,
p.MotiveId,
BuyingMultiple,
lp.UnitsInBox UnidadesCaja,
p.GenericFamilyId,
p.GroupId,
CAST(1 AS BIT) PermitirEdicion,
@oc_local PurchaseOrderId,
ps.DistributionSuggest SugeridoHoy,
diasInv.Days DiasInventarioCD

FROM aux.PurchaseOrderDetails det
LEFT JOIN aux.PurchaseSummary ps ON det.SKU = ps.SKU
LEFT JOIN products.ProductsSpecialPurchase psp ON det.SKU = psp.SKU
LEFT JOIN products.Products p on det.SKU = p.SKU
LEFT JOIN setup.PriceAndCost pc on det.SKU = pc.SKU
INNER JOIN products.Manufacturers m on psp.ManufacturerId = m.Id
INNER JOIN Salcobrand.products.FullProducts fp on det.SKU = fp.SKU
LEFT JOIN suggest.PromotionProducts sp on sp.SKU = det.SKU
left JOIN [setup].[LogisticParameters] lp on lp.SKU = det.SKU
OUTER APPLY [suggest].[GetInventoryDays](det.SKU,det.Quantity+ISNULL(ps.DCStock,psp.StockCD)+ISNULL(DCTransit,psp.ActiveOrders)-ISNULL(ps.StoresShortage, psp.StoresShortage),GETDATE()) a
OUTER APPLY [suggest].[GetInventoryDays](p.SKU,ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) diasInv
WHERE det.PurchaseOrderId = @oc_local


END
ELSE IF(@specialPurchase = 0 AND @investmentPurchase = 0)
BEGIN
	SELECT 
		sg.SKU,
		p.[Name] Nombre,
		p.Category Categoria,
		m.[Name] Laboratorio,
		p.logisticareaid Rubro ,
		CASE WHEN fp.PetitorioMinimo > 0 THEN 'SI' ELSE 'NO' END Petitorio,
		FillRate ,
		DaysToCover DiasInventario,
		ISNULL(sg.AdditionalDaysToCover,0) DiasAdicionales,
		pc.Price Precio,
		pc.Cost Costo,
		(pc.Cost * sg.Purchase) CostoTotal, 
		StoresStock StockLocales,
		DCStock StockCD,
		StockOut Quiebre,
		LastMonthSale VentaUltimoMes,
		sg.Suggest Sugerencia,
		sg.SBDemand,
		sg.PurchaseSuggest SugerenciaFinal,
		--case when sp.SKU is null then sg.Purchase else 0 end  Compra,
	    sg.Purchase Compra,
		case when sp.SKU is null then cast (0 as bit)  else cast (1 as bit) end IsPromotion,
		a.[Days] DiasCompra,
		Coverage Cobertura,
		DCTransit TransitoCD,
		StoresTransit TransitoLocal,
		StoresShortage Faltante,
		sg.TotalRanking TotalRanking,
		sg.OPACCurrent, sg.OPACUpcoming
		, sg.LostSaleLastMonth, sg.FitNextMonth 
		, NULL LeadTime
		, NULL ServiceLevel
		,p.Motive
		,p.MotiveId
		,BuyingMultiple
		,lp.UnitsInBox UnidadesCaja
		,p.GenericFamilyId
		,p.GroupId
		,CAST(CASE WHEN B.SKU IS NOT NULL THEN 0 else 1 END AS BIT) PermitirEdicion
		,CAST(CASE WHEN gen.SKU IS NOT NULL THEN 1 ELSE 0 END AS BIT) Licitado
		,sg.DistributionSuggest SugeridoHoy
		,diasInv.Days DiasInventarioCD
	FROM  aux.PurchaseSummary sg
	INNER JOIN products.Products p on sg.sku = p.sku
	LEFT JOIN setup.PriceAndCost pc on sg.SKU = pc.SKU
	INNER JOIN products.Manufacturers m on p.ManufacturerId = m.Id
	INNER JOIN Salcobrand.products.FullProducts fp on sg.SKU = fp.SKU
	INNER JOIN users.SkuUsers su on sg.SKU = su.SKU
	LEFT JOIN suggest.PromotionProducts sp on sp.SKU = sg.SKU
	left JOIN [setup].[LogisticParameters] lp on lp.SKU = sg.SKU
	LEFT JOIN (
	SELECT SKU
	FROM aux.PurchaseOrderDetails
	GROUP BY SKU) b ON b.SKU = sg.SKU
	LEFT JOIN products.GenericTenderedSKU gen ON sg.SKU = gen.SKU
	OUTER APPLY [suggest].[GetInventoryDays](sg.SKU,sg.PURCHASE+sg.DCStock+sg.DCTransit-sg.StoresShortage,GETDATE()) a
	OUTER APPLY [suggest].[GetInventoryDays](p.SKU,sg.DCStock+sg.DCTransit-sg.StoresShortage,GETDATE()) diasInv
	WHERE	(p.GroupId = @groupId_Local OR @groupId_Local IS NULL) 
	AND		(su.Username = @USERNAME_Local OR @USERNAME_Local IS NULL)
	AND		(p.GenericFamilyId = @genericFamilyId_Local OR @genericFamilyId_Local IS NULL)
	ORDER BY gen.SKU DESC, sg.SKU ASC

END
ELSE IF(@specialPurchase = 1 AND @investmentPurchase = 0)
BEGIN
	SELECT 
		p.SKU,
		p.[Name] Nombre ,
		p.Category Categoria,
		m.[Name] Laboratorio,
		p.LogisticAreaId Rubro,
		CASE WHEN fp.PetitorioMinimo > 0 THEN 'SI' ELSE 'NO' END Petitorio,
		s.FillRate ,
		s.DaysToCover DiasInventario,
		0 DiasAdicionales,
		p.Price  Precio,
		p.Cost Costo,
		(p.Cost * s.Purchase) CostoTotal, 
		s.StoresStock StockLocales,
		p.StockCD,
		s.StockOut Quiebre ,
		CAST(p.LastMonthSale AS FLOAT) VentaUltimoMes,
		s.suggest Sugerencia ,
		cast (NULL as float) SBDemand, --FALTA
		s.PurchaseSuggest SugerenciaFinal,
		s.Purchase Compra,
	    cast (0 as bit) IsPromotion,
		a.[Days] DiasCompra,
		s.Coverage Cobertura,
		p.ActiveOrders TransitoCD,
		s.StoresTransit TransitoLocal,
		p.StoresShortage Faltante,
	    p.TotalRanking,
		CAST(ISNULL(sg.OPACCurrent,0) AS BIT) OPACUpcoming
		,sg.LostSaleLastMonth, sg.FitNextMonth
		,CAST(s.LeadTime AS float) LeadTime
		,s.ServiceLevel
		,p.Motive
		,p.MotiveId
		,BuyingMultiple
		,lp.UnitsInBox UnidadesCaja
		,CAST(CASE WHEN B.SKU IS NOT NULL THEN 0 else 1 END AS BIT) PermitirEdicion
		,sg.DistributionSuggest SugeridoHoy
		,diasInv.Days DiasInventarioCD
	FROM  aux.PurchaseSummarySpecial s
	INNER JOIN products.Productsspecialpurchase p on p.SKU = s.SKU
	INNER JOIN products.Manufacturers m on m.Id = p.ManufacturerId
	INNER JOIN Salcobrand.products.FullProducts fp on s.SKU = fp.SKU
    LEFT JOIN aux.PurchaseSummary sg ON s.SKU = sg.SKU
	LEFT JOIN [setup].[LogisticParameters] lp on lp.SKU = s.SKU
	LEFT JOIN (
	SELECT SKU
	FROM aux.PurchaseOrderDetails det
	INNER JOIN AUX.PurchaseOrders oc ON det.PurchaseOrderId = oc.Id
	WHERE oc.CreatedBy = @username
	AND PurchaseType = 'ESP'
	GROUP BY SKU) b ON b.SKU = s.SKU
	OUTER APPLY [suggest].[GetInventoryDays](s.SKU,s.PURCHASE+p.StockCD+p.ActiveOrders-p.StoresShortage,GETDATE()) a
	OUTER APPLY [suggest].[GetInventoryDays](p.SKU,p.StockCD+p.ActiveOrders-p.StoresShortage,GETDATE()) diasInv
	WHERE s.Username = @username

END
ELSE IF(@specialPurchase = 0 AND @investmentPurchase = 1)
BEGIN

DECLARE @res int;
SET @res = 
(
SELECT TOP 1 isnull (cast (SimulatedAmount as int), 0) MontoSimulado
from [aux].OptimizationSkus i
WHERE  i.Username = @USERNAME);

SELECT 
		sg.SKU,
		p.[Name] Nombre,
		p.Category Categoria,
		m.[Name] Laboratorio,
		p.logisticareaid Rubro ,
		CASE WHEN fp.PetitorioMinimo > 0 THEN 'SI' ELSE 'NO' END Petitorio,
		FillRate ,
		DaysToCover DiasInventario,
		ISNULL(sg.AdditionalDaysToCover,0) DiasAdicionales,
		pc.Price Precio,
		pc.Cost Costo,
		(pc.Cost)*(Cantidad * CASE WHEN UnidadLogisticaDesc = 'Caja'   
					          THEN l.UnitsInBox 
							  WHEN UnidadLogisticaDesc = 'Layer'  
							  THEN l.UnitsInLayer 
							  WHEN UnidadLogisticaDesc = 'Pallet' 
							  THEN L.UnitsInPallet 
							  WHEN UnidadLogisticaDesc = 'No especificado'
							  THEN 1
							  ELSE l.UnitsInBox END)
        CostoTotal, 
		StoresStock StockLocales,
		DCStock StockCD,
		StockOut Quiebre,
		LastMonthSale VentaUltimoMes,
		sg.Suggest Sugerencia,
		sg.SBDemand,
		sg.PurchaseSuggest SugerenciaFinal,
		Cantidad * CASE WHEN UnidadLogisticaDesc  = 'Caja'   
				THEN l.UnitsInBox 
			WHEN UnidadLogisticaDesc = 'Layer'  
				THEN l.UnitsInLayer 
		   WHEN UnidadLogisticaDesc = 'Pallet' 
				THEN L.UnitsInPallet 
		   WHEN UnidadLogisticaDesc = 'No especificado'
				THEN 1
		   ELSE l.UnitsInBox 
		   END   Compra,
	    case when sp.SKU is null then cast (0 as bit)  else cast (1 as bit) end IsPromotion,
		a.[Days] DiasCompra,
		Coverage Cobertura,
		DCTransit TransitoCD,
		StoresTransit TransitoLocal,
		StoresShortage Faltante,
		sg.TotalRanking TotalRanking,
		sg.OPACCurrent, sg.OPACUpcoming
		, sg.LostSaleLastMonth, sg.FitNextMonth 
		, NULL LeadTime
		, NULL ServiceLevel
		,@res Inversion
		,det.PurchaseOrderId
		,BuyingMultiple
		,l.UnitsInBox UnidadesCaja
		,p.Motive
		,p.MotiveId
		,sg.DistributionSuggest SugeridoHoy
		,diasInv.Days DiasInventarioCD
	FROM  aux.PurchaseSummary sg
	INNER JOIN [aux].[InvestmentPurchase] inv on sg.SKU = inv.Sku 
	INNER JOIN products.Products p on sg.sku = p.sku
	INNER JOIN products.Manufacturers m on p.ManufacturerId = m.Id
	INNER JOIN Salcobrand.products.FullProducts fp on sg.SKU = fp.SKU 
	INNER JOIN setup.PriceAndCost pc on sg.SKU = pc.SKU
	INNER JOIN setup.LogisticParameters l ON sg.Sku = l.SKU
	LEFT JOIN suggest.PromotionProducts sp on sp.SKU = sg.SKU
	LEFT JOIN aux.PurchaseOrderDetails det on sg.SKU = det.sku 
	OUTER APPLY [suggest].[GetInventoryDays](sg.SKU, Cantidad * CASE WHEN UnidadLogisticaDesc  = 'Caja'   
				THEN l.UnitsInBox 
			WHEN UnidadLogisticaDesc = 'Layer'  
				THEN l.UnitsInLayer 
		   WHEN UnidadLogisticaDesc = 'Pallet' 
				THEN L.UnitsInPallet 
		   WHEN UnidadLogisticaDesc = 'No especificado'
				THEN 1
		   ELSE l.UnitsInBox 
		   END 
		      +sg.DCStock+sg.DCTransit-sg.StoresShortage,GETDATE()) a
	OUTER APPLY [suggest].[GetInventoryDays](p.SKU,sg.DCStock+sg.DCTransit-sg.StoresShortage,GETDATE()) diasInv
	WHERE (inv.Username = @username)
	AND (det.PurchaseOrderId = @oc OR @oc IS NULL)
	ORDER BY Cantidad * CASE WHEN UnidadLogisticaDesc  = 'Caja'   
				THEN l.UnitsInBox 
			WHEN UnidadLogisticaDesc = 'Layer'  
				THEN l.UnitsInLayer 
		   WHEN UnidadLogisticaDesc = 'Pallet' 
				THEN L.UnitsInPallet 
		   WHEN UnidadLogisticaDesc = 'No especificado'
				THEN 1
		   ELSE l.UnitsInBox 
		   END  desc


END
END