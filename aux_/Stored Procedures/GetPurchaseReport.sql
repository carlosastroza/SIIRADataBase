﻿
-- =============================================
-- Author:		Jorge I.
-- Create date: 18-05-2018
-- Description:	Obtiene los indicadores de compra para jefes
-- =============================================

CREATE PROCEDURE [aux].[GetPurchaseReport]
AS
BEGIN
	SET NOCOUNT ON;
		DECLARE @fecha DATE = CAST(GETDATE() as date)

	SELECT	datos.Id,
			datos.Nombre [Comprador],
			COUNT(datos.IdGrupo) [GruposHoy],
			SUM(num_skus) [CantSkus],			
			CASE WHEN (SUM (num_skus)-(SUM(CompraSugZero))) != 0 
				THEN  CAST(SUM(Comprado) as float)/CAST((SUM (num_skus)-(SUM(CompraSugZero))) as float)
				ELSE 1 END [PorcSkusComprados],
			SUM (num_skus)-(SUM(CompraSugZero)+SUM(SinCompraSugerida)) [CantSkusconCompra],
			ISNULL(SUM(Comprado),(0)) [CantSkusComprados],
			CAST(ISNULL(SUM(costo),0) as float) [CostoTotal],
			SUM(CompraSugZero) [CantSkusCompCero],
			SUM(SinCompraSugerida) [CantSkusSinCompSug]
	FROM (
	SELECT	u.Id,
			u.Nombre, 
			bi.GroupId [IdGrupo],
			COUNT(bi.SKU) num_skus,
			SUM(bi.Quantity) cantidad,
			SUM(bi.Cost) costo,
			SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END) CompraSugZero,
			SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END) SinCompraSugerida,
			SUM(CASE WHEN oc.CreatedDate IS NULL THEN 0 ELSE 1 END) Comprado
	FROM aux.BaseIndicadores bi
	LEFT JOIN (SELECT SKU, Purchase CompraSugerida FROM [aux].[PurchaseSummary]) ps on ps.SKU = bi.SKU
	LEFT JOIN stdusuarios.dbo.usuario u ON u.id = bi.comprador
	LEFT JOIN (SELECT	od.PurchaseOrderId,
						od.SKU,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date) = @fecha
				) oc ON oc.sku = bi.sku
	WHERE bi.FechaCompra = @fecha
	AND bi.SKU NOT IN (select SKU from salcobrand.products.genericfamilydetail)
	GROUP BY bi.GroupId,u.Nombre,u.Id
	) datos
 
		GROUP BY datos.Nombre,datos.Id

	UNION 
	SELECT bi.comprador Id,u.Nombre Comprador, count(distinct groupid)GruposHoy, count(distinct bi.SKU) CantSkus  
		,case when sum(case when ps.CompraSugerida>0 then 1 else 0 end)!=0 
		then sum(case when oc.CreatedDate IS NULL then 0 else 1 end)/sum(case when ps.CompraSugerida>0 then 1 else 0 end) else 1 end  PorcSkusComprados
		,sum(case when ps.CompraSugerida>0 then 1 else 0 end) CantSkusconCompra
		,sum(case when oc.CreatedDate IS NULL then 0 else 1 end) CantSkusComprado
		,sum(isnull(bi.Cost,0)) CostoTotal 
		,sum(case when ps.CompraSugerida=0 then 1 else 0 end) CantSkusCompCero
		,sum(case when ps.CompraSugerida IS NULL then 1 else 0 end) CantSkusSinCompSug

		FROM aux.BaseIndicadores bi
		INNER JOIN (SELECT SKU, Purchase CompraSugerida FROM aux.[PurchaseSummary]
					where SKU in (select SKU FROM products.GenericTenderedSKU)) ps on ps.SKU=bi.SKU
		LEFT JOIN (SELECT	od.PurchaseOrderId,
						od.SKU,
						od.Quantity,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date) = @fecha ) oc on oc.SKU=bi.sku
				LEFT JOIN stdusuarios.dbo.usuario u ON u.id  = bi.comprador

	WHERE bi.FechaCompra=@fecha
	group by bi.comprador,u.Nombre 
	ORDER BY [PorcSkusComprados] asc
END