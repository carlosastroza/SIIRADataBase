﻿-- =============================================
-- Author:		Carlos A
-- Create date: 03-15-2018
-- Description:	sp que obtiene Unidades Logísticas
-- =============================================
CREATE PROCEDURE [aux].[GetLogisticParameters]
@USERNAME NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON;

IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USERNAME = null;
END

SELECT 
	p.[SKU], 
	p.name Descripcion,
	[UnitsInBox] UnidadesCaja,
	[UnitsInLayer] UnidadesLayer,
	[UnitsInPallet] UnidadesPallet,
	[RoundToBoxes] RedondeadoCaja,
	[RoundToLayers] RedondeadoLayer,
	[RoundToPallets] RedondeadoPallet,
	[ConveniencePack] PackConvenienciaI,
	[ConveniencePack%] PackConveniencia,
	[RoundThreshold] UmbralRedondeo,
	BuyingMultiple
	FROM [setup].[LogisticParameters] l
	inner join products.products p on l.sku = p.sku
	INNER JOIN users.SkuUsers su on p.SKU = su.SKU
	WHERE (@USERNAME is null or su.Username = @USERNAME)
	AND (TotalRanking <> 'Z')
	order by p.SKU asc


END
