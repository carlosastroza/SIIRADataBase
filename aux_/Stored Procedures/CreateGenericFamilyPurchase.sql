﻿CREATE PROCEDURE [aux].[CreateGenericFamilyPurchase]
 @FAMILY_ID INT,
 @USERNAME NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON;
	
	DECLARE @FAMILIA INT = @FAMILY_ID;
	DECLARE @USUARIO NVARCHAR(MAX) = @USERNAME;

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Creando órdenes de Compra GEN , SP[aux].[CreateGenericFamilyPurchase]', host_name(), @USUARIO, 'POST')

	DECLARE @MaxDaysInt INT;
	DECLARE @MaxDays TABLE(
	Id NVARCHAR(MAX),
	Value INT
	)
	INSERT INTO @MaxDays
	EXEC [setup].[GetConfigurationDictionary] 'SB'
	SET @MaxDaysInt = (SELECT Value FROM @MaxDays)
	

	DECLARE @ResultadoTemporal TABLE (
	Ranking INT,
	SKU INT,
	Compra INT, 
	Sugerencia INT,
	RUT INT,
	Rubro VARCHAR(MAX),
	LeadTime INT, 
	ManufacturerId INT, 
	DIAS_SOBRESTOCK INT)
	DECLARE @COMPRA_FAMILIA INT = (
	SELECT SUM(purchase) FROM aux.PurchaseSummary ps
	INNER JOIN products.Products p ON ps.SKU = p.SKU
	WHERE GenericFamilyId = @FAMILIA
	);

	INSERT INTO @ResultadoTemporal
	SELECT	RANK() OVER (PARTITION BY m.RUT, pr.ManufacturerId, pr.LogisticAreaId ORDER BY pr.SKU ASC) AS [Rank],
	s.SKU, ISNULL(s.Purchase,0), S.PurchaseSuggest,m.RUT, pr.LogisticAreaId, g.leadtime, m.Id, a.Days
	FROM aux.PurchaseSummary s
	INNER JOIN products.Products pr on s.SKU = pr.SKU
	INNER JOIN products.Manufacturers m ON pr.ManufacturerId = m.Id
	INNER JOIN products.groups g on pr.groupid = g.id
	LEFT JOIN setup.PriceAndCost pc ON s.SKU = pc.SKU
	OUTER APPLY [suggest].[GetInventoryDays](s.SKU,@COMPRA_FAMILIA+s.DCStock+s.DCTransit-s.StoresShortage,GETDATE()) a
	WHERE (pr.GenericFamilyId = @FAMILIA)
	AND   (pc.Cost > 0)
	AND (pr.ManufacturerId <> 92)

	BEGIN TRY
		MERGE INTO aux.PurchaseOrders as oc
		USING (SELECT * FROM @ResultadoTemporal WHERE Ranking = 1) AS rs
		on rs.Rut = oc.RUT AND rs.Rubro COLLATE DATABASE_DEFAULT =  oc.LogisticAreaId AND oc.ManufacturerId = rs.ManufacturerId AND oc.PurchaseType = 'GEN' AND oc.ParentId IS NULL AND oc.CreatedBy = @USERNAME
		WHEN NOT MATCHED THEN
		INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate,LeadTime, ReceptionDate, isFree, PurchaseType,IsRetail, ManufacturerId, AvailableToBuy)
		VALUES(rs.Rut, rs.Rubro, 1,@USERNAME, GETDATE(),rs.LeadTime,DATEADD(DAY, LeadTime, CONVERT(DATE, GETDATE())), 0, 'GEN', 1, rs.ManufacturerId, 'RDY');
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló creando órdenes de compra GEN, SP [aux].[CreateGenericFamilyPurchase], ' + ERROR_MESSAGE() , host_name(), @USERNAME, 'POST');
		THROW;
	END CATCH

Declare 
@id int, 
@count int
Set @id=1
select @count=count(*)from @ResultadoTemporal 
DECLARE @sku int, @rut int,@lab int, @rubro VARCHAR(MAX), @ORDEN_COMPRA int, @sku_cantidad int, @sugerencia int, @SKU_DETALLE INT;

DECLARE @GENERA_SOBRESTOCK INT = 0;
DECLARE @CANTIDAD_DIAS_SS  INT = 0;

while @id<=@count
begin

SET @sku = (select SKU from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @rut = (select RUT from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @lab = (select ManufacturerId from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @rubro = (select TOP 1 Rubro from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @sku_cantidad = (select Compra from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @ORDEN_COMPRA = (select top 1 oc.ID from aux.PurchaseOrders oc where oc.RUT = @rut AND oc.LogisticAreaId = @rubro AND oc.ManufacturerId = @lab AND oc.PurchaseType = 'GEN' AND CreatedBy = @USERNAME)
SET @SKU_DETALLE  = (select top 1 count(*) from aux.PurchaseOrderDetails ocD where ocd.SKU = @sku AND PurchaseOrderId = @ORDEN_COMPRA);
SET @sugerencia = (select Sugerencia from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);

IF @sku_cantidad > 0
BEGIN
UPDATE aux.PurchaseSummary SET Purchased = 1 WHERE SKU = @sku
END


SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) 
						  FROM @ResultadoTemporal 
						  WHERE (DIAS_SOBRESTOCK > @MaxDaysInt)
						  AND   (RUT = @rut) 
						  AND   (Rubro = @rubro) 
						  AND   (ManufacturerId = @lab));


IF @CANTIDAD_DIAS_SS > 0 BEGIN
UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @ORDEN_COMPRA
SET @GENERA_SOBRESTOCK = 1;
END
ELSE
BEGIN
UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @ORDEN_COMPRA
END 

IF @SKU_DETALLE > 0 BEGIN
	BEGIN TRY
		UPDATE aux.PurchaseOrderDetails set Quantity = @sku_cantidad WHERE SKU = @sku;
	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Falló actualizando sku '+ @sku +' , SP [aux].[CreateGenericFamilyPurchase], ' + ERROR_MESSAGE() , host_name(), @USUARIO, 'POST');
		THROW;
	END CATCH
END
ELSE
BEGIN
	BEGIN TRY
		INSERT INTO aux.PurchaseOrderDetails VALUES(@ORDEN_COMPRA,@sku,@SKU_CANTIDAD, @sugerencia);
	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Falló insertando sku '+ @sku +' , SP [aux].[CreateGenericFamilyPurchase], ' + ERROR_MESSAGE() , host_name(), @USUARIO, 'POST');
		THROW;
	END CATCH
END

select @id=@id+1
end

DELETE FROM aux.PurchaseOrderDetails WHERE Quantity = 0;
DELETE FROM aux.PurchaseOrders WHERE ID not in (select PurchaseOrderId from aux.PurchaseOrderDetails) and PurchaseType = 'GEN'

INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],Usuario)
VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Órdenes de compra GEN creadas correctamente, SP [aux].[CreateGenericFamilyPurchase], ' , host_name(),@USUARIO)

SELECT @GENERA_SOBRESTOCK;

END
