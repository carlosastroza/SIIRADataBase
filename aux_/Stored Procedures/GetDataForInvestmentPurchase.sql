﻿-- =============================================
-- Author:		PLAN Z
-- Create date: 2018-05-24
-- Description:	Se obtienen los datos, por Rut, para realizar la compra por inversión
-- =============================================
CREATE PROCEDURE [aux].[GetDataForInvestmentPurchase]
@USERNAME NVARCHAR(MAX),
@MONTO BIGINT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @usuario_local NVARCHAR(50) = @USERNAME;
	DECLARE @monto_local    BIGINT = @MONTO;

	UPDATE aux.OptimizationSkus set SimulatedAmount = @monto_local WHERE Username = @USERNAME
	
	select p.SKU
		,p.TotalRanking
		,case when TotalRanking like 'A' then 30 else(case when TotalRanking like 'B' then 25 else (case when TotalRanking like 'C' then 15 else (case when TotalRanking like 'D' then 10 else (case when TotalRanking like 'E' then 5 else (case when TotalRanking like 'Z' then 0 else 20 end) end) end) end) end) end DaysToCover
	into #days
	from [products].[Products] p
	inner join aux.OptimizationSkus o ON p.SKU = o.SKU
	WHERE (o.Username = @usuario_local)

	DELETE FROM aux.DataForInvestmentPurchase WHERE Username = @usuario_local
	DELETE FROM aux.InvestmentPurchase WHERE Username = @usuario_local


	INSERT INTO  [aux].[DataForInvestmentPurchase]
	select p.SKU
		,Cost Costo
		,pc.MargenUnitario
		,case when isnull(ls.StockCD,0)+ISNULL(ls.ActiveOrders,0)>0 then isnull(ls.StockCD,0)+ISNULL(ls.ActiveOrders,0) else 0 end StockAndActOrder
		,fit.Fit
		,case when average.AvgFit*60 -isnull(ls.StockCD,0)-isnull(d.activeOrders,0) >0 then average.AvgFit*60 -isnull(ls.StockCD,0)-isnull(d.activeOrders,0) else 0 end MaxPurchase
		,isnull(UNLOG_Quantity,1) UNLOG_Quantity
		,isnull(UNLOG_Desc,'No especificado') UNLOG_Desc
		,pc.MargenUnitario*fit.Fit*isnull(UNLOG_Quantity,1)
		/(case when isnull(ls.StockCD,0)+ISNULL(ls.ActiveOrders,0)>0 then isnull(ls.StockCD,0)+ISNULL(ls.ActiveOrders,0) else 1 end) IndicadorMagno
		,0
		,average.AvgFit
		,@usuario_local 
	from [products].[Manufacturers] m
	inner join [products].[Products] p on p.ManufacturerId=m.Id
	inner join ( SELECT [SKU]
					,[Price]
					,[Cost]
					,Price-Cost MargenUnitario
				FROM [setup].[PriceAndCost] where cost is not null) pc on pc.sku=p.SKU
	left join [setup].[LastStock] ls on ls.sku=p.SKU
	left join #days da on da.sku=p.sku -- modificar
	cross apply [function].GetFit(p.sku,isnull(da.DaysToCover,0),getdate()) fit
	cross apply [function].GetAvgFit(p.sku) average
	left join (
	 SELECT a.SKU, SUM(UnidadesCompra) as activeOrders
		  --FROM [Salcobrand].[aux].[TempPurchaseOrders] a
		  FROM series.PurchaseOrders a
		  inner join [setup].[SuggestParameters] b on b.SKU=a.SKU
		  inner join #days c on c.sku =a.sku
		  where FechaExpiracionActual between cast(GETDATE() as date) and dateadd(day,60,getdate())
			and EstadoOOCC=2
			and UnidadesRecibidas=0 and UnidadesCompra>0
			and Store=996
	group by a.SKU
	) d on d.sku=p.SKU
	left join(
		  select SKU
			,case when RoundToBoxes=1 then cast('Caja' as varchar) else case when RoundToLayers=1 then 'Layer' else case when RoundToPallets=1 then 'Pallet' else 'Sin Especificar' end end end UNLOG_Desc
			,case when cast([RoundToBoxes] as int)*UnitsInBox+cast([RoundToLayers] as int)*UnitsInLayer+cast([RoundToPallets] as int)*UnitsInPallet > 0 then cast([RoundToBoxes] as int)*UnitsInBox+cast([RoundToLayers] as int)*UnitsInLayer+cast([RoundToPallets] as int)*UnitsInPallet else 1 end  UNLOG_Quantity
		FROM [setup].[LogisticParameters]
	) unlog on unlog.SKU=p.SKU
	inner join  [aux].[OptimizationSkus] os on os.sku=p.SKU
	WHERE os.Username = @usuario_local
	
END