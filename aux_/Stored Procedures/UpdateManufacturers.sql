﻿
CREATE PROCEDURE [aux].[UpdateManufacturers]
@ID INT,
@MESES INT,
@COMPRADOR NVARCHAR(50)
AS
BEGIN
SET NOCOUNT ON;

UPDATE [products].[Manufacturers] 
SET FillRateMonths = @meses WHERE id = @id

IF @COMPRADOR = 'Desasignar'  BEGIN
DELETE FROM products.DefaultManufacturerBuyer WHERE ManufacturerId = @ID
END
ELSE BEGIN

DECLARE @Tabla AS TABLE(Id INT, Comprador NVARCHAR(50))
INSERT INTO @Tabla VALUES (@ID, @COMPRADOR)
MERGE INTO products.DefaultManufacturerBuyer as def
USING @tabla AS t
on  def.ManufacturerId = t.Id
WHEN MATCHED THEN
UPDATE SET def.Username = t.Comprador
WHEN NOT MATCHED THEN
INSERT (ManufacturerId, Username)
VALUES (t.Id, t.Comprador);

END

END

