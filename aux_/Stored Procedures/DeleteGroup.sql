﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 19-04-2018
-- Description:	Eliminar Grupo
-- =============================================

CREATE PROCEDURE [aux].[DeleteGroup]
@ID     INT
AS
BEGIN

SET NOCOUNT ON;

UPDATE products.products 
set GroupId = null, 
isGroupLocked = 0 
Where GroupId = @id; 

DELETE FROM [products].[Groups] WHERE Id = @id;


end
