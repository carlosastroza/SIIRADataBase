﻿
CREATE PROCEDURE [aux].[getSKUGroupDownload]
@id INT,
@USERNAME NVARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USERNAME);
IF ISNULL(@Usuario_admin,0) = 1 BEGIN
SET @USERNAME = null;
END

SELECT p.sku, CAST(case when IsGroupLocked = 1 then 1 else 0 end AS int) Bloqueado 
from products.Products p
INNER JOIN users.SkuUsers su on p.SKU = su.SKU
where GroupId = @id
AND (@USERNAME IS NULL OR @USERNAME = su.Username)
AND (p.TotalRanking <> 'Z')

END

