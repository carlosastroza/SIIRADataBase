﻿-- =============================================
-- Author:		Juan M  Hernandez
-- Create date: 01-02-2018
-- Description:	crea órdenes compra ges, prametros ges y actualiza SuggestProducts
-- =============================================
CREATE PROCEDURE [aux].[UpdatePurchaseGes]

@USUARIO NVARCHAR(MAX),
@PROVEEDORES int,
@DISTRIBUIDOR INT,
@RUBRO_COMPRA NVARCHAR(10),
@PRODUCTO INT,
@DIAS_DESDE INT,
@DIAS_HASTA INT,
@BM INT = NULL
AS
BEGIN

SET NOCOUNT ON;

DECLARE @MaxDaysInt INT;
DECLARE @MaxDays TABLE(
Id NVARCHAR(MAX),
Value INT
)
INSERT INTO @MaxDays
EXEC [setup].[GetConfigurationDictionary] 'SB'
SET @MaxDaysInt = (SELECT Value FROM @MaxDays)

DECLARE @USUARIO_LOCAL NVARCHAR(MAX)  = @USUARIO 

IF @USUARIO IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USUARIO = null;
END

DECLARE @FILTRO_DIAS_INV BIT = NULL;

IF @RUBRO_COMPRA = '' BEGIN
SET @RUBRO_COMPRA = NULL;
END

IF @DISTRIBUIDOR = 0 BEGIN
SET @DISTRIBUIDOR = NULL;
END

IF @DIAS_DESDE <> 0 OR @DIAS_HASTA <> 0 BEGIN
SET @FILTRO_DIAS_INV = 1;
END


------------- sku para filtro de proveedores ------------
SELECT p.SKU 
INTO #SkuManufacturer 
FROM products.Products p
INNER JOIN products.Manufacturers m on m.Id = p.ManufacturerId
INNER JOIN products.Vademecum v on v.SKU = p.SKU
WHERE ManufacturerId = @PROVEEDORES

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Creando órdenes de Compra GES , SP[aux].[UpdatePurchaseGes]', host_name(), @Usuario_lOCAL, 'POST')

	DECLARE @ResultadoTemporalProveedor TABLE (SKU INT,Compra INT, Sugerido INT,RUT INT,Rubro VARCHAR(15),LeadTime INT, ManufacturerId INT, DIAS_SOBRESTOCK INT)

	
	SELECT  SKU, IdZona 
	INTO #vanemecum
	FROM products.Vademecum


	INSERT INTO @ResultadoTemporalProveedor
	SELECT p.SKU, p.Purchase  Compra, p.PurchaseSuggest,m.RUT, pro.LogisticAreaId Rubro, g.LeadTime, m.Id,a.[Days]
	FROM  aux.PurchaseSummary p
	INNER JOIN #vanemecum v on v.SKU = p.SKU 
	INNER JOIN products.Products pro on pro.SKU = p.SKU
	INNER JOIN products.Manufacturers m on m.Id = pro.ManufacturerId
	INNER JOIN products.Groups g on g.Id= pro.GroupId
	INNER JOIN setup.PriceAndCost pac on pac.sku  = p.SKU
	INNER JOIN users.SkuUsers su on p.SKU = su.SKU
	LEFT JOIN setup.LogisticParameters lp ON p.SKU = lp.SKU
	OUTER APPLY [suggest].[GetInventoryDays](p.SKU, p.PURCHASE+p.DCStock+p.DCTransit-p.StoresShortage,GETDATE()) a
	OUTER APPLY [suggest].[GetInventoryDays](p.SKU,p.DCStock+p.DCTransit-p.StoresShortage,GETDATE()) diasInv
	LEFT JOIN (select * from [function].SKUPurchased ('GES')) pod on pod.SKU = v.SKU and pod.RUT  = m.RUT COLLATE DATABASE_DEFAULT
	WHERE pac.Cost > 0 
	AND (@USUARIO is null or su.Username = @USUARIO)
	AND (P.SKU = @PRODUCTO or @PRODUCTO = 0) AND (P.SKU in (select sku from #SkuManufacturer ) or @PROVEEDORES = 0)
	AND (@RUBRO_COMPRA IS NULL OR pro.LogisticAreaId = @RUBRO_COMPRA)
	AND ((@FILTRO_DIAS_INV IS NULL) OR  (diasInv.Days BETWEEN @DIAS_DESDE AND @DIAS_HASTA) OR (diasInv.Days IS NULL) )
	AND (@DISTRIBUIDOR IS NULL OR v.IdZona = @DISTRIBUIDOR) --CHECKEAR
	AND (lp.BuyingMultiple = @BM OR @BM = 0)

----------------------------------- ACTUALIZO PurchaseOrders con los datos del Proveedor -------------------------------------
	BEGIN TRY
		MERGE INTO aux.PurchaseOrders as po
		USING @ResultadoTemporalProveedor AS rt
		on rt.rut = po.Rut and rt.Rubro COLLATE DATABASE_DEFAULT =	po.LogisticAreaId and po.IsRetail = 1 
		and po.ManufacturerId = rt.ManufacturerId AND po.PurchaseType = 'GES' AND po.ParentId IS NULL AND po.CreatedBy = @Usuario_lOCAL
		WHEN NOT MATCHED THEN
		INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate, LeadTime, ReceptionDate, isFree, PurchaseType, IsRetail,ManufacturerId,AvailableToBuy)
		VALUES (rt.rut,rt.Rubro,1,@Usuario_lOCAL, GETDATE(),rt.LeadTime,DATEADD(DAY, rt.LeadTime, GETDATE()),0,  'GES',1,rt.ManufacturerId,'RDY');

	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló creando órdenes de compra GES, SP [aux].[UpdatePurchaseGes], ' + ERROR_MESSAGE() , host_name(), @Usuario_lOCAL, 'POST');
		THROW;
	END CATCH
	
		

----------------------------  Detalle con los datos del Proveedor    --------------------------------------------

Declare 
@id int, 
@count int
Set @id=1
select @count=count(*)from @ResultadoTemporalProveedor 
DECLARE @sku int, @rut int,@lab int, @rubro VARCHAR(MAX), @ORDEN_COMPRA int, @sku_cantidad int,  @SKU_DETALLE INT, @MANUFACTUREID INT, @SUGERIDO INT

DECLARE @GENERA_SOBRESTOCK INT = 0;
DECLARE @CANTIDAD_DIAS_SS  INT = 0;

while @id<=@count
begin

	SET @sku = (select top 1 SKU from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalProveedor) as it where rank=@id);
	SET @rut = (select top 1 RUT from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalProveedor) as it where rank=@id);
	SET @lab = (select ManufacturerId from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalProveedor) as it where rank=@id);
	SET @rubro = (select TOP 1 Rubro from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalProveedor) as it where rank=@id);
	SET @sku_cantidad = (select top 1 Compra from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalProveedor) as it where rank=@id);
	SET @ORDEN_COMPRA = (select top 1 oc.ID from aux.PurchaseOrders oc where oc.RUT = @rut AND oc.LogisticAreaId = @rubro and IsRetail = 1 
																		AND ParentId IS NULL AND oc.PurchaseType = 'GES' and oc.CreatedBy = @USUARIO_LOCAL)
	SET @SKU_DETALLE  = (select top 1 count(*) from aux.PurchaseOrderDetails ocD 
						 inner join aux.PurchaseOrders pc on pc.Id = ocD.PurchaseOrderId
					     where ocd.SKU = @sku and ocD.PurchaseOrderId = @ORDEN_COMPRA);

	SET @SUGERIDO = (select Sugerido from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalProveedor) as it where rank=@id);

IF @sku_cantidad > 0
BEGIN
UPDATE aux.PurchaseSummary SET Purchased = 1 WHERE SKU = @sku
END
	
	SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) 
						  FROM @ResultadoTemporalProveedor 
						  WHERE (DIAS_SOBRESTOCK > @MaxDaysInt)
						  AND   (RUT = @rut) 
						  AND   (Rubro = @rubro) 
						  AND   (ManufacturerId = @lab)
						  AND   (Compra > 0));


	IF @CANTIDAD_DIAS_SS > 0 BEGIN
		UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @ORDEN_COMPRA
		SET @GENERA_SOBRESTOCK = @GENERA_SOBRESTOCK+1;
	END
	ELSE
	BEGIN
		UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @ORDEN_COMPRA
	END 


	--SELECT @sku SKU, @lab RUT, @rubro RUBRO, @sku_cantidad CANTIDAD, @orden_compra OC, @sku_detalle DET

	IF @SKU_DETALLE > 0  BEGIN
		BEGIN TRY
			UPDATE aux.PurchaseOrderDetails set Quantity = @sku_cantidad WHERE SKU = @sku and PurchaseOrderId = @ORDEN_COMPRA;
		END TRY 
		BEGIN CATCH
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló actualizando sku '+ @sku +' , SP [aux].[UpdatePurchaseGes], ' + ERROR_MESSAGE() , host_name(), @Usuario_lOCAL, 'POST');
			THROW;
		END CATCH
	END
	ELSE
	BEGIN
		IF @ORDEN_COMPRA is not null 
		BEGIN
			BEGIN TRY
			 	INSERT INTO aux.PurchaseOrderDetails VALUES(@ORDEN_COMPRA,@sku,@SKU_CANTIDAD,@SUGERIDO);
			END TRY 
			BEGIN CATCH
					INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
					VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló insertando sku '+ @sku +' , SP [aux].[UpdatePurchaseGes], ' + ERROR_MESSAGE() , host_name(), @USUARIO_LOCAL, 'POST');
					THROW;
			END CATCH
		END
	END

	select @id=@id+1
end


-------------- Pendiente que los distribuidores no tienen LT---------- 
    DECLARE @ResultadoTemporalDistribuidor TABLE (SKU INT,Compra INT,Sugerido INT, RUT INT,Rubro VARCHAR(MAX),LeadTime INT,ManufacturerId INT, DIAS_SOBRESTOCK INT, IdZona INT)
	INSERT INTO @ResultadoTemporalDistribuidor
	SELECT  p.SKU, p.Purchase, p.PurchaseSuggest,p.RUT, pro.LogisticAreaId Rubro, g.LeadTime,m.Id,a.[Days], p.IdZona
	FROM  aux.PurchaseSummaryDistributor p
	INNER JOIN products.Products pro on pro.SKU = p.SKU
	INNER JOIN products.Groups g on g.Id= pro.GroupId
	INNER JOIN products.Manufacturers m on m.Id = pro.ManufacturerId
	INNER JOIN setup.PriceAndCostGES pac on pac.sku  = p.SKU and pac.IdZona = p.IdZona
	INNER JOIN users.SkuUsers su on p.SKU = su.SKU
	OUTER APPLY [suggest].[GetInventoryDays](p.SKU,p.PURCHASE,GETDATE()) a
	LEFT JOIN (select * from [function].SKUPurchased ('GES')) pod on pod.SKU = p.SKU and pod.RUT  = m.RUT COLLATE DATABASE_DEFAULT
	LEFT JOIN setup.LogisticParameters lp ON p.SKU = lp.SKU
	WHERE pac.Cost > 0 
	AND (@USUARIO is null or su.Username = @USUARIO)
	AND (P.SKU = @PRODUCTO or @PRODUCTO = 0) AND (P.SKU in (select sku from #SkuManufacturer ) or @PROVEEDORES = 0)
	AND (@RUBRO_COMPRA IS NULL OR pro.LogisticAreaId = @RUBRO_COMPRA)
	AND (@DISTRIBUIDOR IS NULL OR p.IdZona = @DISTRIBUIDOR)
	AND (lp.BuyingMultiple = @BM OR @BM = 0)


-------------------ACTUALIZO PurchaseOrders con los datos del Distribuidor----------------------
	BEGIN TRY
		MERGE INTO aux.PurchaseOrders as po
		USING @ResultadoTemporalDistribuidor AS rt
		on rt.rut = po.Rut and rt.Rubro COLLATE DATABASE_DEFAULT =	po.LogisticAreaId  and po.IsRetail = 0 and po.PurchaseType = 'GES' AND po.IdZona = rt.IdZona
																										   AND po.ParentId IS NULL AND po.CreatedBy = @Usuario_lOCAL
		WHEN NOT MATCHED THEN
		INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate, LeadTime, ReceptionDate, isFree, PurchaseType, IsRetail,ManufacturerId, IdZona,AvailableToBuy)
		VALUES (rt.rut,rt.Rubro,1,@Usuario_lOCAL, GETDATE(),rt.LeadTime,DATEADD(DAY, rt.LeadTime, GETDATE()),0,  'GES', 0,rt.ManufacturerId, rt.IdZona,'RDY');


	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló creando órdenes de compra GES para Distribuidores, SP [aux].[UpdatePurchaseGes], ' + ERROR_MESSAGE() , host_name(), @Usuario_lOCAL, 'POST');
		THROW;
	END CATCH




----------------------------  Detalle con los datos del Distribuidor    --------------------------------------------

Declare 
@id2 int, 
@count2 int
Set @id2=1
select @count2=count(*)from @ResultadoTemporalDistribuidor
DECLARE @sku2 int, @rut2 int,@lab2 int,  @rubro2 VARCHAR(MAX), @ORDEN_COMPRA2 int, @sku_cantidad2 int,  @SKU_DETALLE2 INT, @SUGERIDO2 INT
,@IdZona2 INT


DECLARE @CANTIDAD_DIAS_SS2  INT = 0;

while @id2<=@count2
begin

SET @sku2 = (select top 1 SKU from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalDistribuidor) as it where rank=@id2);
SET @rut2 = (select top 1 RUT from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalDistribuidor) as it where rank=@id2);
SET @lab2 = (select TOP 1 ManufacturerId from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalDistribuidor) as it where rank=@id2);
SET @rubro2 = (select TOP 1 Rubro from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalDistribuidor) as it where rank=@id2);
SET @sku_cantidad2 = (select top 1 Compra from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalDistribuidor) as it where rank=@id2);
SET @IdZona2 = (select top 1 IdZona from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporalDistribuidor) as it where rank=@id2);
SET @ORDEN_COMPRA2 = (select top 1 oc.ID from aux.PurchaseOrders oc where oc.RUT = @rut2 AND oc.LogisticAreaId = @rubro2 and IsRetail = 0  AND IdZona =@IdZona2 
																						 AND oc.ParentId IS NULL AND oc.CreatedBy = @USUARIO_LOCAL)

SET @SKU_DETALLE2  = (select top 1 count(*) from aux.PurchaseOrderDetails ocD 
					  inner join aux.PurchaseOrders pc on pc.Id = ocD.PurchaseOrderId
					  where ocd.SKU = @sku2 and ocD.PurchaseOrderId = @ORDEN_COMPRA2);


SET @SUGERIDO2 = (select TOP 1 Sugerido from (select  *,RANK()OVER (ORDER BY IdZona ASC)AS RANK from @ResultadoTemporalDistribuidor) as it where rank=@id2);
SET @CANTIDAD_DIAS_SS2 = ( SELECT COUNT(*) 
							  FROM @ResultadoTemporalDistribuidor 
							  WHERE (DIAS_SOBRESTOCK > @MaxDaysInt)
							  AND   (RUT = @rut2) 
							  AND   (Rubro = @rubro2) 
							  AND   (ManufacturerId = @lab2)
							  AND   (IdZona = @IdZona2)
							  AND   (Compra > 0)
							  );


	IF @CANTIDAD_DIAS_SS2 > 0 BEGIN
		UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @ORDEN_COMPRA2 --AND RUT = @rut2 AND IdZona = @IdZona2
		SET @GENERA_SOBRESTOCK = @GENERA_SOBRESTOCK+1;
	END
	ELSE
	BEGIN
		UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @ORDEN_COMPRA2 --AND RUT = @rut2 AND IdZona = @IdZona2
	END 


	IF @SKU_DETALLE2 > 0 
	BEGIN
		BEGIN TRY
			UPDATE aux.PurchaseOrderDetails set Quantity = @sku_cantidad2 WHERE SKU = @sku2 and PurchaseOrderId = @ORDEN_COMPRA2;
		END TRY 
		BEGIN CATCH
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló actualizando sku '+ @sku2 +' (Distribuidor) , SP [aux].[UpdatePurchaseGes], ' + ERROR_MESSAGE() , host_name(), @Usuario_lOCAL, 'POST');
			THROW;

		END CATCH
	END
	ELSE
	BEGIN
		IF @ORDEN_COMPRA2 is not null 
			BEGIN
				BEGIN TRY
		 			INSERT INTO aux.PurchaseOrderDetails VALUES(@ORDEN_COMPRA2,@sku2,@SKU_CANTIDAD2, @SUGERIDO2);
				END TRY 
				BEGIN CATCH
					INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
					VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Falló insertando sku '+ @sku2 +' (Distribuidor) , SP [aux].[UpdatePurchaseGes], ' + ERROR_MESSAGE() , host_name(), @Usuario_lOCAL, 'POST');
					THROW;
				END CATCH
		END
	END

	SELECT @id2=@id2+1
	end


-------------------ACTUALIZO PurchaseOrders con los datos del Distribuidor----------------------

DELETE FROM aux.PurchaseOrderDetails WHERE Quantity <= 0;
DELETE FROM aux.PurchaseOrders WHERE ID not in (select PurchaseOrderId from aux.PurchaseOrderDetails) and PurchaseType = 'GES'


---------------------Actualizo los estados de gestión ---------------------------------------
update aux.PurchaseSummary
set Status = 1
where sku in (
				select sku 
				from aux.PurchaseOrderDetails pod
				inner join aux.PurchaseOrders po on po.Id = pod.PurchaseOrderId
				where po.IsRetail = 1)


update aux.PurchaseSummary
set Status = 0
where sku not in (
				select sku 
				from aux.PurchaseOrderDetails pod
				inner join aux.PurchaseOrders po on po.Id = pod.PurchaseOrderId
				where po.IsRetail = 1)
select sku, IdZona
INTO #tablaaux
from aux.PurchaseOrderDetails pod
inner join aux.PurchaseOrders po on po.Id = pod.PurchaseOrderId
where po.IsRetail = 0

 UPDATE d
 set status = 1
 from aux.PurchaseSummaryDistributor d
 INNER JOIN  #tablaaux ta on d.IdZona = ta.IdZona and d.SKU = ta.SKU
 where d.sku in (select sku from #tablaaux )  


UPDATE d
set status = 0
from aux.PurchaseSummaryDistributor d
where Purchase= 0 or Purchase is null

drop table #SkuManufacturer

INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],Usuario)
VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Órdenes de compra GES creadas correctamente, SP [aux].[UpdatePurchaseGes], ' , host_name(),@Usuario_lOCAL);

DECLARE @CANTIDAD_OC INT;

SET @CANTIDAD_OC = (SELECT COUNT(*) FROM(
					SELECT	
					DISTINCT prov.RUT, prov.Rubro, prov.ManufacturerId
					FROM @ResultadoTemporalProveedor prov
					WHERE prov.Compra > 0
					UNION
					SELECT	
					DISTINCT dis.RUT, dis.Rubro, dis.ManufacturerId
					FROM @ResultadoTemporalDistribuidor dis
					WHERE dis.Compra > 0)a );


SELECT @GENERA_SOBRESTOCK OC_GSS, @CANTIDAD_OC OC_GENERADAS;


END
--------------------------------------------------------------