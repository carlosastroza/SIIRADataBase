﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-07-18
-- Description: Obtiene los encabezados de las órdenes de compra en PRICING y ERP con su estado
--
-- Update:		Gabriel Espinoza Erices
-- Date:		2018-08-08
-- Description: Se actualiza información de Isapre para mostrar compras GES
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrderHeaders]
	@sku INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

    --DECLARE @sku int = NULL;
	-- OBTENEMOS TODAS LAS OCS QUE NO SE HAN ENVIADO AL ERP
	select 
		'Por enviar a ERP' PurchaseOrderStatus
		, '-' PurchaseStatus
		, NULL Id
		, ocp.Id PricingPurchaseId
		, ocp.RUT RUT
		, CASE 
			WHEN ocp.IsRetail = 1 THEN m.[Name] 
			ELSE di.DistributorName
		  END Manufacturer 
		, ISNULL(di.[Isapre], '-') Isapre 
		, ocp.LogisticAreaId [Zone]
		, ISNULL(ISNULL(u.Nombre, ocp.CreatedBy), 'No Informado') Buyer
		, CAST(ocp.CreatedDate AS DATE) PurchaseDate
		, ocp.ReceptionDate ReceptionDate
		, ocp.OrderComment
	from (
		select DISTINCT po.*
		from purchase.PurchaseOrders po
		inner join purchase.PurchaseOrderDetails pod ON po.Id = pod.PurchaseOrderId
		WHERE (pod.SKU = @sku OR @sku IS NULL)
	)ocp
	inner join products.Manufacturers m on ocp.ManufacturerId = m.Id
	left join StdUsuarios.dbo.Usuario u ON ocp.CreatedBy = u.Id  COLLATE Modern_Spanish_CI_AS
	left join [setup].[DistributorIsapre] di on ocp.IdZona = di.[IdZona]
	where ocp.Id NOT IN (
		select Nro_oc_pricing from purchase.SentPurchaseOrders
	)


	UNION ALL
	-- OBTENEMOS TODAS LAS OCS QUE SE ENVIARON AL ERP PERO NO HAN VUELTO
    --DECLARE @sku int = NULL;
	select  
		'Enviada a ERP' PurchaseOrderStatus
		, 'Desconocido' PurchaseStatus
		, NULL Id
		, ocp.Id PricingPurchaseId
		, ocp.RUT RUT
		, CASE 
			WHEN ocp.IsRetail = 1 THEN m.[Name] 
			ELSE di.DistributorName
		  END Manufacturer 
		, ISNULL(di.[Isapre], '-') Isapre 
		, ocp.LogisticAreaId [Zone]
		, ISNULL(ISNULL(u.Nombre, ocp.CreatedBy), 'No Informado') Buyer
		, CAST(ocp.CreatedDate AS date) PurchaseDate
		, ocp.ReceptionDate ReceptionDate
		, ocp.OrderComment
	from (
		select DISTINCT po.* from purchase.PurchaseOrders po
		inner join purchase.PurchaseOrderDetails pod ON po.Id = pod.PurchaseOrderId
		WHERE (pod.SKU = @sku OR @sku IS NULL)
	)ocp
	inner join products.Manufacturers m on ocp.ManufacturerId = m.Id
	left join StdUsuarios.dbo.Usuario u ON ocp.CreatedBy = u.Id  COLLATE Modern_Spanish_CI_AS
	left join [setup].[DistributorIsapre] di on ocp.IdZona = di.[IdZona]
	where ocp.Id IN (
		select Nro_oc_pricing from purchase.SentPurchaseOrders
	) AND ocp.Id NOT IN (
		select IdOrigen 
		FROM series.PurchaseOrders
		WHERE OrigenOrden IN('PRC', 'P')
	) AND ocp.Id NOT IN (
		select IdOrigen 
		FROM series.DailyPurchaseORders
		WHERE OrigenOrden IN('PRC', 'P')
	)


	UNION ALL
	-- OBTENEMOS TODAS LAS OCS QUE ESTAN EN EL ERP PERO NO LAS CREAMOS DESDE PRICING
    --DECLARE @sku int = NULL;
	select 
		  CASE 
			WHEN po.OrigenOrden IN('PRC', 'P') THEN 'Procesada por ERP'
			ELSE '-'
		  END PurchaseOrderStatus
		, CASE 
			WHEN po.EstadoOOCC = 1 THEN 'Registrada'
			WHEN po.EstadoOOCC = 2 AND po.FechaRecepcion IS NOT NULL THEN 'Recepcionada'
			WHEN po.EstadoOOCC = 2 AND po.FechaRecepcion IS NULL THEN 'Autorizada'
			WHEN po.EstadoOOCC = 3 THEN 'Anulada'
			ELSE 'No Especificado' --TODO: Validar Estados
		  END PurchaseStatus
		, po.Id Id
		, CASE 
			WHEN po.OrigenOrden IN('P', 'PRC') THEN po.IdOrigen
			ELSE NULL
		  END PricingPurchaseId
		, CASE 
			WHEN ISNULL(po.IdZona, 37) = 37 THEN po.RUT 
			ELSE di.DistributorRUT
		  END Manufacturer
		, CASE 
			WHEN ISNULL(po.IdZona, 37) = 37 THEN m.[Name] 
			ELSE di.DistributorName
		  END Manufacturer
		, ISNULL(di.[Isapre], '-') Isapre 
		, po.LogisticAreaId COLLATE Modern_Spanish_CI_AS [Zone]
		, COALESCE(u.Nombre, ur.Username, po.BuyerId, 'No Informado') Buyer
		, po.[Date] PurchaseDate
		, ISNULL(po.FechaRecepcion,po.FechaExpiracionActual) ReceptionDate
		, 'ERP no envía glosa a PRICING' OrderComment
	FROM (
		select DISTINCT 
			Id, RUT, po.ManufacturerId, p.LogisticAreaId, [Date], FechaExpiracionActual
			, FechaRecepcion, BuyerIdOrigen BuyerId, OrigenOrden, IdOrigen, EstadoOOCC
			, IdZona
		from series.DailyPurchaseOrders po
		inner join products.Products p on po.SKU = p.SKU
		WHERE (po.SKU = @sku OR @sku IS NULL)
		UNION
		SELECT DISTINCT 
			Id, RUT, po.ManufacturerId, p.LogisticAreaId, [Date], FechaExpiracionActual
			, FechaRecepcion, BuyerIdOrigen BuyerId, OrigenOrden, IdOrigen, EstadoOOCC
			, IdZona
		FROM series.PurchaseOrders po
		inner join products.Products p on po.SKU = p.SKU
		WHERE (po.SKU = @sku OR @sku IS NULL)
	) po
	inner join products.Manufacturers m on po.ManufacturerId = m.Id
	left join users.UserRules ur ON po.BuyerId = ur.BuyerId
	left join StdUsuarios.dbo.Usuario u ON ur.Username = u.Id 
	left join [setup].[DistributorIsapre] di on po.IdZona = di.[IdZona]
END