﻿-- =============================================
-- Author:		Juan M Hernandez - C. astroza
-- Create date: 27/02/2018 - 2018-08-16
-- Description:	Obtener los GES con sugerido de compra
-- =============================================

CREATE PROCEDURE [aux].[GetSummaryGES]
@USERNAME NVARCHAR(MAX),
@PROVEEDORES INT,
@DISTRIBUIDOR INT,
@RUBRO NVARCHAR(10),
@SKU INT,
@OC INT,
@DIASINV_DESDE INT,
@DIASINV_HASTA INT,
@BM INT = NULL
AS
BEGIN

SET NOCOUNT ON;

DECLARE @USUARIO_LOCAL NVARCHAR(MAX) = @USERNAME;
DECLARE @FILTRO_DIAS_INV BIT = NULL;

IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USERNAME = null;
END

IF @OC = 0 BEGIN
SET @OC = null
END

IF @RUBRO = '' BEGIN
SET @RUBRO = NULL;
END

IF @DISTRIBUIDOR = 0 BEGIN
SET @DISTRIBUIDOR = NULL;
END

IF @DIASINV_DESDE <> 0 OR @DIASINV_HASTA <> 0 BEGIN
SET @FILTRO_DIAS_INV = 1;
END



------------- sku para filtro de proveedores ------------
SELECT p.SKU 
INTO #SkuManufacturer 
FROM products.Products p
INNER JOIN products.Manufacturers m on m.Id = p.ManufacturerId
INNER JOIN products.Vademecum v on v.SKU = p.SKU
WHERE ManufacturerId = @PROVEEDORES 
-------------  detalle de OC proveedores ----------------
select po.Id, po.RUT, pod.sku, pod.Quantity
INTO #Proveedores
from aux.PurchaseOrders po
INNER JOIN aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
where po.RUT not in (select DistributorRUT from products.Vademecum) 
group by po.Id, po.RUT, pod.sku, pod.Quantity
	

SELECT *
,cast (DENSE_RANK() over (order by sku desc) as int ) [Rank]
 FROM (
	             /*Dsitribuidores*/
				SELECT
									   
					   distinct
					   d.[Name] Laboratorio,
					   p.Category Categoria,
					   d.RUT,
					   i.Isapre,
					   i.IdZona,
					   p.[Name] Nombre,
					   p.LogisticAreaId Rubro,
					   v.SKU,
					   NULL FillRate, 
					   NULL DiasInventario,
					   NULL DiasAdicionales,
					   NULL RemainingStockDays,
					   NULL FaltanteDistribuidor,
					   ps.LastMonthSalesGES RotacionDistribuidor,
					   pc.Price Precio ,
					   pc.Cost Costo ,
					   --(pc.Cost * ps.Purchase) CostoTotal, 			
					   CASE WHEN ps.Purchase > 0 THEN (pc.Cost * ps.Purchase) ELSE NULL END CostoTotal,		   
					   NULL StockLocales,
					   NULL StockCD,
					   NULL Quiebre,
					   NULL  VentaUltimoMes,
					   NULL DemandaSB,
					   NULL PUDemand,
					   NULL Sugerencia,
					   CASE WHEN ps.LastMonthSalesGES is not null 
					   THEN ISNULL(ps.LastMonthSalesGES, 0) 
					   else NULL end SugerenciaFinal,
					   ps.Purchase Compra,
					   cast (0 as bit) IsPromotion,
					   CASE WHEN ps.Purchase > 0 THEN a.[Days] ELSE NULL END DiasCompra,
					   NULL Cobertura,
					   NULL TransitoCD,
					   NULL TransitoLocal,
					   NULL Faltante,
					   NULL TotalRanking,
					   CAST(CASE WHEN(CASE WHEN pod.SKU IS NOT NULL THEN 1 ELSE 0 END) >0
												THEN 'comprado.png'
												ELSE 
												  CASE WHEN ps.Status = 1 
												  THEN  'proceso.png'
												  ELSE 'no_comprado.png' END
												END
					   as nvarchar) [Status],
					   CAST(CASE WHEN(CASE WHEN pod.SKU IS NOT NULL THEN 1 ELSE 0 END) >0
												THEN 'Comprado'
												ELSE 
												  CASE WHEN ps.Status = 1 
												  THEN  'En proceso'
												  ELSE 'Por comprar' END
												END
					   as nvarchar) [ToolTip],
					   1 IsDistributor,
					   null LostSaleLastMonth,
					   null FitNextMonth,
					   p.Motive,
					   p.motiveId,
					   lp.BuyingMultiple,
					   lp.UnitsInBox UnidadesCaja,
					   b.Id PurchaseOrderId,
					   CAST(CASE WHEN b.SKU IS NOT NULL THEN 0 else 1 END AS BIT) PermitirEdicion,
					   NULL SugeridoHoy,
					   NULL DiasInventarioCD
				FROM Products.Vademecum v				
				INNER JOIN  Products.Distributors d on d.RUT = v.DistributorRUT
				INNER JOIN Products.Products p on p.SKU = v.SKU
				INNER JOIN users.SkuUsers su on p.SKU = su.SKU
				LEFT JOIN aux.PurchaseSummaryDistributor ps on ps.SKU = v.SKU AND ps.RUT  COLLATE DATABASE_DEFAULT = v.DistributorRUT AND v.IdZona = ps.IdZona
				INNER JOIN setup.DistributorIsapre i on i.IdZona = ps.IdZona
				LEFT JOIN setup.PriceAndCostGES pc on pc.SKU = v.SKU and pc.IdZona = ps.IdZona 
				LEFT JOIN ( select * from [function].SKUPurchased ('GES'))pod on pod.SKU = v.SKU and pod.RUT COLLATE DATABASE_DEFAULT = d.RUT and pod.IdZona = ps.IdZona
				LEFT JOIN [setup].[LogisticParameters] lp on lp.SKU = ps.SKU
				LEFT JOIN(
					SELECT TOP 1
					SKU, oc.Id, oc.IdZona
					FROM aux.PurchaseOrders oc
					INNER JOIN aux.PurchaseOrderDetails det ON oc.Id = det.PurchaseOrderId
					WHERE OC.CreatedBy = @USUARIO_LOCAL
					AND IdZona <> 37
				)b ON p.SKU = b.SKU AND v.IdZona = b.IdZona

				OUTER APPLY [suggest].[GetInventoryDays](p.SKU,ps.Purchase,GETDATE()) a
				WHERE    (@USERNAME IS NULL OR su.Username = @USERNAME) 
						 AND (v.SKU = @SKU or @SKU = 0) 
						 AND (v.SKU in (select sku from #SkuManufacturer ) or @PROVEEDORES = 0)
						 AND (@OC IS NULL OR b.Id = @OC)
						 AND (@RUBRO IS NULL OR p.LogisticAreaId = @RUBRO)
						 AND (@DISTRIBUIDOR IS NULL OR v.IdZona = @DISTRIBUIDOR)
						 AND (LP.BuyingMultiple = @BM OR @BM = 0  OR @BM is null)
				
				UNION  
				/* Proveedores */
				SELECT	
					   distinct
				       m.[Name] Laboratorio,
					   p.Category Categoria,
					   m.RUT,
					   null Isapre,
					   37 IdZona,
					   p.[Name] Nombre,
					   p.LogisticAreaId Rubro,
					   ps.SKU,
					   ps.FillRate ,
					   ps.DaysToCover DiasInventario,
					   CASE WHEN ps.AdditionalDaysToCover = 0 THEN NULL ELSE ps.AdditionalDaysToCover END DiasAdicionales,
					   ps.RemainingStockDays,
					   null FaltanteDistribuidor,
					   null RotacionDistribuidor,
					   pc.Price  Precio,
					   pc.Cost Costo,
					   (pc.Cost * ps.Purchase) CostoTotal, 
					   ps.StoresStock StockLocales,
					   ps.DCStock StockCD,
					   ps.StockOut Quiebre ,
					   ps.LastMonthSale VentaUltimoMes,
					   ps.SBDemand DemandaSB,
					   ps.PUDemand,
					   ps.suggest Sugerencia ,
					   ps.PurchaseSuggest SugerenciaFinal,
					   
					   isnull (ps.Purchase, 0) Compra,
					  
					   case when sp.SKU is null then cast (0 as bit)  else cast (1 as bit) end IsPromotion, 
					   a.[Days] DiasCompra,
					   ps.Coverage Cobertura,
					   ps.DCTransit TransitoCD,
					   ps.StoresTransit TransitoLocal,
					   ps.StoresShortage Faltante,
					   ps.TotalRanking,
					   CAST(CASE WHEN (CASE WHEN pod.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0
												THEN 'comprado.png'
												ELSE 
												  CASE WHEN ps.Status =1  
												  THEN  'proceso.png'
												  ELSE 'no_comprado.png' END
												END
									   as nvarchar) [Status],
									   CAST(CASE WHEN(CASE WHEN pod.SKU IS NOT NULL THEN 1 ELSE 0 END) >0
												THEN 'Comprado'
												ELSE 
												  CASE WHEN ps.Status = 1 
												  THEN  'En proceso'
												  ELSE 'Por comprar' END
												END
					   as nvarchar) [ToolTip],
						0 IsDistributor

						, ps.LostSaleLastMonth
						,ps.FitNextMonth
						,p.Motive 
						,p.MotiveId
						,lp.BuyingMultiple
					    ,lp.UnitsInBox UnidadesCaja
						,b.Id PurchaseOrderId
						,CAST(CASE WHEN b.SKU IS NOT NULL THEN 0 else 1 END AS BIT) PermitirEdicion
						,ps.DistributionSuggest SugeridoHoy
						,diasInv.Days DiasInventarioCD
				FROM aux.PurchaseSummary ps
				INNER JOIN Products.Products p on p.SKU = ps.SKU
				INNER JOIN products.Manufacturers m on m.Id = p.ManufacturerId
				LEFT JOIN setup.PriceAndCost pc on pc.SKU =ps.SKU
				INNER JOIN users.SkuUsers su on p.SKU = su.SKU
				LEFT JOIN (
				SELECT SKU, SUM(LastMonthSalesGES)LastMonthSalesGES  FROM aux.PurchaseSummaryDistributor
				GROUP BY SKU
				) vta ON ps.SKU = vta.SKU
				LEFT JOIN #Proveedores prov on prov.SKU = ps.SKU AND prov.RUT = m.RUT
				LEFT JOIN products.AdditionalStockDays asd on ps.SKU = asd.SKU
				LEFT JOIN (select * from [function].SKUPurchased ('GES')) pod on pod.SKU = ps.SKU and pod.RUT  = m.RUT COLLATE DATABASE_DEFAULT
				LEFT JOIN suggest.PromotionProducts sp on sp.SKU = ps.SKU
				LEFT JOIN [setup].[LogisticParameters] lp on lp.SKU = ps.SKU

				LEFT JOIN(
					SELECT TOP 1
					SKU, oc.Id
					FROM aux.PurchaseOrders oc
					INNER JOIN aux.PurchaseOrderDetails det ON oc.Id = det.PurchaseOrderId
					WHERE OC.CreatedBy = @USUARIO_LOCAL
					AND IdZona = 37
				)b ON p.SKU = b.SKU

				LEFT JOIN (
				SELECT DISTINCT SKU, Idzona FROM
				products.Vademecum
				) v ON ps.SKU = v.SKU


				OUTER APPLY [suggest].[GetInventoryDays](p.SKU,ps.Purchase+ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) a
				OUTER APPLY [suggest].[GetInventoryDays](p.SKU,ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) diasInv
				WHERE (@USERNAME IS NULL OR su.Username = @USERNAME) 
					  AND (PS.SKU = @SKU or @SKU = 0) AND (ps.SKU in (select sku from #SkuManufacturer ) or @PROVEEDORES = 0)
					  AND (@OC IS NULL OR b.Id = @OC)
					  AND (ps.SKU IN (SELECT SKU FROM products.Vademecum))
					  AND (@RUBRO IS NULL OR p.LogisticAreaId = @RUBRO)
					  AND ((@FILTRO_DIAS_INV IS NULL) OR  (diasInv.Days BETWEEN @DIASINV_DESDE AND @DIASINV_HASTA) OR (diasInv.Days IS NULL) )
					  AND (@DISTRIBUIDOR IS NULL OR v.IdZona = @DISTRIBUIDOR)
					  AND (LP.BuyingMultiple = @BM OR @BM = 0  OR @BM is null)
				
				) a 
				order by A.SKU
				

DROP TABLE #Proveedores
DROP TABLE #SkuManufacturer


end