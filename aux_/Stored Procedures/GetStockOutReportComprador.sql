﻿

-- =============================================
-- Author:		Jorge I.
-- Create date: 24-05-2018
-- Description:	Obtiene los indicadores de quiebre de stock para compradores
-- =============================================

CREATE PROCEDURE [aux].[GetStockOutReportComprador]
	@IdComprador NVARCHAR(50)
AS
BEGIN

	--DECLARE @IdComprador NVARCHAR(50)='pvargas'
		SELECT	a.SKU,
			a.Name ProdName,
			a.GroupId,
			a.Fabricante,
			a.cobertura,
			ISNULL(CAST((CASE	WHEN a.[DíasCD]>=5 THEN 0
						WHEN a.LTProv IS NULL THEN a.suggest5 -(a.[StockCD])	
						WHEN (a.LTProv+1)<=a.[DíasCD] THEN (CASE WHEN a.suggest5<=(a.[StockCD]+a.[UnidadesCompra]) THEN 0 ELSE a.suggest5 -(a.[StockCD]+a.[UnidadesCompra]) END )
						ELSE (a.LTProv+1)-a.[DíasCD]*(a.suggest5/5) END) AS INT),0) [Faltante],
			ISNULL(a.LocalesQuebrados,0) LocalesQuebrados,
			ISNULL(CAST(a.LocalesQuebrados as float)/a.Cobertura,0) PorcQuiebreLocales
	FROM [reports].[AuxStockOutReports] a 
	WHERE a.IdComprador = @Idcomprador
	


END
