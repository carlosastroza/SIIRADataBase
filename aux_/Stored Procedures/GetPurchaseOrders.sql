﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-15
-- Description:	Obtiene el resumen de las órdenes de compra antes que se están gestionando en una determinada pantalla.

--UPDATE: Se implementa consideración para las ordenes con estado GSS - Genera Sobre Stock. jibacache
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrders]
	@type nvarchar(3),
	@USERNAME nvarchar(max)
AS
BEGIN

SET NOCOUNT ON;

SELECT  po.Id,
			po.PurchaseType,
			l.RUT,
			ISNULL(isa.Isapre,'-') Isapre,
			l.Laboratory,
			po.LogisticAreaId,
			po.AvailableToBuy PermiteComprar,
			CASE 
				WHEN po.AvailableToBuy = 'RDY' THEN 'OC Correcta'
				WHEN po.AvailableToBuy = 'PEN' THEN 'Pendiente'
				WHEN po.AvailableToBuy = 'GSS' THEN 'Genera Sobrestock'
				ELSE 'Rechazada'
			END Mensaje,
			CASE 
				WHEN po.AvailableToBuy = 'RDY' THEN 'success'
				WHEN po.AvailableToBuy = 'PEN' THEN 'warning'
				WHEN po.AvailableToBuy = 'GSS' THEN 'warning'
				ELSE 'danger'
			END MensajeColor,
			qs.[#Skus] QSKU,
			qs.PurchaseQuantity,
			CASE WHEN po.IdZona = 37 THEN qs.PurchaseCost ELSE ges.PurchaseCost END PurchaseCost,
			--qs.PurchaseCost,
			po.IsFree,
			po.IsRetail,
			po.CreatedDate,
			po.ReceptionDate,
			po.OrderComment,
			po.Store,
			d.Id,
			d.[Description]
	FROM (
		SELECT DISTINCT po.*
			FROM aux.PurchaseOrders po
			--INNER JOIN aux.PurchaseOrderDetails pod ON po.Id = pod.PurchaseOrderId
			--INNER JOIN products.Products p ON pod.SKU = p.SKU
			--INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
			--INNER JOIN users.SkuUsers su on pod.SKU = su.SKU
			WHERE CreatedBy = @USERNAME) po

	LEFT  JOIN setup.DistributorIsapre isa ON po.IdZona = isa.IdZona

	--proveedores o laboratorios
	INNER JOIN (SELECT	r.RUT,
						ISNULL(m.[Name],d.[Name]) Laboratory,
						m.Id ManufacturerId
						FROM (SELECT RUT FROM products.Manufacturers 
					    UNION 
					    SELECT RUT FROM products.Distributors) r
						LEFT JOIN products.Manufacturers m ON r.RUT = m.RUT
						LEFT JOIN products.Distributors d ON r.RUT = d.RUT) l ON po.RUT = l.RUT AND (po.ManufacturerId = l.ManufacturerId OR po.RUT IN (SELECT RUT FROM products.Distributors))

	LEFT JOIN (SELECT	PurchaseOrderId,
						count(*) [#Skus],
						SUM(Quantity) PurchaseQuantity,
						SUM(Quantity*CASE WHEN oc.PurchaseType = 'ESP' THEN psp.Cost ELSE pc.Cost END) PurchaseCost
						FROM aux.PurchaseOrderDetails pod
						INNER JOIN aux.PurchaseOrders oc on pod.PurchaseOrderId = oc.Id
						LEFT JOIN setup.PriceAndCost pc ON pod.SKU = pc.SKU	
						LEFT JOIN products.ProductsSpecialPurchase psp ON pod.SKU = psp.SKU			
						GROUP BY PurchaseOrderId) qs ON po.Id = qs.PurchaseOrderId

	LEFT JOIN (SELECT	det.PurchaseOrderId,
						SUM(Quantity*pc.Cost) PurchaseCost
						FROM aux.PurchaseOrderDetails det
						INNER JOIN aux.PurchaseOrders oc ON det.PurchaseOrderId = oc.Id					
						INNER JOIN setup.PriceAndCostGES pc ON det.SKU = pc.SKU AND oc.IdZona = pc.IdZona
						WHERE oc.IdZona <> 37
						GROUP BY det.PurchaseOrderId) ges ON po.Id = ges.PurchaseOrderId


	INNER JOIN setup.DeliveryType d ON po.DeliveryTypeId = d.Id
	
	WHERE po.[PurchaseType] = @type
END
