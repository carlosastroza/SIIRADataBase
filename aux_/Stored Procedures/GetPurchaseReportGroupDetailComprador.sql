﻿

---- =============================================
---- Author:		Jorge I.
---- Create date:	23-05-2018
---- Description:	Obtiene los indicadores de compra para compradores detallados dentro de un grupo de skus
---- =============================================

CREATE PROCEDURE [aux].[GetPurchaseReportGroupDetailComprador]
	@IdComprador NVARCHAR(50),
	@IdGrupoSKU int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @fecha DATE = CAST(GETDATE() as date)


	SELECT	bi.GroupId [IdGrupo],
			bi.SKU,
			bi.Name NombreProducto,
			ISNULL(bi.Quantity,0) CantidadComprado,
			CAST(ISNULL(bi.Cost,0) as float) [Costo],
			ps.CompraSugerida,
			CASE WHEN ps.CompraSugerida > 0 THEN CAST(ISNULL(bi.Quantity,0)/ps.CompraSugerida as float) ELSE  CAST(0 as float) END PorcComprado
	FROM aux.BaseIndicadores bi
	LEFT JOIN (SELECT	SKU,
						Purchase CompraSugerida 
				FROM suggest.purchasesuggest) ps on ps.SKU=bi.SKU
	LEFT JOIN stdusuarios.dbo.usuario u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS=bi.comprador
	LEFT JOIN (SELECT	od.PurchaseOrderId,
						od.SKU,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date)= @fecha
				) oc ON oc.sku = bi.sku
	WHERE bi.FechaCompra=@fecha
	AND u.Id = @idComprador
	AND bi.GroupId = @IdGrupoSKU
	AND bi.SKU NOT IN (SELECT SKU FROM Salcobrand.products.GenericFamilyDetail)
	
	UNION
		SELECT	bi.GroupId [IdGrupo],
			bi.SKU,
			bi.Name NombreProducto,
			ISNULL(bi.Quantity,0) CantidadComprado,
			CAST(ISNULL(bi.Cost,0) as float) [Costo],
			ps.CompraSugerida,
			CASE WHEN ps.CompraSugerida > 0 THEN CAST(ISNULL(bi.Quantity,0)/ps.CompraSugerida as float) ELSE  CAST(0 as float) END PorcComprado
	FROM aux.BaseIndicadores bi
	LEFT JOIN (SELECT	SKU,
						Purchase CompraSugerida 
				FROM suggest.purchasesuggest) ps on ps.SKU=bi.SKU
	LEFT JOIN stdusuarios.dbo.usuario u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS=bi.comprador
	LEFT JOIN (SELECT	od.PurchaseOrderId,
						od.SKU,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date)= @fecha
				) oc ON oc.sku = bi.sku
	WHERE bi.FechaCompra=@fecha
	AND u.Id = @idComprador
	AND bi.GroupId = @IdGrupoSKU
	AND bi.SKU IN (SELECT SKU FROM products.GenericTenderedSKU)
END
