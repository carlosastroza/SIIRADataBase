﻿-- =============================================
-- Author:		C Astroza
-- Create date: 2018-03-28
-- Description:	Obtiene las órdenes de compra creadas el día (detalle)
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrdersCreatedTodayDetail]
@USERNAME NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USERNAME);
IF ISNULL(@Usuario_admin,0) = 1 BEGIN
SET @USERNAME = null;
END

SELECT 
d.SKU,
p.Name Nombre, 
Quantity Cantidad, 
oc.Id OC, 
oc.PurchaseType TipoCompra, 
oc.RUT, 
oc.LogisticAreaId Rubro, 
d.Quantity*co.Cost Costo, 
CAST(CASE WHEN oc.IsFree = 1 THEN 'SI' ELSE 'NO' END AS nvarchar) Gratis,
oc.ReceptionDate FechaRecepcion,
oc.OrderComment Glosa, 
oc.Store Local
FROM purchase.PurchaseOrderDetails d
INNER JOIN purchase.PurchaseOrders oc on d.PurchaseOrderId = oc.Id
INNER JOIN products.Products p on d.SKU = p.SKU
INNER JOIN setup.PriceAndCost co on d.SKU = co.SKU

INNER JOIN users.SkuUsers su on d.SKU = su.SKU
WHERE cast(oc.CreatedDate as date)= convert(date,getdate())
AND(@USERNAME is null or su.Username = @USERNAME)		
		

END
