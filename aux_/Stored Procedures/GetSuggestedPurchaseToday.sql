﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 2018-08-14
-- Description:	Obtiene Sugeridos de compra
-- =============================================

CREATE PROCEDURE [aux].[GetSuggestedPurchaseToday]
@USERNAME NVARCHAR(MAX),
@DIA INT,
@TIPO_COMPRA NVARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

SET DATEFIRST 1;
DECLARE @DOW INT = DATEPART(DW, GETDATE())

DECLARE @username_local NVARCHAR(MAX) = @USERNAME;
DECLARE @dia_local INT = @DIA;
IF @dia_local IS NULL BEGIN
SET @dia_local = @DOW
END
IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @username_local = null;
END

DECLARE @GEN BIT = NULL;
DECLARE @GES BIT = NULL;

IF @TIPO_COMPRA = 'GEN' BEGIN
SET @GEN = 1;
END

IF @TIPO_COMPRA = 'GES' BEGIN
SET @GES = 1;
END


SELECT
p.SKU, PurchaseSuggest, GroupId GroupId, ISNULL(G.FantasyName, G.Name) GroupName
FROM products.Products p
LEFT JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
INNER JOIN users.SkuUsers su ON p.SKU = su.SKU
INNER JOIN products.Groups g ON p.GroupId = g.Id
WHERE ( (PurchaseOnMonday = 1 and @dia_local = 1) or
		  (PurchaseOnTuesday = 1 and @dia_local = 2) or
		  (PurchaseOnWednesday = 1 and @dia_local = 3) or
		  (PurchaseOnThursday = 1 and @dia_local = 4) or
		  (PurchaseOnFriday = 1 and @dia_local = 5) or
		  (PurchaseOnSaturday = 1 and @dia_local = 6) or
		  (PurchaseOnSunday = 1 and @dia_local = 7))
AND (@username_local IS NULL OR su.Username = @username_local)
AND (@GEN IS NULL OR p.IsGeneric = 1)
AND (@GES IS NULL OR p.SKU IN (SELECT SKU FROM products.Vademecum))

ORDER BY 1 ASC

END