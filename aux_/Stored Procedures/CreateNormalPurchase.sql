﻿CREATE PROCEDURE [aux].[CreateNormalPurchase]
 @GRUPO INT,
 @USERNAME NVARCHAR(50),
 @skus NVARCHAR(MAX) = NULL
AS
BEGIN
SET NOCOUNT ON;

DECLARE @USUARIO_BACK NVARCHAR(50) = @USERNAME;
	
IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USERNAME = NULL;
END

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Creando órdenes de Compra GEN , SP[aux].[CreateGenericFamilyPurchase]', host_name(), @USUARIO_BACK, 'POST')

DECLARE @skus_enviados TABLE (SKU INT NOT NULL PRIMARY KEY);
INSERT INTO @skus_enviados 
SELECT ISNULL(CAST([Name] AS INT), -1) FROM dbo.SplitString(@skus)

DECLARE @MaxDaysInt INT;
DECLARE @MaxDays TABLE(
Id NVARCHAR(MAX),
Value INT
)
INSERT INTO @MaxDays
EXEC [setup].[GetConfigurationDictionary] 'SB'
SET @MaxDaysInt = (SELECT Value FROM @MaxDays)


DECLARE @ResultadoTemporal TABLE (
	Ranking INT,
	SKU INT,
	Compra INT,
	Laboratorio INT, 
	RUT INT, 
	Rubro NVARCHAR(MAX), 
	LeadTime INT, 
	PurchaseSuggest INT,
	DIAS_SOBRESTOCK INT,
	GrupoPersonalizado INT)

INSERT INTO @ResultadoTemporal
SELECT	RANK() OVER (PARTITION BY m.RUT, p.ManufacturerId, p.LogisticAreaId ORDER BY p.SKU ASC) AS [Rank],
		p.SKU,
		ISNULL(ps.Purchase,0),
		p.ManufacturerId,
		m.RUT,
		p.LogisticAreaId,
		g.LeadTime,
		ps.PurchaseSuggest,
		a.Days,
		CASE WHEN g.CustomGroup <> 0 THEN g.Id ELSE 0 END Personalizado
FROM products.Products p
INNER JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
INNER JOIN products.Manufacturers m on p.ManufacturerId = m.Id
INNER JOIN products.Groups g on p.GroupId = g.Id
INNER JOIN users.SkuUsers su on p.SKU = su.SKU
INNER JOIN setup.PriceAndCost pc on p.SKU = pc.SKU
OUTER APPLY [suggest].[GetInventoryDays](p.SKU,ps.PURCHASE+ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) a
WHERE	(p.GroupId = @GRUPO) and
		(@USERNAME IS NULL OR su.Username = @USERNAME) and
		(pc.Cost > 0) and
		(@skus is null or p.SKU IN (SELECT SKU FROM @skus_enviados))


DECLARE @SKU_A_COMPRAR INT = (SELECT COUNT(*) FROM @ResultadoTemporal WHERE Compra > 0);

	BEGIN TRY
		MERGE INTO aux.PurchaseOrders as oc
		USING (SELECT * FROM @ResultadoTemporal WHERE Ranking = 1) AS rs
		on rs.Rut = oc.RUT AND rs.Rubro COLLATE DATABASE_DEFAULT =  oc.LogisticAreaId AND rs.Laboratorio = oc.ManufacturerId 
		AND oc.PurchaseType = 'NOR' AND oc.ParentId IS NULL AND oc.CreatedBy = @USUARIO_BACK AND oc.CustomGroup = rs.GrupoPersonalizado
		WHEN NOT MATCHED THEN
		INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate,LeadTime, ReceptionDate, isFree, PurchaseType,IsRetail, ManufacturerId, AvailableToBuy, CustomGroup)
		VALUES(rs.Rut, rs.Rubro, 1,@USUARIO_BACK, GETDATE(),rs.LeadTime,DATEADD(DAY, LeadTime, CONVERT(DATE, GETDATE())), 0, 'NOR', 1, rs.Laboratorio, 'RDY',GrupoPersonalizado);

	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló creando órdenes de compra GEN, SP [aux].[CreateGenericFamilyPurchase], ' + ERROR_MESSAGE() , host_name(), @USUARIO_BACK, 'POST');
		THROW;
	END CATCH

DECLARE @id int, 
		@count int
Set @id=1
select @count=count(*)from @ResultadoTemporal 
DECLARE @sku int, @Rut int,@Lab INT,@rubro VARCHAR(MAX), @ORDEN_COMPRA int, @sku_cantidad int,  @SKU_DETALLE INT, @PurchaseSuggest INT
DECLARE @CustomGroup INT;

DECLARE @GENERA_SOBRESTOCK INT = 0;
DECLARE @CANTIDAD_DIAS_SS  INT = 0;

while @id<=@count
begin
SET @PurchaseSuggest   = (select PurchaseSuggest from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @sku   = (select SKU from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @Rut   = (select RUT from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @rubro = (select Rubro from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @Lab   = (select Laboratorio from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @sku_cantidad = (select Compra from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET  @CustomGroup  = (select GrupoPersonalizado from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @ORDEN_COMPRA = (select top 1 oc.ID from aux.PurchaseOrders oc where oc.RUT = @Rut AND oc.LogisticAreaId = @rubro AND ManufacturerId = @Lab 
AND PurchaseType = 'NOR' and ParentId IS NULL AND CreatedBy = @USUARIO_BACK and CustomGroup = @CustomGroup)
SET @SKU_DETALLE  = (select top 1 count(*) from aux.PurchaseOrderDetails ocD where ocd.SKU = @sku AND PurchaseOrderId = @ORDEN_COMPRA);


IF @sku_cantidad > 0
BEGIN
	UPDATE aux.PurchaseSummary SET Purchased = 1 WHERE SKU = @sku
END


SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) 
						  FROM @ResultadoTemporal 
						  WHERE (DIAS_SOBRESTOCK > @MaxDaysInt)
						  AND   (RUT = @rut) 
						  AND   (Rubro = @rubro) 
						  AND   (Laboratorio = @lab)
						  AND   (Compra > 0));

IF @CANTIDAD_DIAS_SS > 0 
BEGIN
	UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @ORDEN_COMPRA

	SET @GENERA_SOBRESTOCK = 1;
END
ELSE
BEGIN
	UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @ORDEN_COMPRA
END 

IF @SKU_DETALLE > 0
	BEGIN
		BEGIN TRY
			UPDATE aux.PurchaseOrderDetails set Quantity = @sku_cantidad WHERE SKU = @sku and PurchaseOrderId = @ORDEN_COMPRA;
		END TRY 
		BEGIN CATCH
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Falló actualizando sku '+ @sku +' , SP [aux].[CreateGenericFamilyPurchase], ' + ERROR_MESSAGE() , host_name(), @USUARIO_BACK, 'POST');
			THROW;
		END CATCH
	END
ELSE
	BEGIN
		BEGIN TRY
			INSERT INTO aux.PurchaseOrderDetails VALUES(@ORDEN_COMPRA,@sku,@SKU_CANTIDAD, @PurchaseSuggest);
		END TRY 
		BEGIN CATCH
			INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
			VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Falló insertando sku '+ @sku +' , SP [aux].[CreateGenericFamilyPurchase], ' + ERROR_MESSAGE() , host_name(), @USUARIO_BACK, 'POST');
			THROW;
		END CATCH
	END

select @id=@id+1
end

DELETE 
FROM aux.PurchaseOrderDetails
WHERE Quantity = 0;

DELETE FROM aux.PurchaseOrders
WHERE	ID not in (select PurchaseOrderId 
					from aux.PurchaseOrderDetails) and
		PurchaseType = 'NOR'

INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],Usuario)
VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Órdenes de compra GEN creadas correctamente, SP [aux].[CreateGenericFamilyPurchase], ' , host_name(),@USUARIO_BACK)

IF @SKU_A_COMPRAR = 0 BEGIN
	SELECT @SKU_A_COMPRAR SKUaComprar
END

IF @SKU_A_COMPRAR > 0 AND @GENERA_SOBRESTOCK > 0 BEGIN
	SELECT @GENERA_SOBRESTOCK GeneraSS
END

IF @SKU_A_COMPRAR > 0 AND @GENERA_SOBRESTOCK = 0 BEGIN
	SELECT 2 Compra
END


END
