﻿
CREATE PROCEDURE [aux].[GetMonthlyEstimatedPurchase]
AS
BEGIN

SET NOCOUNT ON;

select 
Rut,
CASE WHEN Division = 1 THEN 'Farma' ELSE 'Consumo Masivo' END Division,
CompraActualUn,
CompraActualNeto,
CompraMensualUn,
CompraMensualNeto
FROM [aux].[MonthlyEstimatedPurchaseView]

END

