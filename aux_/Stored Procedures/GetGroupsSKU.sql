﻿
CREATE PROCEDURE [aux].[GetGroupsSKU]
@USERNAME NVARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

--IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
--SET @USERNAME = null;
--END

SELECT 
p.SKU,
p.Name Nombre,
p.TotalRanking,
ISNULL(ISNULL(g.Name,g.FantasyName),'Desasignado') Grupo,
M.Name Proveedor,
p.LogisticAreaId Rubro,
g.OrderCycle CicloCompra,
g.LeadTime LeadTime,
p.IsGroupLocked Bloqueado
FROM products.Products p
LEFT JOIN products.Groups g on p.GroupId = g.Id
INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
--INNER JOIN users.SkuUsers su on p.SKU = su.SKU
WHERE (isnull(p.TotalRanking,'null') <> 'Z')
--AND (@USERNAME IS NULL or su.Username = @USERNAME)
ORDER BY p.SKU ASC

END

