﻿CREATE PROCEDURE [aux].[GetTruckQuantity]
@USERNAME NVARCHAR(50)
AS
BEGIN

SET NOCOUNT ON;	

select isnull(a.rut, p.rut) RUT, isnull(a.ReceptionDate, p.ReceptionDate) ReceptionDate
  , a.Unidades UnidadesA, a.Cost CostoA, a.Pallets PalletsA, a.Camiones CamionesA
  , p.Unidades UnidadesP, p.Cost CostoP, p.Pallets PalletsP, p.Camiones CamionesP
from (
  SELECT *
  FROM [aux].[TruckQuantity_Results]
  WHERE scheme= 'a'
) a 
full join (
  SELECT *
  FROM [aux].[TruckQuantity_Results]
  WHERE scheme= 'p'
) p on p.RUT=a.RUT and p.ReceptionDate=a.ReceptionDate

END

