﻿-- =============================================
-- Author:		Juan M Hernandez,  Carlos Astroza
-- Create date: 2018-04-24,  2018-08-10
-- Description:	obtener los proveedores de compras especiales

-- =============================================

CREATE PROCEDURE [aux].[GetManufacturersSpecialPurchase]
@USERNAME NVARCHAR(MAX)
--,@ZONA_COMPRA INT
AS
BEGIN

SET NOCOUNT ON;

	SELECT DISTINCT 
	CAST(ManufacturerId AS NVARCHAR(50)) [Value], 
	m.Name [Text]
	FROM products.ProductsSpecialPurchase p
	INNER JOIN products.Manufacturers m on p.ManufacturerId = m.Id
	--LEFT JOIN products.Vademecum v ON p.SKU = v.SKU
	--WHERE v.IdZona = @ZONA_COMPRA OR @ZONA_COMPRA = 37
	ORDER BY [Text] ASC

END

