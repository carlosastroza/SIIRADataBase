﻿CREATE PROCEDURE [aux].[LoadSpecialPurchases]
@ID_PROCESO uniqueidentifier,
@USERNAME NVARCHAR(MAX)
--,@ZONA INT
AS
BEGIN

SET NOCOUNT ON;

DECLARE @USERNAME_lOCAL NVARCHAR(MAX) = @USERNAME; 

IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USERNAME = null;
END

  DECLARE @Tabla AS TABLE (
  SKU INT, Purchase INT)

  INSERT INTO @Tabla 
  SELECT lo.sku, CASE WHEN isnull (Purchase, 0) > 0 THEN isnull (Purchase, 0) ELSE 0 END 
  from aux.LoadSkuSpecialPurchases lo
  LEFT JOIN products.Vademecum V on LO.SKU = V.SKU
  where (id_proceso = @ID_PROCESO)
  --AND (V.IdZona = @ZONA OR @ZONA = 37)

  MERGE INTO aux.SpecialPurchase as psf
  USING @Tabla AS p
  on p.sku = psf.sku
  WHEN NOT MATCHED THEN
  INSERT (sku, Username, Purchase)
  VALUES (p.sku,@USERNAME_LOCAL, p.Purchase)
  WHEN MATCHED THEN
  Update SET psf.Purchase =  CASE WHEN p.Purchase > 0 THEN p.Purchase ELSE 0 END;

  DELETE FROM aux.LoadSkuSpecialPurchases WHERE id_proceso = @ID_PROCESO
  

  SELECT COUNT(*) FROM @Tabla;
  

end

