﻿
-- =============================================
-- Author:		Juan M  Hernandez
-- Create date: 2018-05-03
-- 
-- =============================================
CREATE PROCEDURE [aux].[CreateSpecialPurchase]
@USUARIO NVARCHAR(MAX)

AS
BEGIN

SET NOCOUNT ON;

DECLARE @USUARIO_AUX NVARCHAR(MAX) = @USUARIO;

	--------------------------------------------------------------------------------------------------------------------------------
	
	DECLARE @MaxDaysInt INT;
	DECLARE @MaxDays TABLE(
	Id NVARCHAR(MAX),
	Value INT
	)
	INSERT INTO @MaxDays
	EXEC [setup].[GetConfigurationDictionary] 'SB'
	SET @MaxDaysInt = (SELECT Value FROM @MaxDays)


	create table #ResultadoTemporal  (
	SKU INT,
	Compra INT, 
	RUT INT,
	Rubro VARCHAR(15),
	LeadTime INT, 
	ManufacturerId INT,
	PurchaseSuggest INT,
	Dias INT
	)

	SET DATEFIRST 1;
    DECLARE @DOW INT = DATEPART(DW, GETDATE())

	INSERT INTO #ResultadoTemporal
	SELECT p.SKU
	, p.Purchase, m.RUT, psp.LogisticAreaId , 
	CASE
		WHEN @DOW = 1 THEN ISNULL(LTMonday,g.LeadTime)
		WHEN @DOW = 2 THEN ISNULL(LTTuesday,g.LeadTime) 
		WHEN @DOW = 3 THEN ISNULL(LTWednesday,g.LeadTime) 
		WHEN @DOW = 4 THEN ISNULL(LTThursday,g.LeadTime) 
		WHEN @DOW = 5 THEN ISNULL(LTFriday,g.LeadTime) 
		WHEN @DOW = 6 THEN ISNULL(LTSaturday,g.LeadTime) 
		WHEN @DOW = 7 THEN ISNULL(LTSunday,g.LeadTime)  
    END LT, m.Id, p.PurchaseSuggest, a.Days
	FROM  aux.PurchaseSummarySpecial p
		INNER JOIN products.ProductsSpecialPurchase psp ON p.SKU = psp.SKU
		LEFT JOIN products.Products pro ON p.SKU = pro.SKU
		INNER JOIN products.Manufacturers m ON psp.ManufacturerId = m.Id
		LEFT JOIN products.Groups g ON pro.GroupId = g.Id
		OUTER APPLY suggest.GetInventoryDays(p.SKU, p.Purchase+psp.StockCD+psp.ActiveOrders-psp.StoresShortage,GETDATE()) a
		WHERE psp.Cost > 0
		AND p.Username = @USUARIO_AUX

--------------------------------------------------------------------------------------------------------------------------------

	MERGE INTO aux.PurchaseOrders as po
	USING #ResultadoTemporal AS rt
	on rt.rut = po.Rut AND rt.Rubro COLLATE DATABASE_DEFAULT =	po.LogisticAreaId AND po.IsRetail = 1 
	AND po.ManufacturerId = rt.ManufacturerId AND po.PurchaseType = 'ESP' AND po.ParentId IS NULL AND po.CreatedBy = @USUARIO_AUX 
	WHEN NOT MATCHED THEN
	INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate, LeadTime, ReceptionDate, isFree, PurchaseType, IsRetail,ManufacturerId, availabletobuy)
	VALUES (rt.rut,rt.Rubro,1,@USUARIO_AUX, GETDATE(),rt.LeadTime,DATEADD(DAY, rt.LeadTime, GETDATE()) ,0,  'ESP',1,rt.ManufacturerId, 'RDY');

----------------------------  Detalle con los datos del Proveedor    --------------------------------------------


DECLARE @GENERA_SOBRESTOCK INT = 0;
Declare 
@id int, 
@count int
Set @id=1
select @count=count(*)from #ResultadoTemporal
DECLARE @sku int, @lab int, @rubro VARCHAR(MAX), @ORDEN_COMPRA int, @sku_cantidad int,  @SKU_DETALLE INT, @MANUFACTUREID INT,@PurchaseSuggest INT

DECLARE @CANTIDAD_DIAS_SS  INT = 0;

while @id<=@count
begin

	SET @sku = (select top 1 SKU from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @PurchaseSuggest = (select top 1 PurchaseSuggest from #ResultadoTemporal where sku = @sku);
	SET @lab = (select top 1 RUT from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @rubro = (select TOP 1 Rubro from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @MANUFACTUREID = (select top 1 ManufacturerId from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @sku_cantidad = (select top 1 Compra from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @ORDEN_COMPRA = (select top 1 oc.ID from aux.PurchaseOrders oc where oc.RUT = @lab AND oc.LogisticAreaId = @rubro 
						 and IsRetail = 1 and PurchaseType = 'ESP' and CreatedBy =  @USUARIO_AUX AND ManufacturerId  = @MANUFACTUREID)
	SET @SKU_DETALLE  = (SELECT COUNT(*) FROM aux.PurchaseOrderDetails det INNER JOIN aux.PurchaseOrders oc ON det.PurchaseOrderId = oc.Id
							WHERE oc.Id = @ORDEN_COMPRA AND SKU = @sku);
    

    SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) 
						  FROM #ResultadoTemporal 
						  WHERE (Dias > @MaxDaysInt)
						  AND   (RUT = @lab) 
						  AND   (Rubro = @rubro) 
						  AND   (ManufacturerId = @MANUFACTUREID));


    IF @CANTIDAD_DIAS_SS > 0 BEGIN
	UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @ORDEN_COMPRA
	SET @GENERA_SOBRESTOCK = 1;
	END
	ELSE
	BEGIN
	UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @ORDEN_COMPRA
	END 

	IF @SKU_DETALLE > 0 BEGIN
			UPDATE aux.PurchaseOrderDetails set Quantity = @sku_cantidad WHERE SKU = @sku;
	END
	ELSE
	BEGIN
		IF @ORDEN_COMPRA is not null 
		BEGIN
		INSERT INTO aux.PurchaseOrderDetails VALUES(@ORDEN_COMPRA,@sku,@SKU_CANTIDAD, @PurchaseSuggest);
		END
	END
	select @id=@id+1
end

---------------------------------------------------------------------------------------------

DELETE FROM aux.PurchaseOrderDetails WHERE Quantity = 0 or Quantity is null;
DELETE FROM aux.PurchaseOrders WHERE ID not in (select PurchaseOrderId from aux.PurchaseOrderDetails) and PurchaseType = 'ESP'

declare @gestionados VARCHAR(MAX)
declare @cont int 
DECLARE @OC_PROBLEMA_FECHA_RECEP INT;

SET @OC_PROBLEMA_FECHA_RECEP = 
(
select COUNT(*) from #ResultadoTemporal 
WHERE LeadTime IS NULL
);


set @cont= (select count(*) from aux.PurchaseOrders po
inner join aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
where PurchaseType = 'ESP' and CreatedBy = @USUARIO_AUX)

IF @cont = 0 BEGIN 
	SET @gestionados = 0; 
	END

ELSE BEGIN 
	IF @GENERA_SOBRESTOCK > 0 BEGIN
		SET @gestionados = '1'
	END
    ELSE BEGIN
		IF @cont = 1 BEGIN

			set @gestionados = 'Se gestiona el siguiente SKU: '
			select @gestionados = COALESCE( @gestionados , ' ') + 
			CAST( pod.sku as nvarchar)  from aux.PurchaseOrders po
			inner join aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
			where PurchaseType = 'ESP' and CreatedBy = @USUARIO_AUX

			--IF @OC_PROBLEMA_FECHA_RECEP > 0
			--BEGIN
			--SET @gestionados += '<br><br>Existen OC sin fecha de recepción.'
			--END

		END
		ELSE BEGIN

			set @gestionados = concat('Se gestionaron ', @cont ,' SKUs.')


			--set @gestionados = concat('Se gestionan ', @cont ,' SKUs, detallados a continuación: ')
			--select @gestionados = COALESCE( @gestionados , ' ') + 
			--CAST(pod.sku as nvarchar)+' '  from aux.PurchaseOrders po
			--inner join aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
			--where PurchaseType = 'ESP' and CreatedBy = @USUARIO_AUX

			
			--IF @OC_PROBLEMA_FECHA_RECEP > 0
			--BEGIN
			--SET @gestionados += '<br><br>Existen OC sin fecha de recepción.'
			--END

		END		
			
			IF @OC_PROBLEMA_FECHA_RECEP > 0
			BEGIN
			SET @gestionados += '<br><br>Existen OC sin fecha de recepción.'
			END

END
END


SELECT @gestionados


drop table #ResultadoTemporal

END
