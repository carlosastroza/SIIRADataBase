﻿
-- =============================================
-- Author:		Jorge I.
-- Create date: 18-05-2018
-- Description:	Obtiene los indicadores de quiebre de stock para jefes
-- =============================================

CREATE PROCEDURE [aux].[GetStockOutReport]
AS
BEGIN
	

	SELECT	x.Nombre Comprador,
					COUNT(x.SKU) TotalSKU,
					SUM(x.QuiebreCD) NumSKUQuebradosCD,
					SUM(x.QuiebreCD)/(cast(COUNT(x.SKU) as float)) PorcSkuQuebradoCD,
					SUM(x.QuiebreLocal) NumSKUQuebradosLoc,
					SUM(x.QuiebreLocal)/(cast(COUNT(x.combinaciones) as float)) PorcSkuQuebradoLoc
					--SUM(x.LocalesQuebrados) CombQuebradas
			FROM (
				SELECT	a.*,
						CASE WHEN a.PorcQuiebre>0.2 THEN 1 ELSE 0 END [QuiebreLocal],
						CASE WHEN a.[DíasCD]>=5 THEN 0
								WHEN (a.[DíasCD]<(a.LTProv+1) and StockTotal<Suggest5) THEN 1 ELSE 1 END [QuiebreCD],
						CAST((CASE	WHEN a.[DíasCD]>=5 THEN 0
									WHEN a.LTProv IS NULL THEN a.suggest5 -(a.[StockCD])	
									WHEN (a.LTProv+1)<=a.[DíasCD] THEN (CASE WHEN a.suggest5<=(a.[StockCD]+a.[UnidadesCompra]) THEN 0 ELSE a.suggest5 -(a.[StockCD]+a.[UnidadesCompra]) END )
									ELSE (a.LTProv+1)-a.[DíasCD]*(a.suggest5/5) END) AS INT) [Faltante]
				FROM [reports].[AuxStockOutReports] a
	) x
	GROUP BY x.Nombre

	
END