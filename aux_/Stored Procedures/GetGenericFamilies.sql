﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 08-01-2018
-- Description:	Obtiene familias de genéricos y resumen de compras de la misma.
-- =============================================

CREATE PROCEDURE [aux].[GetGenericFamilies]
@USERNAME NVARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

IF @USERNAME IN (SELECT Username FROM setup.adminUsers) BEGIN
SET @USERNAME = null
END


select 
CASE 
	WHEN SUM(CASE WHEN PO.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN 'comprado.png'
	WHEN SUM(CASE WHEN OD.SKU IS NOT NULL THEN 1 ELSE 0 END) > 0  THEN  'proceso.png'
ELSE 'no_comprado.png'
END Estado,
CASE 
	WHEN SUM(CASE WHEN PO.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN 'Comprado'
	WHEN SUM(CASE WHEN OD.SKU IS NOT NULL THEN 1 ELSE 0 END) > 0  THEN  'Está en gestión'
ELSE 'Por Comprar'
END ToolTip,
f.Id Familia, 
f.[Name] Descripcion,
CASE 
	WHEN SUM(CASE WHEN PO.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN SUM(po.Quantity)
	WHEN SUM(CASE WHEN OD.SKU IS NOT NULL THEN 1 ELSE 0 END) > 0  THEN  SUM(od.Quantity)
END Unidades,
CASE 
	WHEN SUM(CASE WHEN PO.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN  SUM(po.Quantity)*ROUND(SUM(po.Cost),2)
	--WHEN SUM(CASE WHEN OD.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN  ROUND(SUM(s.Cost),2)
	WHEN SUM(CASE WHEN OD.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN  SUM(od.Monto)
END Costo,
ten.SKU SKU_Licitado,
CASE 
	WHEN SUM(CASE WHEN PO.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN dbo.ConcatenateCreatedGenericSKU(f.Id,@USERNAME)
	WHEN SUM(CASE WHEN OD.SKU IS NOT NULL THEN 1 ELSE 0 END)  > 0  THEN dbo.ConcatenatePendingGenericSKU(f.Id,@USERNAME)
END SKU_Seleccionado

from Salcobrand.products.GenericFamily f
left join Salcobrand.products.GenericFamilyDetail gf on f.Id = gf.GenericFamilyId
--left join setup.PriceAndCost s ON gf.SKU = s.SKU
left join (select * from [function].[SKUPurchased] ('GEN')) po ON gf.SKU = po.SKU
left join (
  select 
  pod.SKU, Quantity, Cost, ROUND(Quantity*Cost,2) Monto
  --pod.* 
  from aux.PurchaseOrders po
  inner join [aux].PurchaseOrderDetails pod ON po.ID = pod.PurchaseOrderId
  left join setup.PriceAndCost s ON pod.SKU = s.SKU
  where po.PurchaseType = 'GEN'
) od ON gf.SKU = od.SKU
INNER JOIN users.SkuUsers su on gf.SKU = su.SKU
LEFT JOIN products.GenericTenderedSKU ten on f.Id = ten.GenericFamilyId
WHERE (@USERNAME IS NULL OR su.Username = @USERNAME)
AND   (gf.SKU NOT IN (SELECT SKU FROM products.Vademecum))
GROUP BY f.Id, f.[Name], ten.SKU
order by Familia asc;

end