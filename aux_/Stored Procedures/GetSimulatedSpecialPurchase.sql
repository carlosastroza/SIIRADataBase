﻿

CREATE PROCEDURE [aux].[GetSimulatedSpecialPurchase]

@FillrateParametro float = null,
@servicelavel float,
@LeadTime int,
@DiasCubrir int,
@User nvarchar(50)


AS
BEGIN

SET NOCOUNT ON;

	DECLARE @Today date = CONVERT(date, getdate())
	
	-----------------------     Obtengo el stock actual del CD y las unidades de las órdenes activas -----------------------------------------

	select SKU, StockCD,  ISNULL(activeOrders, 0) ActiveOrders
	INTO #stock
	from suggest.PurchaseSuggest
	WHERE SKU  in (select sku from aux.SpecialPurchase where Username = @User)


	
	-----------------------  CÁLCULO LA COMPRA SEGÚN LOS DÍAS DE COBERTURA ------------------------------
	DECLARE @Dias INT = ISNULL(@DiasCubrir, 0) + ISNULL (@LeadTime, 0);
	if @Dias = 0
	begin 
		set @Dias= null 
	end 


	SELECT SP.SKU, Shortage.Shortage
		, SUM(CASE WHEN Pedidos.Date>=SP.PurchaseDate AND Pedidos.Date <= Dateadd(day, isnull(@dias, 0)+SP.DaysToCover, SP.PurchaseDate) THEN Pedidos.PedidoEfectivo else 0 end) n,
		min(CASE WHEN Pedidos.Date>=SP.PurchaseDate AND Pedidos.Date <= Dateadd(day, isnull(@dias, 0)+SP.DaysToCover, SP.PurchaseDate) THEN Pedidos.Date end)mind,
		max(CASE WHEN Pedidos.Date>=SP.PurchaseDate AND Pedidos.Date <= Dateadd(day, isnull(@dias, 0)+SP.DaysToCover, SP.PurchaseDate) THEN Pedidos.Date end)maxd,
		CEILING([Salcobrand].[function].NormalProbability (
			SUM(CASE WHEN Pedidos.Date>=SP.PurchaseDate AND Pedidos.Date <= Dateadd(day, isnull(@dias, 0)+SP.DaysToCover, SP.PurchaseDate) THEN Pedidos.PedidoEfectivo else 0 end),
			std.dvStd * SQRT(isnull(@dias, 0)+SP.DaysToCover), MAX(ISNULL(SP.ServiceLevel,0)))
		) + ISNULL(Shortage.Shortage,0) Suggest
	INTO #compra
	FROM [setup].[SuggestParameters] SP
	INNER JOIN	products.ProductsSpecialPurchase pp ON pp.SKU=sp.SKU
	INNER JOIN   [Salcobrand].[minmax].[DistributionOrdersForecastCurrent] Pedidos  ON SP.sku=Pedidos.sku
	INNER JOIN (-- Subquery con el promedio de las desviaciones estandar por dia de la semana, cuando no son 0.
		SELECT a.Sku,avg(a.StdPedidos) dvStd
		FROM (
			SELECT SKU,DATEPART(dw,Date) dow,STDEV(PedidoEfectivo) StdPedidos,COUNT(*) Total 
			FROM [Salcobrand].[minmax].[DistributionOrdersForecastCurrent] 
			GROUP BY SKU,DATEPART(dw,Date) 
			HAVING SUM(PedidoEfectivo)>0
		) a
		GROUP BY a.Sku
	) std ON std.SKU = Pedidos.sku 
	LEFT JOIN (
		SELECT ls.SKU,SUM(ls.suggest)-SUM(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) -SUM(ls.Transit)-SUM(ISNULL(d.Picking,0))  Shortage
		FROM Salcobrand.series.LastStock ls 
		INNER JOIN Salcobrand.stores.Stores S on S.Store=ls.Store
		LEFT JOIN (SELECT sku,store,Picking FROM Salcobrand.series.DespachosCD WHERE date=DATEADD(day,-1,@Today)) d on d.SKU=ls.SKU and d.Store=ls.Store 
		WHERE ls.suggest>(CASE WHEN ls.stock>=0 THEN ls.stock ELSE 0 END) +ls.Transit+ISNULL(d.Picking,0)
		GROUP BY ls.SKU
	) Shortage ON Shortage.SKU= SP.sku 
	WHERE  sp.sku in (select sku from aux.SpecialPurchase where Username = @User)
	GROUP BY SP.SKU,isnull(SP.ServiceLevel,0.95),SP.DaysToCover,SP.PurchaseDate,SP.NextPurchaseDate,SP.LeadTime,std.dvStd,Shortage.Shortage

	--select * from #compra


	--------------------------  Considero stock CD, fill rate y active Orders ------------------------------------------------
	declare @minFillrate float = 0.75
	select  s.sku
		, case 
			when (
				isnull(c.Suggest,0)-isnull(st.ActiveOrders,0)-isnull(st.StockCD,0))/(case when isnull(@FillrateParametro,isnull([FillRate],1))<@minFillrate 
																							then @minFillrate 
																							else isnull(@FillrateParametro,isnull([FillRate],1)) end)< 0 
			then 0 
			else (isnull(c.Suggest,0)-isnull(st.ActiveOrders,0)-isnull(st.StockCD,0))/(case when isnull(@FillrateParametro,isnull([FillRate],1))<@minFillrate 
																							then @minFillrate 
																							else isnull(@FillrateParametro,isnull([FillRate],1)) end) 
	end compra,
	c.Suggest,
	st.ActiveOrders, st.StockCD

	into #comprafillrate
	FROM suggest.PurchaseSuggest s
	inner join #compra c on c.SKU = s.SKU
	inner join #stock st on st.SKU = s.SKU


	------------------------------   Resultado   -------------------------------------------------------------------------------
	SELECT psp.SKU, sug.UnidadesTotales, sug.Suggest
	INTO #result
	FROM (
		select sp.* from aux.SpecialPurchase sp
		inner join products.Productsspecialpurchase b ON sp.SKU = b.SKU
		where sp.Username = @User
	) psp
	LEFT JOIN (
		SELECT ps.SKU ,
			case 
				when  r.UnidadesTotales >=0 then r.UnidadesTotales -- los numeros negativos representan mas stock y unidades en transito
				else 0 
				end UnidadesTotales 
			,C.Suggest
		FROM suggest.PurchaseSuggest ps
		INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
		CROSS APPLY [function].[RoundLogisticUnits] ((SELECT compra FROM #comprafillrate where SKU = ps.sku), null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet,lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
		iNNER JOIN #compra C ON C.SKU = ps.SKU 
		WHERE ps.SKU in (select sku from #comprafillrate)
	) sug ON psp.SKU = sug.SKU


	-- Por lo menos comprar faltante + venta promedio*days to cover {restando stock CD y Transito a CD (falta hacerlo)} para los SKU sin modelo
	update x 
		set UnidadesTotales =   CASE    WHEN (id.venta_promedio IS NULL) 
                                        THEN NULL 
                                        ELSE 
                                            CASE    WHEN (ISNULL(s.Shortage,0)+ISNULL(id.venta_promedio,0)*@Dias-s.StockCD-s.ActiveOrders)>0 
                                                    THEN (ISNULL(s.Shortage,0)+ISNULL(id.venta_promedio,0)*s.daystocover-s.StockCD-s.ActiveOrders) 
                                                    ELSE 0 
                                            END
                                END /   CASE    WHEN ISNULL(@FillrateParametro,ISNULL([FillRate],1))<@minFillrate 
                                                THEN @minFillrate 
                                                ELSE ISNULL(@FillrateParametro,ISNULL([FillRate],1)) 
                                        END
	from #result x
	left join suggest.PurchaseSuggest s on x.SKU = s.SKU
	inner join products.Productsspecialpurchase pr on pr.sku=s.sku
	left join reports.InventoryDays id on id.sku=s.sku
	left join [suggest].[PromotionProducts] pp on pp.sku=s.sku
	where x.suggest is null and pp.sku is null and pr.TotalRanking not like 'E'
	and s.SKU not in (Select SKU from salcobrand.products.GenericFamilyDetail where sku not in (select sku from  [products].[GenericTenderedSKU]))

    UPDATE r
        SET UnidadesTotales =   case    when red.UnidadesTotales >=0 
                                        then red.UnidadesTotales 
				                        else 0 
				                end  
    FROM #result r 
    INNER JOIN [setup].[LogisticParameters] lp on lp.sku=r.sku
    CROSS APPLY [function].[RoundLogisticUnits] (r.UnidadesTotales, null, lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet,lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) red
    
	select * from #result

	drop  table #stock
	drop  table #compra
	drop table #comprafillrate
	drop table #result
end