﻿-- =============================================
-- Author:		Juan M Hernandez, Carlos Astroza
-- Create date: 2018-04-23, 2018-08-10
-- Description:	obtener las unidades logísticas de compras especiales
-- =============================================

CREATE PROCEDURE [aux].[GetLogisticAreaSpecialPurchase]
@PROVEEDOR INT,
@USERNAME NVARCHAR(MAX)
--,@ZONA_COMPRA INT
AS
BEGIN

SET NOCOUNT ON;

	SELECT 
	DISTINCT LogisticAreaId [Value],
	LogisticAreaId  [Text]
	FROM products.ProductsSpecialPurchase p 
	--LEFT JOIN products.Vademecum v ON p.SKU = v.SKU
	WHERE  (@PROVEEDOR is null or  @PROVEEDOR = p.ManufacturerId)
	--AND (v.IdZona = @ZONA_COMPRA OR @ZONA_COMPRA = 37)
	ORDER BY [Text]

END