﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [aux].[UpdateSuggestedPurchaseParameters]
	@sku INT,
	@serviceLevel FLOAT 

AS
BEGIN
	SET NOCOUNT ON;

	-- we check is a valid service level
	IF NOT EXISTS(SELECT Distinct ServiceLevel FROM Salcobrand.minmax.PoissonSuggest WHERE ServiceLevel = @serviceLevel)
	BEGIN
		RAISERROR('El nivel de servicio especificado no se encuentra en la lista de niveles de servicios esperables', 16, 1)
	END
	ELSE BEGIN
		-- we set the service level for the product
		UPDATE products.Products
			SET ServiceLevel = @serviceLevel
		WHERE SKU = @sku
	
	END 
	-- we return the product that was updated 
	SELECT
	RUT, m.Name Manufacturer, p.LogisticAreaId, TotalRanking, SKU, p.Name, ServiceLevel Id, FORMAT(ServiceLevel, 'P1', 'es-CL') Description
	FROM products.Products p
	INNER JOIN products.Manufacturers m ON P.ManufacturerId = m.Id
	WHERE p.SKU = @sku
END