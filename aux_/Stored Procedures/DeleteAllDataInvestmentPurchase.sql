﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 2018-08-27
-- Description:	Elimina data de tablas de compras de inversión

-- =============================================

CREATE PROCEDURE  [aux].[DeleteAllDataInvestmentPurchase]
@USERNAME NVARCHAR(100)
AS
BEGIN

SET NOCOUNT ON;

DELETE FROM aux.OptimizationSkus WHERE Username  =@USERNAME
DELETE FROM aux.DataForInvestmentPurchase WHERE Username = @USERNAME
DELETE FROM AUX.InvestmentPurchase WHERE Username = @USERNAME


end
