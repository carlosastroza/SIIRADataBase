﻿-- =============================================
-- Author:		Carlos A
-- Create date: 2018-08-01
-- Description:	Obtiene un resumen previo del grupo antes de generar la órden de compra.
-- =============================================

CREATE PROCEDURE [aux].[GetGroupPurchasePreview]  
@GRUPO INT
AS
BEGIN

SET NOCOUNT ON;

SELECT 
CAST(g.Id AS nvarchar) + ' - ' + ISNULL(FantasyName,g.Name) Grupo,
COUNT(*) CantidadSKU,
SUM(Purchase) Unidades,
SUM(Purchase*pc.Cost) MontoCompra
FROM products.Products p
INNER JOIN products.Groups g ON p.GroupId = g.Id
LEFT JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
LEFT JOIN setup.PriceAndCost pc ON p.SKU = pc.SKU
WHERE g.Id = @GRUPO
AND ISNULL(Purchase,0) > 0
GROUP BY G.Id, FantasyName, g.Name
end
