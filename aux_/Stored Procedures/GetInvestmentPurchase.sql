﻿CREATE PROCEDURE [aux].[GetInvestmentPurchase]
@USERNAME NVARCHAR(255)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @res int;
SET @res = 
(
SELECT TOP 1 isnull (cast (SimulatedAmount as int), 0) MontoSimulado
from [aux].OptimizationSkus i
INNER JOIN users.SkuUsers su ON i.Sku = su.SKU
WHERE @USERNAME IS NULL OR i.Username = @USERNAME);





DECLARE @username_Local NVARCHAR (MAX) = 'admin'
DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @username_Local);
IF ISNULL(@Usuario_admin,0) = 1 BEGIN
SET @username_Local = null;
END



SELECT 
		sg.SKU,
		p.[Name] Nombre,
		m.[Name] Laboratorio,
		p.logisticareaid Rubro ,
		CASE WHEN fp.PetitorioMinimo > 0 THEN 'SI' ELSE 'NO' END Petitorio,
		FillRate ,
		DaysToCover DiasInventario,
		ISNULL(sg.AdditionalDaysToCover,0) DiasAdicionales,
		pc.Price Precio,
		pc.Cost Costo,
		(pc.Cost)*(Cantidad * CASE WHEN UnidadLogisticaDesc = 'Caja'   
					          THEN l.UnitsInBox 
							  WHEN UnidadLogisticaDesc = 'Layer'  
							  THEN l.UnitsInLayer 
							  WHEN UnidadLogisticaDesc = 'Pallet' 
							  THEN L.UnitsInPallet 
							  WHEN UnidadLogisticaDesc = 'No especificado'
							  THEN 1
							  ELSE l.UnitsInBox END)
        CostoTotal, 
		StoresStock StockLocales,
		DCStock StockCD,
		StockOut Quiebre,
		LastMonthSale VentaUltimoMes,
		sg.Suggest Sugerencia,
		sg.SBDemand,
		sg.PurchaseSuggest SugerenciaFinal,
		Cantidad * CASE WHEN UnidadLogisticaDesc  = 'Caja'   
				THEN l.UnitsInBox 
			WHEN UnidadLogisticaDesc = 'Layer'  
				THEN l.UnitsInLayer 
		   WHEN UnidadLogisticaDesc = 'Pallet' 
				THEN L.UnitsInPallet 
		   WHEN UnidadLogisticaDesc = 'No especificado'
				THEN 1
		   ELSE l.UnitsInBox 
		   END   Compra,
	    case when sp.SKU is null then cast (0 as bit)  else cast (1 as bit) end IsPromotion,
		a.[Days] DiasCompra,
		Coverage Cobertura,
		DCTransit TransitoCD,
		StoresTransit TransitoLocal,
		StoresShortage Faltante,
		sg.TotalRanking TotalRanking,
		sg.OPACCurrent, sg.OPACUpcoming
		, sg.LostSaleLastMonth, sg.FitNextMonth 
		, NULL LeadTime
		, NULL ServiceLevel
		,@res Inversion
	FROM  aux.PurchaseSummary sg
	INNER JOIN [aux].[InvestmentPurchase] inv on sg.SKU = inv.Sku 
	INNER JOIN products.Products p on sg.sku = p.sku
	INNER JOIN products.Manufacturers m on p.ManufacturerId = m.Id
	INNER JOIN Salcobrand.products.FullProducts fp on sg.SKU = fp.SKU 
	INNER JOIN users.SkuUsers su on sg.SKU = su.SKU
	INNER JOIN setup.PriceAndCost pc on sg.SKU = pc.SKU
	INNER JOIN setup.LogisticParameters l ON sg.Sku = l.SKU
	LEFT JOIN suggest.PromotionProducts sp on sp.SKU = sg.SKU
	CROSS APPLY [suggest].[GetInventoryDays](sg.SKU, Cantidad * CASE WHEN UnidadLogisticaDesc  = 'Caja'   
				THEN l.UnitsInBox 
			WHEN UnidadLogisticaDesc = 'Layer'  
				THEN l.UnitsInLayer 
		   WHEN UnidadLogisticaDesc = 'Pallet' 
				THEN L.UnitsInPallet 
		   WHEN UnidadLogisticaDesc = 'No especificado'
				THEN 1
		   ELSE l.UnitsInBox 
		   END 
		      +sg.DCStock+sg.DCTransit-sg.StoresShortage,GETDATE()) a
	WHERE su.Username = @username_Local OR @username_Local IS NULL 
	ORDER BY Cantidad * CASE WHEN UnidadLogisticaDesc  = 'Caja'   
				THEN l.UnitsInBox 
			WHEN UnidadLogisticaDesc = 'Layer'  
				THEN l.UnitsInLayer 
		   WHEN UnidadLogisticaDesc = 'Pallet' 
				THEN L.UnitsInPallet 
		   WHEN UnidadLogisticaDesc = 'No especificado'
				THEN 1
		   ELSE l.UnitsInBox 
		   END  desc
END



