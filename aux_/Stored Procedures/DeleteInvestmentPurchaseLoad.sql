﻿-- =============================================
-- Author:		C. Astroza
-- Create date: 2018-05-30
-- Description:	Borrar cargados por el Usuario Compra Inversión
-- =============================================
CREATE PROCEDURE [aux].[DeleteInvestmentPurchaseLoad]
@USERNAME NVARCHAR(MAX)
AS
BEGIN

DELETE FROM aux.OptimizationSkus
WHERE Username = @USERNAME

DELETE FROM aux.InvestmentPurchase
WHERE Username = @USERNAME

END