﻿-- =============================================
-- Author:		Juan M  Hernandez
-- Create date: 22-03-2018
-- Description:	Actualiza parámetros ges en PurchaseSummaryDistributor
-- =============================================
CREATE PROCEDURE [aux].[UpdateRotationSummary]

AS
BEGIN

SET NOCOUNT ON;

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Actualizando tabla [aux].[PurchaseSummaryDistributor]  , SP[aux].[[UpdateRotationSummary]]', host_name(),  'POST')

	

	BEGIN TRY
		TRUNCATE TABLE aux.PurchaseSummaryDistributor
		INSERT INTO  aux.PurchaseSummaryDistributor
		SELECT
		   ps.SKU,
		   d.RUT,
		   v.IdZona, 
		   dr.LastMonthSalesGES, 
		   case when dr.LastMonthSalesGES is not null 
		   then ISNULL(dr.LastMonthSalesGES, 0) 
		   else null end PurchaseSuggest,
		   ISNULL(dr.LastMonthSalesGES, 0) Purchase,
		   0 [Status] 

		FROM Products.Vademecum v
		INNER JOIN  Products.Distributors d on d.RUT = v.DistributorRUT
		INNER JOIN aux.PurchaseSummary ps on ps.SKU = v.SKU
		left JOIN products.DistributorRotation dr on dr.SKU = v.SKU and dr.IdZona = v.IdZona

		-- actualiza la compra de GES y realiza la resta correspondiente al nuevo archivo de rotación 
		exec aux.UpdatePurchaseSummaryGES

	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA','Falló la actualización de la rotación de los distribuidores en el SP [aux].[PurchaseSummaryDistributor] ', host_name(),  'POST');
		THROW;
	END CATCH

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Actualizando tabla [aux].[PurchaseSummaryDistributor] realizada con éxito , SP[aux].[[UpdateRotationSummary]]', host_name(),  'POST')

END






