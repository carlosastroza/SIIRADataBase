﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 26-04-2018
-- Description:	Obtiene Grupos de compra Comprados hoy
-- Update 2018-07-30: Se cruza con OC recibidas, para reflejar el estado de la OC.
-- =============================================

CREATE PROCEDURE [aux].[GetGroupsPurchasedToday]
@USERNAME NVARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USERNAME = null;
END

SELECT 
g.Id, 
isnull(g.FantasyName,g.Name) NombreFantasia, 
SUM(dt.Quantity) UnidadesTotales,
ROUND(SUM(dt.Quantity*pc.Cost), 0) MontoCompra, 
g.LeadTime,oc.ReceptionDate FechaRecepcion, 
CASE 
WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 1 THEN 'registrado.png' 
WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 2 THEN 'comprado.png' 
WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 3 THEN 'no_comprado.png' 
ELSE 'proceso.png' END Estado,
CASE 
WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 1 THEN 'Registrada' 
WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 2 THEN 'Orden de compra confirmada por el ERP' 
WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 3 THEN 'Ordern de compra anulada por el ERP' 
ELSE 'Orden de Compra en espera por el ERP' END ToolTip
from purchase.PurchaseOrderDetails dt
INNER JOIN purchase.PurchaseOrders oc ON dt.PurchaseOrderId = oc.Id
INNER JOIN products.Products p on dt.SKU = p.SKU
INNER JOIN setup.PriceAndCost pc ON p.SKU = pc.SKU
INNER JOIN products.Groups g on p.GroupId = g.Id
INNER JOIN users.SkuUsers su on p.SKU = su.SKU
LEFT JOIN (
			SELECT IdOrigen, EstadoOOCC
			FROM series.DailyPurchaseOrders
			--WHERE EstadoOOCC = 2
			) 
			dpc ON oc.Id = dpc.IdOrigen
WHERE oc.PurchaseType = 'NOR'
AND CAST(oc.CreatedDate AS DATE) = CAST(GETDATE() AS date)
AND (@USERNAME is null or su.Username = @USERNAME)
GROUP BY g.Id, g.FantasyName, g.LeadTime, oc.CreatedDate, oc.ReceptionDate, g.Name, dpc.IdOrigen, EstadoOOCC


END

