﻿CREATE PROCEDURE [aux].[SimulatedAmount]
@USERNAME NVARCHAR(255)
AS
BEGIN

SET NOCOUNT ON;

--IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
--SET @USERNAME = null
--END

DECLARE @res NVARCHAR(50);

SET @res = 
(
SELECT TOP 1 FORMAT(SimulatedAmount,N'c0', 'es-CL') MontoSimulado
from [aux].OptimizationSkus i
--INNER JOIN users.SkuUsers su ON i.Sku = su.SKU
--WHERE @USERNAME IS NULL OR su.Username = @USERNAME
WHERE i.Username = @USERNAME
);

SELECT ISNULL(@res,'')


END

