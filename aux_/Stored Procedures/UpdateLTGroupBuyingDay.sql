﻿CREATE PROCEDURE [aux].[UpdateLTGroupBuyingDay]
@ID  INT,
@DIA INT,
@LT  INT,
@USERNAME NVARCHAR(MAX),
@FECHA DATETIME
AS
BEGIN
SET NOCOUNT ON;

IF @DIA = 1 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, LTMonday = @LT  WHERE Id = @ID
UPDATE products.Groups SET LTMonday = null WHERE Id = @ID AND LTMonday = 0
END
IF @DIA = 2 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, LTTuesday = @LT WHERE Id = @ID
UPDATE products.Groups SET LTTuesday = null WHERE Id = @ID AND LTTuesday = 0
END
IF @DIA = 3 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, LTWednesday = @LT WHERE Id = @ID
UPDATE products.Groups SET LTWednesday = null WHERE Id = @ID AND LTWednesday = 0
END
IF @DIA = 4 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, LTThursday = @LT WHERE Id = @ID
UPDATE products.Groups SET LTThursday = null WHERE Id = @ID AND LTThursday = 0
END
IF @DIA = 5 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, LTFriday = @LT WHERE Id = @ID
UPDATE products.Groups SET LTFriday = null WHERE Id = @ID AND LTFriday = 0
END
IF @DIA = 6 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, LTSaturday = @LT WHERE Id = @ID
UPDATE products.Groups SET LTSaturday = null WHERE Id = @ID AND LTSaturday = 0
END
IF @DIA = 7 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, LTSunday = @LT WHERE Id = @ID
UPDATE products.Groups SET LTSunday = null WHERE Id = @ID AND LTSunday = 0
END


END
