﻿
-- =============================================
-- Author:		Juan M  Hernandez
-- Create date: 2018-05-03
-- 
-- =============================================
CREATE PROCEDURE [aux].[UpdatePurchaseSpecial]
@USUARIO NVARCHAR(MAX)

AS
BEGIN

SET NOCOUNT ON;

DECLARE @USUARIO_AUX NVARCHAR(MAX) = @USUARIO;

DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USUARIO);
IF ISNULL(@Usuario_admin,0) = 1 BEGIN
SET @USUARIO = null;
END
	--------------------------------------------------------------------------------------------------------------------------------

	create table #ResultadoTemporal  (
	SKU INT,
	Compra INT, 
	RUT INT,
	Rubro VARCHAR(15),
	LeadTime INT, 
	ManufacturerId INT,
	PurchaseSuggest INT,
	Dias INT
	)

	SET DATEFIRST 1;
    DECLARE @DOW INT = DATEPART(DW, GETDATE())

	INSERT INTO #ResultadoTemporal
	SELECT p.SKU, p.Purchase, m.RUT, pro.LogisticAreaId , 
	CASE
		WHEN @DOW = 1 THEN ISNULL(LTMonday,g.LeadTime)
		WHEN @DOW = 2 THEN ISNULL(LTTuesday,g.LeadTime) 
		WHEN @DOW = 3 THEN ISNULL(LTWednesday,g.LeadTime) 
		WHEN @DOW = 4 THEN ISNULL(LTThursday,g.LeadTime) 
		WHEN @DOW = 5 THEN ISNULL(LTFriday,g.LeadTime) 
		WHEN @DOW = 6 THEN ISNULL(LTSaturday,g.LeadTime) 
		WHEN @DOW = 7 THEN ISNULL(LTSunday,g.LeadTime)  
    END LT, m.Id, p.PurchaseSuggest, a.Days
	FROM  aux.PurchaseSummarySpecial p
		INNER JOIN products.Products pro on pro.SKU = p.SKU
		INNER JOIN products.Manufacturers m on m.Id = pro.ManufacturerId
		LEFT JOIN setup.PriceAndCost pac on pac.sku  = p.SKU
		LEFT JOIN products.Groups g on pro.GroupId = g.Id
		CROSS APPLY suggest.GetInventoryDays(p.SKU, p.Purchase,GETDATE()) a
	WHERE pac.Cost > 0
	AND (@USUARIO is null or p.Username = @USUARIO) 

--------------------------------------------------------------------------------------------------------------------------------

	MERGE INTO aux.PurchaseOrders as po
	USING #ResultadoTemporal AS rt
	on rt.rut = po.Rut and rt.Rubro COLLATE DATABASE_DEFAULT =	po.LogisticAreaId and po.IsRetail = 1 and po.ManufacturerId = rt.ManufacturerId and po.PurchaseType = 'ESP'
	
	WHEN NOT MATCHED THEN
	INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate, LeadTime, ReceptionDate, isFree, PurchaseType, IsRetail,ManufacturerId, availabletobuy)
	VALUES (rt.rut,rt.Rubro,1,@USUARIO_AUX, CONVERT(date,GETDATE()),rt.LeadTime,DATEADD(DAY, rt.LeadTime, CONVERT(DATE, GETDATE())),0,  'ESP',1,rt.ManufacturerId, 'RDY');

----------------------------  Detalle con los datos del Proveedor    --------------------------------------------


DECLARE @GENERA_SOBRESTOCK INT = 0;
Declare 
@id int, 
@count int
Set @id=1
select @count=count(*)from #ResultadoTemporal
DECLARE @sku int, @lab int, @rubro VARCHAR(MAX), @ORDEN_COMPRA int, @sku_cantidad int,  @SKU_DETALLE INT, @MANUFACTUREID INT,@PurchaseSuggest INT

DECLARE @CANTIDAD_DIAS_SS  INT = 0;

while @id<=@count
begin

	SET @sku = (select top 1 SKU from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @PurchaseSuggest = (select top 1 PurchaseSuggest from #ResultadoTemporal where sku = @sku);
	SET @lab = (select top 1 RUT from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @rubro = (select TOP 1 Rubro from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @sku_cantidad = (select top 1 Compra from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);
	SET @ORDEN_COMPRA = (select top 1 oc.ID from aux.PurchaseOrders oc where oc.RUT = @lab AND oc.LogisticAreaId = @rubro and IsRetail = 1 and PurchaseType = 'ESP')
	SET @SKU_DETALLE  = (select top 1 count(*) from aux.PurchaseOrderDetails ocD 
						 inner join aux.PurchaseOrders pc on pc.Id = ocD.PurchaseOrderId
					 where ocd.SKU = @sku and ocD.PurchaseOrderId = @ORDEN_COMPRA);
    SET @MANUFACTUREID = (select top 1 ManufacturerId from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from #ResultadoTemporal) as it where rank=@id);

    SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) 
						  FROM #ResultadoTemporal 
						  WHERE (Dias > 60)
						  AND   (RUT = @lab) 
						  AND   (Rubro = @rubro) 
						  AND   (ManufacturerId = @MANUFACTUREID));

    IF @CANTIDAD_DIAS_SS > 0 BEGIN
	UPDATE aux.PurchaseOrders set AvailableToBuy = 'PEN' WHERE Id = @ORDEN_COMPRA
	SET @GENERA_SOBRESTOCK = 1;
	END
	ELSE
	BEGIN
	UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @ORDEN_COMPRA
	END 

	IF @SKU_DETALLE > 0 BEGIN
			UPDATE aux.PurchaseOrderDetails set Quantity = @sku_cantidad WHERE SKU = @sku;
	END
	ELSE
	BEGIN
		IF @ORDEN_COMPRA is not null 
		BEGIN
			 	INSERT INTO aux.PurchaseOrderDetails VALUES(@ORDEN_COMPRA,@sku,@SKU_CANTIDAD, @PurchaseSuggest);
		END
	END
	select @id=@id+1
end

---------------------------------------------------------------------------------------------

DELETE FROM aux.PurchaseOrderDetails WHERE Quantity = 0 or Quantity is null;
DELETE FROM aux.PurchaseOrders WHERE ID not in (select PurchaseOrderId from aux.PurchaseOrderDetails) and PurchaseType = 'ESP'

declare @gestionados VARCHAR(MAX)
declare @cont int 
DECLARE @OC_PROBLEMA_FECHA_RECEP INT;

SET @OC_PROBLEMA_FECHA_RECEP = 
(
select COUNT(*) from #ResultadoTemporal 
WHERE LeadTime IS NULL
);


set @cont= (select count(*) from aux.PurchaseOrders po
inner join aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
where PurchaseType = 'ESP' and CreatedBy = @USUARIO_AUX)

IF @cont = 0 BEGIN 
	SET @gestionados = 0; 
	END

ELSE BEGIN 
	IF @GENERA_SOBRESTOCK > 0 BEGIN
		SET @gestionados = '1'
	END
    ELSE BEGIN
		IF @cont = 1 BEGIN

			set @gestionados = 'Se gestiona el siguiente SKU: '
			select @gestionados = COALESCE( @gestionados , ' ') + 
			CAST( pod.sku as nvarchar)  from aux.PurchaseOrders po
			inner join aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
			where PurchaseType = 'ESP' and CreatedBy = @USUARIO_AUX

			--IF @OC_PROBLEMA_FECHA_RECEP > 0
			--BEGIN
			--SET @gestionados += '<br><br>Existen OC sin fecha de recepción.'
			--END

		END
		ELSE BEGIN

			set @gestionados = concat('Se gestionan ', @cont ,' SKUs, detallados a continuación: ')
			select @gestionados = COALESCE( @gestionados , ' ') + 
			CAST(pod.sku as nvarchar)+' '  from aux.PurchaseOrders po
			inner join aux.PurchaseOrderDetails pod on pod.PurchaseOrderId = po.Id
			where PurchaseType = 'ESP' and CreatedBy = @USUARIO_AUX

			
			--IF @OC_PROBLEMA_FECHA_RECEP > 0
			--BEGIN
			--SET @gestionados += '<br><br>Existen OC sin fecha de recepción.'
			--END

		END		
			
			IF @OC_PROBLEMA_FECHA_RECEP > 0
			BEGIN
			SET @gestionados += '<br><br>Existen OC sin fecha de recepción.'
			END

END
END


SELECT @gestionados


drop table #ResultadoTemporal

END


