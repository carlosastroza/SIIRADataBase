﻿-- =============================================
-- Author:		Juan M  Hernandez
-- Create date: 2018-05-03
-- 
-- =============================================
CREATE PROCEDURE [aux].[CreateSummaryPurchaseSpecial]

@USUARIO NVARCHAR(MAX),
@NIVEL_SERVICIO NVARCHAR(10),
@FILL_RATE INT ,
@DIAS INT , 
@LEAD_TIME INT
--,@ZONA INT 

AS
BEGIN

SET NOCOUNT ON;

DECLARE @USUARIO_LOCAL NVARCHAR(50) = @USUARIO;

--IF @USUARIO IN (SELECT Username FROM setup.AdminUsers) BEGIN
--SET @USUARIO = null;
--END

	DECLARE @FR FLOAT = ISNULL (@FILL_RATE, 0 );
	SET @FR = @FR/100
	DECLARE @NS FLOAT = ISNULL (cast (@NIVEL_SERVICIO as float), 0 );
	SET @NS = @NS/100
	
	DECLARE @DIAS_C INT = ISNULL (@DIAS, 0 );
	DECLARE @LT INT = ISNULL (@LEAD_TIME, 0 );
	
	--------------------------------------------------------------------------------------------------------------------------------

	SELECT 
	   s.[SKU]
      ,@FR [FillRate]
      ,@DIAS+p.DaysToCover DaysToCover
      ,p.[StoresStock]
      ,p.[DCStock]
      ,p.[StockOut]
      ,p.[LastMonthSale]
      ,p.[Suggest]
      ,null [PurchaseSuggest]
      ,s.Purchase [Purchase]
      ,p.[Coverage]
      ,p.[DCTransit]
      ,p.[StoresTransit]
      ,p.[StoresShortage]
      ,p.[TotalRanking]
      ,p.[Status]
      ,p.[IsGES]
      ,p.[IsGeneric]
	  ,s.[Username]
	  ,@NS ServiceLevel
	  ,@LT LeadTime
	into #ResultadoTemporal
	FROM aux.SpecialPurchase s
	LEFT JOIN aux.PurchaseSummary p on p.SKU = s.SKU
	WHERE s.Username = @USUARIO_LOCAL

	------------------------------------------------------------------------------------------------------------------------------
	--SELECT *  FROM aux.PurchaseSummarySpecial
	MERGE INTO aux.PurchaseSummarySpecial AS PS
	USING #ResultadoTemporal AS RT
	ON PS.SKU = RT.SKU AND PS.Username = rt.[Username]
	WHEN NOT MATCHED THEN 
	INSERT ([SKU] ,[FillRate],[DaysToCover],[StoresStock],[DCStock], [StockOut], [LastMonthSale], [Suggest],
			[PurchaseSuggest],[Purchase],[Coverage],[DCTransit],[StoresTransit],[StoresShortage],[TotalRanking],
			[Status],[IsGES],[IsGeneric],[Username],ServiceLevel, LeadTime)
	VALUES (RT.[SKU] ,RT.[FillRate],RT.[DaysToCover],RT.[StoresStock],RT.[DCStock],RT.[StockOut],RT.[LastMonthSale],RT.[Suggest],
			RT.[PurchaseSuggest],RT.[Purchase],RT.[Coverage],RT.[DCTransit],RT.[StoresTransit],RT.[StoresShortage],RT.[TotalRanking],
			RT.[Status],RT.[IsGES],RT.[IsGeneric],RT.[Username],RT.ServiceLevel, RT.LeadTime)
	WHEN  MATCHED THEN 
		Update SET PS.FillRate = @FR, PS.[DaysToCover] = @DIAS_C , PS.ServiceLevel = @NS, PS.LeadTime = @LT;


	------------------------------------------------------------------------------------------------------------------------------	
	CREATE TABLE #compra (sku int, compra int, Suggest int)
	INSERT INTO #compra 
	EXEC aux.GetSimulatedSpecialPurchase @FR, @NS, @LT, @DIAS_C, @USUARIO_LOCAL
	------------------------------------------------------------------------------------------------------------------------------

	UPDATE  ps
	SET ps.PurchaseSuggest = c.compra, ps.Purchase = isnull (sp.purchase , c.compra), Suggest = c.Suggest, ps.DaysToCover = ps1.DaysToCover+@DIAS_C
	FROM aux.PurchaseSummarySpecial ps
	INNER JOIN #compra c on c.sku = ps.sku
	INNER JOIN aux.SpecialPurchase sp on sp.SKU =ps.SKU
	LEFT JOIN aux.PurchaseSummary ps1 on ps.SKU = ps1.SKU
	WHERE ps.Username = @USUARIO_LOCAL 
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	DROP TABLE #compra
	DROP TABLE #ResultadoTemporal

	

END



--------------------------------------------------------------