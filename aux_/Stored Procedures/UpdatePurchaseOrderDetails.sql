﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 2018-07-06
-- Description:	Actualiza PurchaseOrderDetails y OC cuándo se modifica compra en Modal
-- =============================================

CREATE PROCEDURE [aux].[UpdatePurchaseOrderDetails]
@SKU INT,
@OC  INT
AS
BEGIN

SET NOCOUNT ON;

DECLARE @PURCHASE_TYPE NVARCHAR(3) = (SELECT PurchaseType FROM aux.PurchaseOrders WHERE Id = @OC);
DECLARE @IDZONA INT = (SELECT IdZona FROM Aux.PurchaseOrders WHERE Id = @OC);
DECLARE @CANTIDAD_DIAS_SS  INT = 0;


IF(@PURCHASE_TYPE = 'GEN' OR @PURCHASE_TYPE = 'NOR')
BEGIN

	--Actualiza cantidad compra de la OC ya generada
	UPDATE t SET CompraOC = CompraSummary 
	FROM (
	SELECT det.Quantity CompraOC, ps.Purchase CompraSummary
	FROM aux.PurchaseOrderDetails det
	INNER JOIN aux.PurchaseSummary ps ON det.SKU = ps.SKU
	WHERE PurchaseOrderId = @OC) t
	
	update ps
	SET Purchase = w.Q
	FROM aux.PurchaseSummary ps
	INNER JOIN (
		SELECT SKU, SUM(Quantity) Q
		FROM aux.PurchaseOrderDetails det
		INNER JOIN aux.PurchaseOrders oc on det.PurchaseOrderId = oc.Id
		WHERE (det.SKU = @sku) AND (oc.PurchaseType = @PURCHASE_TYPE)
		GROUP BY SKU
	) w ON ps.SKU = w.SKU



END
ELSE IF (@PURCHASE_TYPE = 'GES')
BEGIN

IF @IDZONA = 37 BEGIN

--Actualiza cantidad compra de la OC ya generada para el proveedor
UPDATE t SET CompraOC = CompraSummary 
FROM (
SELECT det.Quantity CompraOC, ps.Purchase CompraSummary
FROM aux.PurchaseOrderDetails det
INNER JOIN aux.PurchaseSummary ps ON det.SKU = ps.SKU
WHERE PurchaseOrderId = @OC) t
	
update ps
SET Purchase = w.Q
FROM aux.PurchaseSummary ps
INNER JOIN (
SELECT SKU, SUM(Quantity) Q
FROM aux.PurchaseOrderDetails det
INNER JOIN aux.PurchaseOrders oc on det.PurchaseOrderId = oc.Id
WHERE (det.SKU = @sku) AND (oc.PurchaseType = @PURCHASE_TYPE) AND (oc.IdZona = @IDZONA)
GROUP BY SKU
) w ON ps.SKU = w.SKU

END
ELSE BEGIN

--Actualiza cantidad compra de la OC ya generada para el distribuidor
UPDATE t SET CompraOC = CompraSummary 
FROM (
SELECT det.Quantity CompraOC, ps.Purchase CompraSummary
FROM aux.PurchaseOrderDetails det
INNER JOIN aux.PurchaseSummaryDistributor ps ON det.SKU = ps.SKU and ps.IdZona = @IDZONA
WHERE PurchaseOrderId = @OC) t
	
update ps
SET Purchase = w.Q
FROM aux.PurchaseSummaryDistributor ps
INNER JOIN (
SELECT SKU, SUM(Quantity) Q
FROM aux.PurchaseOrderDetails det
INNER JOIN aux.PurchaseOrders oc on det.PurchaseOrderId = oc.Id
WHERE (det.SKU = @sku) AND (oc.PurchaseType = @PURCHASE_TYPE) and (OC.IdZona = @IDZONA)
GROUP BY SKU
) w ON ps.SKU = w.SKU

END

END
ELSE IF (@PURCHASE_TYPE = 'ESP')
BEGIN
	UPDATE t SET CompraOC = CompraSummary
	FROM (
	SELECT det.Quantity CompraOC, ps.Purchase CompraSummary
	FROM aux.PurchaseOrderDetails det
	INNER JOIN aux.PurchaseSummarySpecial ps on det.SKU = ps.SKU
	WHERE PurchaseOrderId = @OC) t

	UPDATE ps
	SET Purchase = w.Q
	FROM aux.PurchaseSummarySpecial ps
	inner join (
		SELECT SKU, SUM(Quantity) Q
		FROM aux.PurchaseOrderDetails det
		INNER JOIN aux.PurchaseOrders oc on det.PurchaseOrderId = oc.Id
		WHERE (det.SKU = @sku) AND (oc.PurchaseType = @PURCHASE_TYPE)
		GROUP BY SKU
	) w ON ps.SKU = w.SKU
END

DELETE FROM aux.PurchaseOrderDetails 
WHERE (PurchaseOrderId = @OC)
AND (Quantity = 0)
DELETE FROM aux.PurchaseOrders WHERE Id NOT IN (SELECT PurchaseOrderId FROM aux.PurchaseOrderDetails)

DECLARE @ResultadoTemporal TABLE (
DIAS_SOBRESTOCK INT,
OC INT)
DECLARE @MAX_INVENTORY_DAYS TABLE(
Llave NVARCHAR(MAX),
Value INT
)
INSERT INTO @MAX_INVENTORY_DAYS
exec [setup].[GetConfigurationDictionary] 'SB'
DECLARE @MAX_DAYS INT = (SELECT Value FROM @MAX_INVENTORY_DAYS);

IF(@PURCHASE_TYPE = 'GEN')
BEGIN
INSERT INTO @ResultadoTemporal
SELECT da.Days, @OC
FROM aux.PurchaseOrderDetails det
INNER JOIN products.Products p ON det.SKU = p.SKU
INNER JOIN (
	SELECT GenericFamilyId, SUM(Purchase) CompraFamilia 
	FROM products.Products p
	INNER JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
	GROUP BY p.GenericFamilyId
	) a ON p.GenericFamilyId = a.GenericFamilyId
INNER JOIN aux.PurchaseSummary s ON p.SKU = s.SKU
CROSS APPLY [suggest].[GetInventoryDays](s.SKU,a.CompraFamilia+s.DCStock+s.DCTransit-s.StoresShortage,GETDATE()) da
WHERE PurchaseOrderId = @OC
END
ELSE
BEGIN
INSERT INTO @ResultadoTemporal
SELECT da.Days, @OC
FROM aux.PurchaseOrderDetails det
INNER JOIN aux.PurchaseSummary s ON det.SKU = s.SKU
CROSS APPLY [suggest].[GetInventoryDays](s.SKU,det.Quantity+s.DCStock+s.DCTransit-s.StoresShortage,GETDATE()) da
WHERE PurchaseOrderId = @OC

END


SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) 
					FROM @ResultadoTemporal 
					WHERE (DIAS_SOBRESTOCK > @MAX_DAYS));
IF @CANTIDAD_DIAS_SS > 0 BEGIN
UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @OC
END
ELSE
BEGIN
UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @OC
END 

end
