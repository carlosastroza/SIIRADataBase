﻿CREATE PROCEDURE [aux].[UpdateGroupBuyingDay]
@ID INT,
@DIA INT,
@USERNAME NVARCHAR(MAX),
@FECHA DATETIME
AS
BEGIN
SET NOCOUNT ON;

IF @DIA = 1 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, PurchaseOnMonday = CASE WHEN PurchaseOnMonday = 1 THEN 0 ELSE 1 END WHERE Id = @ID
END
IF @DIA = 2 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, PurchaseOnTuesday = CASE WHEN PurchaseOnTuesday = 1 THEN 0 ELSE 1 END WHERE Id = @ID
END
IF @DIA = 3 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, PurchaseOnWednesday = CASE WHEN PurchaseOnWednesday = 1 THEN 0 ELSE 1 END WHERE Id = @ID
END
IF @DIA = 4 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, PurchaseOnThursday = CASE WHEN PurchaseOnThursday = 1 THEN 0 ELSE 1 END WHERE Id = @ID
END
IF @DIA = 5 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, PurchaseOnFriday = CASE WHEN PurchaseOnFriday = 1 THEN 0 ELSE 1 END WHERE Id = @ID
END
IF @DIA = 6 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, PurchaseOnSaturday = CASE WHEN PurchaseOnSaturday = 1 THEN 0 ELSE 1 END WHERE Id = @ID
END
IF @DIA = 7 BEGIN
UPDATE products.Groups SET ModifiedBy = @USERNAME, ModifiedOn = @FECHA, PurchaseOnSunday = CASE WHEN PurchaseOnSunday = 1 THEN 0 ELSE 1 END WHERE Id = @ID
END

END
