﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 08-01-2018
-- Description:	Obtiene SKU x Familia
-- Modificada: Juan M Hernandez 2018-04-03
-- =============================================

CREATE PROCEDURE [aux].[GetGenericSKU]
@FAMILIA int
AS
BEGIN

SET NOCOUNT ON;

DECLARE @COMPRA_FAMILIA INT = (
SELECT SUM(purchase) FROM aux.PurchaseSummary ps
INNER JOIN products.Products p ON ps.SKU = p.SKU
WHERE GenericFamilyId = @FAMILIA
);

SELECT 
	--CAST(CASE WHEN gen.SKU IS NOT NULL THEN 1 ELSE 0 END AS BIT) Licitado,
	sg.SKU,
	p.Name Nombre,
	m.Name Laboratorio,
	CASE WHEN fp.PetitorioMinimo > 0 THEN 'SI' ELSE 'NO' END Petitorio,
	FillRate ,
	DaysToCover DiasInventario,
	ISNULL(sg.AdditionalDaysToCover,0) DiasAdicionales,
	pc.Price Precio,
	pc.Cost Costo,
	StoresStock StockLocales,
	DCStock StockCD,
	StockOut Quiebre,
	LastMonthSale VentaUltimoMes,
	sg.Suggest Sugerencia,
	sg.SBDemand,
	sg.PurchaseSuggest SugerenciaFinal,
	sg.Purchase Compra,
	a.Days DiasCompra,
	Coverage Cobertura,
	DCTransit TransitoCD,
	StoresTransit TransitoLocal,
	StoresShortage Faltante,
	sg.TotalRanking TotalRanking,
	fp.LogisticAreaId Rubro
   
FROM products.Products p
INNER JOIN aux.PurchaseSummary sg ON p.SKU = sg.SKU
INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
INNER JOIN Salcobrand.products.FullProducts fp on p.SKU = fp.SKU
LEFT JOIN setup.priceandCost pc on p.SKU = pc.SKU
CROSS APPLY [suggest].[GetInventoryDays](sg.SKU,@COMPRA_FAMILIA+sg.DCStock+sg.DCTransit-sg.StoresShortage,GETDATE()) a
--LEFT JOIN products.GenericTenderedSKU gen ON p.SKU = gen.SKU
WHERE p.GenericFamilyId  = @FAMILIA


ORDER BY SKU asc
end

