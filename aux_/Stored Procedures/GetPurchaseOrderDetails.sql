﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-07-18
-- Description:	Obtiene el detalle de una OC
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrderDetails]
	@orderId INT,
	@pricingOrderId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--traemos el detalle de la OC de las compras diarias
	select 
		CASE 
			WHEN ISNULL(po.IdZona, 37) = 37 THEN po.RUT
			ELSE di.DistributorRUT
		  END RUT
		, CASE 
			WHEN ISNULL(po.IdZona, 37) = 37 THEN m.[Name]
			ELSE di.DistributorName 
		  END Manufacturer
		, ISNULL(di.Isapre, '-') Isapre
		, p.LogisticAreaId [Zone]
		, ISNULL(u.Nombre, BuyerIdOrigen) Buyer
		, po.[Date] PurchaseDate, po.FechaRecepcion ReceptionDate, po.FechaExpiracionActual DeliveryDate
		, po.SKU
		, po.UnidadesCompra PurchaseUnits, po.UnidadesRecibidas ReceivedUnits, CASE WHEN po.UnidadesCompra = 0 THEN 0 ELSE po.UnidadesRecibidas / (po.UnidadesCompra*1.0) END FillRate
	from series.DailyPurchaseOrders po
	LEFT JOIN products.Manufacturers m on po.ManufacturerId = m.Id
	LEFT JOIN products.Products p on po.SKU = p.SKU
	LEFT JOIN users.UserRules ur ON po.BuyerIdOrigen = ur.BuyerId
	LEFT JOIN StdUsuarios.dbo.Usuario u ON ur.Username = u.Id
	LEFT JOIN setup.DistributorIsapre di ON po.IdZona = di.IdZona
	WHERE po.Id = @orderId

	UNION
	--traemos el detalle de la OC de las compras históricas (últimos 90 días)
	select 
		CASE 
			WHEN ISNULL(po.IdZona, 37) = 37 THEN po.RUT
			ELSE di.DistributorRUT
		  END RUT
		, CASE 
			WHEN ISNULL(po.IdZona, 37) = 37 THEN m.[Name]
			ELSE di.DistributorName 
		  END Manufacturer
		, ISNULL(di.Isapre, '-') Isapre
		, p.LogisticAreaId [Zone]
		, ISNULL(u.Nombre, BuyerIdOrigen) Buyer
		, po.[Date] PurchaseDate, po.FechaRecepcion ReceptionDate, po.FechaExpiracionActual DeliveryDate
		, po.SKU
		, po.UnidadesCompra PurchaseUnits, po.UnidadesRecibidas ReceivedUnits, CASE WHEN po.UnidadesCompra = 0 THEN 0 ELSE po.UnidadesRecibidas / (po.UnidadesCompra*1.0) END FillRate
	from series.PurchaseOrders po
	LEFT JOIN products.Manufacturers m on po.ManufacturerId = m.Id
	LEFT JOIN products.Products p on po.SKU = p.SKU
	LEFT JOIN users.UserRules ur ON po.BuyerIdOrigen = ur.BuyerId
	LEFT JOIN StdUsuarios.dbo.Usuario u ON ur.Username = u.Id
	LEFT JOIN setup.DistributorIsapre di ON po.IdZona = di.IdZona
	WHERE po.Id = @orderId

	UNION
	--traemos el detalle de la OC de PRICING
	

	select
		po.RUT
		, CASE 
			WHEN ISNULL(po.IdZona, 37) = 37 THEN m.[Name]
			ELSE di.DistributorName 
		  END Manufacturer
		, ISNULL(di.Isapre, '-') Isapre
		, p.LogisticAreaId [Zone]
		, ISNULL(u.Nombre, po.CreatedBy) Buyer
		, po.CreatedDate PurchaseDate, CAST(NULL AS DATE) ReceptionDate, po.ReceptionDate DeliveryDate
		, pod.SKU
		, pod.Quantity PurchaseUnits, 0 ReceivedUnits, CAST(NULL AS FLOAT) FillRate
	from purchase.PurchaseOrders po
	inner join purchase.PurchaseOrderDetails pod on po.Id = pod.PurchaseOrderId
	inner join products.Products p ON pod.SKU = p.SKU
	inner join products.Manufacturers m ON p.ManufacturerId = m.Id
	LEFT JOIN StdUsuarios.dbo.Usuario u ON po.CreatedBy = u.Id
	LEFT JOIN setup.DistributorIsapre di ON po.IdZona = di.IdZona
	where po.WasProcessed = 0
		AND po.Id = @pricingOrderId

END
