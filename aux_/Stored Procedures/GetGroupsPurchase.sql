﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 26-04-2018
-- Description:	Obtiene Grupos de compra por Usuario y día de Compra
-- =============================================

CREATE PROCEDURE [aux].[GetGroupsPurchase]
@USERNAME NVARCHAR(MAX),
@DIA INT
AS
BEGIN

SET NOCOUNT ON;

SET DATEFIRST 1;
DECLARE @DOW INT = DATEPART(DW, GETDATE())

DECLARE @username_local NVARCHAR(MAX) = @USERNAME;
DECLARE @dia_local INT = @DIA;

--DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USERNAME);
--IF ISNULL(@Usuario_admin,0) = 1 BEGIN
IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @username_local = null;
END

SELECT
	CASE 
     WHEN CantidadEnProceso > 0 THEN 'proceso.png' WHEN CantidadConSugerido <> CantidadSKUGrupo THEN 'no_comprado.png' ELSE 'comprado.png'END Estado,
	 CASE
	 WHEN CantidadEnProceso > 0 THEN 'En proceso.' WHEN CantidadConSugerido <> CantidadSKUGrupo THEN 'Alguno de los SKUs del Grupo no tiene Sugerido' ELSE 'Todos los SKUs del grupo tienen sugerido.'END ToolTip
	,[Id]
	,g.RUT Rut
	,g.LogisticAreaId Bodega
	,ISNULL(G.FantasyName,g.Name) NombreFantasia,
	 CONCAT(ISNULL(a.CantidadSKUConCompra,0),' / ',a.CantidadSKUGrupo) SKUCompra
	,CONCAT(ISNULL(a.CantidadConSugerido,0),' / ',a.CantidadSKUGrupo) SKUConSugerido
	,a.Suma UnidadesTotales
	,ROUND(a.MontoCompra,0) MontoCompra
	,CASE WHEN a.CantidadSKUConCompra > 0 
	THEN CAST(DATEADD(DAY,
	CASE 
	WHEN @DOW = 1 THEN ISNULL(LTMonday,LeadTime)
    WHEN @DOW = 2 THEN ISNULL(LTTuesday,LeadTime) 
	WHEN @DOW = 3 THEN ISNULL(LTWednesday,LeadTime) 
	WHEN @DOW = 4 THEN ISNULL(LTThursday,LeadTime) 
	WHEN @DOW = 5 THEN ISNULL(LTFriday,LeadTime) 
	WHEN @DOW = 6 THEN ISNULL(LTSaturday,LeadTime) 
	WHEN @DOW = 7 THEN ISNULL(LTSunday,LeadTime)  	
	END
	,GETDATE()) AS date) ELSE null END FechaRecepcion


FROM [products].[Groups] g
	INNER JOIN (
			SELECT p.[GroupId], count(*) CantidadSKUGrupo, COUNT(det.SKU) CantidadEnProceso, b.CantidadSKUConCompra, b.Suma, MontoCompra, CantidadConSugerido
			FROM [products].[Products] p
			INNER JOIN users.SkuUsers su on p.SKU = su.SKU
			LEFT JOIN aux.PurchaseOrderDetails det on p.SKU = det.SKU
			--LEFT JOIN aux.PurchaseOrders oc ON det.PurchaseOrderId = oc.Id
			LEFT JOIN (
						SELECT GroupId, 
						COUNT(*) CantidadSKUConCompra,
						CASE WHEN COUNT(*) <> 0 THEN SUM(ps.Purchase) ELSE 0 END Suma,
						CAST(CASE WHEN COUNT(*) <> 0 THEN SUM(ps.Purchase*pc.Cost) ELSE 0 END AS FLOAT) MontoCompra 
						FROM products.Products p
						INNER JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
						INNER JOIN setup.PriceAndCost pc ON p.SKU = pc.SKU
						WHERE   (ps.PurchaseSuggest > 0)
						GROUP BY p.GroupId) b on p.GroupId = b.GroupId
			
			LEFT JOIN (
			SELECT GroupId, 
			COUNT(*) CantidadConSugerido
			FROM products.Products p
			INNER JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
			INNER JOIN setup.PriceAndCost pc ON p.SKU = pc.SKU
			WHERE   (ps.PurchaseSuggest IS  NOT NULL)
			GROUP BY p.GroupId) c on p.GroupId = c.GroupId
			WHERE (@username_local IS NULL OR su.Username = @username_local) 
			and   (p.SKU not in (select SKU from products.Vademecum))
			and   (IsGeneric = 0)
			--AND   (oc.PurchaseType = 'NOR')

			group by p.GroupId, CantidadSKUConCompra, Suma, MontoCompra, CantidadConSugerido
	) 
	a on g.Id = a.GroupId

			
			WHERE g.Id NOT IN (
			SELECT GroupId
			FROM purchase.PurchaseOrderDetails DET
			INNER JOIN purchase.PurchaseOrders OC ON DET.PurchaseOrderId = OC.Id
			INNER JOIN products.Products p on DET.SKU = p.SKU
			WHERE OC.PurchaseType = 'NOR'
			AND   CAST(OC.CreatedDate AS DATE) = CAST(GETDATE() AS DATE)
			GROUP BY p.GroupId)


	AND ( (PurchaseOnMonday = 1 and @dia_local = 1) or
		  (PurchaseOnTuesday = 1 and @dia_local = 2) or
		  (PurchaseOnWednesday = 1 and @dia_local = 3) or
		  (PurchaseOnThursday = 1 and @dia_local = 4) or
		  (PurchaseOnFriday = 1 and @dia_local = 5) or
		  (PurchaseOnSaturday = 1 and @dia_local = 6) or
		  (PurchaseOnSunday = 1 and @dia_local = 7)
		)

END
