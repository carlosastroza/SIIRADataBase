﻿
CREATE PROCEDURE [aux].[getSKUGroup]
@id INT,
@USERNAME NVARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USERNAME);
IF ISNULL(@Usuario_admin,0) = 1 BEGIN
SET @USERNAME = null;
END

SELECT 
p.[SKU],
p.[Name] Nombre,
f.ManufacturerDesc Proveedor,
--f.LogisticAreaID Rubro,
p.LogisticAreaId Rubro,
p.[TotalRanking],
[IsGroupLocked] Bloqueado,
su.Username
FROM [products].[Products] p
INNER JOIN Salcobrand.products.FullProducts f on p.SKU = f.SKU
INNER JOIN users.SkuUsers su on p.SKU = su.SKU
WHERE p.GroupId = @id
AND (@USERNAME IS NULL OR @USERNAME = su.Username)
AND (ISNULL(p.TotalRanking,'null') <> 'Z')

END

