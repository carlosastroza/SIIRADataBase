﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 20-03-2018
-- Description:	Insertar o Actualizar SKU de un Grupo
-- =============================================

CREATE PROCEDURE [aux].[GroupSKUUpdate]
@id_proceso     uniqueidentifier,
@GRUPO			INT
AS
BEGIN

SET NOCOUNT ON;

DECLARE @ResultadoTemporal TABLE (
SKU INT,
Bloqueado INT)

INSERT INTO @ResultadoTemporal (SKU, Bloqueado)
SELECT p.SKU, p.Locked
	FROM [aux].GroupSKULoad p
	where (p.ProcessId = @id_proceso)

DECLARE @valores VARCHAR(MAX)
DECLARE @bloqueados VARCHAR(MAX)

SELECT @valores = COALESCE(@valores + ', ', '') + CONCAT('Se Movieron ',count(*), ' SKU del Grupo ', P.GroupId,' .')
FROM aux.GroupSKULoad l
INNER JOIN products.products p on l.sku = p.SKU
INNER JOIN products.Groups g on p.GroupId = g.id
WHERE p.IsGroupLocked = 0
group by p.GroupId, G.Name;

SELECT @bloqueados = COALESCE(@bloqueados+', ', ' Los SKU ( ') + CONCAT(CAST(p.SKU AS nvarchar),' Grupo[',p.GroupId,']')
FROM aux.GroupSKULoad l
INNER JOIN products.products p on l.sku = p.SKU
WHERE p.IsGroupLocked = 1

SET @bloqueados = @bloqueados + ' ) Están Bloqueados y no se pueden mover de Grupo.'
select ISNULL(@valores,'') + ISNULL(@bloqueados,'');



MERGE INTO products.products as p
USING @ResultadoTemporal AS t
on p.sku = t.sku
WHEN MATCHED AND p.IsGroupLocked = 0 THEN
UPDATE SET IsGroupLocked = CAST(t.Bloqueado AS BIT), GroupId = @GRUPO;

TRUNCATE TABLE [aux].GroupSKULoad



END
