﻿-- =============================================
-- Author:		Juan M  Hernandez
-- Create date: 01-02-2018
-- Description:	Actualiza compra y días adicionales de las tablas aux.PurchaseSummary y aux.PurchaseSummaryDistributor
-- =============================================
CREATE PROCEDURE [aux].[UpdateSKUPurchaseGes]
@SKU_C INT,
@RUT  NVARCHAR(15),
@IdZona int,
@COMPRA INT,
@DIAS_ADICIONALES INT,
@USUARIO NVARCHAR(MAX),
@SUGERENCIACOMPRA int
AS
BEGIN

SET NOCOUNT ON;

UPDATE aux.PurchaseSummary SET ModifyBy = @USUARIO, ModifyOn = GETDATE() WHERE SKU = @SKU_C;

DECLARE @RANKING_SKU NVARCHAR(1) = (SELECT TotalRanking FROM products.Products WHERE SKU = @SKU_C);
DECLARE @COMPRA_SUMMARY INT;
DECLARE @compra_local int  
DECLARE @dias_adicionales_local int 

/* Redondeo compra */
DECLARE @COMPRA_REDONDEADA INT;
DECLARE @COMPRA_REDONDEADA_TABLA TABLE(Unidades INT)

IF @COMPRA > 0 BEGIN
	INSERT INTO @COMPRA_REDONDEADA_TABLA
	select UnidadesTotales
	from aux.PurchaseSummary ps
	INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	CROSS APPLY [function].[RoundLogisticUnits](@compra,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
	where ps.sku = @SKU_C

	SET @COMPRA_REDONDEADA = (SELECT Unidades FROM @COMPRA_REDONDEADA_TABLA);
END
/*END Redondeo compra */

CREATE TABLE #CompraRedondeadaDias ([sku] int, [unidades] int ) 
INSERT INTO #CompraRedondeadaDias exec [aux].[PurchaseForAdditionalDay] @SKU_C, @DIAS_ADICIONALES, null 


IF @IdZona = 37 BEGIN

SET @COMPRA_SUMMARY = (SELECT Purchase FROM aux.PurchaseSummary WHERE SKU = @SKU_C);
--CREATE TABLE #CompraRedondeadaDias ([sku] int, [unidades] int ) 
--INSERT INTO #CompraRedondeadaDias exec [aux].[PurchaseForAdditionalDay] @SKU_C, @DIAS_ADICIONALES, null 

 
IF @DIAS_ADICIONALES = 0 
	BEGIN
	SET @compra_local = @SUGERENCIACOMPRA
	set @dias_adicionales_local = null
end else begin 
	set  @compra_local = ISNULL((select top 1 unidades from #CompraRedondeadaDias),0)
	set @dias_adicionales_local = CASE WHEN ISNULL(@RANKING_SKU,'') <> 'E' THEN @DIAS_ADICIONALES ELSE 0 END
end

UPDATE aux.PurchaseSummary 
SET AdditionalDaysToCover = @dias_adicionales_local, Purchase =  @compra_local, ModifyBy = @USUARIO, ModifyOn = GETDATE()
WHERE SKU = @SKU_C;

IF @COMPRA_SUMMARY <> @COMPRA BEGIN
UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = NULL, Purchase = ISNULL(@COMPRA_REDONDEADA,0) WHERE SKU = @SKU_C;
END


END
ELSE BEGIN --Actualizar purchasesummaryDistributor

SET @COMPRA_SUMMARY = (SELECT Purchase FROM aux.PurchaseSummaryDistributor WHERE SKU = @SKU_C AND IdZona = @IdZona);
--CREATE TABLE #CompraRedondeadaDiasDistribuidor ([sku] int, [unidades] int ) 
--INSERT INTO #CompraRedondeadaDiasDistribuidor exec [aux].[PurchaseForAdditionalDay] @SKU_C, @DIAS_ADICIONALES, null 
 
IF @DIAS_ADICIONALES = 0 
	BEGIN
	SET @compra_local = @SUGERENCIACOMPRA
	set @dias_adicionales_local = null
end else begin 
	set  @compra_local = ISNULL((select top 1 unidades from #CompraRedondeadaDias),0)
	set @dias_adicionales_local = CASE WHEN ISNULL(@RANKING_SKU,'') <> 'E' THEN @DIAS_ADICIONALES ELSE 0 END
end

UPDATE aux.PurchaseSummaryDistributor 
SET Purchase =  @compra_local
WHERE SKU = @SKU_C;

IF @COMPRA_SUMMARY <> @COMPRA BEGIN
UPDATE aux.PurchaseSummaryDistributor SET Purchase = ISNULL(@COMPRA_REDONDEADA,0) WHERE SKU = @SKU_C;
END



END


DROP TABLE #CompraRedondeadaDias
--------------------   Actualizar dias adicionales en la tabla PurchaseSummary  -------------------------------------------

--DECLARE @Count_Dist int = (select count (*) from products.Distributors where RUT = @RUT)
--IF @Count_Dist = 0 
--BEGIN 
--	 INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso, Usuario)
--	 VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Actualizando tabla [aux].[PurchaseSummary] para productos GES , SP[aux].[UpdateSKUPurchaseGES]', host_name(),  'POST', @USUARIO)

--	 DECLARE @AUX  INT  = (SELECT COUNT(*) FROM aux.PurchaseSummary WHERE SKU = @SKU_C AND purchase = @COMPRA )
	 
--	 IF @AUX > 0
--	 BEGIN
--		 if @DIAS_ADICIONALES>0 begin
			
--			CREATE TABLE #CompraRedondeada ( [SKu] int ,[unidadesTotales] int ) 
--			INSERT INTO #CompraRedondeada exec [aux].[PurchaseForAdditionalDay] @SKU_C, @DIAS_ADICIONALES, null
			
--			UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = @DIAS_ADICIONALES, Purchase = (select top 1 unidadesTotales from #CompraRedondeada)  WHERE SKU = @SKU_C;
--		 end
--		 else begin
--			 UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = @DIAS_ADICIONALES, Purchase = @SUGERENCIACOMPRA  WHERE SKU = @SKU_C;
--		 end
--	 END
--	 ELSE
--	 BEGIN 
		
--			UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = 0, Purchase = ISNULL(@COMPRA_REDONDEADA,0) WHERE SKU = @SKU_C;
--			--UPDATE aux.PurchaseSummaryDistributor SET  Purchase = ISNULL(@COMPRA_REDONDEADA,0) WHERE SKU = @SKU_C AND IdZona = @IdZona;
--	 END 		
	 	
--END
--ELSE
--BEGIN
--	 --UPDATE aux.PurchaseSummaryDistributor SET  Purchase = ISNULL(@COMPRA_REDONDEADA,0) WHERE SKU = @SKU_C AND IdZona = @IdZona ;
--	 UPDATE aux.PurchaseSummary SET  Purchase = ISNULL(@COMPRA_REDONDEADA,0) WHERE SKU = @SKU_C

--END
--	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso, Usuario)
--	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Actualizada tabla [aux].[PurchaseSummary] para productos GES , SP[aux].[UpdateSKUPurchaseGES]', host_name(),  'POST', @USUARIO)


END



--------------------------------------------------------------


