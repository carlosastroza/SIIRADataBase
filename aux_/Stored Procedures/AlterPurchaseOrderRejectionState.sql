﻿
-- =============================================
-- Author:		Jorge I.
-- Create date: 28-05-2018
-- Description:	Modifica el estado de rechazo de una orden de compra
-- =============================================
CREATE PROCEDURE [aux].[AlterPurchaseOrderRejectionState]
	@IdPurchaseOrder int,
	@Rejected nvarchar(3)
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE aux.PurchaseOrders 
	SET AvailableToBuy = @Rejected
	WHERE Id = @IdPurchaseOrder
END
