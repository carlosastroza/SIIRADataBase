﻿CREATE PROCEDURE [aux].[UpdateSKUPurchase]
 @SKU_C INT,
 @DIAS_ADICIONALES INT,
 @COMPRA_CON_DIAS INT, -- toma el sugerido de compra
 @COMPRA INT, --Compra ingresada en la celda 'Compra'
 @USERNAME NVARCHAR(50)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @COMPRA_REDONDEADA INT;
DECLARE @RANKING_SKU NVARCHAR(1) = (SELECT TotalRanking FROM products.Products WHERE SKU = @SKU_C);

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Actualizando tabla [aux].[PurchaseSummary] , SP[aux].[UpdateSKUPurchase]', host_name(),  'POST')
	
	DECLARE @COMPRA_SUMMARY INT = (SELECT ISNULL(Purchase, 0) FROM aux.PurchaseSummary WHERE SKU = @SKU_C);

	DECLARE @COMPRA_REDONDEADA_TABLA TABLE(
	Unidades INT)

	IF @COMPRA > 0 BEGIN

		INSERT INTO @COMPRA_REDONDEADA_TABLA
		select UnidadesTotales
		from aux.PurchaseSummary ps
		INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
		CROSS APPLY [function].[RoundLogisticUnits](@compra,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
		where ps.sku = @SKU_C

		SET @COMPRA_REDONDEADA = (SELECT Unidades FROM @COMPRA_REDONDEADA_TABLA);

	END

	BEGIN TRY
		
		CREATE TABLE #CompraRedondeadaDias ([sku] int, [unidades] int ) 
		INSERT INTO #CompraRedondeadaDias exec [aux].[PurchaseForAdditionalDay] @SKU_C, @DIAS_ADICIONALES, null 

		declare @compra_local int  
		declare @dias_adicionales_local int 
		 
		IF @DIAS_ADICIONALES = 0 
		  BEGIN
			SET @compra_local = @COMPRA_CON_DIAS
			set @dias_adicionales_local = null
		end else begin 
			set  @compra_local = ISNULL((select top 1 unidades from #CompraRedondeadaDias),0)
			set @dias_adicionales_local = CASE WHEN ISNULL(@RANKING_SKU,'') <> 'E' THEN @DIAS_ADICIONALES ELSE 0 END
		end

		UPDATE aux.PurchaseSummary 
		SET AdditionalDaysToCover = @dias_adicionales_local, Purchase =  @compra_local, ModifyBy = @USERNAME, ModifyOn = GETDATE()
		WHERE SKU = @SKU_C;

		IF @COMPRA_SUMMARY <> @COMPRA BEGIN
		UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = NULL, Purchase = ISNULL(@COMPRA_REDONDEADA,0) WHERE SKU = @SKU_C;
	    END

	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],  Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA','Falló la actualización sabre la tabla [aux].[PurchaseSummary] en el SP [aux].[UpdateSKUPurchase]', host_name(),  'POST');
		THROW;
	END CATCH

	drop table #CompraRedondeadaDias

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Actualizada tabla [aux].[PurchaseSummary] correctamente, SP [aux].[UpdateSKUPurchase], ' , host_name())

END
