﻿

-- =============================================
-- Author:		Jorge I.
-- Create date: 23-05-2018
-- Description:	Obtiene los indicadores de ordenes de compra para jefes


-- UPDATE: Se corrige query ya que estaba contando mal los SKU
-- =============================================

CREATE PROCEDURE [aux].[GetPurchaseOrdersReport]
AS
BEGIN
	SELECT	Nombre Comprador,
			SUM(Monto) MontoCompra,
			COUNT(Id) NumOCs
	FROM (SELECT	po.Id,
					u.Nombre,
					SUM((pod.Quantity * pac.Cost)) Monto
			FROM aux.PurchaseOrders po
			LEFT JOIN StdUsuarios.dbo.Usuario u ON po.CreatedBy = u.Id
			INNER JOIN aux.PurchaseOrderDetails pod ON pod.PurchaseOrderId = po.Id
			INNER JOIN setup.PriceAndCost pac ON pac.SKU = pod.SKU
			WHERE AvailableToBuy  = 'PEN'
			GROUP BY po.Id,u.Nombre) datos
	GROUP BY datos.Nombre	
END


