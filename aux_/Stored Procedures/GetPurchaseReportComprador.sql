﻿

---- =============================================
---- Author:		Jorge I.
---- Create date:	23-05-2018
---- Description:	Obtiene los indicadores de compra para compradores
---- =============================================

CREATE PROCEDURE [aux].[GetPurchaseReportComprador]
@IdComprador NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
DECLARE @fecha DATE = CAST(GETDATE() as date)
	--DECLARE @IdComprador NVARCHAR(50) = 'pvargas'

	SELECT	u.Id,
			u.Nombre,
			bi.GroupId [IdGrupo],
			bi.FantasyName [NombreGrupo], 
			COUNT(bi.SKU) as CuentaSKU,
			ISNULL(SUM(bi.Quantity),0) CantidadComprado,
			ISNULL(SUM(bi.Cost),0) [Costo],
			SUM(ps.CompraSugerida) CompraSugerida,
			SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END) CompraZero,
			SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END) SinCompraSugerida,
			SUM(CASE WHEN oc.CreatedDate IS NULL THEN 0 ELSE 1 END) Comprado,
			CASE 
				WHEN (COUNT(bi.sku)-(SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END)+SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END))) != 0 
					THEN SUM(CASE WHEN oc.CreatedDate IS NULL THEN 0 ELSE 1 END)/(COUNT(bi.sku)-(SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END)+SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END))) 
					ELSE 1 END PorcSkusComprado
	FROM aux.BaseIndicadores bi
	LEFT JOIN (SELECT	SKU,
						Purchase CompraSugerida 
				FROM suggest.purchasesuggest) ps on ps.SKU=bi.SKU
	LEFT JOIN stdusuarios.dbo.usuario u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS=bi.comprador
	LEFT JOIN (	SELECT	od.PurchaseOrderId,
						od.SKU,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date)= @fecha
				) oc ON oc.sku = bi.sku
	WHERE bi.FechaCompra=@fecha
	AND u.Id = @idComprador
	AND bi.SKU NOT IN (SELECT SKU FROM Salcobrand.products.GenericFamilyDetail)
	GROUP BY u.Id,u.Nombre,bi.GroupId,bi.FantasyName


	UNION 

	SELECT	u.Id,
			u.Nombre,
			bi.GroupId [IdGrupo],
			bi.FantasyName [NombreGrupo], 
			COUNT(bi.SKU) as CuentaSKU,
			ISNULL(SUM(bi.Quantity),0) CantidadComprado,
			ISNULL(SUM(bi.Cost),0) [Costo],
			SUM(ps.CompraSugerida) CompraSugerida,
			SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END) CompraZero,
			SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END) SinCompraSugerida,
			SUM(CASE WHEN oc.CreatedDate IS NULL THEN 0 ELSE 1 END) Comprado,
			CASE 
				WHEN (COUNT(bi.sku)-(SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END)+SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END))) != 0 
					THEN SUM(CASE WHEN oc.CreatedDate IS NULL THEN 0 ELSE 1 END)/(COUNT(bi.sku)-(SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END)+SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END))) 
					ELSE 1 END PorcSkusComprado
	FROM aux.BaseIndicadores bi
	LEFT JOIN (SELECT	SKU,
						Purchase CompraSugerida 
				FROM suggest.purchasesuggest) ps on ps.SKU=bi.SKU
	LEFT JOIN stdusuarios.dbo.usuario u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS=bi.comprador
	LEFT JOIN (SELECT	od.PurchaseOrderId,
						od.SKU,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date)= @fecha
				) oc ON oc.sku = bi.sku
	WHERE bi.FechaCompra=@fecha
	AND u.Id = @idComprador
	AND bi.SKU IN (SELECT SKU FROM products.GenericTenderedSKU)
	GROUP BY u.Id,u.Nombre,bi.GroupId,bi.FantasyName


	ORDER BY PorcSkusComprado asc
END
