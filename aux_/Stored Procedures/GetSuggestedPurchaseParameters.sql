﻿
CREATE PROCEDURE [aux].[GetSuggestedPurchaseParameters]
AS
BEGIN

SET NOCOUNT ON;

SELECT
RUT, m.Name Manufacturer, p.LogisticAreaId, TotalRanking, SKU, p.Name, ServiceLevel Id, FORMAT(ServiceLevel, 'P1', 'es-CL') Description
FROM products.Products p
INNER JOIN products.Manufacturers m ON P.ManufacturerId = m.Id


END