﻿
CREATE PROCEDURE [aux].[GetManufacturers]
AS
BEGIN

SET NOCOUNT ON;

SELECT [Id]
      ,[Name] Nombre
      ,[RUT]
      ,[FillRateMonths] Meses
	  ,ISNULL(ur.BuyerId,0) Id
	  ,ISNULL(ur.Username,'') Description
  FROM [products].[Manufacturers] m
  LEFT JOIN products.DefaultManufacturerBuyer d ON m.Id = d.ManufacturerId
  LEFT JOIN users.UserRules ur ON d.Username = ur.Username


END

