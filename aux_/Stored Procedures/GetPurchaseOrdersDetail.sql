﻿-- =============================================
-- Author:		Jorge Ibacache
-- Create date: 15-08-2017
-- Description:	Obtiene el detalle de las órdenes de compra antes que se están gestionando en una determinada pantalla.
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrdersDetail]
	@type nvarchar(3),
	@USERNAME nvarchar(max)
AS
BEGIN


	--DECLARE @type nvarchar(3) = 'ESP';
	--DECLARE @USERNAME nvarchar(max) = 'jibacache'

	SET NOCOUNT ON;

	SELECT  po.Id IdOC,
			l.RUT,
			ISNULL(isa.Isapre,'-') Isapre,
			l.Laboratory Laboratorio,
			po.LogisticAreaId Rubro,
			CASE 
				WHEN po.AvailableToBuy = 'RDY' THEN 'OC Correcta'
				WHEN po.AvailableToBuy = 'PEN' THEN 'Pendiente'
				WHEN po.AvailableToBuy = 'GSS' THEN 'Genera Sobrestock'
				ELSE 'Rechazada'
			END [Estado Bloqueo],
			qs.[#Skus] QSKU,
			qs.PurchaseQuantity UnidadesTotales,
			CASE WHEN po.IdZona = 37 THEN qs.PurchaseCost ELSE ges.PurchaseCost END MontoTotal,
			CASE WHEN po.IsFree = 1 THEN 'SI' ELSE 'NO' END Gratis,
			po.CreatedDate FEmision,
			po.ReceptionDate FRecepcion,
			po.OrderComment Glosa,
			po.Store,
			d.[Description] TipoDespacho,
			pod.SKU,
			pod.Quantity Cantidad,
			pod.PurchaseSuggest Sugerido
	FROM (SELECT DISTINCT po.*
			FROM aux.PurchaseOrders po
			WHERE CreatedBy = @USERNAME) po
	INNER JOIN aux.PurchaseOrderDetails pod ON po.Id = pod.PurchaseOrderId
	LEFT  JOIN setup.DistributorIsapre isa ON po.IdZona = isa.IdZona
	--proveedores o laboratorios
	INNER JOIN (SELECT	r.RUT,
						ISNULL(m.[Name],d.[Name]) Laboratory,
						m.Id ManufacturerId
				FROM (SELECT RUT FROM products.Manufacturers 
						UNION 
						SELECT RUT FROM products.Distributors) r
				LEFT JOIN products.Manufacturers m ON r.RUT = m.RUT
				LEFT JOIN products.Distributors d ON r.RUT = d.RUT) l ON po.RUT = l.RUT AND (po.ManufacturerId = l.ManufacturerId OR po.RUT IN (SELECT RUT FROM products.Distributors))
	LEFT JOIN (SELECT	PurchaseOrderId,
						count(*) [#Skus],
						SUM(Quantity) PurchaseQuantity,
						SUM(Quantity*CASE WHEN oc.PurchaseType = 'ESP' THEN psp.Cost ELSE pc.Cost END) PurchaseCost
				FROM aux.PurchaseOrderDetails pod
				INNER JOIN aux.PurchaseOrders oc on pod.PurchaseOrderId = oc.Id
				LEFT JOIN setup.PriceAndCost pc ON pod.SKU = pc.SKU	
				LEFT JOIN products.ProductsSpecialPurchase psp ON pod.SKU = psp.SKU			
				GROUP BY PurchaseOrderId) qs ON po.Id = qs.PurchaseOrderId
	LEFT JOIN (SELECT	det.PurchaseOrderId,
						SUM(Quantity*pc.Cost) PurchaseCost
				FROM aux.PurchaseOrderDetails det
				INNER JOIN aux.PurchaseOrders oc ON det.PurchaseOrderId = oc.Id					
				INNER JOIN setup.PriceAndCostGES pc ON det.SKU = pc.SKU AND oc.IdZona = pc.IdZona
				WHERE oc.IdZona <> 37
				GROUP BY det.PurchaseOrderId) ges ON po.Id = ges.PurchaseOrderId
	INNER JOIN setup.DeliveryType d ON po.DeliveryTypeId = d.Id
	WHERE po.[PurchaseType] = @type
END
