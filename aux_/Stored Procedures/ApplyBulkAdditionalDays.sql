﻿CREATE PROCEDURE [aux].[ApplyBulkAdditionalDays]
@GRUPO INT,
@DIAS  INT
,@skus NVARCHAR(MAX) = NULL
AS
BEGIN
SET NOCOUNT ON;

DECLARE @skus_enviados TABLE (SKU INT NOT NULL PRIMARY KEY);
INSERT INTO @skus_enviados 
SELECT ISNULL(CAST([Name] AS INT), -1) FROM dbo.SplitString(@skus)


CREATE TABLE #CompraRedondeada ([sku] int, [unidades] int )	 
INSERT INTO #CompraRedondeada exec [aux].[PurchaseForAdditionalDay] null, @DIAS, @grupo 


UPDATE a 
SET AdditionalDaysToCover = CASE WHEN @DIAS > 0 THEN @DIAS ELSE null END, 
Purchase =  CASE WHEN @DIAS > 0 THEN [unidades] ELSE PurchaseSuggest END
FROM (
SELECT ps.AdditionalDaysToCover, @DIAS Dias, ps.Purchase, r.[unidades], ps.PurchaseSuggest 
FROM aux.PurchaseSummary ps
INNER JOIN #CompraRedondeada r ON ps.SKU = r.SKU
WHERE ps.SKU IN (SELECT SKU FROM @skus_enviados)
) a
  
SELECT COUNT(*)
FROM #CompraRedondeada c
INNER JOIN products.Products p ON c.sku = p.SKU
AND p.SKU IN (SELECT SKU FROM @skus_enviados)

DROP TABLE #CompraRedondeada

END
