﻿-- =============================================
-- Author:		C Astroza
-- Create date: 2018-03-21
-- Description:	Obtiene las órdenes de compra creadas el día.
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrdersCreatedToday]
@USERNAME NVARCHAR(MAX)
AS
BEGIN

DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USERNAME);
IF ISNULL(@Usuario_admin,0) = 1 BEGIN
SET @USERNAME = null;
END

	SET NOCOUNT ON;

		SELECT 
		CASE 
		WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 1 THEN 'registrado.png' 
		WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 2 THEN 'comprado.png' 
		WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 3 THEN 'no_comprado.png' 
		ELSE 'proceso.png' END Estado,
		CASE 
		WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 1 THEN 'Registrada' 
		WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 2 THEN 'Orden de compra confirmada por el ERP' 
		WHEN dpc.IdOrigen IS NOT NULL AND EstadoOOCC = 3 THEN 'Ordern de compra anulada por el ERP' 
		ELSE 'Orden de Compra en espera por el ERP' END ToolTip,
		po.CreatedBy Comprador
		,po.Id, po.PurchaseType
		, l.RUT, l.Laboratory, po.LogisticAreaId
		, qs.[#Skus] QSKU, qs.PurchaseQuantity, qs.PurchaseCost
		, CAST(CASE WHEN po.IsFree = 1 THEN 'SI' ELSE 'NO' END AS nvarchar) Gratis, 
		--po.CreatedDate, 
		po.ReceptionDate, po.OrderComment
		, po.Store--, d.Id, d.[Description]
	FROM (
        select distinct po.*--, p.ManufacturerId
        from [purchase].[PurchaseOrders] po
        inner join [purchase].PurchaseOrderDetails pod ON po.Id = pod.PurchaseOrderId
        inner join products.Products p ON pod.SKU = p.SKU
        inner join products.Manufacturers m ON p.ManufacturerId = m.Id

		inner join users.SkuUsers su on pod.SKU = su.SKU
		WHERE @USERNAME is null or su.Username = @USERNAME

    ) po
	INNER JOIN (
		--proveedores o laboratorios
		select r.RUT, ISNULL(m.[Name], d.[Name]) Laboratory, m.Id ManufacturerId
		from (
			select RUT from products.Manufacturers 
			UNION 
			select RUT from products.Distributors
		) r
		left join products.Manufacturers m ON r.RUT = m.RUT
		left join products.Distributors d ON r.RUT = d.RUT
	)l on po.RUT = l.RUT and (po.ManufacturerId = l.ManufacturerId OR po.RUT IN(SELECT RUT FROM products.Distributors))
	INNER JOIN (
		select 
			PurchaseOrderId
			, count(*) [#Skus]
			, SUM(Quantity) PurchaseQuantity 
			, SUM(Quantity*pc.Cost) PurchaseCost
		FROM [purchase].PurchaseOrderDetails pod
		LEFT JOIN setup.PriceAndCost pc ON pod.SKU = pc.SKU

		inner join users.SkuUsers su on pod.SKU = su.SKU
		where @USERNAME is null or su.Username = @USERNAME

		GROUP BY PurchaseOrderId
	) qs ON po.Id = qs.PurchaseOrderId
	inner join setup.DeliveryType d ON po.DeliveryTypeId = d.Id
	LEFT JOIN (
			SELECT DISTINCT IdOrigen, EstadoOOCC
			FROM series.DailyPurchaseOrders
			WHERE IdOrigen IS NOT NULL
			) 
			dpc ON po.Id = dpc.IdOrigen
	WHERE CAST(po.createddate AS date) = convert(date,getdate())

END
