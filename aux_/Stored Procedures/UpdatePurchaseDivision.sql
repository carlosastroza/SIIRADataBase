﻿
-- =============================================
-- Author:		C. Astroza
-- Create date: 2018-17-07
-- Description:	Actualiza Cantidades OC
-- =============================================
CREATE PROCEDURE [aux].[UpdatePurchaseDivision]
@ProcessId UNIQUEIDENTIFIER,
@OC INT
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM aux.LoadPurchaseOrdersDivision
WHERE (Unidades = 0)
AND (ProcessId = @ProcessId)

DECLARE @ResultadoTemporal TABLE (DIAS_SOBRESTOCK INT,OC INT)
DECLARE @MaxDaysInt INT;
DECLARE @MaxDays TABLE(
Id NVARCHAR(MAX),
Value INT
)
INSERT INTO @MaxDays
EXEC [setup].[GetConfigurationDictionary] 'SB'
SET @MaxDaysInt = (SELECT Value FROM @MaxDays)

DECLARE @CANT_DIV INT = (SELECT  MAX(CorrelativoOC) FROM aux.LoadPurchaseOrdersDivision WHERE ProcessId = @ProcessId);
DECLARE	@id int, @MAX_OC INT
SET @id=1

WHILE @id<=@CANT_DIV
BEGIN

IF @id = 1 BEGIN

/* A la primera OC (la original) actualizamos la cantidad */
UPDATE s SET AntiguaCompra = NuevaCompra
FROM(
	SELECT lo.Unidades NuevaCompra, det.Quantity AntiguaCompra 
	from aux.LoadPurchaseOrdersDivision lo
	INNER JOIN aux.PurchaseOrderDetails det ON lo.SKU = det.SKU
	WHERE (CorrelativoOC = 1)
	AND   (ProcessId = @ProcessId)
	AND   (PurchaseOrderId = @OC)
) s

/* Actualizar estado de bloqueo de la primera OC*/
INSERT INTO @ResultadoTemporal
SELECT a.Days, det.PurchaseOrderId
FROM products.Products p
INNER JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
INNER JOIN aux.PurchaseOrderDetails det ON p.SKU = det.SKU
CROSS APPLY [suggest].[GetInventoryDays](p.SKU,det.Quantity+ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) a
WHERE PurchaseOrderId = @OC

DECLARE @CANTIDAD_DIAS_SS  INT = 0;
SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) FROM @ResultadoTemporal WHERE (DIAS_SOBRESTOCK > @MaxDaysInt));
IF @CANTIDAD_DIAS_SS > 0 BEGIN UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @OC END
ELSE BEGIN UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @OC END 
/* End actualizar estado bloqueo primera OC*/

END
ELSE BEGIN

INSERT INTO aux.PurchaseOrders(ParentId,RUT,LogisticAreaId,OrderComment,DeliveryTypeId,Store,CreatedBy,CreatedDate,LeadTime,ReceptionDate,IsFree,PurchaseType,IsRetail,ManufacturerId,AvailableToBuy,IdZona, CustomGroup)
SELECT @OC,RUT,LogisticAreaId,OrderComment,DeliveryTypeId,Store,CreatedBy,CreatedDate,LeadTime,ReceptionDate,IsFree,PurchaseType,IsRetail,ManufacturerId,AvailableToBuy,IdZona, CustomGroup
FROM aux.PurchaseOrders
WHERE Id = @OC

SET @MAX_OC = (SELECT MAX(ID) FROM aux.PurchaseOrders WHERE ParentId = @OC);

INSERT INTO aux.PurchaseOrderDetails(PurchaseOrderId,SKU,Quantity)
SELECT @MAX_OC,SKU,Unidades
FROM aux.LoadPurchaseOrdersDivision
WHERE (CorrelativoOC = @id)
AND (ProcessId = @ProcessId)

/* Actualizar estado bloqueo OC creadas */
DELETE FROM @ResultadoTemporal

INSERT INTO @ResultadoTemporal
SELECT a.Days, det.PurchaseOrderId
FROM products.Products p
INNER JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
INNER JOIN aux.PurchaseOrderDetails det ON p.SKU = det.SKU
CROSS APPLY [suggest].[GetInventoryDays](p.SKU,det.Quantity+ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) a
WHERE PurchaseOrderId = @MAX_OC

SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) FROM @ResultadoTemporal WHERE (DIAS_SOBRESTOCK > @MaxDaysInt));
IF @CANTIDAD_DIAS_SS > 0 BEGIN UPDATE aux.PurchaseOrders set AvailableToBuy = 'GSS' WHERE Id = @MAX_OC END
ELSE BEGIN UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @MAX_OC END 

/* END Actualizar estado bloqueo OC creadas */

END

SELECT @id=@id+1
END

DELETE FROM aux.LoadPurchaseOrdersDivision
WHERE ProcessId = @ProcessId

END
