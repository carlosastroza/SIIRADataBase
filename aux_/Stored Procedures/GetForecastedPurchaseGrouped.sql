﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-08-15
-- Description:	Obtiene el reporte de venta proyectada para mostrar por GUI

-- UPDATE: Jorge I. se modifica la query para traer los costos
-- =============================================
CREATE PROCEDURE [aux].[GetForecastedPurchaseGrouped]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	m.RUT,
			m.[Name] Laboratorio,
			SUM(fp.[MES ACTUAL]) MesActual,
			SUM(pc.Cost*fp.[MES ACTUAL]) CostoMesActual,
			SUM(fp.[MES 2]) MesSiguiente,
			SUM(pc.Cost*fp.[MES 2]) CostoMesSiguiente,
			SUM(fp.[MES 3]) MesSubSiguiente,
			SUM(pc.Cost*fp.[MES 3]) CostoMesSubSiguiente,
			SUM([MES ACTUAL]) + SUM([MES 2]) + SUM([MES 3]) Total,
			SUM(pc.Cost*fp.[MES ACTUAL]) + SUM(pc.Cost*fp.[MES 2]) + SUM(pc.Cost*fp.[MES 3]) CostoTotal
	from reports.ForecastedPurchase fp
	INNER JOIN products.Products p ON fp.SKU = p.SKU
	LEFT JOIN setup.PriceAndCost pc ON p.SKU = pc.SKU
	INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
	GROUP BY m.RUT, m.[Name]
	ORDER BY m.RUT, m.[Name]
END