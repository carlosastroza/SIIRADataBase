﻿
CREATE PROCEDURE [aux].[GetProductsSpecialPurchase]
@PROVEEDOR INT,
@RUBRO     NVARCHAR(MAX),
@USERNAME NVARCHAR(MAX)
--,@IDZONA INT
AS
BEGIN

SET NOCOUNT ON;

SELECT 
p.sku SKU,
p.name NombreProducto,
CAST(CASE WHEN a.SKU > 0 then 1 else 0 end as bit) isSeleccionado
FROM products.ProductsSpecialPurchase p
LEFT JOIN (
SELECT * FROM aux.SpecialPurchase
WHERE Username = @USERNAME
) a ON P.SKU = A.SKU
--LEFT JOIN products.Vademecum v ON p.SKU = v.SKU
WHERE (ManufacturerId = @PROVEEDOR)
AND   (LogisticAreaId = @RUBRO)
AND   (ISNULL(TotalRanking,'') <> 'Z')
--AND   (v.IdZona = @IDZONA OR @IDZONA = 37)

END

