﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 2018-08-27
-- Description:	Elimina OC Y sus detalles.

-- Modificado:		Juan Manuel
-- Create date: 2018-08-09
-- Description:	Acualiza status de las summary

-- =============================================

CREATE PROCEDURE [aux].[DeletePurchaseOrder]
@OC  INT
AS
BEGIN

SET NOCOUNT ON;

DECLARE @ISRETAIL BIT = (select ISRETAIL from aux.PurchaseOrders WHERE ID = @OC)

IF @ISRETAIL = 0
BEGIN 
	UPDATE PD
	SET PD.status = 0
	FROM AUX.PurchaseSummaryDistributor PD
	WHERE SKU IN (
		select POD.SKU from aux.PurchaseOrderDetails POD
		INNER JOIN aux.PurchaseOrders PO ON PO.Id = POD.PurchaseOrderId  WHERE POD.PurchaseOrderId = @OC
	)
END
ELSE BEGIN 
UPDATE PD
	SET PD.status = 0
	FROM AUX.PurchaseSummary PD
	WHERE SKU IN (
		select POD.SKU from aux.PurchaseOrderDetails POD
		INNER JOIN aux.PurchaseOrders PO ON PO.Id = POD.PurchaseOrderId  WHERE POD.PurchaseOrderId = @OC
	)
END 

DELETE FROM aux.PurchaseOrderDetails WHERE PurchaseOrderId = @OC
DELETE FROM aux.PurchaseOrders WHERE Id = @OC

end
