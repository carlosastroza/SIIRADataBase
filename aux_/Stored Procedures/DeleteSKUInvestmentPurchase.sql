﻿-- =============================================
-- Author:		C. Astroza
-- Create date: 2018-05-30
-- Description:	Borrar SKU de la compra por inversión.
-- =============================================
CREATE PROCEDURE [aux].[DeleteSKUInvestmentPurchase]
@USERNAME NVARCHAR(MAX),
@SKU INT
AS
BEGIN

DELETE FROM aux.OptimizationSkus
WHERE (Username = @USERNAME) 
AND (SKU = @SKU)

END