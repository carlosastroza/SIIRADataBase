﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 2018-07-26
-- Description:	Obtiene OC sin fecha de recepción
-- =============================================

CREATE PROCEDURE [aux].[GetOCWithOutReceptionDate]
@PURCHASE_TYPE NVARCHAR(3),
@USERNAME NVARCHAR(50)
AS
BEGIN

SET NOCOUNT ON;

SELECT COUNT(DISTINCT o.Id)
FROM aux.PurchaseOrders o
WHERE (PurchaseType = @PURCHASE_TYPE)
AND   (o.AvailableToBuy = 'RDY')
AND   (ReceptionDate IS NULL)
AND   (o.CreatedBy = @USERNAME)

END

