﻿-- =============================================
-- Author:		Carlos A
-- Create date: 2018-07-10
-- Description:	Obtiene Info logística de una OC	
-- =============================================

CREATE PROCEDURE [aux].[GetLogisticInfoFromPurchase]
@OC INT
AS
BEGIN

SELECT det.SKU, p.Name Nombre, Quantity Compra,BuyingMultiple, UnitsInBox UnidadesCaja, UnitsInLayer UnidadesLayer, UnitsInPallet UnidadesPallet
FROM aux.PurchaseOrderDetails det
INNER JOIN products.Products p ON det.SKU = p.SKU
LEFT JOIN setup.LogisticParameters lp ON det.SKU = lp.SKU
WHERE PurchaseOrderId = @OC

END
