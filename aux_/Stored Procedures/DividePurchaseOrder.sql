﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-04-02
-- Description:	Divide una OC del esquema AUX 
-- =============================================
CREATE PROCEDURE [aux].[DividePurchaseOrder]
	@purchaseOrderId INT,
	@prioritySKUs NVARCHAR(MAX),
	@divideInto INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @workingData TABLE (
	  SKU INT NOT NULL
	  , RemainingStockDays INT NULL
	  , Quantity INT NOT NULL
	  , IsPriority BIT NOT NULL
	  , [Order] INT NOT NULL
	  , PurchaseSuggest int null
	  , OC INT NULL
	);
	declare @priorityTbl TABLE (
	  PrioritySKU INT NOT NULL
	);
	insert into @priorityTbl
	select cast([Name] as INT) from Salcobrand.dbo.SplitString(@prioritySKUs)


	DECLARE @total FLOAT = (SELECT SUM(Quantity) FROM aux.PurchaseOrderDetails WHERE PurchaseOrderId = @purchaseOrderId) / @divideInto;

	--calculamos la OC a la que pertenecería cada SKU
	insert into @workingData (SKU, RemainingStockDays, Quantity, IsPriority, [Order], PurchaseSuggest , OC)
	select *
		, NTILE(@divideInto) over (order by [order]) OC
		--, SUM(Quantity) OVER(ORDER BY [Order] ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
		--, @total
		--, SUM(Quantity) OVER(ORDER BY [Order] ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) / @total
	from 
	(
	  select 
		a.SKU, a.RemainingStockDays, d.Quantity
		, CASE WHEN t.PrioritySKU IS NULL THEN 0 ELSE 1 END IsPriority
		, ROW_NUMBER() OVER (ORDER BY (CASE WHEN t.PrioritySKU IS NULL THEN 0 ELSE 1 END) DESC, RemainingStockDays ASC) [order]
		,a.PurchaseSuggest
	  from aux.PurchaseSummary a
	  inner join aux.PurchaseOrderDetails d ON a.SKU = d.SKU
	  left join @priorityTbl t ON a.SKU = t.PrioritySKU
	  where PurchaseOrderId = @purchaseOrderId
	) w
	order by [order]

	UPDATE @workingData SET [OC] = 1 WHERE IsPriority = 1
	--select * from @workingData order by OC


	--El siguiente cursor realiza el trabajo de dividir la OC en varias OC más pequeñas.
	DECLARE @auxOcID TABLE (Id INT);
	DECLARE @auxOC INT

	DECLARE cur CURSOR FOR (select DISTINCT OC from @workingData WHERE OC <> 1)
	OPEN cur
	FETCH NEXT FROM cur INTO @auxOC
	WHILE @@FETCH_STATUS = 0  
	BEGIN

	select * from aux.PurchaseOrders
	
		INSERT aux.PurchaseOrders OUTPUT INSERTED.Id INTO @auxOcID
		select @purchaseOrderId, RUT, LogisticAreaId, OrderComment, DeliveryTypeId, Store, CreatedBy, CreatedDate, LeadTime, ReceptionDate, IsFree, PurchaseType, IsRetail, ManufacturerId,AvailableToBuy
		from aux.PurchaseOrders WHERE Id = @purchaseOrderId

		INSERT INTO aux.PurchaseOrderDetails (PurchaseOrderId, SKU, Quantity,PurchaseSuggest )
		select aux.Id, wd.SKU, wd.Quantity, wd.PurchaseSuggest
		from @workingData wd
		cross join @auxOcID aux
		WHERE wd.OC = @auxOC

		DELETE FROM @auxOcID

		delete d
		--select *
		from aux.PurchaseOrderDetails d
		inner join @workingData wd ON d.SKU = wd.SKU
		where wd.OC = @auxOC and d.PurchaseOrderId = @purchaseOrderId

		FETCH NEXT FROM cur INTO @auxOC
	END   
	CLOSE cur
	DEALLOCATE cur

END
