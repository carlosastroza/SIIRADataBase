﻿

---- =============================================
---- Author:		Jorge I.
---- Create date:	23-05-2018
---- Description:	Obtiene los indicadores de compra para compradores
---- =============================================

CREATE PROCEDURE [aux].[GetPurchaseOrdersReportComprador]
@IdComprador NVARCHAR(50)
AS
BEGIN
	--DECLARE @IdComprador NVARCHAR(50) = 'admin'
	SELECT	--u.Id,
			po.Id IdOc,
			m.Name Laboratorio,
			COUNT(det.SKU) NumSKU,
			SUM(det.Quantity*pc.Cost) MontoCompra,
			--CASE WHEN po.AvailableToBuy = 1 THEN 'Autorizado' ELSE 'Pendiente' END as Estado
			CASE WHEN po.AvailableToBuy = 'RDY' THEN 'Autorizado' ELSE 'Pendiente' END as Estado

	FROM aux.PurchaseOrders po
	LEFT JOIN StdUsuarios.dbo.Usuario u on po.CreatedBy = u.Id COLLATE SQL_Latin1_General_CP1_CI_AS
	INNER JOIN products.Manufacturers m ON po.ManufacturerId = m.Id
	INNER JOIN aux.PurchaseOrderDetails det on po.id = det.PurchaseOrderId
	INNER JOIN setup.PriceAndCost pc on det.SKU = pc.SKU
	WHERE u.Id = @IdComprador
	--AND AvailableToBuy  = 0
	AND AvailableToBuy  = 'RDY'

	GROUP BY u.id,po.Id,m.Name,CASE WHEN po.AvailableToBuy = 'RDY' THEN 'Autorizado' ELSE 'Pendiente' END
	--GROUP BY u.id,po.Id,m.Name,CASE WHEN po.AvailableToBuy = 1 THEN 'Autorizado' ELSE 'Pendiente' END

END
