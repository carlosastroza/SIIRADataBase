﻿
CREATE PROCEDURE [aux].[CreatePurchaseOrders]
	@TIPO_COMPRA NVARCHAR(3),
	@USERNAME   NVARCHAR(MAX)
AS
BEGIN

	DECLARE @USUARIO_LOCAL NVARCHAR(MAX) = @USERNAME;

	IF @USERNAME IN (SELECT USERNAME FROM setup.AdminUsers) BEGIN
		SET @USERNAME = null;
	END

	INSERT INTO [purchase].[PurchaseOrders]
	SELECT DISTINCT oc.Id,
					oc.ParentId,
					oc.RUT,
					oc.LogisticAreaId,
					oc.OrderComment,
					oc.DeliveryTypeId,
					oc.Store,
					oc.CreatedBy,
					oc.CreatedDate,
					oc.LeadTime,
					oc.ReceptionDate,
					oc.IsFree,
					oc.PurchaseType,
					0,
					oc.IsRetail,
					oc.ManufacturerId,
					oc.IdZona
	FROM aux.[PurchaseOrders] oc
	INNER JOIN aux.PurchaseOrderDetails det on oc.Id = det.PurchaseOrderId
	LEFT JOIN (
		select * 
		from users.SkuUsers 
		WHERE (@USERNAME is null or Username = @USERNAME)
	) su on det.SKU = su.SKU
	WHERE (oc.PurchaseType = @TIPO_COMPRA)
	AND	  (su.SKU is not null OR @TIPO_COMPRA = 'ESP')
	AND   (oc.AvailableToBuy = 'RDY')
	AND   (ReceptionDate IS NOT NULL)
	AND   (oc.CreatedBy = @USUARIO_LOCAL)


	-- CON MATRIZ DE COSTO PROVEEDORES
	INSERT INTO [purchase].[PurchaseOrderDetails]
	SELECT	de.Id,
			de.PurchaseOrderId, 
			de.SKU, 
			de.Quantity, 
			ISNULL(p.Cost,psp.Cost), 
			ISNULL (de.PurchaseSuggest, 0) 
	FROM aux.PurchaseOrderDetails de
	INNER JOIN aux.PurchaseOrders oc on de.PurchaseOrderId = oc.Id
	LEFT JOIN setup.PriceAndCost p  on de.SKU = p.SKU
	LEFT JOIN products.ProductsSpecialPurchase psp ON de.SKU = psp.SKU
	--INNER JOIN users.SkuUsers su on de.SKU = su.SKU
	--WHERE (oc.PurchaseType = @TIPO_COMPRA)
	--AND   (oc.AvailableToBuy = 'RDY')
	--AND   (oc.IdZona = 37)
	--AND   (ReceptionDate IS NOT NULL)
	--AND   (oc.CreatedBy = @USUARIO_LOCAL)
	LEFT JOIN (
		select * 
		from users.SkuUsers 
		WHERE (@USERNAME is null or Username = @USERNAME)
	) su on de.SKU = su.SKU
	WHERE (oc.PurchaseType = @TIPO_COMPRA)
	AND	  (su.SKU is not null OR @TIPO_COMPRA = 'ESP')
	AND   (oc.AvailableToBuy = 'RDY')
	AND   (ReceptionDate IS NOT NULL)
	AND   (oc.CreatedBy = @USUARIO_LOCAL)


	-- CON MATRIZ DE COSTO DISTRIBUIDORES
	INSERT INTO [purchase].[PurchaseOrderDetails]
	SELECT	de.Id,
			de.PurchaseOrderId, 
			de.SKU, 
			de.Quantity, 
			p.Cost, 
			ISNULL (de.PurchaseSuggest, 0) 
	FROM aux.PurchaseOrderDetails de
	INNER JOIN aux.PurchaseOrders oc on de.PurchaseOrderId = oc.Id
	INNER JOIN setup.PriceAndCostGES p  on de.SKU = p.SKU and p.IdZona = oc.IdZona
	--INNER JOIN users.SkuUsers su on de.SKU = su.SKU
	--WHERE (oc.PurchaseType = @TIPO_COMPRA)
	----AND   (@USERNAME is null or su.Username = @USERNAME)
	--AND   (oc.AvailableToBuy = 'RDY')
	--AND   (oc.IdZona != 37)
	--AND   (ReceptionDate IS NOT NULL)
	--AND   (oc.CreatedBy = @USUARIO_LOCAL)
	LEFT JOIN (
		select * 
		from users.SkuUsers 
		WHERE (@USERNAME is null or Username = @USERNAME)
	) su on de.SKU = su.SKU
	WHERE (oc.PurchaseType = @TIPO_COMPRA)
	AND	  (su.SKU is not null OR @TIPO_COMPRA = 'ESP')
	AND   (oc.AvailableToBuy = 'RDY')
	AND   (ReceptionDate IS NOT NULL)
	AND   (oc.CreatedBy = @USUARIO_LOCAL)

	--SE ACTUALIZA EL ESTADO DE LAS ORDENES CON SOBRESTOCK, PASA DE GSS A PEN, A LA ESPERA DE APROBACIÓN DE UNA JEFATURA

	UPDATE oc
	SET oc.AvailableToBuy = 'PEN'
	FROM aux.[PurchaseOrders] oc
	WHERE	oc.PurchaseType = @TIPO_COMPRA AND
			oc.AvailableToBuy = 'GSS' AND
			(oc.CreatedBy = @USUARIO_LOCAL)
	
	SELECT COUNT(DISTINCT oc.Id)
	FROM aux.PurchaseOrders OC
	INNER JOIN aux.PurchaseOrderDetails d on oc.Id = d.PurchaseOrderId
	----INNER JOIN users.SkuUsers su on d.SKU = su.SKU
	--WHERE (PurchaseType = @TIPO_COMPRA)
	----AND   (@USERNAME IS NULL OR su.Username = @USERNAME)
	--AND   (o.AvailableToBuy = 'RDY' OR o.AvailableToBuy = 'PEN')
	--AND   (ReceptionDate IS NOT NULL)
	--AND   (o.CreatedBy = @USUARIO_LOCAL)
    LEFT JOIN (
		select * 
		from users.SkuUsers 
		WHERE (@USERNAME is null or Username = @USERNAME)
	) su on d.SKU = su.SKU
	WHERE (oc.PurchaseType = @TIPO_COMPRA)
	AND	  (su.SKU is not null OR @TIPO_COMPRA = 'ESP')
	AND   (oc.AvailableToBuy = 'RDY')
	AND   (ReceptionDate IS NOT NULL)
	AND   (oc.CreatedBy = @USUARIO_LOCAL)


	DELETE FROM aux.PurchaseOrderDetails 
	WHERE Id IN (
		SELECT Id 
		FROM [purchase].[PurchaseOrderDetails]
	)

	DELETE FROM aux.PurchaseOrders 
	WHERE Id IN (
		SELECT Id 
		FROM [purchase].PurchaseOrders
	)

	IF(@TIPO_COMPRA = 'ESP') 
	BEGIN 
	
		DELETE pss
		FROM aux.PurchaseSummarySpecial pss
		WHERE (@USERNAME IS NULL OR pss.Username = @USERNAME)

		DELETE sp
		FROM [aux].[SpecialPurchase] sp
		WHERE (@USERNAME IS NULL OR sp.Username = @USERNAME)

	END

END
