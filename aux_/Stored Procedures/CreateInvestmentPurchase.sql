﻿CREATE PROCEDURE [aux].[CreateInvestmentPurchase]
 @USERNAME NVARCHAR(50)
AS
BEGIN
SET NOCOUNT ON;

--DECLARE @USUARIO_BACK NVARCHAR(50) = @USERNAME;
	
--DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USUARIO_BACK);
--IF ISNULL(@Usuario_admin,0) = 1 BEGIN
--SET @USERNAME = null;
--END

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Creando órdenes de Compra GEN , SP[aux].[CreateInvestmentPurchase]', host_name(), @USERNAME, 'POST')

DECLARE @ResultadoTemporal TABLE (
SKU INT,
Compra INT,
Laboratorio INT, 
RUT INT, 
Rubro NVARCHAR(MAX), 
LeadTime INT, 
PurchaseSuggest INT
--,DIAS_SOBRESTOCK INT
)

SET DATEFIRST 1;
DECLARE @DOW INT = DATEPART(DW, GETDATE())

INSERT INTO @ResultadoTemporal
SELECT ip.Sku, 
ip.Cantidad * 
CASE WHEN UnidadLogisticaDesc = 'Caja'   
THEN lp.UnitsInBox 
WHEN UnidadLogisticaDesc = 'Layer'  
THEN lp.UnitsInLayer 
WHEN UnidadLogisticaDesc = 'Pallet' 
THEN lp.UnitsInPallet 
WHEN UnidadLogisticaDesc = 'No especificado'
THEN 1
ELSE lp.UnitsInBox END,
p.ManufacturerId, m.RUT, p.LogisticAreaId, 
CASE WHEN @DOW = 1 THEN ISNULL(LTMonday,LeadTime)
WHEN @DOW = 2 THEN ISNULL(LTTuesday,LeadTime) 
WHEN @DOW = 3 THEN ISNULL(LTWednesday,LeadTime) 
WHEN @DOW = 4 THEN ISNULL(LTThursday,LeadTime) 
WHEN @DOW = 5 THEN ISNULL(LTFriday,LeadTime) 
WHEN @DOW = 6 THEN ISNULL(LTSaturday,LeadTime) 
WHEN @DOW = 7 THEN ISNULL(LTSunday,LeadTime)  
END LT, ps.PurchaseSuggest
--, a.Days
FROM aux.InvestmentPurchase ip
INNER JOIN setup.LogisticParameters lp ON ip.Sku = lp.SKU
INNER JOIN products.Products P ON ip.Sku = p.SKU
INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
LEFT JOIN products.Groups g ON p.GroupId = g.Id
INNER JOIN aux.PurchaseSummary ps ON ip.Sku = ps.SKU
INNER JOIN setup.PriceAndCost pc on ip.SKU = pc.SKU
--OUTER APPLY [suggest].[GetInventoryDays](p.SKU,(Cantidad*CASE WHEN UnidadLogisticaDesc = 'Caja'   THEN lp.UnitsInBox WHEN UnidadLogisticaDesc = 'Layer'  THEN lp.UnitsInLayer WHEN UnidadLogisticaDesc = 'Pallet' THEN LP.UnitsInPallet ELSE lp.UnitsInBox END)+ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) a
WHERE (@USERNAME IS NULL OR ip.Username = @USERNAME)
AND (pc.Cost > 0)

	BEGIN TRY
		MERGE INTO aux.PurchaseOrders as oc
		USING @ResultadoTemporal AS rs
		on rs.Rut = oc.RUT AND rs.Rubro COLLATE DATABASE_DEFAULT =  oc.LogisticAreaId AND rs.Laboratorio = oc.ManufacturerId AND oc.PurchaseType = 'INV' AND oc.ParentId IS NULL AND  oc.CreatedBy = @USERNAME
		WHEN NOT MATCHED THEN
		INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate,LeadTime, ReceptionDate, isFree, PurchaseType,IsRetail, ManufacturerId, AvailableToBuy)
		VALUES(rs.Rut, rs.Rubro, 1,@USERNAME, GETDATE(),rs.LeadTime,DATEADD(DAY, LeadTime,GETDATE()), 0, 'INV', 1, rs.Laboratorio, 'RDY');


	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Error', 'Salcobrand.SIIRA',' Falló creando órdenes de compra GEN, SP [aux].[CreateInvestmentPurchase], ' + ERROR_MESSAGE() , host_name(), @USERNAME, 'POST');
		THROW;
	END CATCH

Declare 
@id int, 
@count int
Set @id=1
select @count=count(*)from @ResultadoTemporal 
DECLARE @sku int, @Rut int,@Lab INT,@rubro VARCHAR(MAX), @ORDEN_COMPRA int, @sku_cantidad int,  @SKU_DETALLE INT, @PurchaseSuggest INT

DECLARE @RETURN INT = 0;
--DECLARE @CANTIDAD_DIAS_SS  INT = 0;

while @id<=@count
begin


SET @PurchaseSuggest   = (select PurchaseSuggest from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @sku   = (select SKU from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @Rut   = (select RUT from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @rubro = (select Rubro from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @Lab   = (select Laboratorio from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @sku_cantidad = (select Compra from (select  *,RANK()OVER (ORDER BY SKU ASC)AS RANK from @ResultadoTemporal) as it where rank=@id);
SET @ORDEN_COMPRA = (select top 1 oc.ID from aux.PurchaseOrders oc where oc.RUT = @Rut AND oc.LogisticAreaId = @rubro AND ManufacturerId = @Lab AND PurchaseType = 'INV' AND oc.ParentId IS NULL AND  oc.CreatedBy = @USERNAME )
SET @SKU_DETALLE  = (select top 1 count(*) from aux.PurchaseOrderDetails ocd INNER JOIN aux.PurchaseOrders oc ON ocd.PurchaseOrderId = oc.Id WHERE oc.Id = @ORDEN_COMPRA AND ocd.SKU = @sku);


IF @SKU_DETALLE > 0 BEGIN
	BEGIN TRY
		UPDATE aux.PurchaseOrderDetails set Quantity = @sku_cantidad WHERE SKU = @sku;
	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Falló actualizando sku '+ @sku +' , SP [aux].[CreateInvestmentPurchase], ' + ERROR_MESSAGE() , host_name(), @USERNAME, 'POST');
		THROW;
	END CATCH
END
ELSE
BEGIN
	BEGIN TRY
		INSERT INTO aux.PurchaseOrderDetails VALUES(@ORDEN_COMPRA,@sku,@SKU_CANTIDAD, @PurchaseSuggest);
	END TRY 
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
		VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Falló insertando sku '+ @sku +' , SP [aux].[CreateInvestmentPurchase], ' + ERROR_MESSAGE() , host_name(), @USERNAME, 'POST');
		THROW;
	END CATCH
END

select @id=@id+1
end

DELETE FROM aux.PurchaseOrderDetails WHERE Quantity = 0;

DELETE s FROM aux.PurchaseOrderDetails s
INNER JOIN aux.PurchaseOrders oc ON s.PurchaseOrderId = oc.Id
WHERE (oc.PurchaseType = 'INV')
AND (OC.CreatedBy = @USERNAME)
AND (SKU NOT IN (SELECT SKU FROM aux.InvestmentPurchase WHERE Username = @USERNAME) )

DELETE FROM aux.PurchaseOrders WHERE ID not in (select PurchaseOrderId from aux.PurchaseOrderDetails) and PurchaseType = 'INV'

INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor],Usuario)
VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Órdenes de compra GEN creadas correctamente, SP [aux].[CreateInvestmentPurchase], ' , host_name(),@USERNAME)

DECLARE @OC_PROBLEMA_FECHA INT, @OC_GENERADAS INT;

SET @OC_PROBLEMA_FECHA =  ISNULL((SELECT COUNT(*) FROM @ResultadoTemporal WHERE LeadTime IS NULL AND Compra > 0 GROUP BY RUT, Laboratorio, Rubro),0);
SET @OC_GENERADAS      =  ISNULL((SELECT COUNT(*) FROM aux.PurchaseOrders po WHERE PurchaseType = 'INV' and CreatedBy = @USERNAME),0)

IF @OC_GENERADAS = 0 BEGIN
SET @RETURN = 0;
END
ELSE BEGIN

SET @RETURN = 1;

IF @OC_PROBLEMA_FECHA > 0 BEGIN
SET @RETURN = 2;
END

END


SELECT @RETURN;


END
