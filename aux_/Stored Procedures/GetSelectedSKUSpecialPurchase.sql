﻿


CREATE PROCEDURE [aux].[GetSelectedSKUSpecialPurchase]
@Username nvarchar(50)
AS
BEGIN

SET NOCOUNT ON;

--IF @Username IN (SELECT Username FROM setup.AdminUsers) BEGIN
--SET @Username = NULL
--END

SELECT 
p.SKU,
p.Name  NombreProducto,
m.Name Proveedor,
p.LogisticAreaId Rubro,
CAST (1 AS BIT) IsSeleccionado
FROM  [aux].SpecialPurchase sp
INNER JOIN products.ProductsSpecialPurchase p ON sp.SKU = p.SKU
INNER JOIN  products.Manufacturers m on m.Id = p.ManufacturerId
WHERE sp.Username = @USERNAME
ORDER BY sp.SKU asc

end

