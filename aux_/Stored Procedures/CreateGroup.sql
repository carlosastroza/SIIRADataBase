﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 08-01-2018
-- Description:	Crea Grupo
-- =============================================

CREATE PROCEDURE [aux].[CreateGroup]
@NOMBRE NVARCHAR(MAX),
@CC     INT,
@LT     INT,
@LU     BIT,
@MA     BIT,
@MI		BIT,
@JU		BIT,
@VI		BIT,
@SA		BIT,
@DO		BIT,
@USERNAME NVARCHAR(MAX),
@FECHA DATETIME
AS
BEGIN

SET NOCOUNT ON;

	INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
	VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Creando grupo '+ @NOMBRE+', SP [aux].[CreateGroup]', host_name())
	
	BEGIN TRY 
		INSERT INTO products.Groups (FantasyName
		  ,[OrderCycle]
		  ,[LeadTime]
		  ,[PurchaseOnMonday]
		  ,[PurchaseOnTuesday]
		  ,[PurchaseOnWednesday]
		  ,[PurchaseOnThursday]
		  ,[PurchaseOnFriday]
		  ,[PurchaseOnSaturday]
		  ,[PurchaseOnSunday]
		  ,ModifiedBy
		  ,ModifiedOn
		  ,CustomGroup) VALUES (@NOMBRE,@CC, @LT,@LU,@MA,@MI,@JU,@VI, @SA, @DO, @USERNAME, @FECHA,1);

		  INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
		  VALUES (getdate(), 'Info', 'Salcobrand.SIIRA',' Grupo '+@NOMBRE+ ' creado correctamente, SP [aux].[CreateGroup], ' , host_name());
	END TRY
	BEGIN CATCH
		INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor])
		VALUES (getdate(), 'Warn', 'Salcobrand.SIIRA',' Grupo '+@NOMBRE+ ' no  Creado, SP [aux].[CreateGroup], ' + ERROR_MESSAGE() , host_name());
		THROW;
	END CATCH
end
