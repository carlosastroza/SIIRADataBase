﻿CREATE PROCEDURE [aux].[UpdatePurchase]
@SKU_C INT,
@COMPRA INT,
@DIAS_ADICIONALES INT,
@SUGERENCIA INT,
@USUARIO NVARCHAR(MAX),
@TIPOCOMPRA NVARCHAR(3)
AS
BEGIN
SET NOCOUNT ON;

	UPDATE aux.PurchaseSummary SET AdditionalDaysToCover = @DIAS_ADICIONALES, PurchaseSuggest = @SUGERENCIA, Purchase = @COMPRA WHERE SKU = @SKU_C;
	
	DECLARE @ResultadoTemporal TABLE (
		SKU INT,Familia INT,RUT INT,Rubro VARCHAR(MAX),Compra INT,DiasAdicionales INT,SugerenciaFinal INT, ManufacturerId INT
	)
	INSERT INTO @ResultadoTemporal
	SELECT @SKU_C
		  , fp.GenericCategory
		  , m.rut, fp.LogisticAreaId
		  , @COMPRA 
		  , @DIAS_ADICIONALES
		  , @SUGERENCIA
		  , m.Id
	FROM products.products p
	INNER JOIN Salcobrand.products.FullProducts fp on p.SKU = fp.SKU
	INNER JOIN products.Manufacturers m on fp.ManufacturerId = m.id
	WHERE p.sku = @SKU_C


	MERGE INTO aux.PurchaseOrders as oc
	USING @ResultadoTemporal AS rs
	on rs.Rut = oc.RUT
	WHEN NOT MATCHED THEN
	INSERT (Rut, LogisticAreaId, DeliveryTypeId, CreatedBy, CreatedDate, ReceptionDate, isFree, PurchaseType,ManufacturerId)
	VALUES(rs.Rut, rs.Rubro, 1, @USUARIO, CONVERT(DATE, GETDATE()),CONVERT(DATE, GETDATE()), 0, @TIPOCOMPRA,rs.ManufacturerId);
	
	DECLARE @RUT_TEMP   INT           = (SELECT RUT   FROM @ResultadoTemporal);
	DECLARE @RUBRO NVARCHAR(MAX) = (SELECT Rubro FROM @ResultadoTemporal);
	DECLARE @ORDEN_COMPRA INT    = (SELECT Id	 FROM aux.PurchaseOrders p WHERE p.RUT = @RUT_TEMP AND p.LogisticAreaId = @RUBRO);
	DECLARE @Q_SKU_DETALLE INT     = (SELECT COUNT(*) FROM aux.PurchaseOrderDetails WHERE SKU = @SKU_C);
	
	select * from aux.PurchaseOrderDetails
	IF @Q_SKU_DETALLE = 0 BEGIN
	INSERT INTO aux.PurchaseOrderDetails VALUES (@ORDEN_COMPRA, @SKU_C, @COMPRA, @SUGERENCIA) END
	ELSE BEGIN
	UPDATE aux.PurchaseOrderDetails SET Quantity = @COMPRA WHERE SKU = @SKU_C
	END
	
	DELETE FROM aux.PurchaseOrderDetails WHERE Quantity = 0;
	DELETE FROM aux.PurchaseOrders WHERE ID not in (select PurchaseOrderId from aux.PurchaseOrderDetails) and PurchaseType = @TIPOCOMPRA

END
