﻿-- =============================================
-- Author:		family Guy
-- Create date: 2018-07-31
-- Description:	Actualización de Purchase summary luego de cargar un nuevo archivo de rotación 
-- =============================================
CREATE PROCEDURE aux.UpdatePurchaseSummaryGES 

AS
BEGIN
	
  SET NOCOUNT ON;

	-- actualizo solo los sku que son GES 
	UPDATE ps
	SET  Purchase = p.Purchase, PurchaseSuggest = p.Purchase
	--select *
	FROM aux.PurchaseSummary ps
	INNER JOIN  suggest.PurchaseSuggest p on p.sku = ps.sku
	WHERE isges = 1 AND isgeneric = 0

	SELECT sku,SUM(PurchaseSuggest)PurchaseSuggest
	INTO #Dist
	FROM aux.PurchaseSummaryDistributor 
	GROUP BY sku

	UPDATE ps
	SET  Purchase = case when ps.Purchase - isnull (psd.PurchaseSuggest, 0) > 0 then ps.PurchaseSuggest - isnull (psd.PurchaseSuggest, 0) else 0 end
	--select *
	from aux.PurchaseSummary ps
	INNER JOIN #Dist psd on psd.sku = ps.sku
	--redondeo la compra sugerida de todos los SKU GES ya que no vienen redondeados

	UPDATE ps
	SET  PurchaseSuggest = r.UnidadesTotales
	--select *
	from aux.PurchaseSummary ps
	--INNER JOIN aux.PurchaseSummaryDistributor psd on psd.sku = ps.sku
	INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	CROSS APPLY [function].[RoundLogisticUnits](ps.PurchaseSuggest,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
	where IsGES = 1 and IsGeneric = 0

	--Teniendo la resta de las compas proveedor - distribuidor redondeo la compra de todos los SKU GES 

	UPDATE ps
	SET  Purchase = r.UnidadesTotales
	--select *
	from aux.PurchaseSummary ps
	--INNER JOIN aux.PurchaseSummaryDistributor psd on psd.sku = ps.sku
	INNER JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
	CROSS APPLY [function].[RoundLogisticUnits](ps.Purchase,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
	where IsGES = 1 and IsGeneric = 0


END
