﻿
CREATE PROCEDURE [aux].[GetGroups]
@USERNAME NVARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @Usuario_admin BIT = (SELECT 1 FROM setup.AdminUsers WHERE Username = @USERNAME);
IF ISNULL(@Usuario_admin,0) = 1 BEGIN
SET @USERNAME = null;
END

SELECT [Id]
      ,g.[Name] Nombre
	  ,a.CantidadSKU
	  ,g.FantasyName NombreFantasia
      ,[OrderCycle] CicloCompra
      ,[LeadTime] LT
	  ,LTMonday
	  ,LTTuesday
	  ,LTWednesday
	  ,LTThursday
	  ,LTFriday
	  ,LTSaturday
	  ,LTSunday
      ,[PurchaseOnMonday] Lunes
      ,[PurchaseOnTuesday] Martes
      ,[PurchaseOnWednesday] Miercoles
      ,[PurchaseOnThursday] Jueves
      ,[PurchaseOnFriday] Viernes
      ,[PurchaseOnSaturday] Sabado
      ,[PurchaseOnSunday] Domingo
	  ,CAST(case when rut <> null then 1 else 0 end as bit) DefaultGroup
      FROM [products].[Groups] g
	 INNER JOIN (
	 SELECT [GroupId], count(*) CantidadSKU 
	 FROM [products].[Products] p
	 INNER JOIN users.SkuUsers su on p.SKU = su.SKU 
	 WHERE @USERNAME IS NULL OR su.Username = @USERNAME 
	 group by GroupId) a on g.Id = a.GroupId 

	 UNION

	 SELECT [Id]
      ,g.[Name] Nombre
	  ,null CantidadSKU
	  ,g.FantasyName NombreFantasia
      ,[OrderCycle] CicloCompra
      ,[LeadTime] LT
	  ,LTMonday
	  ,LTTuesday
	  ,LTWednesday
	  ,LTThursday
	  ,LTFriday
	  ,LTSaturday
	  ,LTSunday
      ,[PurchaseOnMonday] Lunes
      ,[PurchaseOnTuesday] Martes
      ,[PurchaseOnWednesday] Miercoles
      ,[PurchaseOnThursday] Jueves
      ,[PurchaseOnFriday] Viernes
      ,[PurchaseOnSaturday] Sabado
      ,[PurchaseOnSunday] Domingo
	  ,CAST(0 AS BIT)
	  FROM products.Groups g
	  WHERE g.Name IS NULL
	  AND (@USERNAME is null or g.ModifiedBy = @USERNAME)

	  AND g.Id NOT IN (

	  SELECT [Id]
      FROM [products].[Groups] g
	 INNER JOIN (
	 SELECT [GroupId], count(*) CantidadSKU 
	 FROM [products].[Products] p
	 INNER JOIN users.SkuUsers su on p.SKU = su.SKU 
	 WHERE @USERNAME IS NULL OR su.Username = @USERNAME 
	 group by GroupId) a on g.Id = a.GroupId 

	  )


	  ORDER BY g.Id ASC

END

