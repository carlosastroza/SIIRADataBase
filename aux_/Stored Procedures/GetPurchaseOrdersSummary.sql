﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-03-15
-- Description:	Obtiene el resumen de las órdenes de compra antes que se están gestionando en una determinada pantalla.
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrdersSummary]
	@type nvarchar(3),
	@USERNAME nvarchar(max)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @USUARIO_LOCAL NVARCHAR(MAX) = @USERNAME;

IF @USERNAME IN (SELECT Username FROM setup.AdminUsers) BEGIN
SET @USERNAME = NULL
END

		SELECT  po.Id,
				po.PurchaseType,
				cast (l.RUT as int ) RUT,
				l.Laboratory,
				po.LogisticAreaId,
				po.AvailableToBuy PermiteComprar,
				qs.[#Skus] QSKU,
				qs.PurchaseQuantity,
				qs.PurchaseCost,
				po.IsFree,
				po.IsRetail,
				po.CreatedDate,
				po.ReceptionDate,
				po.OrderComment,
				po.Store,
				d.Id,
				d.[Description]
	FROM (
        select distinct po.*
        from aux.PurchaseOrders po
  --      inner join aux.PurchaseOrderDetails pod ON po.Id = pod.PurchaseOrderId
  --      inner join products.Products p ON pod.SKU = p.SKU
  --      inner join products.Manufacturers m ON p.ManufacturerId = m.Id
		--inner join users.SkuUsers su on pod.SKU = su.SKU
		WHERE po.CreatedBy = @USUARIO_LOCAL
    ) po
	INNER JOIN (
		--proveedores o laboratorios
		select r.RUT, ISNULL(m.[Name], d.[Name]) Laboratory, m.Id ManufacturerId
		from (
			select RUT from products.Manufacturers 
			UNION 
			select RUT from products.Distributors
		) r
		left join products.Manufacturers m ON r.RUT = m.RUT
		left join products.Distributors d ON r.RUT = d.RUT
	)l on po.RUT = l.RUT and (po.ManufacturerId = l.ManufacturerId OR po.RUT IN(SELECT RUT FROM products.Distributors))
	INNER JOIN (
		select 
			PurchaseOrderId
			, count(*) [#Skus]
			, SUM(Quantity) PurchaseQuantity 
			, SUM(Quantity*CASE WHEN PurchaseType = 'ESP' THEN psp.Cost ELSE pc.Cost END) PurchaseCost
		FROM aux.PurchaseOrderDetails pod
		INNER JOIN aux.PurchaseOrders oc ON pod.PurchaseOrderId = oc.id
		LEFT JOIN setup.PriceAndCost pc ON pod.SKU = pc.SKU
		LEFT JOIN products.ProductsSpecialPurchase psp ON pod.SKU = psp.SKU
		--INNER JOIN users.SkuUsers su on pod.SKU =  su.SKU
		--WHERE @USERNAME is null or su.Username = @USERNAME
		GROUP BY PurchaseOrderId
	) qs ON po.Id = qs.PurchaseOrderId
	inner join setup.DeliveryType d ON po.DeliveryTypeId = d.Id
	where po.[PurchaseType] = @type

END


