﻿CREATE PROCEDURE [aux].[UpdateGroups]
@ID_GRUPO     INT,
@NOMBRE       NVARCHAR(MAX),
@CICLO_COMPRA INT,
@LEAD_TIME    INT,
@USERNAME     NVARCHAR(MAX),
@FECHA        DATETIME

AS
BEGIN
SET NOCOUNT ON;

UPDATE [products].[Groups] 
SET 
FantasyName = @NOMBRE,
OrderCycle = @CICLO_COMPRA, 
LeadTime = @LEAD_TIME, 
ModifiedBy          = @USERNAME,
ModifiedOn          = @FECHA
 
WHERE Id = @ID_GRUPO

END
