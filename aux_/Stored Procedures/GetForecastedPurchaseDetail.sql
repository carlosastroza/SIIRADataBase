﻿-- =============================================
-- Author:		Gabriel Espinoza Erices
-- Create date: 2018-08-15
-- Description:	Obtiene el reporte de venta proyectada para descargar por GUI
-- =============================================
CREATE PROCEDURE [aux].[GetForecastedPurchaseDetail]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	fp.División Division,
			fp.Categoría Categoria,
			fp.Subcategoría SubCategoria,
			m.RUT,
			m.[Name] Laboratorio,
			fp.Posicionamiento,
			fp.Ranking,
			fp.SKU,
			fp.[MES ACTUAL] MesActual,
			pc.Cost * fp.[MES ACTUAL] CostoMesActual,
			fp.[MES 2] MesSiguiente,
			pc.Cost * fp.[MES 2] CostoMesSiguiente,
			fp.[MES 3] MesSubSiguiente,
			pc.Cost * fp.[MES 3] CostoMesSubSiguiente
	FROM reports.ForecastedPurchase fp
	INNER JOIN products.Products p ON fp.SKU = p.SKU
	LEFT JOIN setup.PriceAndCost pc ON p.SKU = pc.SKU
	INNER JOIN products.Manufacturers m ON p.ManufacturerId = m.Id
	ORDER BY fp.SKU
END