﻿
-- =============================================
-- Author:		Diego Fuenzalida
-- Create date: 2018-07-10
-- Description:	Actualiza una tabla con todas las compras para la estimación de camiones
-- =============================================

CREATE PROCEDURE [aux].[LoadTotalPurchaseView]

AS
BEGIN

SET NOCOUNT ON;


SELECT b.*
into #purch
FROM [purchase].[PurchaseOrders] b
where WasProcessed = 0
	and ReceptionDate > cast(getdate() as date) 
	and b.CreatedDate = cast(getdate() as date)
	and not exists (
		select OrderId
		from (
			select Nro_oc_pricing OrderId, SentDate
			from [purchase].[SentPurchaseOrders]
			where fecha_emi = convert(nvarchar(20),cast(getdate() as date),112)
			group by Nro_oc_pricing, SentDate
		) a
		where a.orderid=b.Id
		group by OrderId
		having count(*) > 1
	)



truncate table aux.TotalPurchaseView

insert into aux.TotalPurchaseView
select x.[SKU],x.RUT,x.ReceptionDate,sum(x.Unidades) Unidades,sum(x.Cost) Cost
from (


select a.[SKU],rut.RUT,rut.ReceptionDate,a.Quantity Unidades,a.Quantity*isnull(pc.Cost,1) Cost
from [purchase].[PurchaseOrderDetails] a
inner join #purch rut on rut.Id=a.PurchaseOrderId
left join [setup].[PriceAndCost] pc on pc.SKU=a.SKU

union all

SELECT a.SKU,a.rut,FechaExpiracionActual, SUM(UnidadesCompra) UnidadesCompra,sum(valoresCompra) valoresCompra
FROM series.PurchaseOrders a
where FechaExpiracionActual > cast(getdate() as date)
	and EstadoOOCC=2
	and UnidadesCompra>0
	and Store=996
group by a.SKU,a.rut,FechaExpiracionActual

union all

SELECT a.SKU,a.rut,FechaExpiracionActual, SUM(UnidadesCompra) UnidadesCompra,sum(valoresCompra) valoresCompra
FROM series.DailyPurchaseOrders a
where FechaExpiracionActual > cast(getdate() as date)
	and EstadoOOCC=2
	and UnidadesCompra>0
	and isnull(Store,996)=996
group by a.SKU,a.rut,FechaExpiracionActual
) x
group by x.[SKU],x.RUT,x.ReceptionDate

drop table #purch
END