﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 2018-08-08
-- Description:	Operación post carga vademécum
-- =============================================
CREATE PROCEDURE [aux].[OperationAfterLoadVademecum]
AS
BEGIN

SET NOCOUNT ON;	

--DECLARE @msg VARCHAR(100) 
--declare @date DATE = GETDATE();

--INSERT INTO [StdLog].[dbo].[Generallog] ([FechaEvento],[Nivel],[NombreAplicacion],[Mensaje],[Servidor], Usuario, Proceso)
--VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de proceso post carga Vademécum, SP [aux].[OperationAfterLoadVademecum]', host_name(), 'System', 'POST')

----2 Segundos
--BEGIN TRY

--SELECT DISTINCT v.SKU, fp.Description, fp.Ges,CASE WHEN fg.SKU IS NOT NULL THEN 1 ELSE 0 END IsGeneric,fg.GenericFamilyId, TotalRanking, ManufacturerId,pa.WMSZone, MotiveId,Motive,CategoriaUnificadaDesc
--INTO #SKU_Vademecum
--FROM products.Vademecum v
--INNER JOIN Salcobrand.Products.FullProducts fp ON v.SKU = fp.SKU
--LEFT  JOIN Salcobrand.products.PurchaseAreas pa ON fp.sku = pa.sku
--LEFT  JOIN Salcobrand.products.GenericFamilyDetail fg ON v.SKU = fg.SKU

--MERGE INTO products.products p
--USING #SKU_Vademecum v ON p.sku = v.sku
--WHEN NOT MATCHED BY TARGET THEN
--INSERT ([SKU], [Name], [IsGes], [IsGeneric], [ManufacturerId], [LogisticAreaId], [TotalRanking], [GenericFamilyId], ServiceLevel,Category,MotiveId, Motive)
--VALUES (v.SKU, v.Description, v.Ges, v.IsGeneric, v.ManufacturerId, v.WMSZone, v.TotalRanking, v.GenericFamilyId, cast(0.95 as float),CategoriaUnificadaDesc,MotiveId, Motive);

--SELECT 0;

--END TRY
--BEGIN CATCH
--	SELECT 1;
--END CATCH


--EXEC [operation].[LoadProducts]
--EXEC [operation].[LoadPriceAndCost]
--EXEC [operation].[SkuUserPermission]; 
--EXEC [operation].[UpdateProductGroups]; 
--EXEC [operation].[LoadPriceAndCostGes]

SELECT 0;
--DROP TABLE #SKU_Vademecum

END

