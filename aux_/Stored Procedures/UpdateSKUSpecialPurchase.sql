﻿-- =============================================
-- Author:		Carlos Astroza
-- Create date: 2018-08-06
-- Description:	Actualiza compra de compras especiales.
-- =============================================
CREATE PROCEDURE [aux].[UpdateSKUSpecialPurchase]
@SKU INT,
@COMPRA INT,
@USUARIO NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @COMPRA_REDONDEADA INT;
DECLARE @COMPRA_REDONDEADA_TABLA TABLE(Unidades INT)

IF @COMPRA > 0 BEGIN

		INSERT INTO @COMPRA_REDONDEADA_TABLA
		select ISNULL(UnidadesTotales,@COMPRA)
		from aux.PurchaseSummarySpecial ps
		LEFT JOIN [setup].[LogisticParameters] lp on lp.sku=ps.sku
		CROSS APPLY [function].[RoundLogisticUnits](@compra,null,lp.UnitsInBox,lp.UnitsInLayer,lp.UnitsInPallet, lp.BuyingMultiple,null,lp.RoundToBoxes,lp.RoundToLayers,lp.RoundToPallets,lp.roundThreshold,lp.ConveniencePack,lp.[ConveniencePack%]) r
		where ps.sku = @SKU
		AND ps.Username = @USUARIO

		SET @COMPRA_REDONDEADA = (SELECT Unidades FROM @COMPRA_REDONDEADA_TABLA);
END

update  aux.PurchaseSummarySpecial set Purchase = ISNULL(@COMPRA_REDONDEADA,0) where SKU = @SKU AND Username = @USUARIO


END