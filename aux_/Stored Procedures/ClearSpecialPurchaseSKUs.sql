﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [aux].[ClearSpecialPurchaseSKUs]
	@USERNAME NVARCHAR(MAX)
AS
BEGIN

	----DECLARE @USERNAME NVARCHAR(MAX) = 'jibacache'
	--SET NOCOUNT ON;
	--IF @USERNAME IN (SELECT USERNAME FROM setup.AdminUsers) BEGIN
	--	SET @USERNAME = null;
	--END

	BEGIN TRAN;
		--SELECT *
		DELETE pss
		FROM aux.PurchaseSummarySpecial pss
		INNER JOIN aux.SpecialPurchase sp ON sp.SKU = pss.SKU
		WHERE (sp.Username = @USERNAME OR @USERNAME IS NULL)

		--SELECT *
		DELETE  sp
		FROM aux.SpecialPurchase sp
		WHERE (sp.Username = @USERNAME OR @USERNAME IS NULL)
	COMMIT TRAN;
END
