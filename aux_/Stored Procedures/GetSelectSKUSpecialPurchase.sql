﻿
CREATE PROCEDURE [aux].[GetSelectSKUSpecialPurchase]
@PROVEEDOR INT,
@RUBRO     VARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

SELECT
sg.SKU,
f.Description Nombre,
--pc.Cost,
CAST(CASE WHEN sp.SKU > 0 then 1 else 0 end as bit) isSeleccionado
FROM aux.PurchaseSummary sg
INNER JOIN Salcobrand.products.FullProducts f on sg.SKU = f.SKU
INNER JOIN setup.PriceAndCost pc on sg.SKU = pc.SKU
LEFT JOIN aux.SpecialPurchase sp on sg.SKU = pc.SKU
WHERE pc.Cost > 0
AND f.ManufacturerId = @PROVEEDOR AND f.LogisticAreaId = @RUBRO
ORDER BY sg.SKU

end

