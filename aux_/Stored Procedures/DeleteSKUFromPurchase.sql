﻿
-- =============================================
-- Author:		Carlos A
-- Create date: 2018-07-03
-- Description:	Elimina SKU de la compra
-- =============================================

CREATE PROCEDURE [aux].[DeleteSKUFromPurchase]
@SKU INT,
@OC  INT
AS
BEGIN

DECLARE @PURCHASE_TYPE NVARCHAR(3) = (SELECT PurchaseType FROM aux.PurchaseOrders WHERE Id = @OC);

SET NOCOUNT ON;

		DELETE FROM aux.PurchaseOrderDetails
		WHERE SKU = @SKU
		AND  PurchaseOrderId = @OC

		DELETE FROM aux.PurchaseOrders 
		WHERE Id NOT IN (SELECT PurchaseOrderId FROM aux.PurchaseOrderDetails)

		/* Actualizar campo compra cuándo se elimina una OC que tenía el SKU*/
		IF(@PURCHASE_TYPE = 'GEN' OR @PURCHASE_TYPE = 'NOR')
		BEGIN
			
		UPDATE ps
			SET Purchase = w.Q
			FROM aux.PurchaseSummary ps
			inner join (
				SELECT SKU, SUM(Quantity) Q
				FROM aux.PurchaseOrderDetails det
				INNER JOIN aux.PurchaseOrders oc on det.PurchaseOrderId = oc.Id
				WHERE (det.SKU = @sku) AND (oc.PurchaseType = @PURCHASE_TYPE)
				GROUP BY SKU
			) w ON ps.SKU = w.SKU

		END
		ELSE IF (@PURCHASE_TYPE = 'ESP')
		BEGIN

		UPDATE ps
			SET Purchase = w.Q
			FROM aux.PurchaseSummarySpecial ps
			inner join (
				SELECT SKU, SUM(Quantity) Q
				FROM aux.PurchaseOrderDetails det
				INNER JOIN aux.PurchaseOrders oc on det.PurchaseOrderId = oc.Id
				WHERE (det.SKU = @sku) AND (oc.PurchaseType = @PURCHASE_TYPE)
				GROUP BY SKU
			) w ON ps.SKU = w.SKU
		END 

		DECLARE @ResultadoTemporal TABLE (
		DIAS_SOBRESTOCK INT,
		OC INT)

		/* Obtener parámetro MaxInventoryDays*/
		DECLARE @MAX_INVENTORY_DAYS TABLE(
		Llave NVARCHAR(MAX),
		Value INT)
		INSERT INTO @MAX_INVENTORY_DAYS
		exec [setup].[GetConfigurationDictionary] 'SB'
		DECLARE @MAX_DAYS INT = (SELECT Value FROM @MAX_INVENTORY_DAYS);


		INSERT INTO @ResultadoTemporal
		SELECT a.Days, det.PurchaseOrderId
		FROM products.Products p
		INNER JOIN aux.PurchaseSummary ps ON p.SKU = ps.SKU
		CROSS APPLY [suggest].[GetInventoryDays](p.SKU,ps.PURCHASE+ps.DCStock+ps.DCTransit-ps.StoresShortage,GETDATE()) a
		INNER JOIN aux.PurchaseOrderDetails det ON p.SKU = det.SKU
		WHERE PurchaseOrderId = @OC

		Declare 
		@id int, 
		@count int
		Set @id=1
		select @count=count(*)from @ResultadoTemporal 
		DECLARE @CANTIDAD_DIAS_SS  INT = 0;

		while @id<=@count
		begin

		SET @CANTIDAD_DIAS_SS = ( SELECT COUNT(*) 
						  FROM @ResultadoTemporal 
						  WHERE (DIAS_SOBRESTOCK > @MAX_DAYS));


		IF @CANTIDAD_DIAS_SS > 0 BEGIN
		UPDATE aux.PurchaseOrders set AvailableToBuy = 'PEN' WHERE Id = @OC
		END
		ELSE
		BEGIN
		UPDATE aux.PurchaseOrders set AvailableToBuy = 'RDY' WHERE Id = @OC
		END 

		select @id=@id+1
		end

end
