﻿

-- =============================================
-- Author:		Jorge I.
-- Create date: 22-05-2018
-- Description:	Obtiene los indicadores de compra de un comprador específico para jefes
-- =============================================

CREATE PROCEDURE [aux].[GetPurchaseReportDetailComprador]
 @compradorId  NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @fecha DATE = CAST(GETDATE() as date)
		

	SELECT	u.Id,
			u.Nombre, 
			bi.FantasyName [NombreGrupo], 
			bi.GroupId [IdGrupo],
			COUNT(bi.SKU) [CantSkus],
			ISNULL(SUM(bi.Quantity),0) [CantSkusComprados],
			ISNULL(SUM(bi.Cost),0) [CostoTotal],
			SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END) CompraZero,
			SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END) SinCompraSugerida,
			SUM(CASE WHEN oc.CreatedDate IS NULL THEN 0 ELSE 1 END) Comprado
	FROM aux.BaseIndicadores bi
	LEFT JOIN (SELECT SKU, 
						Purchase CompraSugerida 
				FROM suggest.purchasesuggest) ps on ps.SKU=bi.SKU
	LEFT JOIN stdusuarios.dbo.usuario u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS = bi.comprador
	LEFT JOIN (SELECT	od.PurchaseOrderId,
						od.SKU,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date)= @fecha) oc ON oc.sku=bi.sku
	WHERE bi.FechaCompra=@fecha
	AND bi.SKU NOT IN (SELECT SKU FROM Salcobrand.products.GenericFamilyDetail)
	AND u.id = @compradorId
	
	GROUP BY u.Id,u.Nombre,bi.GroupId,bi.FantasyName
	
	
	UNION

		SELECT	u.Id,
			u.Nombre, 
			bi.FantasyName [NombreGrupo], 
			bi.GroupId [IdGrupo],
			COUNT(bi.SKU) [CantSkus],
			ISNULL(SUM(bi.Quantity),0) [CantSkusComprados],
			ISNULL(SUM(bi.Cost),0) [CostoTotal],
			SUM(CASE WHEN ps.CompraSugerida=0 Then 1 ELSE 0 END) CompraZero,
			SUM(CASE WHEN ps.CompraSugerida IS NULL THEN 1 ELSE 0 END) SinCompraSugerida,
			SUM(CASE WHEN oc.CreatedDate IS NULL THEN 0 ELSE 1 END) Comprado
	FROM aux.BaseIndicadores bi
	LEFT JOIN (SELECT SKU, 
						Purchase CompraSugerida 
				FROM suggest.purchasesuggest) ps on ps.SKU=bi.SKU
	LEFT JOIN stdusuarios.dbo.usuario u ON u.id COLLATE SQL_Latin1_General_CP1_CI_AS = bi.comprador
	LEFT JOIN (SELECT	od.PurchaseOrderId,
						od.SKU,
						po.CreatedDate,
						po.ReceptionDate,
						po.CreatedBy Usuario 
				FROM [purchase].[PurchaseOrderDetails] od
				LEFT JOIN [purchase].[PurchaseOrders] po ON od.PurchaseOrderId = po.Id
				WHERE cast(po.CreatedDate as date)= @fecha) oc ON oc.sku=bi.sku
	WHERE bi.FechaCompra=@fecha
	AND bi.SKU in (SELECT SKU FROM products.GenericTenderedSKU)
	AND u.id =@compradorId

	GROUP BY u.Id,u.Nombre,bi.GroupId,bi.FantasyName
END
