﻿CREATE PROCEDURE [aux].[GetInvestedAmount]
@USERNAME NVARCHAR(255)
AS
BEGIN

SET NOCOUNT ON;	
DECLARE @res NVARCHAR(50);

SET @res = 
(
SELECT FORMAT(SUM(Cantidad*unlog.UNLOG_Quantity*pc.Cost),N'c0', 'es-CL') MontoInvertido
  FROM [aux].[InvestmentPurchase] p
    left join(
      select SKU
      ,case when RoundToBoxes=1 then cast('Caja' as varchar) else case when RoundToLayers=1 then 'Layer' else case when RoundToPallets=1 then 'Pallet' else 'Sin Especificar' end end end UNLOG_Desc
      ,case when cast([RoundToBoxes] as int)*UnitsInBox+cast([RoundToLayers] as int)*UnitsInLayer+cast([RoundToPallets] as int)*UnitsInPallet > 0 then cast([RoundToBoxes] as int)*UnitsInBox+cast([RoundToLayers] as int)*UnitsInLayer+cast([RoundToPallets] as int)*UnitsInPallet else 1 end  UNLOG_Quantity
    FROM [setup].[LogisticParameters]
  ) unlog on unlog.SKU=p.SKU
  inner join (select * from [setup].[PriceAndCost] where cost is not null) pc on pc.sku=p.SKU
  WHERE Username = @USERNAME
)

SELECT ISNULL(@res,'')


END

