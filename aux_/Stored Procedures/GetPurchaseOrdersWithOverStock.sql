﻿-- =============================================
-- Author:		C Astroza
-- Create date: 2018-03-21
-- Description:	Obtiene las órdenes de compra que tienen problema de sobreStock
-- =============================================
CREATE PROCEDURE [aux].[GetPurchaseOrdersWithOverStock]
@USERNAME NVARCHAR(50) = 'castroza' -- Éste usuario se deja como por defecto ya que puede visualizar OC de ambas divisiones, esto es para
									-- que no se caiga mientras publicamos.
AS
BEGIN
SET NOCOUNT ON;

DECLARE @DIV NVARCHAR(10) = (
SELECT  [dbo].[GetUsersDivision](@USERNAME)
);

	SELECT	po.Id,
			po.PurchaseType,
			l.RUT,
			l.Laboratory,
			po.LogisticAreaId,
			us.Nombre Comprador,
			qs.[#Skus] QSKU,
			qs.PurchaseQuantity,
			qs.PurchaseCost,
			CAST(CASE WHEN po.IsFree = 1 THEN 'SI' ELSE 'NO' END AS nvarchar) Gratis, 
			po.CreatedDate,
			po.ReceptionDate, 
			po.OrderComment
	FROM aux.PurchaseOrders po
	LEFT JOIN StdUsuarios.dbo.Usuario us ON po.CreatedBy = us.Id
	INNER JOIN (select	r.RUT,
						ISNULL(m.[Name],d.[Name]) Laboratory,
						m.Id ManufacturerId
						from (	select RUT
						from products.Manufacturers 
						UNION 
						select RUT from products.Distributors) r
						LEFT JOIN products.Manufacturers m ON r.RUT = m.RUT
						LEFT JOIN products.Distributors d ON r.RUT = d.RUT) l on po.RUT = l.RUT and (po.ManufacturerId = l.ManufacturerId OR po.RUT IN(SELECT RUT FROM products.Distributors))
	
	INNER JOIN (select	PurchaseOrderId,
						count(*) [#Skus],
						SUM(Quantity) PurchaseQuantity,
						SUM(Quantity*pc.Cost) PurchaseCost
						FROM aux.PurchaseOrderDetails pod
						LEFT JOIN setup.PriceAndCost pc ON pod.SKU = pc.SKU
						INNER JOIN Salcobrand.products.fullproducts FP on pod.SKU = fp.SKU
						WHERE fp.DivisionUnificadaId IN (SELECT * FROM  dbo.SplitString(@DIV))
						GROUP BY PurchaseOrderId) qs ON po.Id = qs.PurchaseOrderId
	INNER JOIN setup.DeliveryType d ON po.DeliveryTypeId = d.Id
	WHERE po.AvailableToBuy = 'PEN'
END
