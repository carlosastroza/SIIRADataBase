﻿
-- =============================================
-- Author:		Christopher Thompson H.
-- Create date: 2018-06-21
-- Description:	Obtiene los compradores de la tabla UserRules.
-- =============================================
CREATE PROCEDURE [users].[GetUserRules]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Username]
	,[BuyerId]
	,[Category]
	,[SubCategory]
	,[VMI]
	,[SupplyChain]
	 FROM [users].[UserRules]
	 

END
