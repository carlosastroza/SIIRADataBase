﻿
-- =============================================
-- Author:		Christopher Thompson H.
-- Create date: 2018-06-21
-- Description:	Modifica los compradores de la tabla UserRules...
-- =============================================
CREATE PROCEDURE [users].[UpdateUserRules]
@username NVARCHAR(MAX),
@BuyerId int
AS
BEGIN
	SET NOCOUNT ON;

		UPDATE users.UserRules
			SET BuyerId=@BuyerId
			WHERE Username=@username

END
