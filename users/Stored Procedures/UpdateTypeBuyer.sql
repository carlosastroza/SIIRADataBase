﻿
-- =============================================
-- Author:		Christopher Thompson H.
-- Create date: 2018-06-21
-- Description:	Modifica el tipo de comprador.
-- =============================================
--EXEC [users].[UpdateTypeBuyer] cflores ,3
CREATE PROCEDURE [users].[UpdateTypeBuyer]
@username NVARCHAR(MAX),
@tipo INT
AS
BEGIN
	SET NOCOUNT ON;

	IF @tipo=1
		BEGIN
			Update users.UserRules
			SET Generic=0
			where Username!=@username
			UPDATE users.UserRules
			SET Generic=1
			WHERE Username=@username
			SELECT * FROM users.UserRules
		END
	ELSE IF @tipo=2
		BEGIN
			Update users.UserRules
			SET Ges=0
			where Username!=@username
			UPDATE users.UserRules
			SET Ges=1
			WHERE Username=@username
			SELECT * FROM users.UserRules
		END
	ELSE IF @tipo=3
		BEGIN
			Update users.UserRules
			SET WildcardConsumo=0
			where Username!=@username
			UPDATE users.UserRules
			SET WildcardConsumo=1
			WHERE Username=@username
		END
	IF @tipo=4
		BEGIN
			Update users.UserRules
			SET WildcardFarma=0
			where Username!=@username
			UPDATE users.UserRules
			SET WildcardFarma=1
			WHERE Username=@username
		END

--BEGIN TRY
--EXEC operation.SkuUserPermission --Correr para cambiar la asignación de comprador. 1 min
--EXEC operation.[CreateAutomaticGroups]--Crear grupos automáticos, 2 seg
--UPDATE products.Products SET GroupId = NULL WHERE IsGroupLocked = 0 -- 0 seg
--EXEC operation.UpdateProductGroups --Asignación de grupos a los SKU - 3 seg

--INSERT INTO [StdLog].[dbo].[Generallog] 
--([FechaEvento],
--[Nivel],
--[NombreAplicacion],
--[Mensaje],[Servidor],
-- Usuario, Proceso)
--VALUES (getdate(), 'Info', 'Salcobrand.SIIRA','Inicio de carga de proceso de automatizacion de grupos., SP [users].[UpdateTypeBuyer]', host_name(), 'System', 'POST')
--END TRY
--BEGIN CATCH

--END CATCH
	


END
