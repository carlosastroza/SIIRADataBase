﻿
-- =============================================
-- Author:		Christopher Thompson H.
-- Create date: 2018-06-21
-- Description:	Modifica los compradores de la tabla UserRules.
-- =============================================
CREATE PROCEDURE [users].[LoadUserRules]

AS
BEGIN
	SET NOCOUNT ON;

MERGE INTO users.UserRules as ur
USING users.SourceUserRules as sur
ON ur.Username=sur.Username
WHEN MATCHED THEN
UPDATE  SET ur.BuyerId=sur.BuyerId , 
		    ur.Category=sur.Category,
		    ur.SubCategory=sur.SubCategory,
		    ur.VMI=sur.VMI,
		    ur.SupplyChain=sur.SupplyChain,
		    ur.LastModifiedBy=sur.LastModifiedBy,
		    ur.LastModifiedOn=sur.LastModifiedOn
WHEN NOT MATCHED THEN
INSERT VALUES(sur.Username,sur.BuyerId,sur.Category,sur.SubCategory,0,0,sur.VMI,sur.SupplyChain,0,0,0,sur.LastModifiedBy,sur.LastModifiedOn);
END

EXEC operation.SkuUserPermission
