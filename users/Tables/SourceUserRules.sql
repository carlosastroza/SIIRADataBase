﻿CREATE TABLE [users].[SourceUserRules] (
    [Username]       NVARCHAR (50)  NOT NULL,
    [BuyerId]        INT            CONSTRAINT [DF_PivotUserRules_BuyerId] DEFAULT ((0)) NULL,
    [Category]       NVARCHAR (50)  NULL,
    [SubCategory]    NVARCHAR (50)  NULL,
    [VMI]            NVARCHAR (MAX) NULL,
    [SupplyChain]    NVARCHAR (MAX) NULL,
    [LastModifiedBy] NVARCHAR (50)  NULL,
    [LastModifiedOn] NVARCHAR (50)  NULL
);

