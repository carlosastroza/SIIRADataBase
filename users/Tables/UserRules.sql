﻿CREATE TABLE [users].[UserRules] (
    [Username]        NVARCHAR (50)  NOT NULL,
    [BuyerId]         INT            CONSTRAINT [DF_UserRules_BuyerId] DEFAULT ((0)) NULL,
    [Category]        NVARCHAR (50)  NULL,
    [SubCategory]     NVARCHAR (50)  NULL,
    [Generic]         BIT            NULL,
    [Ges]             BIT            NULL,
    [VMI]             NVARCHAR (MAX) NULL,
    [SupplyChain]     NVARCHAR (MAX) NULL,
    [WildcardFarma]   BIT            CONSTRAINT [DF_UserRules_WildcardFarma] DEFAULT ((0)) NULL,
    [WildcardConsumo] BIT            CONSTRAINT [DF_UserRules_WildcardConsumo] DEFAULT ((0)) NULL,
    [IsAdmin]         BIT            CONSTRAINT [DF_UserRules_isAdmin] DEFAULT ((0)) NULL,
    [LastModifiedBy]  NVARCHAR (50)  NULL,
    [LastModifiedOn]  NVARCHAR (50)  NULL
);

