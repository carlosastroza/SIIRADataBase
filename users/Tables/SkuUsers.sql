﻿CREATE TABLE [users].[SkuUsers] (
    [SKU]      INT           NOT NULL,
    [Username] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_SkuUsers] PRIMARY KEY CLUSTERED ([SKU] ASC, [Username] ASC)
);

