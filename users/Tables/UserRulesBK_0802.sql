﻿CREATE TABLE [users].[UserRulesBK_0802] (
    [Username]        NVARCHAR (50)  NOT NULL,
    [BuyerId]         INT            NULL,
    [Category]        NVARCHAR (50)  NULL,
    [SubCategory]     NVARCHAR (50)  NULL,
    [Generic]         BIT            NULL,
    [Ges]             BIT            NULL,
    [VMI]             NVARCHAR (MAX) NULL,
    [SupplyChain]     NVARCHAR (MAX) NULL,
    [WildcardFarma]   BIT            NULL,
    [WildcardConsumo] BIT            NULL,
    [IsAdmin]         BIT            NULL,
    [LastModifiedBy]  NVARCHAR (50)  NULL,
    [LastModifiedOn]  NVARCHAR (50)  NULL
);

