﻿CREATE TABLE [purchase].[PurchaseOrderDetailsStaging] (
    [Id]              INT        NOT NULL,
    [PurchaseOrderId] INT        NOT NULL,
    [SKU]             INT        NOT NULL,
    [Quantity]        INT        NOT NULL,
    [Cost]            FLOAT (53) NOT NULL,
    [PurchaseSuggest] INT        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

