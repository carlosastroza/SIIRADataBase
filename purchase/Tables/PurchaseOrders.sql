﻿CREATE TABLE [purchase].[PurchaseOrders] (
    [Id]             INT            NOT NULL,
    [ParentId]       INT            NULL,
    [RUT]            INT            NOT NULL,
    [LogisticAreaId] NVARCHAR (50)  NOT NULL,
    [OrderComment]   NVARCHAR (MAX) NULL,
    [DeliveryTypeId] INT            NOT NULL,
    [Store]          INT            NULL,
    [CreatedBy]      NVARCHAR (255) NULL,
    [CreatedDate]    DATETIME       NOT NULL,
    [LeadTime]       INT            NULL,
    [ReceptionDate]  DATE           NULL,
    [IsFree]         BIT            NOT NULL,
    [PurchaseType]   NVARCHAR (3)   NOT NULL,
    [WasProcessed]   BIT            CONSTRAINT [DF_PurchaseOrders_WasProcessed] DEFAULT ((0)) NOT NULL,
    [IsRetail]       INT            NULL,
    [ManufacturerId] INT            NULL,
    [IdZona]         INT            NULL,
    CONSTRAINT [PK__Purchase__3214EC07950C9363] PRIMARY KEY CLUSTERED ([Id] ASC)
);

