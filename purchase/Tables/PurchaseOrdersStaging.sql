﻿CREATE TABLE [purchase].[PurchaseOrdersStaging] (
    [Id]             INT            NOT NULL,
    [ParentId]       INT            NULL,
    [RUT]            INT            NOT NULL,
    [LogisticAreaId] NVARCHAR (50)  NOT NULL,
    [OrderComment]   NVARCHAR (MAX) NULL,
    [DeliveryTypeId] INT            NOT NULL,
    [Store]          INT            NULL,
    [CreatedBy]      NVARCHAR (255) NULL,
    [CreatedDate]    DATE           NOT NULL,
    [LeadTime]       INT            NULL,
    [ReceptionDate]  DATE           NULL,
    [IsFree]         BIT            NOT NULL,
    [PurchaseType]   NVARCHAR (3)   NOT NULL,
    [WasProcessed]   BIT            NOT NULL,
    [IsRetail]       INT            NULL,
    [ManufacturerId] INT            NULL,
    [IdZona]         INT            NULL,
    CONSTRAINT [PK__Purchase__id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

