﻿CREATE TABLE [purchase].[PurchaseOrderDetails] (
    [Id]              INT        NOT NULL,
    [PurchaseOrderId] INT        NOT NULL,
    [SKU]             INT        NOT NULL,
    [Quantity]        INT        NOT NULL,
    [Cost]            FLOAT (53) NOT NULL,
    [PurchaseSuggest] INT        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PurchaseOrderDetails_PurchaseOrders] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [purchase].[PurchaseOrders] ([Id])
);

