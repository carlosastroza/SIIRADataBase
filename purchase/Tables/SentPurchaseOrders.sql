﻿CREATE TABLE [purchase].[SentPurchaseOrders] (
    [RUT]            INT           NOT NULL,
    [Nro_oc_pricing] INT           NOT NULL,
    [SKU]            NVARCHAR (7)  NOT NULL,
    [Cantidad]       INT           NOT NULL,
    [Fecha_emi]      NVARCHAR (10) NOT NULL,
    [Fecha_recep]    NVARCHAR (10) NULL,
    [Unidad]         NVARCHAR (2)  NOT NULL,
    [SentDate]       DATETIME      NOT NULL
);

